<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//active
Route::post('/activate_license', function(){
	$response = [
		"status" => true,
		"message" => "Actived! Thanks for purchasing Botble CMS by Sang Nguyen",
		"data" => null,
		"lic_response" => sha1('!@#$%^&*()'),
	];

	return response()->json($response);
});

//deactive
Route::post('/deactivate_license', function(){
	$response = [
		"status" => true,
		"error" => false,
		"message" => "Verified! Thanks for purchasing Botble CMS by Sang Nguyen",
	];
	return response()->json($response);
});


//verify
Route::post('/verify_license', function(){
	$response = [
		"status" => true,
		"error" => false,
		"message" => "Verified! Thanks for purchasing Botble CMS by Sang Nguyen",
	];
	return response()->json($response);
});



