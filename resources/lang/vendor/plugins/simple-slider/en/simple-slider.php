<?php

return [
    'create'   => 'New slider',
    'edit'     => 'Edit slider',
    'menu'     => 'Simple sliders',
    'settings' => [
        'title'                    => 'Simple sliders',
        'description'              => 'Settings for Simple sliders',
        'using_assets'             => 'Using default assets?',
        'using_assets_description' => 'If using assets option is enabled then below scripts will be auto added to front site.',
    ],
    'forms' => [
    	'type_slider' => 'Kiểu hiển thị Item slider',
    	'description1' => 'Mô tả ngắn style',
    	'description2' => 'Mô tả ngắn style',
    	'description3' => 'Mô tả ngắn style',
    ],
];
