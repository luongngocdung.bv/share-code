<?php

return [
    'title' => 'Sản phẩm',
    'select' => 'Lựa Chọn',
    'product_page_id' => 'Trang Sản Phẩm',
    'description' => 'Cài đặt cho plugin Sản phẩm',
    'currency' => 'Tiền tệ',
];
