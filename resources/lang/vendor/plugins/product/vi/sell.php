<?php

return [
    'form' => [
        'timesale' => 'Giá giảm giá',
        'pricetime' => 'Thời gian bán',
        'pricesell' => 'Giá bán',
        'priceSaleOff' => 'Giảm Giá',
        'pricecost' => 'Giá nhập',
        'price_promotion_start' => 'Thời gian bắt đầu giảm giá',
        'price_promotion_end' => 'Thời gian kết thúc giảm giá',
        'currency' => 'Tiền tệ',
        'change' => 'Chọn thời gian',
        'button_add_timesaleoff' => 'Thêm thời gian/giá giảm giá',
        'amoundprice' => 'Số lượng bán',
    ],
];
