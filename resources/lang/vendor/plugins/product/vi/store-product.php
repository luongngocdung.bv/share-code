<?php

return [
    'create' => 'Thêm sản phẩm mới',
    'edit' => 'Sửa sản phẩm',
    'menu' => 'Kho sản phẩm',
    'settings' => [
        'description' => 'Cài đặt kho sản phẩm',
        'title' => 'Kho sản phẩm',
        'using_assets' => 'Mặc định style',
        'using_assets_description' => 'Nếu tùy chọn sử dụng tài sản được bật thì các tập lệnh bên dưới sẽ được tự động thêm vào trang web phía trước.',
    ],
];
