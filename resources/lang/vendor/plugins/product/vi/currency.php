<?php

return [
    'billion' => 'Tỷ',
    'million' => 'Triệu',
    'after_number' => 'Hiển thị sau',
    'before_number' => 'Hiển thị trước',
    'currencies' => 'Tiền tệ',
    'is_default' => 'Mặc định',
    'is_prefix_symbol' => 'Ký tự',
    'name' => 'Tên tiền tệ',
    'new_currency' => 'Thêm mới tiền tệ',
    'remove' => 'Xoá',
    'save_settings' => 'Lưu cài đặt',
    'setting_description' => 'Danh sách Tiền tệ',
    'symbol' => 'Ký tự',
];
