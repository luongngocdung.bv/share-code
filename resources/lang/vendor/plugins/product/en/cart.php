<?php

return [
	'name'   => 'Carts',
	'namelist'   => 'List Carts',
    'create' => 'New cart',
    'edit'   => 'Edit cart',
    'form' =>[
    	'pricecost' => 'Price Cost',
    ],
];
