<?php

return [
    'name' => 'Stores',
    'create' => 'New store',
    'edit' => 'Edit store',
];
