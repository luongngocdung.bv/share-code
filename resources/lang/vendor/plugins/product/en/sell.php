<?php

return [
    'form' => [
        'pricecost' => 'Price Cost',
        'pricesell' => 'Price Sell',
        'pricetime' => 'Price Time',
        'change' => 'Click Time',
        'timesale' => 'Price SaleOff',
        'amoundprice' => 'Price Amound',
        'priceSaleOff' => ' SaleOff',
        'price_promotion_end' => 'Time SaleOff End',
        'price_promotion_start' => 'Time SaleOff Start',
        'button_add_timesaleoff' => 'Add Time SaleOff',
        'currency' => 'Currency',
    ],
];
