<?php

return [
    'name' => 'Sizes',
    'create' => 'New size',
    'edit' => 'Edit size',
];
