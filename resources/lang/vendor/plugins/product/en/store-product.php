<?php

return [
    'create' => 'New Product',
    'edit' => 'Edit product',
    'menu' => 'Store products',
    'settings' => [
        'title' => 'Store products',
        'description' => 'Settings for Store products',
        'using_assets' => 'Using default assets?',
        'using_assets_description' => 'If using assets option is enabled then below scripts will be auto added to front site.',
    ],
];
