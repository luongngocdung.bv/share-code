-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 26, 2020 lúc 05:04 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `camp_botble`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'ZLKcLRxzQH9E8XMMWd6NITIH1J8Qdljb', 1, '2017-11-15 06:57:09', '2017-11-15 06:57:09', '2017-11-15 06:57:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `audit_histories`
--

CREATE TABLE `audit_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `module` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_user` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `audit_histories`
--

INSERT INTO `audit_histories` (`id`, `user_id`, `module`, `request`, `action`, `user_agent`, `ip_address`, `reference_user`, `reference_id`, `reference_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-13 05:20:51', '2020-04-13 05:20:51'),
(2, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Qu\\u1ea7n \\u00e1o\",\"slug\":\"quan-ao\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"0\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Quần áo', 'info', '2020-04-13 06:20:19', '2020-04-13 06:20:19'),
(3, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Qu\\u1ea7n \\u00e1o tr\\u1ebb em\",\"slug\":\"quan-ao-tre-em\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"1\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 2, 'Quần áo trẻ em', 'primary', '2020-04-13 06:23:22', '2020-04-13 06:23:22'),
(4, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Qu\\u1ea7n \\u00e1o s\\u01a1 sinh\",\"slug\":\"quan-ao-so-sinh\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"1\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 3, 'Quần áo sơ sinh', 'primary', '2020-04-13 06:23:41', '2020-04-13 06:23:41'),
(5, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Qu\\u1ea7n \\u00e1o s\\u01a1 sinh\",\"slug\":\"quan-ao-so-sinh\",\"slug_id\":\"72\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"1\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 3, 'Quần áo sơ sinh', 'primary', '2020-04-13 06:23:49', '2020-04-13 06:23:49'),
(6, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Qu\\u1ea7n \\u00e1o\",\"slug\":\"quan-ao\",\"slug_id\":\"70\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"0\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Quần áo', 'primary', '2020-04-13 06:24:01', '2020-04-13 06:24:01'),
(7, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"san-pham-1\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"20\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Sản phẩm 1', 'info', '2020-04-13 08:10:03', '2020-04-13 08:10:03'),
(8, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"san-pham-1\",\"slug_id\":\"73\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"20\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Sản phẩm 1', 'primary', '2020-04-13 08:10:46', '2020-04-13 08:10:46'),
(9, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"san-pham-1\",\"slug_id\":\"73\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\",\"2\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Sản phẩm 1', 'primary', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(10, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)\",\"slug\":\"san-pham-1duplicate\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 2, 'Sản phẩm 1(Duplicate)', 'primary', '2020-04-13 08:14:20', '2020-04-13 08:14:20'),
(11, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicate\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 3, 'Sản phẩm 1(Duplicate)(Duplicate)', 'primary', '2020-04-13 08:14:30', '2020-04-13 08:14:30'),
(12, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-13 08:14:39', '2020-04-13 08:14:39'),
(13, 1, 'product', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-13 08:14:45', '2020-04-13 08:14:45'),
(14, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"\\u00c1o thun\",\"slug\":\"ao-thun\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"3\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 4, 'Áo thun', 'primary', '2020-04-13 09:27:37', '2020-04-13 09:27:37'),
(15, 1, 'procategory', '[]', 'deleted', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 4, 'Áo thun', 'danger', '2020-04-13 09:34:37', '2020-04-13 09:34:37'),
(16, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Th\\u1eb1ng Cha\",\"slug\":\"quan-ao\",\"slug_id\":\"70\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"0\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Thằng Cha', 'primary', '2020-04-13 09:34:51', '2020-04-13 09:34:51'),
(17, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Th\\u1eb1ng con th\\u1ee9 nh\\u1ea5t\",\"slug\":\"quan-ao-tre-em\",\"slug_id\":\"71\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"1\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 2, 'Thằng con thứ nhất', 'primary', '2020-04-13 09:35:02', '2020-04-13 09:35:02'),
(18, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Th\\u1eb1ng con th\\u1ee9 hai\",\"slug\":\"quan-ao-so-sinh\",\"slug_id\":\"72\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"1\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 3, 'Thằng con thứ hai', 'primary', '2020-04-13 09:35:11', '2020-04-13 09:35:11'),
(19, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Th\\u1eb1ng ch\\u00e1u th\\u1ee9 nh\\u1ea5t\",\"slug\":\"thang-chau-thu-nhat\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"3\",\"description\":null,\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 5, 'Thằng cháu thứ nhất', 'info', '2020-04-13 09:35:25', '2020-04-13 09:35:25'),
(20, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"th\\u1eb1ng ch\\u00e1u th\\u1ee9 hai\",\"slug\":\"thang-chau-thu-hai\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"3\",\"description\":null,\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 6, 'thằng cháu thứ hai', 'info', '2020-04-13 09:35:38', '2020-04-13 09:35:38'),
(21, 1, 'procategory', '{\"_token\":\"7nKvCwlYNZkp0JNjUBLoC025EuXcDWIqBd3tyyOF\",\"name\":\"Th\\u1eb1ng Cha th\\u1ee9 2\",\"slug\":\"thang-cha-thu-2\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"0\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"0\",\"type_menu\":\"0\",\"is_new\":\"0\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 7, 'Thằng Cha thứ 2', 'primary', '2020-04-13 10:01:19', '2020-04-13 10:01:19'),
(22, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-20 07:52:30', '2020-04-20 07:52:30'),
(23, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-20 08:12:36', '2020-04-20 08:12:36'),
(24, 1, 'procategory', '{\"_token\":\"pKqScAfVlbIa30sNMlQz9unUNkbNAAiALcxi45SP\",\"name\":\"Th\\u1eb1ng Cha\",\"slug\":\"quan-ao\",\"slug_id\":\"70\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProCategory\",\"parent_id\":\"0\",\"description\":\"Qu\\u1ea7n \\u00e1o\",\"is_default\":\"1\",\"type_menu\":\"0\",\"is_new\":\"1\",\"is_hot\":\"0\",\"is_featured\":\"0\",\"icon\":null,\"order\":\"0\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36', '127.0.0.1', 1, 1, 'Thằng Cha', 'primary', '2020-04-20 10:07:56', '2020-04-20 10:07:56'),
(25, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-22 07:22:28', '2020-04-22 07:22:28'),
(26, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-22 07:22:28', '2020-04-22 07:22:28'),
(27, 1, 'post', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"test\",\"slug\":\"test\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Blog\\\\Models\\\\Post\",\"description\":\"test\",\"is_featured\":\"0\",\"content\":\"<p>test<\\/p>\",\"gallery\":\"[{\\\"img\\\":\\\"users\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"},{\\\"img\\\":\\\"users\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"}]\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"publish_date\":\"2020-04-22\",\"publish_time\":\"14:34\",\"update_time_to_current\":\"1\",\"status\":\"published\",\"format_type\":null,\"categories\":[\"12\",\"13\",\"16\",\"14\",\"17\",\"15\",\"18\"],\"image\":\"users\\/avatar.jpg\",\"tag\":\"jhjhj\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 56, 'test', 'info', '2020-04-22 08:12:17', '2020-04-22 08:12:17'),
(28, 1, 'tag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"test\",\"slug\":\"test\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Blog\\\\Models\\\\Post\",\"description\":\"test\",\"is_featured\":\"0\",\"content\":\"<p>test<\\/p>\",\"gallery\":\"[{\\\"img\\\":\\\"users\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"},{\\\"img\\\":\\\"users\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"}]\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"publish_date\":\"2020-04-22\",\"publish_time\":\"14:34\",\"update_time_to_current\":\"1\",\"status\":\"published\",\"format_type\":null,\"categories\":[\"12\",\"13\",\"16\",\"14\",\"17\",\"15\",\"18\"],\"image\":\"users\\/avatar.jpg\",\"tag\":\"jhjhj\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 26, 'jhjhj', 'info', '2020-04-22 08:12:17', '2020-04-22 08:12:17'),
(29, 1, 'post', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"test\",\"slug\":\"test\",\"slug_id\":\"81\",\"model\":\"Botble\\\\Blog\\\\Models\\\\Post\",\"description\":\"test\",\"is_featured\":\"0\",\"content\":\"<p>test<\\/p>\",\"gallery\":\"[{\\\"img\\\":\\\"users\\\\\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"},{\\\"img\\\":\\\"users\\\\\\/avatar.jpg\\\",\\\"description\\\":\\\"\\\"}]\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"publish_date\":\"2020-04-23\",\"publish_time\":\"15:12\",\"status\":\"published\",\"format_type\":null,\"categories\":[\"12\",\"13\",\"16\",\"14\",\"17\",\"15\",\"18\"],\"image\":\"users\\/avatar.jpg\",\"tag\":\"jhjhj\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 56, 'test', 'primary', '2020-04-22 08:37:41', '2020-04-22 08:37:41'),
(30, 1, 'menu', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"Menu ch\\u00ednh\",\"deleted_nodes\":null,\"menu_nodes\":\"[{\\\"id\\\":\\\"600\\\",\\\"class\\\":\\\"megamenu1\\\",\\\"title\\\":\\\"Trang ch\\u1ee7\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/vi\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"children\\\":[{\\\"id\\\":\\\"601\\\",\\\"title\\\":\\\"Mua ngay\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"https:\\/\\/codecanyon.net\\/item\\/botble-cms-php-platform-based-on-laravel-framework\\/16928182?ref=botble\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_blank\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"id\\\":\\\"602\\\",\\\"title\\\":\\\"Tin t\\u1ee9c\\\",\\\"referenceId\\\":\\\"11\\\",\\\"referenceType\\\":\\\"Botble\\\\\\\\Blog\\\\\\\\Models\\\\\\\\Category\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/new-update\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"children\\\":[{\\\"id\\\":\\\"606\\\",\\\"title\\\":\\\"Th\\u01b0 vi\\u1ec7n \\u1ea3nh\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/vi\\/galleries\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"id\\\":\\\"594\\\",\\\"title\\\":\\\"Li\\u00ean h\\u1ec7\\\",\\\"referenceId\\\":\\\"17\\\",\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/lien-he\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":1,\\\"children\\\":[]}],\\\"position\\\":1}],\\\"position\\\":0}]\",\"target\":\"_self\",\"title\":\"Li\\u00ean h\\u1ec7\",\"custom-url\":\"\\/vi\\/galleries\",\"icon-font\":null,\"class\":null,\"locations\":[\"main-menu\"],\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 10, 'Menu chính', 'primary', '2020-04-22 10:14:57', '2020-04-22 10:14:57'),
(31, 1, 'menu_location', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"Menu ch\\u00ednh\",\"deleted_nodes\":null,\"menu_nodes\":\"[{\\\"id\\\":\\\"600\\\",\\\"class\\\":\\\"megamenu1\\\",\\\"title\\\":\\\"Trang ch\\u1ee7\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/vi\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"children\\\":[{\\\"id\\\":\\\"601\\\",\\\"title\\\":\\\"Mua ngay\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"https:\\/\\/codecanyon.net\\/item\\/botble-cms-php-platform-based-on-laravel-framework\\/16928182?ref=botble\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_blank\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"id\\\":\\\"602\\\",\\\"title\\\":\\\"Tin t\\u1ee9c\\\",\\\"referenceId\\\":\\\"11\\\",\\\"referenceType\\\":\\\"Botble\\\\\\\\Blog\\\\\\\\Models\\\\\\\\Category\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/new-update\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"children\\\":[{\\\"id\\\":\\\"606\\\",\\\"title\\\":\\\"Th\\u01b0 vi\\u1ec7n \\u1ea3nh\\\",\\\"referenceId\\\":\\\"0\\\",\\\"referenceType\\\":\\\"\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/vi\\/galleries\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":0,\\\"children\\\":[]},{\\\"id\\\":\\\"594\\\",\\\"title\\\":\\\"Li\\u00ean h\\u1ec7\\\",\\\"referenceId\\\":\\\"17\\\",\\\"referenceType\\\":\\\"Botble\\\\\\\\Page\\\\\\\\Models\\\\\\\\Page\\\",\\\"customUrl\\\":\\\"http:\\/\\/video4k.vnn:811\\/lien-he\\\",\\\"class\\\":\\\"\\\",\\\"target\\\":\\\"_self\\\",\\\"iconFont\\\":\\\"\\\",\\\"position\\\":1,\\\"children\\\":[]}],\\\"position\\\":1}],\\\"position\\\":0}]\",\"target\":\"_self\",\"title\":\"Li\\u00ean h\\u1ec7\",\"custom-url\":\"\\/vi\\/galleries\",\"icon-font\":null,\"class\":null,\"locations\":[\"main-menu\"],\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 2, '', 'info', '2020-04-22 10:14:57', '2020-04-22 10:14:57'),
(32, 1, 'protag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"test\",\"slug\":\"test\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProTag\",\"description\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 1, 'test', 'info', '2020-04-22 11:39:27', '2020-04-22 11:39:27'),
(33, 1, 'product', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-22 11:45:16', '2020-04-22 11:45:16'),
(34, 1, 'product', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-22 11:45:25', '2020-04-22 11:45:25'),
(35, 1, 'product', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(36, 1, 'protag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 2, 'product tag 2', 'info', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(37, 1, 'protag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 3, 'tag 3', 'info', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(38, 1, 'protag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 4, 'tag 4', 'info', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(39, 1, 'protag', '{\"_token\":\"OBvKWC2zK9yNV7bmSQ51wu8xIEJFx2WQkaAQKkUj\",\"name\":\"th\\u1eed nghi\\u1ec7m\",\"slug\":\"thu-nghiem\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\ProTag\",\"description\":null,\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"en_US\",\"ref_from\":\"1\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 5, 'thử nghiệm', 'info', '2020-04-22 11:51:03', '2020-04-22 11:51:03'),
(40, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-22 20:49:50', '2020-04-22 20:49:50'),
(41, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-22 20:49:50', '2020-04-22 20:49:50'),
(42, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-23 00:01:46', '2020-04-23 00:01:46'),
(43, 1, 'simple-slider', '{\"_token\":\"93TnjIsdbehyYV4XIZfqNr5WydUeFhkFZ6eAzmeU\",\"name\":\"main slider\",\"key\":\"main-slider\",\"description\":\"main-slider\",\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 1, 'main slider', 'info', '2020-04-23 00:02:22', '2020-04-23 00:02:22'),
(44, 1, 'simple-slider-item', '{\"_token\":\"93TnjIsdbehyYV4XIZfqNr5WydUeFhkFZ6eAzmeU\",\"simple_slider_id\":\"1\",\"title\":\"Trang ch\\u1ee7\",\"link\":\"https:\\/\\/benhvienhoabinh.vn\\/nguoi-tham-gia-bao-hiem-can-biet.html\",\"description\":\"d\",\"type_slider\":\"1\",\"content_des1\":\"d\",\"content_des2\":\"d\",\"content_des3\":\"d\",\"order\":\"0\",\"image\":\"1476890029-hero01-540x360.jpg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 1, 1, 'Trang chủ', 'info', '2020-04-23 00:08:42', '2020-04-23 00:08:42'),
(45, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-23 08:30:58', '2020-04-23 08:30:58'),
(46, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-23 10:59:17', '2020-04-23 10:59:17'),
(47, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-24 01:13:42', '2020-04-24 01:13:42'),
(48, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-24 01:26:16', '2020-04-24 01:26:16'),
(49, 1, 'member', '{\"_token\":\"Wb9EtcbO8hgVhvarzeA3vx9kS8f9wbwB1wUGbrWz\",\"first_name\":\"D\\u0169ng Gia\",\"last_name\":\"L\\u01b0\\u01a1ng\",\"email\":\"luongngocdung.vina@gmail.com\",\"password\":\"123456\",\"password_confirmation\":\"123456\",\"submit\":\"apply\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, '', 'info', '2020-04-24 01:51:51', '2020-04-24 01:51:51'),
(50, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-24 07:06:08', '2020-04-24 07:06:08'),
(51, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"m\\u00e0u \\u0111\\u1ecf\",\"submit\":\"apply\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'màu đỏ', 'info', '2020-04-24 08:03:11', '2020-04-24 08:03:11'),
(52, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"1\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:22:21', '2020-04-24 08:22:21'),
(53, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"1\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:23:22', '2020-04-24 08:23:22'),
(54, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"1\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:23:55', '2020-04-24 08:23:55'),
(55, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u xanh\",\"submit\":\"apply\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Màu xanh', 'info', '2020-04-24 08:24:29', '2020-04-24 08:24:29'),
(56, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u xanh\",\"submit\":\"save\",\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Màu xanh', 'primary', '2020-04-24 08:24:37', '2020-04-24 08:24:37'),
(57, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u Tr\\u1eafng\",\"submit\":\"save\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Màu Trắng', 'info', '2020-04-24 08:24:46', '2020-04-24 08:24:46'),
(58, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u \\u0111\\u1ecf\",\"submit\":\"save\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Màu đỏ', 'info', '2020-04-24 08:24:56', '2020-04-24 08:24:56'),
(59, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u tr\\u1eafng\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":\"3\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 5, 'Màu trắng', 'info', '2020-04-24 08:26:04', '2020-04-24 08:26:04'),
(60, 1, 'color', '{\"ids\":[\"5\",\"3\",\"2\",\"1\"],\"class\":\"Botble\\\\Product\\\\Tables\\\\ColorTable\"}', 'deleted', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 5, 'Màu trắng', 'danger', '2020-04-24 08:26:15', '2020-04-24 08:26:15'),
(61, 1, 'color', '{\"ids\":[\"5\",\"3\",\"2\",\"1\"],\"class\":\"Botble\\\\Product\\\\Tables\\\\ColorTable\"}', 'deleted', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Màu Trắng', 'danger', '2020-04-24 08:26:16', '2020-04-24 08:26:16'),
(62, 1, 'color', '{\"ids\":[\"5\",\"3\",\"2\",\"1\"],\"class\":\"Botble\\\\Product\\\\Tables\\\\ColorTable\"}', 'deleted', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Màu xanh', 'danger', '2020-04-24 08:26:16', '2020-04-24 08:26:16'),
(63, 1, 'color', '{\"ids\":[\"5\",\"3\",\"2\",\"1\"],\"class\":\"Botble\\\\Product\\\\Tables\\\\ColorTable\"}', 'deleted', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'màu đỏ', 'danger', '2020-04-24 08:26:16', '2020-04-24 08:26:16'),
(64, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u \\u0111\\u1ecf\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Màu đỏ', 'info', '2020-04-24 08:27:02', '2020-04-24 08:27:02'),
(65, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u \\u0110en\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 7, 'Màu Đen', 'info', '2020-04-24 08:27:13', '2020-04-24 08:27:13'),
(66, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u Xanh\",\"slug\":\"mau-xanh\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 8, 'Màu Xanh', 'info', '2020-04-24 08:27:23', '2020-04-24 08:27:23'),
(67, 1, 'color', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"M\\u00e0u Tr\\u1eafng\",\"slug\":\"mau-trang\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 9, 'Màu Trắng', 'info', '2020-04-24 08:27:34', '2020-04-24 08:27:34'),
(68, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"8\",\"9\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:27:52', '2020-04-24 08:27:52'),
(69, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:27:59', '2020-04-24 08:27:59');
INSERT INTO `audit_histories` (`id`, `user_id`, `module`, `request`, `action`, `user_agent`, `ip_address`, `reference_user`, `reference_id`, `reference_name`, `type`, `created_at`, `updated_at`) VALUES
(70, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:31:11', '2020-04-24 08:31:11'),
(71, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"option1\":[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 08:59:04', '2020-04-24 08:59:04'),
(72, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"option1\":[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\",\"rgb(255, 192, 203)\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 09:00:36', '2020-04-24 09:00:36'),
(73, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"option1\":[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 09:05:27', '2020-04-24 09:05:27'),
(74, 1, 'product', '{\"_token\":\"F8EKIYY4RxfNljJNjWSnlQUrA98jBXN384s1pJy1\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"option1\":[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\",\"rgb(255, 192, 203)\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 09:06:33', '2020-04-24 09:06:33'),
(75, 1, 'of the system', '[]', 'logged out', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'System Admin', 'info', '2020-04-24 09:21:06', '2020-04-24 09:21:06'),
(76, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-24 09:21:44', '2020-04-24 09:21:44'),
(77, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"XXL\",\"slug\":\"xxl\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 10, 'XXL', 'info', '2020-04-24 09:27:19', '2020-04-24 09:27:19'),
(78, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"XL\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 11, 'XL', 'info', '2020-04-24 09:27:27', '2020-04-24 09:27:27'),
(79, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"X\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 12, 'X', 'info', '2020-04-24 09:27:33', '2020-04-24 09:27:33'),
(80, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"XX\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 13, 'XX', 'info', '2020-04-24 09:27:40', '2020-04-24 09:27:40'),
(81, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"L\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 14, 'L', 'info', '2020-04-24 09:27:48', '2020-04-24 09:27:48'),
(82, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"M\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 15, 'M', 'info', '2020-04-24 09:27:54', '2020-04-24 09:27:54'),
(83, 1, 'size', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"S\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Size\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 16, 'S', 'info', '2020-04-24 09:28:01', '2020-04-24 09:28:01'),
(84, 1, 'product', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"sizes\":[\"10\",\"12\",\"15\"],\"option1\":[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\",\"rgb(255, 192, 203)\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 09:33:37', '2020-04-24 09:33:37'),
(85, 1, 'product', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"sizes\":[\"10\",\"12\",\"15\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', 'primary', '2020-04-24 09:33:50', '2020-04-24 09:33:50'),
(86, 1, 'store', '{\"_token\":\"k4LoMKjHccvWRNbVqeAIDT6zRPgGMfKGJfPzykkC\",\"name\":\"Kho 1\",\"submit\":\"apply\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'Kho 1', 'info', '2020-04-24 12:10:07', '2020-04-24 12:10:07'),
(87, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-24 23:49:31', '2020-04-24 23:49:31'),
(88, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-25 05:41:04', '2020-04-25 05:41:04'),
(89, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"test product\",\"key\":\"o-ok\",\"description\":\"fdfdf\",\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'test product', 'info', '2020-04-25 10:03:13', '2020-04-25 10:03:13'),
(90, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"test product\",\"key\":\"o-ok\",\"description\":\"r\\u00ear\",\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'test product', 'primary', '2020-04-25 10:04:54', '2020-04-25 10:04:54'),
(91, 1, 'store-product-item', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"store_product_id\":\"1\",\"title\":\"fsf\",\"link\":\"ssdsd\",\"description\":\"sdsds\",\"order\":\"0\",\"image\":\"1476890029-hero01-540x360.jpg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'fsf', 'info', '2020-04-25 10:14:01', '2020-04-25 10:14:01'),
(92, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"test product\",\"key\":\"o-ok\",\"description\":\"r\\u00ear\",\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'test product', 'primary', '2020-04-25 10:14:07', '2020-04-25 10:14:07'),
(93, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"Kho 1\",\"key\":\"kho 1\",\"description\":\"kho 1\",\"submit\":\"apply\",\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Kho 1', 'info', '2020-04-25 10:36:06', '2020-04-25 10:36:06'),
(94, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"Kho 1\",\"key\":\"kho 1\",\"description\":\"kho 1\",\"submit\":\"apply\",\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Kho 1', 'primary', '2020-04-25 10:37:05', '2020-04-25 10:37:05'),
(95, 1, 'store-product-item', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"store_product_id\":\"2\",\"title\":\"dfdf\",\"link\":\"dfdf\",\"description\":\"dfdfd\",\"order\":\"34\",\"image\":null}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'dfdf', 'info', '2020-04-25 10:52:30', '2020-04-25 10:52:30'),
(96, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"Kho 1\",\"key\":\"kho 1\",\"description\":\"kho 1\",\"submit\":\"apply\",\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Kho 1', 'primary', '2020-04-25 10:52:38', '2020-04-25 10:52:38'),
(97, 1, 'store-product-item', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"store_product_id\":\"2\",\"title\":\"Th\\u00eam tr\\u01b0\\u1eddng bi\\u1ebfn t\\u1ea5u kh\\u00e1c c\\u00e1c page\",\"link\":\"https:\\/\\/benhvienhoabinh.vn\\/nguoi-tham-gia-bao-hiem-can-biet.html\",\"description\":\"4456645\",\"order\":\"0\",\"image\":null}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Thêm trường biến tấu khác các page', 'info', '2020-04-25 11:08:56', '2020-04-25 11:08:56'),
(98, 1, 'store-product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"Kho 1\",\"key\":\"kho 1\",\"description\":\"kho 1\",\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Kho 1', 'primary', '2020-04-25 11:15:43', '2020-04-25 11:15:43'),
(99, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"3\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"sizes\":[\"10\",\"12\",\"15\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, '3', 'primary', '2020-04-25 11:27:43', '2020-04-25 11:27:43'),
(100, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"1\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"sizes\":[\"10\",\"12\",\"15\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, '1', 'primary', '2020-04-25 11:28:11', '2020-04-25 11:28:11'),
(101, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"2\",\"slug\":\"san-pham-1duplicateduplicateduplicate\",\"slug_id\":\"76\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"colors\":[\"6\",\"7\",\"9\"],\"sizes\":[\"10\",\"12\",\"15\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":\"test,product tag 2,tag 3,tag 4\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 4, '2', 'primary', '2020-04-25 11:28:37', '2020-04-25 11:28:37'),
(102, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"1\",\"slug\":\"fsf\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 5, '1', 'primary', '2020-04-25 11:29:13', '2020-04-25 11:29:13'),
(103, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"1\",\"slug\":\"fsf\",\"slug_id\":\"100\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 5, '1', 'primary', '2020-04-25 11:29:48', '2020-04-25 11:29:48'),
(104, 1, 'color', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"M\\u00e0u \\u0111\\u1ecf\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Color\",\"code\":\"#ff0000\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Màu đỏ', 'primary', '2020-04-25 12:18:43', '2020-04-25 12:18:43'),
(105, 1, 'features', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"Trung qu\\u1ed1c\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Features\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 1, 'Trung quốc', 'info', '2020-04-25 12:24:42', '2020-04-25 12:24:42'),
(106, 1, 'features', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"L\\u00e0o\",\"slug\":null,\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Features\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"save\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 2, 'Lào', 'info', '2020-04-25 12:24:48', '2020-04-25 12:24:48'),
(107, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicate\",\"slug_id\":\"75\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"7\",\"8\",\"9\"],\"sizes\":[\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Sản phẩm 1(Duplicate)(Duplicate)', 'primary', '2020-04-25 12:26:30', '2020-04-25 12:26:30'),
(108, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicate\",\"slug_id\":\"75\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"7\",\"8\",\"9\"],\"sizes\":[\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Sản phẩm 1(Duplicate)(Duplicate)', 'primary', '2020-04-25 12:32:31', '2020-04-25 12:32:31'),
(109, 1, 'product', '{\"_token\":\"NY2GloORt7niDHIChguaQiYZ74weigyRa7zNx8At\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1(Duplicate)(Duplicate)\",\"slug\":\"san-pham-1duplicateduplicate\",\"slug_id\":\"75\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"sdsds3434\",\"is_featured\":\"0\",\"content\":\"<p>dsdsd34343<\\/p>\",\"video\":\"h\\u1ea3i d\\u01b0\\u01a1ng434343\",\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"7\",\"8\",\"9\"],\"sizes\":[\"10\",\"11\",\"12\",\"13\",\"15\",\"16\"],\"pricesale\":\"343434343\",\"amound\":\"33434\",\"price_sale_start\":\"2020-04-23\",\"price_sale_end\":\"2020-04-25\",\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":\"users\\/avatar.jpg\",\"pricesell\":\"203434\",\"pricecost\":\"20000\",\"pricetime\":\"2020-03-31\",\"protag\":null}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 3, 'Sản phẩm 1(Duplicate)(Duplicate)', 'primary', '2020-04-25 12:38:29', '2020-04-25 12:38:29'),
(110, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-25 23:38:40', '2020-04-25 23:38:40'),
(111, 1, 'product', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Sản phẩm 1', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(112, 1, 'protag', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'fgfgf', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(113, 1, 'protag', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 7, 'trtrt', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(114, 1, 'protag', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 8, 'ưewe', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(115, 1, 'protag', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 9, 'xâx', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(116, 1, 'protag', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"0\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'created', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 10, 'gfgfg', 'info', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(117, 1, 'product', '{\"_token\":\"DoSc20XTuIiS2QreMviW9WM14T2M4WyQTnli00iB\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"103\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Sản phẩm 1', 'primary', '2020-04-26 01:57:25', '2020-04-26 01:57:25'),
(118, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-26 04:23:05', '2020-04-26 04:23:05'),
(119, 1, 'to the system', NULL, 'logged in', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 0, 1, 'System Admin', 'info', '2020-04-26 07:09:39', '2020-04-26 07:09:39'),
(120, 1, 'product', '{\"_token\":\"jchfcsiQtfIIOng3Nxn8oMtAMjtqqVoI0UG7bcnN\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"103\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000\",\"pricecost\":\"25000\",\"currency_id\":\"5\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Sản phẩm 1', 'primary', '2020-04-26 07:45:05', '2020-04-26 07:45:05'),
(121, 1, 'product', '{\"_token\":\"jchfcsiQtfIIOng3Nxn8oMtAMjtqqVoI0UG7bcnN\",\"name\":\"S\\u1ea3n ph\\u1ea9m 1\",\"slug\":\"sarn-ph\",\"slug_id\":\"103\",\"model\":\"Botble\\\\Product\\\\Models\\\\Product\",\"description\":\"S\\u1ea3n ph\\u1ea9m 1\",\"is_featured\":\"0\",\"content\":\"<p>S\\u1ea3n ph\\u1ea9m 1<\\/p>\",\"video\":\"S\\u1ea3n ph\\u1ea9m 1\",\"images\":[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"],\"features\":[\"1\",\"2\"],\"colors\":[\"6\",\"9\"],\"sizes\":[\"10\",\"12\",\"16\"],\"option1\":[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"],\"seo_meta\":{\"seo_title\":null,\"seo_description\":null},\"submit\":\"apply\",\"language\":\"vi\",\"ref_from\":null,\"status\":\"published\",\"format_type\":null,\"procategories\":[\"1\"],\"imagedl\":null,\"pricesell\":\"500000434343434\",\"pricecost\":\"25000343443434\",\"currency_id\":\"5\",\"pricetime\":\"2020-05-09\",\"protag\":\"fgfgf,trtrt,\\u01b0ewe,x\\u00e2x,gfgfg\"}', 'updated', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36', '127.0.0.1', 1, 6, 'Sản phẩm 1', 'primary', '2020-04-26 07:52:50', '2020-04-26 07:52:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blocks`
--

CREATE TABLE `blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blocks`
--

INSERT INTO `blocks` (`id`, `name`, `alias`, `description`, `content`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Sample block', 'sample-block', 'This is a sample block', '<p><span style=\"color:#e67e22;\">This block will be shown on the contact page!</span></p>', 'published', 1, '2019-03-11 19:30:01', '2019-03-11 19:30:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(10) UNSIGNED NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `icon` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent_id`, `description`, `status`, `author_id`, `author_type`, `icon`, `is_featured`, `order`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorized', 0, 'Demo', 'published', 0, 'Botble\\ACL\\Models\\User', '', 0, 0, 1, '2016-07-09 16:32:39', '2016-11-25 07:31:58'),
(6, 'Events', 0, 'Event description', 'published', 1, 'Botble\\ACL\\Models\\User', '', 1, 0, 0, '2016-08-02 22:13:34', '2016-11-25 07:32:02'),
(7, 'Projects', 6, 'Projects description', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 3, 0, '2016-08-02 22:13:52', '2017-04-30 19:58:41'),
(8, 'Portfolio', 9, 'Description', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 0, '2016-09-27 09:32:06', '2017-04-30 19:58:21'),
(9, 'Business', 0, 'Business', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 1, 2, 0, '2016-09-28 09:38:25', '2017-04-30 19:59:12'),
(10, 'Resources', 11, 'Resource', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 4, 0, '2016-09-28 09:39:46', '2017-04-30 19:58:55'),
(11, 'New & Updates', 0, 'News and Update', 'published', 1, 'Botble\\ACL\\Models\\User', '', 1, 5, 0, '2016-09-28 09:40:25', '2016-11-25 07:31:56'),
(12, 'Chưa phân loại', 0, 'Chuyên mục chưa phân loại', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 1, '2018-04-13 09:02:12', '2018-04-13 09:02:12'),
(13, 'Sự kiện', 0, 'Sự kiện', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 1, 0, 0, '2018-04-13 09:04:30', '2018-04-13 10:01:11'),
(14, 'Doanh nghiệp', 0, 'Chuyên mục doanh nghiệp', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 0, '2018-04-13 09:04:49', '2018-04-13 09:04:49'),
(15, 'Tin tức & cập nhật', 0, 'Chuyên mục tin tức và cập nhật', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 0, '2018-04-13 09:05:06', '2018-04-13 09:05:06'),
(16, 'Dự án', 13, 'Chuyên mục dự án', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 1, 0, 0, '2018-04-13 09:05:23', '2018-04-13 10:05:52'),
(17, 'Đầu tư', 14, 'Chuyên mục doanh nghiệp', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 0, '2018-04-13 09:06:44', '2018-04-13 09:06:44'),
(18, 'Nguồn lực', 15, 'Chuyên mục nguồn lực', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 0, 0, '2018-04-13 09:08:01', '2018-04-13 09:08:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_color` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `colors`
--

INSERT INTO `colors` (`id`, `name`, `code_color`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Màu đỏ', '#ff0000', 'published', '2020-04-24 08:27:02', '2020-04-25 12:18:43'),
(7, 'Màu Đen', '#fff', 'published', '2020-04-24 08:27:13', '2020-04-24 08:27:13'),
(8, 'Màu Xanh', '#fff', 'published', '2020-04-24 08:27:23', '2020-04-24 08:27:23'),
(9, 'Màu Trắng', '#fff', 'published', '2020-04-24 08:27:34', '2020-04-24 08:27:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `address`, `content`, `subject`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Demo contact', 'admin@admin.com', '0123456789', 'Somewhere in the world', 'The sample content', NULL, '2017-01-15 21:19:27', '2017-01-15 21:25:47', 'unread');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact_replies`
--

CREATE TABLE `contact_replies` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_prefix_symbol` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `decimals` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_default` tinyint(4) NOT NULL DEFAULT 0,
  `exchange_rate` double NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `symbol`, `is_prefix_symbol`, `decimals`, `order`, `is_default`, `exchange_rate`, `created_at`, `updated_at`) VALUES
(4, 'USD', '$', 1, 0, 0, 1, 1, '2020-04-26 07:44:57', '2020-04-26 07:44:57'),
(5, 'VNĐ', 'đ', 0, 0, 1, 0, 1, '2020-04-26 07:44:57', '2020-04-26 07:54:24');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `use_for` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_for_id` int(10) UNSIGNED NOT NULL,
  `field_item_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dashboard_widgets`
--

CREATE TABLE `dashboard_widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dashboard_widgets`
--

INSERT INTO `dashboard_widgets` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'widget_posts_recent', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(2, 'widget_analytics_general', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(3, 'widget_analytics_page', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(4, 'widget_analytics_browser', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(5, 'widget_analytics_referrer', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(6, 'widget_audit_logs', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(7, 'widget_request_errors', '2017-11-30 18:26:50', '2017-11-30 18:26:50'),
(8, 'widget_total_plugins', '2019-03-11 19:29:10', '2019-03-11 19:29:10'),
(9, 'widget_total_pages', '2019-03-11 19:29:10', '2019-03-11 19:29:10'),
(10, 'widget_total_users', '2019-03-11 19:29:10', '2019-03-11 19:29:10'),
(11, 'widget_total_themes', '2019-03-11 19:29:10', '2019-03-11 19:29:10'),
(12, 'widget_products_recent', '2020-04-13 07:35:32', '2020-04-13 07:35:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dashboard_widget_settings`
--

CREATE TABLE `dashboard_widget_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dashboard_widget_settings`
--

INSERT INTO `dashboard_widget_settings` (`id`, `settings`, `user_id`, `widget_id`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 2, 1, '2017-11-30 18:27:00', '2020-04-22 20:51:41'),
(2, NULL, 1, 3, 1, 1, '2017-11-30 18:27:00', '2020-04-22 20:51:41'),
(3, NULL, 1, 4, 3, 1, '2017-11-30 18:27:00', '2017-11-30 18:27:14'),
(4, NULL, 1, 5, 4, 1, '2017-11-30 18:27:00', '2017-11-30 18:27:14'),
(5, NULL, 1, 6, 6, 1, '2017-11-30 18:27:00', '2020-04-22 20:51:39'),
(6, NULL, 1, 7, 7, 1, '2017-11-30 18:27:00', '2020-04-22 20:51:39'),
(7, '{\"state\":\"collapse\"}', 1, 2, 0, 1, '2017-11-30 18:27:00', '2020-04-22 20:51:41'),
(8, NULL, 1, 12, 5, 1, '2020-04-22 20:51:39', '2020-04-22 20:51:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `features`
--

INSERT INTO `features` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Trung quốc', 'published', '2020-04-25 12:24:42', '2020-04-25 12:24:42'),
(2, 'Lào', 'published', '2020-04-25 12:24:48', '2020-04-25 12:24:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `field_groups`
--

CREATE TABLE `field_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rules` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `field_items`
--

CREATE TABLE `field_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `field_group_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) DEFAULT 0,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instructions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `description`, `status`, `is_featured`, `order`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Photography', 'This is description', 'published', 1, 0, 'galleries/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg', 1, '2016-10-13 09:49:13', '2019-09-13 17:03:28'),
(2, 'Nature', 'Nature gallery', 'published', 1, 0, 'galleries/1476513483-misty-mountains-1280x720.jpg', 1, '2016-10-13 09:56:07', '2019-09-13 17:03:28'),
(3, 'New Day', 'This is demo gallery', 'published', 1, 0, 'galleries/1476520418-supergirl-season-2-1280x720.jpg', 1, '2016-10-13 09:56:44', '2019-09-13 17:03:28'),
(4, 'Morning', 'Hello', 'published', 1, 0, 'galleries/1476513486-power-rangers-red-ranger-4k-1280x720.jpg', 1, '2016-10-13 09:57:30', '2019-09-13 17:03:29'),
(5, 'Happy day', 'Demo', 'published', 1, 0, 'galleries/1476513488-spectacular-sunrise-4k-1280x720.jpg', 1, '2016-10-13 09:58:11', '2019-09-13 17:03:29'),
(6, 'Perfect', 'This is perfect description', 'published', 1, 0, 'galleries/1476513493-world-of-tanks-football-event-1280x720.jpg', 1, '2016-10-13 09:58:40', '2019-09-13 17:03:29'),
(7, 'Nhiếp ảnh', 'Bộ sưu tập nhiếp ảnh', 'published', 1, 0, 'galleries/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg', 1, '2017-12-16 17:02:39', '2019-09-13 17:03:29'),
(8, 'Thiên nhiên', 'Bộ sưu tập ảnh thiên nhiên', 'published', 1, 0, 'galleries/1476513483-misty-mountains-1280x720.jpg', 1, '2017-12-16 17:03:19', '2019-09-13 17:03:29'),
(9, 'Ngày mới', 'Bộ sưu tập ảnh ngày mới', 'published', 1, 0, 'galleries/1476520418-supergirl-season-2-1280x720.jpg', 1, '2017-12-16 17:03:48', '2019-09-13 17:03:29'),
(10, 'Buổi sáng', 'Bộ sưu tập ảnh buổi sáng', 'published', 1, 0, 'galleries/1476513486-power-rangers-red-ranger-4k-1280x720.jpg', 1, '2017-12-16 17:04:20', '2019-09-13 17:03:29'),
(11, 'Ngày hạnh phúc', 'Bộ sưu tập ảnh ngày hạnh phúc', 'published', 1, 0, 'galleries/1476513488-spectacular-sunrise-4k-1280x720.jpg', 1, '2017-12-16 17:04:43', '2019-09-13 17:03:29'),
(12, 'Hoàn hảo', 'Bộ sưu tập ảnh hoàn hảo', 'published', 1, 0, 'galleries/1476513493-world-of-tanks-football-event-1280x720.jpg', 1, '2017-12-16 17:05:01', '2019-09-13 17:03:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gallery_meta`
--

CREATE TABLE `gallery_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `gallery_meta`
--

INSERT INTO `gallery_meta` (`id`, `reference_id`, `images`, `reference_type`, `created_at`, `updated_at`) VALUES
(70, 1, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:00:24', '2019-09-13 17:03:31'),
(71, 2, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:04:54', '2019-09-13 17:03:32'),
(72, 3, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:05:05', '2019-09-13 17:03:33'),
(73, 4, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:05:13', '2019-09-13 17:03:34'),
(74, 5, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:05:21', '2019-09-13 17:03:35'),
(75, 6, '[{\"img\":\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"description\":null},{\"img\":\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\",\"description\":null}]', 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 10:05:28', '2019-09-13 17:03:36'),
(77, 8, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:03:19', '2017-12-16 17:03:19'),
(78, 7, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:03:25', '2017-12-16 17:03:25'),
(79, 9, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:03:48', '2017-12-16 17:03:48'),
(80, 10, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:04:20', '2017-12-16 17:04:20'),
(81, 11, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:04:43', '2017-12-16 17:04:43'),
(82, 12, NULL, 'Botble\\Gallery\\Models\\Gallery', '2017-12-16 17:05:01', '2017-12-16 17:05:01'),
(85, 79, NULL, 'Botble\\Blog\\Models\\Post', '2017-12-16 17:56:46', '2017-12-16 17:56:46'),
(86, 80, NULL, 'Botble\\Blog\\Models\\Post', '2017-12-16 18:07:16', '2017-12-16 18:07:16'),
(87, 75, NULL, 'Botble\\Blog\\Models\\Post', '2017-12-17 18:37:49', '2017-12-17 18:37:49'),
(88, 81, NULL, 'Botble\\Blog\\Models\\Post', '2017-12-18 18:43:36', '2017-12-18 18:43:36'),
(89, 82, NULL, 'Botble\\Blog\\Models\\Post', '2017-12-18 18:44:57', '2017-12-18 18:44:57'),
(92, 44, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:38:58', '2018-04-13 09:38:58'),
(94, 48, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:40:18', '2018-04-13 09:40:18'),
(96, 49, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:41:32', '2018-04-13 09:41:32'),
(98, 50, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:42:27', '2018-04-13 09:42:27'),
(122, 51, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:58:23', '2018-04-13 09:58:23'),
(123, 52, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:02:20', '2018-04-13 10:02:20'),
(125, 53, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:03:07', '2018-04-13 10:03:07'),
(129, 54, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:04:20', '2018-04-13 10:04:20'),
(130, 55, NULL, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:06:40', '2018-04-13 10:06:40'),
(136, 1, NULL, 'Botble\\Page\\Models\\Page', '2020-03-10 19:06:21', '2020-03-10 19:06:21'),
(137, 17, NULL, 'Botble\\Page\\Models\\Page', '2020-03-10 19:33:16', '2020-03-10 19:33:16'),
(138, 56, '[{\"img\":\"users\\/avatar.jpg\",\"description\":\"\"},{\"img\":\"users\\/avatar.jpg\",\"description\":\"\"}]', 'Botble\\Blog\\Models\\Post', '2020-04-22 08:12:17', '2020-04-22 08:37:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `languages`
--

CREATE TABLE `languages` (
  `lang_id` int(10) UNSIGNED NOT NULL,
  `lang_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_locale` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_flag` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `lang_order` int(11) NOT NULL DEFAULT 0,
  `lang_is_rtl` tinyint(3) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `languages`
--

INSERT INTO `languages` (`lang_id`, `lang_name`, `lang_locale`, `lang_code`, `lang_flag`, `lang_is_default`, `lang_order`, `lang_is_rtl`) VALUES
(44, 'English', 'en', 'en_US', 'us', 0, 0, 0),
(45, 'Tiếng Việt', 'vi', 'vi', 'vn', 1, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `language_meta`
--

CREATE TABLE `language_meta` (
  `lang_meta_id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `lang_meta_code` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_meta_origin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `language_meta`
--

INSERT INTO `language_meta` (`lang_meta_id`, `reference_id`, `lang_meta_code`, `reference_type`, `lang_meta_origin`) VALUES
(3, 4, 'en_US', 'Botble\\Blog\\Models\\Post', 'a9d40e38f773df46bfe3857c89404a5f'),
(4, 5, 'en_US', 'Botble\\Blog\\Models\\Post', '9cf3508b0becc5829411b251ab1d7d68'),
(5, 6, 'en_US', 'Botble\\Blog\\Models\\Post', '14ea2c977bff486d5d93caeeddb25433'),
(6, 7, 'en_US', 'Botble\\Blog\\Models\\Post', '1e3d1a0e8a2d0b0a820f6fa5ff37913b'),
(7, 8, 'en_US', 'Botble\\Blog\\Models\\Post', 'efa2fcd9b0bc7221fc37de650db1095d'),
(8, 9, 'en_US', 'Botble\\Blog\\Models\\Post', '5aa291a9490cf5a6b770e4ef67eecf51'),
(9, 10, 'en_US', 'Botble\\Blog\\Models\\Post', 'ddf6b2634ac599fca598c9f707d7e967'),
(10, 11, 'en_US', 'Botble\\Blog\\Models\\Post', '0162b0f534be9b5c0de3a2c21ee12a22'),
(11, 12, 'en_US', 'Botble\\Blog\\Models\\Post', '58d24bc8ca3ef58e26b6f5f2e97feb52'),
(13, 14, 'en_US', 'Botble\\Blog\\Models\\Post', '6a95efc9f3493402f6edc17125a5f621'),
(14, 19, 'en_US', 'Botble\\Blog\\Models\\Post', 'be26223edcf68e5f63a1a6437ba0be15'),
(15, 15, 'en_US', 'Botble\\Blog\\Models\\Post', '36292b94b5bc330e88721f87c05e3d1c'),
(16, 16, 'en_US', 'Botble\\Blog\\Models\\Post', 'e4bc15912ab7551f41dda75cab017005'),
(17, 17, 'en_US', 'Botble\\Blog\\Models\\Post', '73bd326091a134d6eb79570db924bb3d'),
(18, 18, 'en_US', 'Botble\\Blog\\Models\\Post', '77999a905526eb38febac6a1e0f1f5d9'),
(19, 20, 'en_US', 'Botble\\Blog\\Models\\Post', '26af70c0ef781166972928bd181ab10b'),
(20, 21, 'en_US', 'Botble\\Blog\\Models\\Post', '3856d68ccb4721e6432dcc1ee7001e20'),
(21, 44, 'vi', 'Botble\\Blog\\Models\\Post', 'a9d40e38f773df46bfe3857c89404a5f'),
(22, 45, 'vi', 'Botble\\Blog\\Models\\Post', '9cf3508b0becc5829411b251ab1d7d68'),
(23, 47, 'vi', 'Botble\\Blog\\Models\\Post', '14ea2c977bff486d5d93caeeddb25433'),
(24, 46, 'vi', 'Botble\\Blog\\Models\\Post', 'efa2fcd9b0bc7221fc37de650db1095d'),
(25, 11, 'en_US', 'Botble\\Blog\\Models\\Category', '4703171c553ee525c3a5436c254619cf'),
(26, 1, 'en_US', 'Botble\\Blog\\Models\\Category', 'ff750ed85cf1ac627f2b323889f78dd6'),
(27, 6, 'en_US', 'Botble\\Blog\\Models\\Category', '5d21f76eddb6b3d536cb390f4cda77bc'),
(28, 7, 'en_US', 'Botble\\Blog\\Models\\Category', 'cd22dfa504c9bf620938f913773df770'),
(29, 8, 'en_US', 'Botble\\Blog\\Models\\Category', 'da650dfca58083229317df283b16fa02'),
(30, 9, 'en_US', 'Botble\\Blog\\Models\\Category', 'faa61a3d1e5630da1dc2cdcb4f11b552'),
(31, 10, 'en_US', 'Botble\\Blog\\Models\\Category', '55be2035a27da7bfcc8ed4aeab28f4f1'),
(32, 5, 'en_US', 'Botble\\Blog\\Models\\Tag', 'f0698ed728cc9c18387840b72346e005'),
(33, 6, 'en_US', 'Botble\\Blog\\Models\\Tag', 'e4ef34ee9099a01fcec8f45a79c5d4ba'),
(34, 1, 'en_US', 'Botble\\Gallery\\Models\\Gallery', 'b5956d5eec65f9edeb008fdd97771f3e'),
(35, 2, 'en_US', 'Botble\\Gallery\\Models\\Gallery', '4367a6d49e1eb6e5fb64d77052997e9c'),
(36, 3, 'en_US', 'Botble\\Gallery\\Models\\Gallery', '0cc8e19b35bb9b0a6b0b45d8a358ee6c'),
(37, 4, 'en_US', 'Botble\\Gallery\\Models\\Gallery', 'a2879f55a54f0e629851df6b48f61241'),
(38, 5, 'en_US', 'Botble\\Gallery\\Models\\Gallery', 'e6ceb4ae110f3d66f9444211a2ac8337'),
(39, 6, 'en_US', 'Botble\\Gallery\\Models\\Gallery', '9f2f8e1a4752378951cc1312b419c102'),
(40, 1, 'en_US', 'Botble\\Menu\\Models\\Menu', '19848c17b2e0b8fd374ae6f4741599c4'),
(41, 3, 'en_US', 'Botble\\Menu\\Models\\Menu', '5d79633980667117eaee456018277ad8'),
(42, 7, 'en_US', 'Botble\\Menu\\Models\\Menu', 'b1ae8e07383b5d47e821dac905c86e6d'),
(43, 8, 'en_US', 'Botble\\Menu\\Models\\Menu', '78d39e8989bebaa53bac83ff0fedc678'),
(44, 9, 'en_US', 'Botble\\Menu\\Models\\Menu', '33a202bdbd1d82470cc1837e85622c5e'),
(45, 10, 'vi', 'Botble\\Menu\\Models\\Menu', '19848c17b2e0b8fd374ae6f4741599c4'),
(47, 23, 'en_US', 'Botble\\Page\\Models\\Page', '0bff9f3639cec70a3f65fc0149ad2b24'),
(48, 1, 'en_US', 'Botble\\Page\\Models\\Page', '3e971ce162e3737ae2b7af1e78c4bca2'),
(49, 17, 'vi', 'Botble\\Page\\Models\\Page', '3e971ce162e3737ae2b7af1e78c4bca2'),
(50, 7, 'vi', 'Botble\\Blog\\Models\\Tag', 'f5a7aacaa745d7b1df533180f61bab95'),
(51, 1, 'en_US', 'Botble\\Block\\Models\\Block', '671424045986775272d0ceb6aab7139a'),
(52, 7, 'vi', 'Botble\\Gallery\\Models\\Gallery', 'b5956d5eec65f9edeb008fdd97771f3e'),
(53, 8, 'vi', 'Botble\\Gallery\\Models\\Gallery', '4367a6d49e1eb6e5fb64d77052997e9c'),
(54, 9, 'vi', 'Botble\\Gallery\\Models\\Gallery', '0cc8e19b35bb9b0a6b0b45d8a358ee6c'),
(55, 10, 'vi', 'Botble\\Gallery\\Models\\Gallery', 'a2879f55a54f0e629851df6b48f61241'),
(56, 11, 'vi', 'Botble\\Gallery\\Models\\Gallery', 'e6ceb4ae110f3d66f9444211a2ac8337'),
(57, 12, 'vi', 'Botble\\Gallery\\Models\\Gallery', '9f2f8e1a4752378951cc1312b419c102'),
(58, 12, 'vi', 'Botble\\Blog\\Models\\Category', 'ff750ed85cf1ac627f2b323889f78dd6'),
(59, 13, 'vi', 'Botble\\Blog\\Models\\Category', '5d21f76eddb6b3d536cb390f4cda77bc'),
(60, 14, 'vi', 'Botble\\Blog\\Models\\Category', 'faa61a3d1e5630da1dc2cdcb4f11b552'),
(61, 15, 'vi', 'Botble\\Blog\\Models\\Category', '4703171c553ee525c3a5436c254619cf'),
(62, 16, 'vi', 'Botble\\Blog\\Models\\Category', 'cd22dfa504c9bf620938f913773df770'),
(63, 17, 'vi', 'Botble\\Blog\\Models\\Category', 'da650dfca58083229317df283b16fa02'),
(64, 18, 'vi', 'Botble\\Blog\\Models\\Category', '55be2035a27da7bfcc8ed4aeab28f4f1'),
(65, 48, 'vi', 'Botble\\Blog\\Models\\Post', '1e3d1a0e8a2d0b0a820f6fa5ff37913b'),
(66, 49, 'vi', 'Botble\\Blog\\Models\\Post', '5aa291a9490cf5a6b770e4ef67eecf51'),
(67, 50, 'vi', 'Botble\\Blog\\Models\\Post', 'ddf6b2634ac599fca598c9f707d7e967'),
(68, 51, 'vi', 'Botble\\Blog\\Models\\Post', '0162b0f534be9b5c0de3a2c21ee12a22'),
(69, 11, 'vi', 'Botble\\Blog\\Models\\Tag', 'f0698ed728cc9c18387840b72346e005'),
(75, 24, 'vi', 'Botble\\Blog\\Models\\Tag', 'e4ef34ee9099a01fcec8f45a79c5d4ba'),
(76, 25, 'en_US', 'Botble\\Blog\\Models\\Tag', 'f5a7aacaa745d7b1df533180f61bab95'),
(77, 52, 'vi', 'Botble\\Blog\\Models\\Post', '58d24bc8ca3ef58e26b6f5f2e97feb52'),
(78, 53, 'vi', 'Botble\\Blog\\Models\\Post', '6a95efc9f3493402f6edc17125a5f621'),
(79, 54, 'vi', 'Botble\\Blog\\Models\\Post', '36292b94b5bc330e88721f87c05e3d1c'),
(80, 55, 'vi', 'Botble\\Blog\\Models\\Post', 'e4bc15912ab7551f41dda75cab017005'),
(81, 6, 'en_US', 'Botble\\Menu\\Models\\Menu', '3deb9e726e379790dcb23e6b0cf26e8f'),
(82, 2, 'vi', 'Botble\\Menu\\Models\\MenuLocation', 'b8c6f006a28da7585aef81e9405306bd'),
(83, 3, 'en_US', 'Botble\\Menu\\Models\\MenuLocation', '3e08298053dbd3ea6ae1a6673f14f305'),
(84, 1, 'en_US', 'Botble\\Menu\\Models\\MenuLocation', '9b3f18ba090b776d3bbac1b450c9ab6a'),
(85, 1, 'vi', 'Botble\\Product\\Models\\ProCategory', 'fbcb0d86fc58b0356ae992242c46cae7'),
(86, 2, 'vi', 'Botble\\Product\\Models\\ProCategory', '8c43953a532abbe0f85ccc684cc936ff'),
(87, 3, 'vi', 'Botble\\Product\\Models\\ProCategory', '13dc282c021be880f6622d05efe5a07b'),
(88, 1, 'vi', 'Botble\\Product\\Models\\Product', '4a22b2993facbe63dcde3f5838ba6161'),
(89, 2, 'vi', 'Botble\\Product\\Models\\Product', 'c83e3f58dd12f8fcdcd433f6f8c89caf'),
(90, 3, 'vi', 'Botble\\Product\\Models\\Product', 'fa39bd968e20e9b4d646a42d2a580d70'),
(91, 4, 'vi', 'Botble\\Product\\Models\\Product', 'c131e2d9a7cc7576ffffad7d78d0a5e6'),
(93, 5, 'vi', 'Botble\\Product\\Models\\ProCategory', '66b7fdb6e2534c0ea67c69c362d306e1'),
(94, 6, 'vi', 'Botble\\Product\\Models\\ProCategory', '022c7d1b8f01b0d5bf8905b018c8d21f'),
(95, 7, 'vi', 'Botble\\Product\\Models\\ProCategory', 'daaf9c7a8f1d19550b68ecc9a1891288'),
(96, 56, 'vi', 'Botble\\Blog\\Models\\Post', '718c77ff3b57bcb218295c5e1b901c8d'),
(97, 26, 'vi', 'Botble\\Blog\\Models\\Tag', '3ed6930326a59260958c7fd1e61800b2'),
(98, 1, 'vi', 'Botble\\Product\\Models\\ProTag', '490b4b5b024254ed26ec17021b40a99c'),
(99, 2, 'vi', 'Botble\\Product\\Models\\ProTag', 'a148809867036cccd18ab5e549d2608f'),
(100, 3, 'vi', 'Botble\\Product\\Models\\ProTag', 'b761d325b17a067bfb3c6cf1cba140b0'),
(101, 4, 'vi', 'Botble\\Product\\Models\\ProTag', '3c080db865d27d997122b9296b2ea362'),
(102, 5, 'en_US', 'Botble\\Product\\Models\\ProTag', '490b4b5b024254ed26ec17021b40a99c'),
(103, 1, 'vi', 'Botble\\SimpleSlider\\Models\\SimpleSlider', '286978ea5b3659355005c0910bdab9e9'),
(104, 6, 'vi', 'Botble\\Product\\Models\\Color', '57f6eabf78006b398e0d0b9c57d0d8b3'),
(105, 7, 'vi', 'Botble\\Product\\Models\\Color', '87aa476680eb15bdaab52361ab38b229'),
(106, 8, 'vi', 'Botble\\Product\\Models\\Color', 'c0ea037bc296391f31989677374693c2'),
(107, 9, 'vi', 'Botble\\Product\\Models\\Color', '7f47d88995c0be55045f0e6b50a6735d'),
(108, 10, 'vi', 'Botble\\Product\\Models\\Size', '4fd897eeaa2ec352ce730b62fc38b6b7'),
(109, 11, 'vi', 'Botble\\Product\\Models\\Size', '426e7503c676f02eff7c76973b88e7a3'),
(110, 12, 'vi', 'Botble\\Product\\Models\\Size', '0e496990df939fc0715952b3c1a1db2f'),
(111, 13, 'vi', 'Botble\\Product\\Models\\Size', 'e156cfd3ef7a863b07a1f1a3ebf097d7'),
(112, 14, 'vi', 'Botble\\Product\\Models\\Size', 'bcf5f4c12df4742ef24beab2a5a7a0f5'),
(113, 15, 'vi', 'Botble\\Product\\Models\\Size', 'be6050abca73352f3233d042b8e4f22d'),
(114, 16, 'vi', 'Botble\\Product\\Models\\Size', '2ea45c2210f2195e2820727e5e26104d'),
(115, 1, 'vi', 'Botble\\StoreProduct\\Models\\StoreProduct', '4709a5c24a2884afb7e75b4a2cef11b3'),
(116, 5, 'vi', 'Botble\\Product\\Models\\Product', 'dab5075544f1e96e1b9197fbe3d5f702'),
(117, 1, 'vi', 'Botble\\Product\\Models\\Features', '9a9712195652cb409b9bc4215f0abbe1'),
(118, 2, 'vi', 'Botble\\Product\\Models\\Features', '8583f3720a0398dfe0ace15f22460006'),
(119, 6, 'vi', 'Botble\\Product\\Models\\Product', '6a8ccb41267df7422bc947fe2c119227'),
(120, 6, 'vi', 'Botble\\Product\\Models\\ProTag', '7426ba9ce79e8871b249bed103a920ee'),
(121, 7, 'vi', 'Botble\\Product\\Models\\ProTag', '5c32fa2c85d10ae76d77a65ff54eaa59'),
(122, 8, 'vi', 'Botble\\Product\\Models\\ProTag', '3836229365b4d90ff52857d5281dfd76'),
(123, 9, 'vi', 'Botble\\Product\\Models\\ProTag', '6afd799b5b61aa54c78151e7a7accf1e'),
(124, 10, 'vi', 'Botble\\Product\\Models\\ProTag', 'ce0834e6c0537d45b9d2eba9d33e4c08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `media_files`
--

CREATE TABLE `media_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED DEFAULT 0,
  `mime_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `media_files`
--

INSERT INTO `media_files` (`id`, `user_id`, `name`, `folder_id`, `mime_type`, `size`, `url`, `options`, `created_at`, `updated_at`, `deleted_at`) VALUES
(87, 1, '2019-09-14 00:16:04', 5, 'image/png', 3161, 'logo/efe3f88b-0517-4e26-a901-f7d20f69ef37.png', '[]', '2019-09-13 17:16:04', '2019-09-13 17:16:04', NULL),
(88, 1, '1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720', 4, 'image/jpeg', 172017, 'galleries/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg', '[]', '2019-11-02 04:22:22', '2019-11-02 04:22:22', NULL),
(89, 1, '1476513483-misty-mountains-1280x720', 4, 'image/jpeg', 115996, 'galleries/1476513483-misty-mountains-1280x720.jpg', '[]', '2019-11-02 04:22:23', '2019-11-02 04:22:23', NULL),
(90, 1, '1476513484-power-rangers-blue-ranger-4k-1280x720', 4, 'image/jpeg', 110182, 'galleries/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg', '[]', '2019-11-02 04:22:24', '2019-11-02 04:22:24', NULL),
(91, 1, '1476513486-power-rangers-red-ranger-4k-1280x720', 4, 'image/jpeg', 131618, 'galleries/1476513486-power-rangers-red-ranger-4k-1280x720.jpg', '[]', '2019-11-02 04:22:25', '2019-11-02 04:22:25', NULL),
(92, 1, '1476513488-spectacular-sunrise-4k-1280x720', 4, 'image/jpeg', 59951, 'galleries/1476513488-spectacular-sunrise-4k-1280x720.jpg', '[]', '2019-11-02 04:22:26', '2019-11-02 04:22:26', NULL),
(93, 1, '1476513490-tulips-4k-8k-768x1280', 4, 'image/jpeg', 79663, 'galleries/1476513490-tulips-4k-8k-768x1280.jpg', '[]', '2019-11-02 04:22:27', '2019-11-02 04:22:27', NULL),
(94, 1, '1476513493-world-of-tanks-football-event-1280x720', 4, 'image/jpeg', 210966, 'galleries/1476513493-world-of-tanks-football-event-1280x720.jpg', '[]', '2019-11-02 04:22:28', '2019-11-02 04:22:28', NULL),
(95, 1, '1476520418-supergirl-season-2-1280x720', 4, 'image/jpeg', 152860, 'galleries/1476520418-supergirl-season-2-1280x720.jpg', '[]', '2019-11-02 04:22:29', '2019-11-02 04:22:29', NULL),
(96, 1, '1476520641-elena-siberian-tigress-4k-1280x720', 4, 'image/jpeg', 212356, 'galleries/1476520641-elena-siberian-tigress-4k-1280x720.jpg', '[]', '2019-11-02 04:22:30', '2019-11-02 04:22:30', NULL),
(97, 1, '1476521053-volkswagen-id-paris-motor-show-4k-1280x720', 4, 'image/jpeg', 143223, 'galleries/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg', '[]', '2019-11-02 04:22:31', '2019-11-02 04:22:31', NULL),
(98, 1, '1476890029-hero01', 3, 'image/jpeg', 106200, 'news/1476890029-hero01.jpg', '[]', '2019-11-02 04:23:28', '2019-11-02 04:23:28', NULL),
(99, 1, '1476890031-hero02', 3, 'image/jpeg', 56843, 'news/1476890031-hero02.jpg', '[]', '2019-11-02 04:23:29', '2019-11-02 04:23:29', NULL),
(100, 1, '1476890033-hero03', 3, 'image/jpeg', 86015, 'news/1476890033-hero03.jpg', '[]', '2019-11-02 04:23:29', '2019-11-02 04:23:29', NULL),
(101, 1, '1476890034-hero04', 3, 'image/jpeg', 73927, 'news/1476890034-hero04.jpg', '[]', '2019-11-02 04:23:30', '2019-11-02 04:23:30', NULL),
(102, 1, '1476890036-hero05', 3, 'image/jpeg', 82371, 'news/1476890036-hero05.jpg', '[]', '2019-11-02 04:23:31', '2019-11-02 04:23:31', NULL),
(103, 1, '1476891035-01', 3, 'image/jpeg', 139584, 'news/1476891035-01.jpg', '[]', '2019-11-02 04:23:31', '2019-11-02 04:23:31', NULL),
(104, 1, '1476891195-02', 3, 'image/jpeg', 135878, 'news/1476891195-02.jpg', '[]', '2019-11-02 04:23:32', '2019-11-02 04:23:32', NULL),
(105, 1, '1476891198-04', 3, 'image/jpeg', 192746, 'news/1476891198-04.jpg', '[]', '2019-11-02 04:23:33', '2019-11-02 04:23:33', NULL),
(106, 1, '1476893532-01', 3, 'image/jpeg', 139584, 'news/1476893532-01.jpg', '[]', '2019-11-02 04:23:34', '2019-11-02 04:23:34', NULL),
(107, 1, '1476893533-02', 3, 'image/jpeg', 190105, 'news/1476893533-02.jpg', '[]', '2019-11-02 04:23:34', '2019-11-02 04:23:34', NULL),
(108, 1, '4381851322-d46fd7d75e-b-660x330', 3, 'image/jpeg', 68706, 'news/4381851322-d46fd7d75e-b-660x330.jpg', '[]', '2019-11-02 04:23:35', '2019-11-02 04:23:35', NULL),
(109, 1, '7717834982-bbd7e12b8c-b-660x330', 3, 'image/jpeg', 49286, 'news/7717834982-bbd7e12b8c-b-660x330.jpg', '[]', '2019-11-02 04:23:35', '2019-11-02 04:23:35', NULL),
(110, 1, '7998125906-4489ed8a2f-b-660x330', 3, 'image/jpeg', 51071, 'news/7998125906-4489ed8a2f-b-660x330.jpg', '[]', '2019-11-02 04:23:36', '2019-11-02 04:23:36', NULL),
(111, 1, 'lock-660x330', 3, 'image/jpeg', 50378, 'news/lock-660x330.jpg', '[]', '2019-11-02 04:23:37', '2019-11-02 04:23:37', NULL),
(112, 1, 'nmnkzkiyqsygikfjinsb-20140717-212636-3-660x330', 3, 'image/jpeg', 64544, 'news/nmnkzkiyqsygikfjinsb-20140717-212636-3-660x330.jpg', '[]', '2019-11-02 04:23:37', '2019-11-02 04:23:37', NULL),
(113, 1, 'old-car-660x330', 3, 'image/jpeg', 70127, 'news/old-car-660x330.jpg', '[]', '2019-11-02 04:23:38', '2019-11-02 04:23:38', NULL),
(114, 1, '300x250', 6, 'image/jpeg', 30686, 'ads/300x250.jpg', '[]', '2019-11-03 17:28:47', '2019-11-03 17:28:47', NULL),
(115, 1, '728x90', 6, 'image/jpeg', 25788, 'ads/728x90.jpg', '[]', '2019-11-03 17:28:48', '2019-11-03 17:28:48', NULL),
(116, 0, 'avatar.jpg', 0, 'image/jpeg', 123144, 'users/avatar.jpg', '[]', '2019-12-09 04:55:28', '2019-12-09 04:55:28', NULL),
(117, 0, 'avatar.jpg', 0, 'image/jpeg', 90104, 'users/avatar.jpg', '[]', '2020-01-15 19:18:04', '2020-01-15 19:18:04', NULL),
(118, 1, 'facebook', 5, 'image/png', 12425, 'logo/facebook.png', '[]', '2020-03-10 18:59:27', '2020-03-10 18:59:27', NULL),
(120, 1, '1476890029-hero01-540x360', 0, 'image/jpeg', 58228, '1476890029-hero01-540x360.jpg', '[]', '2020-04-23 00:06:44', '2020-04-23 00:06:44', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `media_folders`
--

CREATE TABLE `media_folders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `media_folders`
--

INSERT INTO `media_folders` (`id`, `user_id`, `name`, `slug`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 'news', 'news', 0, '2019-09-13 17:09:12', '2019-09-13 17:09:12', NULL),
(4, 1, 'galleries', 'galleries', 0, '2019-09-13 17:14:34', '2019-09-13 17:14:34', NULL),
(5, 1, 'logo', 'logo', 0, '2019-09-13 17:15:52', '2019-09-13 17:15:52', NULL),
(6, 1, 'ads', 'ads', 0, '2019-11-03 17:28:35', '2019-11-03 17:28:35', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `media_settings`
--

CREATE TABLE `media_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `last_name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_id` int(10) UNSIGNED DEFAULT NULL,
  `email_verify_token` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `members`
--

INSERT INTO `members` (`id`, `first_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `dob`, `phone`, `confirmed_at`, `last_name`, `description`, `gender`, `avatar_id`, `email_verify_token`) VALUES
(1, 'Dũng Gia', 'luongngocdung.vina@gmail.com', '$2y$10$BzpbjaHnjWJnvETNv0bQpu5c6z9UNiCajVhs0sT7tP3w5/EVoOJqW', NULL, '2020-04-24 01:51:51', '2020-04-24 01:51:51', NULL, NULL, NULL, 'Lương', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `member_activity_logs`
--

CREATE TABLE `member_activity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `member_password_resets`
--

CREATE TABLE `member_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Main menu', 'main-menu', 'published', '2016-06-17 17:53:45', '2016-08-14 20:25:34'),
(3, 'Top Right Menu', 'right-menu', 'published', '2016-08-03 03:20:10', '2016-09-27 08:30:46'),
(6, 'Social', 'social', 'published', '2016-10-19 23:26:54', '2016-10-19 23:26:54'),
(7, 'Favorite website', 'favorite-website', 'published', '2016-10-21 10:21:23', '2016-10-21 10:21:23'),
(8, 'My links', 'my-links', 'published', '2016-10-21 10:24:36', '2016-10-21 10:24:36'),
(9, 'Featured Categories', 'featured-categories', 'published', '2016-10-21 10:52:59', '2016-10-21 10:52:59'),
(10, 'Menu chính', 'menu-chinh', 'published', '2016-11-15 00:56:14', '2016-11-15 00:56:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_locations`
--

CREATE TABLE `menu_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_locations`
--

INSERT INTO `menu_locations` (`id`, `menu_id`, `location`, `created_at`, `updated_at`) VALUES
(1, 1, 'main-menu', '2018-11-29 09:19:48', '2018-11-29 09:19:48'),
(2, 10, 'main-menu', '2018-11-29 09:19:55', '2018-11-29 09:19:55'),
(3, 6, 'header-menu', '2018-11-29 09:20:42', '2018-11-29 09:20:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_nodes`
--

CREATE TABLE `menu_nodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `reference_id` int(10) UNSIGNED DEFAULT 0,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_font` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_class` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `has_child` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_nodes`
--

INSERT INTO `menu_nodes` (`id`, `menu_id`, `parent_id`, `reference_id`, `reference_type`, `url`, `icon_font`, `position`, `title`, `css_class`, `target`, `has_child`, `created_at`, `updated_at`) VALUES
(543, 3, 0, 5, 'Botble\\Page\\Models\\Page', '', '', 4, '', '', '_self', 0, '2016-09-27 08:40:29', '2016-11-06 18:35:31'),
(544, 3, 0, 1, 'Botble\\Page\\Models\\Page', '', '', 5, 'Contact', '', '_self', 0, '2016-09-27 08:40:29', '2016-10-15 18:45:57'),
(545, 1, 0, 0, NULL, '', '', 0, 'Home', '', '_self', 0, '2016-09-27 09:16:52', '2016-09-27 09:27:13'),
(557, 3, 0, 7, 'Botble\\Blog\\Models\\Category', '', '', 3, 'Projects', '', '_self', 0, '2016-09-27 09:31:43', '2016-10-15 18:45:57'),
(558, 3, 0, 8, 'Botble\\Blog\\Models\\Category', '', '', 2, 'Portfolio', '', '_self', 0, '2016-09-27 09:32:22', '2016-10-15 18:45:57'),
(559, 3, 0, 0, NULL, '/downloads', '', 1, 'Downloads', '', '_self', 0, '2016-09-30 21:32:32', '2016-10-15 18:45:57'),
(560, 3, 0, 0, NULL, '/galleries', '', 0, 'Galleries', '', '_self', 0, '2016-10-15 18:45:57', '2016-10-15 18:45:57'),
(562, 1, 561, 6, 'Botble\\Blog\\Models\\Category', '', '', 0, 'Events', '', '_self', 0, '2016-10-19 08:09:16', '2016-10-19 08:09:16'),
(563, 1, 566, 8, 'Botble\\Blog\\Models\\Category', '', '', 0, 'Portfolio', '', '_self', 0, '2016-10-19 08:09:16', '2016-10-21 10:26:25'),
(564, 1, 566, 7, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Projects', '', '_self', 0, '2016-10-19 08:09:16', '2016-10-21 10:26:25'),
(565, 1, 561, 10, 'Botble\\Blog\\Models\\Category', '', '', 2, 'Resources', '', '_self', 0, '2016-10-19 08:09:16', '2016-10-19 08:10:27'),
(566, 1, 0, 0, NULL, '/galleries', '', 3, 'Galleries', '', '_self', 0, '2016-10-19 08:09:16', '2017-12-08 19:39:34'),
(567, 1, 561, 9, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Business', '', '_self', 0, '2016-10-19 08:09:50', '2016-10-19 08:09:50'),
(568, 1, 0, 1, 'Botble\\Page\\Models\\Page', '', '', 4, 'Contact', '', '_self', 0, '2016-10-19 08:10:27', '2017-12-08 19:39:34'),
(569, 1, 0, 0, NULL, 'https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182?ref=botble', '', 1, 'Purchase', '', '_blank', 0, '2016-10-19 08:11:43', '2016-11-19 02:26:19'),
(571, 6, 0, 0, NULL, 'https://facebook.com', 'fa fa-facebook', 0, 'Facebook', '', '_self', 0, '2016-10-19 23:28:25', '2016-10-19 23:28:25'),
(572, 6, 0, 0, NULL, 'https://twitter.com', 'fa fa-twitter', 1, 'Twitter', '', '_self', 0, '2016-10-19 23:28:26', '2016-10-19 23:28:26'),
(573, 6, 0, 0, NULL, 'https://plus.google.com', 'fa fa-google-plus', 2, 'Google Plus', '', '_self', 0, '2016-10-19 23:29:24', '2016-10-19 23:29:24'),
(574, 6, 0, 0, NULL, 'https://github.com', 'fa fa-github', 3, 'Github', '', '_self', 0, '2016-10-19 23:29:24', '2016-10-19 23:29:24'),
(575, 7, 0, 0, NULL, 'http://speckyboy.com', '', 0, 'Speckyboy Magazine', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(576, 7, 0, 0, NULL, 'http://tympanus.com', '', 1, 'Tympanus-Codrops', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(577, 7, 0, 0, NULL, 'https://kipalog.com/', '', 2, 'Kipalog Blog', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(578, 7, 0, 0, NULL, 'http://www.sitepoint.com', '', 3, 'SitePoint', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(579, 7, 0, 0, NULL, 'http://www.creativebloq.com/', '', 4, 'CreativeBloq', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(580, 7, 0, 0, NULL, 'http://techtalk.vn', '', 5, 'Techtalk', '', '_self', 0, '2016-10-21 10:24:16', '2016-10-21 10:24:16'),
(581, 8, 0, 0, NULL, '/', '', 0, 'Homepage', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(582, 8, 0, 11, 'Botble\\Blog\\Models\\Category', '', '', 1, 'New & Updates', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(583, 8, 0, 0, NULL, '/galleries', '', 2, 'Galleries', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(584, 8, 0, 1, 'Botble\\Page\\Models\\Page', '', '', 3, 'Contact', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(585, 8, 0, 11, 'Botble\\Blog\\Models\\Category', '', '', 4, 'New & Updates', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(586, 8, 0, 7, 'Botble\\Blog\\Models\\Category', '', '', 5, 'Projects', '', '_self', 0, '2016-10-21 10:25:49', '2016-10-21 10:25:49'),
(587, 9, 0, 9, 'Botble\\Blog\\Models\\Category', '', '', 0, 'Business', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(588, 9, 0, 6, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Events', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(589, 9, 0, 11, 'Botble\\Blog\\Models\\Category', '', '', 2, 'New & Updates', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(590, 9, 0, 8, 'Botble\\Blog\\Models\\Category', '', '', 3, 'Portfolio', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(591, 9, 0, 7, 'Botble\\Blog\\Models\\Category', '', '', 4, 'Projects', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(592, 9, 0, 10, 'Botble\\Blog\\Models\\Category', '', '', 5, 'Resources', '', '_self', 0, '2016-10-21 10:53:16', '2016-10-21 10:53:16'),
(594, 10, 602, 17, 'Botble\\Page\\Models\\Page', '', '', 1, 'Liên hệ', '', '_self', 0, '2016-11-15 01:12:57', '2020-04-22 10:14:58'),
(600, 10, 0, 0, NULL, 'http://video4k.vnn:811/vi', '', 0, 'Trang chủ', 'megamenu1', '_self', 1, '2016-11-19 02:26:19', '2020-04-22 10:14:58'),
(601, 10, 600, 0, NULL, 'https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182?ref=botble', '', 0, 'Mua ngay', '', '_blank', 0, '2016-11-19 02:26:19', '2020-04-22 10:14:58'),
(602, 10, 600, 11, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Tin tức', '', '_self', 1, '2016-11-19 02:26:19', '2020-04-22 10:14:58'),
(603, 10, 602, 6, 'Botble\\Blog\\Models\\Category', '', '', 0, 'Sự kiện', '', '_self', 0, '2016-11-19 02:26:19', '2016-11-25 07:52:21'),
(604, 10, 602, 9, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Doanh nghiệp', '', '_self', 0, '2016-11-19 02:26:19', '2016-11-25 07:52:21'),
(605, 10, 602, 10, 'Botble\\Blog\\Models\\Category', '', '', 2, 'Tài nguyên', '', '_self', 0, '2016-11-19 02:26:19', '2016-11-25 07:52:21'),
(606, 10, 602, 0, NULL, 'http://video4k.vnn:811/vi/galleries', '', 0, 'Thư viện ảnh', '', '_self', 0, '2016-11-19 02:26:19', '2020-04-22 10:14:58'),
(607, 10, 606, 8, 'Botble\\Blog\\Models\\Category', '', '', 0, 'Cá nhân', '', '_self', 0, '2016-11-19 02:26:19', '2016-11-25 07:52:21'),
(608, 10, 606, 7, 'Botble\\Blog\\Models\\Category', '', '', 1, 'Dự án', '', '_self', 0, '2016-11-19 02:26:19', '2016-11-25 07:52:21'),
(609, 1, 0, 11, 'Botble\\Blog\\Models\\Category', '', '', 2, 'New & Updates', '', '_self', 0, '2017-12-08 19:39:34', '2018-01-17 18:35:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `meta_boxes`
--

CREATE TABLE `meta_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `meta_boxes`
--

INSERT INTO `meta_boxes` (`id`, `reference_id`, `meta_key`, `meta_value`, `reference_type`, `created_at`, `updated_at`) VALUES
(1, 4, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2017-12-11 09:07:56', '2017-12-11 09:07:56'),
(2, 1, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Page\\Models\\Page', '2018-01-17 18:35:24', '2019-03-11 19:30:22'),
(3, 12, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:02:12', '2018-04-13 09:02:12'),
(4, 13, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:04:30', '2018-04-13 09:04:30'),
(5, 14, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:04:49', '2018-04-13 09:04:49'),
(6, 15, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:05:06', '2018-04-13 09:05:06'),
(7, 16, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:05:23', '2018-04-13 09:05:23'),
(8, 17, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:06:44', '2018-04-13 09:06:44'),
(9, 18, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Category', '2018-04-13 09:08:01', '2018-04-13 09:08:01'),
(10, 44, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 09:38:58', '2018-04-13 09:38:58'),
(11, 48, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 09:40:11', '2018-04-13 09:40:11'),
(12, 49, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 09:41:28', '2018-04-13 09:41:28'),
(13, 7, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:41:28', '2018-04-13 09:41:28'),
(14, 50, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 09:42:20', '2018-04-13 09:42:20'),
(15, 8, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:42:20', '2018-04-13 09:42:20'),
(16, 9, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:42:20', '2018-04-13 09:42:20'),
(17, 51, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 09:43:09', '2018-04-13 09:43:09'),
(18, 10, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:43:09', '2018-04-13 09:43:09'),
(19, 11, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:45:22', '2018-04-13 09:45:22'),
(22, 24, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:59:52', '2018-04-13 09:59:52'),
(23, 25, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2018-04-13 10:00:35', '2018-04-13 10:00:35'),
(24, 52, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 10:02:20', '2018-04-13 10:02:20'),
(25, 53, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 10:02:59', '2018-04-13 10:02:59'),
(26, 54, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 10:03:46', '2018-04-13 10:03:46'),
(27, 55, 'seo_meta', '[{\"seo_title\":null,\"seo_keyword\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2018-04-13 10:06:40', '2018-04-13 10:06:40'),
(28, 17, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Page\\Models\\Page', '2020-03-10 19:33:16', '2020-03-10 19:33:16'),
(29, 1, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:20:19', '2020-04-13 06:20:19'),
(30, 2, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:23:22', '2020-04-13 06:23:22'),
(31, 3, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:23:41', '2020-04-13 06:23:41'),
(32, 1, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-13 08:10:03', '2020-04-13 08:10:03'),
(33, 2, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:20', '2020-04-13 08:14:20'),
(34, 3, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:30', '2020-04-13 08:14:30'),
(35, 4, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:39', '2020-04-13 08:14:39'),
(37, 5, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 09:35:25', '2020-04-13 09:35:25'),
(38, 6, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 09:35:38', '2020-04-13 09:35:38'),
(39, 7, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProCategory', '2020-04-13 10:01:19', '2020-04-13 10:01:19'),
(40, 56, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Post', '2020-04-22 08:12:17', '2020-04-22 08:12:17'),
(41, 26, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Blog\\Models\\Tag', '2020-04-22 08:12:17', '2020-04-22 08:12:17'),
(42, 1, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:39:27', '2020-04-22 11:39:27'),
(43, 2, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(44, 3, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(45, 4, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(46, 5, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:51:03', '2020-04-22 11:51:03'),
(48, 6, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:02', '2020-04-24 08:27:02'),
(49, 7, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:13', '2020-04-24 08:27:13'),
(50, 8, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:23', '2020-04-24 08:27:23'),
(51, 9, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:34', '2020-04-24 08:27:34'),
(52, 10, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:19', '2020-04-24 09:27:19'),
(53, 11, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:27', '2020-04-24 09:27:27'),
(54, 12, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:33', '2020-04-24 09:27:33'),
(55, 13, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:40', '2020-04-24 09:27:40'),
(56, 14, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:48', '2020-04-24 09:27:48'),
(57, 15, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:54', '2020-04-24 09:27:54'),
(58, 16, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Size', '2020-04-24 09:28:01', '2020-04-24 09:28:01'),
(59, 5, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-25 11:29:13', '2020-04-25 11:29:13'),
(60, 1, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Features', '2020-04-25 12:24:42', '2020-04-25 12:24:42'),
(61, 2, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Features', '2020-04-25 12:24:48', '2020-04-25 12:24:48'),
(62, 6, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\Product', '2020-04-26 01:57:08', '2020-04-26 01:57:08'),
(63, 6, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(64, 7, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(65, 8, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(66, 9, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(67, 10, 'seo_meta', '[{\"seo_title\":null,\"seo_description\":null}]', 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_04_09_032329_create_base_tables', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(7, '2016_06_01_000004_create_oauth_clients_table', 1),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2016_06_10_230148_create_acl_tables', 1),
(10, '2016_06_14_230857_create_menus_table', 1),
(11, '2016_06_28_221418_create_pages_table', 1),
(12, '2016_10_05_074239_create_setting_table', 1),
(13, '2016_11_28_032840_create_dashboard_widget_tables', 1),
(14, '2016_12_16_084601_create_widgets_table', 1),
(15, '2017_05_09_070343_create_media_tables', 1),
(16, '2017_11_03_070450_create_slug_table', 1),
(17, '2018_11_03_054815_create_menu_locations_table', 1),
(18, '2018_11_07_151241_update_column_status_for_pages_table', 1),
(19, '2018_11_07_151701_update_column_status_for_menus_table', 1),
(21, '2015_06_29_025744_create_audit_history', 2),
(24, '2015_06_18_033822_create_blog_table', 4),
(25, '2018_11_07_051340_change_status_column_for_blog', 4),
(26, '2018_11_07_154204_remove_unused_column_in_blog_table', 4),
(28, '2016_06_17_091537_create_contacts_table', 5),
(29, '2018_11_08_001804_update_column_status_in_contacts_table', 5),
(32, '2016_10_03_032336_create_languages_table', 7),
(33, '2016_10_13_150201_create_galleries_table', 8),
(34, '2018_11_08_005616_update_status_column_in_galleries_table', 8),
(36, '2017_10_04_140938_create_member_table', 9),
(38, '2016_05_28_112028_create_system_request_logs_table', 11),
(40, '2018_11_24_012701_rename_column_featured_in_blog_tables', 12),
(41, '2018_11_24_012823_rename_column_featured_in_galleries_table', 12),
(42, '2018_11_24_013406_rename_column_featured_in_pages_table', 12),
(43, '2013_04_09_062329_create_revisions_table', 13),
(44, '2019_01_05_053554_create_jobs_table', 13),
(45, '2019_01_18_155032_update_members_table', 13),
(46, '2019_01_20_071422_add_author_type_to_posts_table', 13),
(47, '2019_02_11_055127_create_member_activity_logs_table', 13),
(48, '2019_02_23_064533_create_contact_replies_table', 13),
(49, '2017_02_13_034601_create_blocks_table', 14),
(50, '2018_11_07_150746_update_column_status_for_blocks_table', 14),
(51, '2017_03_27_150646_re_create_custom_field_tables', 15),
(52, '2018_11_08_004530_update_status_column_in_field_groups_table', 15),
(53, '2019_03_14_082019_update_blog_table_name', 16),
(54, '2019_04_05_112137_update_table_members', 17),
(55, '2019_04_06_103857_update_profile_image_column_in_users_table', 17),
(56, '2019_05_27_133842_change_column_status_menus_table', 18),
(57, '2019_05_27_134005_change_column_status_pages_table', 18),
(58, '2019_05_27_134310_change_column_status_blocks_table', 18),
(59, '2019_05_27_134410_change_column_status_posts_table', 18),
(60, '2019_05_27_134446_change_column_status_categories_table', 18),
(61, '2019_05_27_134633_change_column_status_field_groups_table', 18),
(62, '2019_05_27_134724_change_column_status_tags_table', 18),
(63, '2019_05_27_134802_change_column_status_galleries_table', 18),
(64, '2019_06_02_121721_add_email_verify_token_column_to_members_table', 19),
(65, '2019_06_24_025057_make_some_columns_nullable_in_media_tables', 19),
(66, '2019_07_11_143550_rename_audit_history_table', 20),
(67, '2019_07_15_042406_change_site_title_from_settings_to_theme_options', 20),
(68, '2019_08_13_033145_remove_unused_columns_in_users_table', 21),
(69, '2019_09_07_030654_update_menu_nodes_table', 21),
(70, '2019_09_07_035526_update_menu_node_reference_type_for_category', 21),
(71, '2019_09_07_045041_update_slugs_table', 21),
(72, '2019_09_07_050058_update_slug_reference_for_blog', 21),
(73, '2019_09_07_050253_update_slug_reference_for_gallery', 21),
(74, '2019_09_07_050405_update_slug_reference_for_page', 21),
(75, '2019_09_07_154718_update_lang_meta_table', 21),
(76, '2019_09_07_155256_update_language_meta_for_block', 21),
(77, '2019_09_07_155716_update_language_meta_for_blog', 21),
(78, '2019_09_07_155917_update_language_meta_for_gallery', 21),
(79, '2019_09_08_014859_update_meta_boxes_table', 21),
(80, '2019_09_08_015552_update_meta_box_data_for_blog', 21),
(81, '2019_09_08_015629_update_meta_box_data_for_page', 21),
(82, '2019_09_08_032534_update_gallery_meta_table', 21),
(83, '2019_09_12_073711_update_media_url', 22),
(84, '2019_10_20_002256_remove_parent_id_in_pages_table', 23),
(85, '2019_10_20_002342_remove_parent_id_in_tags_table', 23),
(86, '2019_08_19_000000_create_failed_jobs_table', 24),
(87, '2019_09_12_073711_update_media_url_for_current_data', 24),
(88, '2020_03_28_020724_make_column_user_id_nullable_table_revisions', 25),
(89, '2016_10_07_193005_create_translations_table', 26),
(90, '2015_06_18_033822_create_product_table', 27),
(92, '2017_07_11_140018_create_simple_slider_table', 28),
(94, '2020_04_26_000002_create_store_product_table', 29);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'nPvD6nCdZ4u5VDofwCaCrg9DTBpkMOJopp2JjdjR', 'http://localhost', 1, 0, 0, '2018-03-20 21:07:20', '2018-03-20 21:07:20'),
(2, NULL, 'Laravel Password Grant Client', 'yoMn1m1jBDOZAAfwX7qOagkGrAoazmzujgLAfpyI', 'http://localhost', 0, 1, 0, '2018-03-20 21:07:20', '2018-03-20 21:07:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-03-20 21:07:20', '2018-03-20 21:07:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('09a880ccdf4f35761565b221378b611954a090fd48a2a710b1531feccced42a104d7208d38508258', '6ec25d436833eb08e9c574e208ce65ee59561517d49f0ac952d292eb80d976bfb11805a6e28f94dc', 0, '2018-04-20 04:08:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderstatuses`
--

CREATE TABLE `orderstatuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `user_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`id`, `name`, `content`, `status`, `user_id`, `image`, `template`, `is_featured`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Contact', '<p>Address: North Link Building, 10 Admiralty Street, 757695 Singapore</p>\r\n\r\n<p>Hotline: 18006268</p>\r\n\r\n<p>Email: contact@botble.com</p>\r\n\r\n<p>[google-map]North Link Building, 10 Admiralty Street, 757695 Singapore[/google-map]</p>\r\n\r\n<p>For the fastest reply, please use the contact form below.</p>\r\n\r\n<p>[contact-form][/contact-form]</p>', 'published', 1, 'https://s3-ap-southeast-1.amazonaws.com/botble/cms/galleries/1476520641-elena-siberian-tigress-4k-1280x720.jpg', 'default', 0, NULL, '2016-07-09 01:05:39', '2020-03-10 19:06:21'),
(17, 'Liên hệ', '<p>Địa chỉ: North Link Building, 10 Admiralty Street, 757695 Singapore</p>\r\n\r\n<p>Đường d&acirc;y n&oacute;ng: 18006268</p>\r\n\r\n<p>Email: contact@botble.com</p>\r\n\r\n<p>[google-map]North Link Building, 10 Admiralty Street, 757695 Singapore[/google-map]</p>\r\n\r\n<p>Để được trả lời nhanh nhất, vui l&ograve;ng li&ecirc;n hệ qua form b&ecirc;n dưới.</p>\r\n\r\n<p>[contact-form][/contact-form]</p>', 'published', 1, 'https://s3-ap-southeast-1.amazonaws.com/botble/cms/galleries/1476513483-misty-mountains-1280x720.jpg', 'default', 0, NULL, '2016-11-06 18:17:25', '2020-03-10 19:33:16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('minhsang2603@gmail.com', '$2y$10$kWX7Vm.TR02TvQ426QLR2uzK6/JjiYIwE.ruTXH6eBQdS8mV5aL0a', '2018-01-24 03:40:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `format_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(4, '13,000+ People Have Bought Our Theme', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p><iframe src=\"https://www.youtube.com/embed/7MxyPHjyu9A\" width=\"420\" height=\"315\"></iframe></p>\r\n<p>Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There’s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n<p> </p>\r\n<p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.</p>\r\n<p>You don’t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going. What?! The Force is strong with this one. I have you now.</p>\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander. There’ll be no one to stop us this time! You’re all clear, kid. Let’s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n<ul>\r\n<li>Dantooine. They’re on Dantooine.</li>\r\n<li>He is here.</li>\r\n<li>Don’t underestimate the Force.</li>\r\n</ul>\r\n<p><img style=\"height: 683px; width: 1024px;\" alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master. But with the blast shield down, I can’t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can’t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n<p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.<br /> You don’t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going. What?! The Force is strong with this one. I have you now.</p>\r\n<p> </p>\r\n<ol>\r\n<li>I care. So, what do you think of her, Han?</li>\r\n<li>You mean it controls your actions?</li>\r\n<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going.</li>\r\n<li>I’m trying not to, kid.</li>\r\n</ol>\r\n<h2>Revenge of the Sith</h2>\r\n<p><img style=\"height: 300px; width: 180px;\" alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can’t get involved! I’ve got work to do! It’s not that I like the Empire, I hate it, but there’s nothing I can do about it right now. It’s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I’m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can’t possibly…</p>\r\n<p>Your eyes can deceive you. Don’t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I’m trying not to, kid.</p>\r\n<p>I’m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They’re on Dantooine.</p>\r\n<p> </p>\r\n<p>Still, she’s got a lot of spirit. I don’t know, what do you think? What!? I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– What good is a reward if you ain’t around to use it? Besides, attacking that battle station ain’t my idea of courage. It’s more like…suicide.</p>\r\n<p>Hey, Luke! May the Force be with you. Kid, I’ve flown from one side of this galaxy to the other. I’ve seen a lot of strange stuff, but I’ve never seen anything to make me believe there’s one all-powerful Force controlling everything. There’s no mystical energy field that controls my destiny. It’s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n<p>You don’t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going. What?! The Force is strong with this one. I have you now.</p>\r\n<p> </p>\r\n<p><img style=\"height: 563px; width: 1000px;\" alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n<p> </p>\r\n<p>Hey, Luke! May the Force be with you. Kid, I’ve flown from one side of this galaxy to the other. I’ve seen a lot of strange stuff, but I’ve never seen anything to make me believe there’s one all-powerful Force controlling everything. There’s no mystical energy field that controls my destiny. It’s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain’t in this for your revolution, and I’m not in it for you, Princess. I expect to be well paid. I’m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n<p>All right. Well, take care of yourself, Han. I guess that’s what you’re best at, ain’t it? Alderaan? I’m not going to Alderaan. I’ve got to go home. It’s late, I’m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n<p> </p>\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img style=\"height: 300px; width: 200px;\" alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n<p>WOW, Nice photo !</p>\r\n<p>I need your help, Luke. She needs your help. I’m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can’t possibly… As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you’re going.</p>\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They’re on Dantooine. You’re all clear, kid. Let’s blow this thing and go home! I’m surprised you had the courage to take the responsibility yourself. I’m trying not to, kid.</p>\r\n<p>I care. So, what do you think of her, Han? Don’t underestimate the Force. I don’t know what you’re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan– I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n<p>You’re all clear, kid. Let’s blow this thing and go home! But with the blast shield down, I can’t even see! How am I supposed to fight? Alderaan? I’m not going to Alderaan. I’ve got to go home. It’s late, I’m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476893533-02.jpg', 8, NULL, '2016-08-02 21:34:18', '2019-09-13 17:03:24'),
(5, 'Top Search Engine Optimization Strategies!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/nmnkzkiyqsygikfjinsb-20140717-212636-3-660x330.jpg', 6, NULL, '2016-08-02 21:36:45', '2019-09-13 17:03:24'),
(6, 'Which Company Would You Choose?', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476893532-01.jpg', 4, NULL, '2016-08-02 21:38:34', '2019-09-13 17:03:25'),
(7, 'Used Car Dealer Sales Tricks Exposed', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/old-car-660x330.jpg', 6, NULL, '2016-08-02 21:39:50', '2019-09-13 17:03:25'),
(8, '20 Ways To Sell Your Product Faster', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890034-hero04.jpg', 3, NULL, '2016-08-02 21:40:56', '2019-09-13 17:03:25');
INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(9, 'The Secrets Of Rich And Famous Writers', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890036-hero05.jpg', 0, NULL, '2016-08-02 21:42:45', '2019-09-13 17:03:25'),
(10, 'Imagine Losing 20 Pounds In 14 Days!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890033-hero03.jpg', 7, NULL, '2016-08-02 21:44:20', '2019-09-13 17:03:25'),
(11, 'Are You Still Using That Slow, Old Typewriter?', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890034-hero04.jpg', 12, NULL, '2016-08-02 21:45:16', '2019-09-13 17:03:25'),
(12, 'A Skin Cream That’s Proven To Work', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890031-hero02.jpg', 12, NULL, '2016-08-02 21:46:22', '2019-09-13 17:03:25'),
(13, '10 Reasons To Start Your Own, Profitable Website!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890029-hero01.jpg', 15, NULL, '2016-08-02 21:47:18', '2019-09-13 17:03:25');
INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(14, 'Simple Ways To Reduce Your Unwanted Wrinkles!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/4381851322-d46fd7d75e-b-660x330.jpg', 1, NULL, '2016-08-02 21:50:39', '2019-09-13 17:03:25'),
(15, 'Apple iMac with Retina 5K display review', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891195-02.jpg', 1, NULL, '2016-08-02 21:51:49', '2019-09-13 17:03:26'),
(16, '10 Reasons To Start Your Own, Profitable Website!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891035-01.jpg', 3, NULL, '2016-08-02 21:52:39', '2019-09-13 17:03:26'),
(17, 'Unlock The Secrets Of Selling High Ticket Items', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891198-04.jpg', 2, NULL, '2016-08-02 21:53:49', '2019-09-13 17:03:26'),
(18, '10,000 Web Site Visitors In One Month:Guaranteed', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476893533-02.jpg', 6, NULL, '2016-08-02 21:54:31', '2019-09-13 17:03:26');
INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(19, 'Are You Still Using That Slow, Old Typewriter?', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891195-02.jpg', 3, NULL, '2016-08-02 21:55:28', '2019-09-13 17:03:26'),
(20, 'A Skin Cream That’s Proven To Work', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476890031-hero02.jpg', 12, NULL, '2016-08-02 21:56:32', '2019-09-13 17:03:26'),
(21, 'Top Search Engine Optimization Strategies!', 'Don’t act so surprised, Your Highness. You weren’t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck.', '<p>Don&rsquo;t act so surprised, Your Highness. You weren&rsquo;t on any mercy mission this time. Several transmissions were beamed to this ship by Rebel spies. I want to know what happened to the plans they sent you. In my experience, there is no such thing as luck. Partially, but it also obeys your commands. I want to come with you to Alderaan. There&rsquo;s nothing for me here now. I want to learn the ways of the Force and be a Jedi, like my father before me. The more you tighten your grip, Tarkin, the more star systems will slip through your fingers.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<blockquote>\r\n<p>We hire people who want to make the best things in the world. -Steve Jobs</p>\r\n</blockquote>\r\n\r\n<p>She must have hidden the plans in the escape pod. Send a detachment down to retrieve them, and see to it personally, Commander.&nbsp;There&rsquo;ll be no one to stop us this time!&nbsp;You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! Partially, but it also obeys your commands.</p>\r\n\r\n<ul>\r\n	<li>Dantooine. They&rsquo;re on Dantooine.</li>\r\n	<li>He is here.</li>\r\n	<li>Don&rsquo;t underestimate the Force.</li>\r\n</ul>\r\n\r\n<p><img alt=\"5825871567_4d477202ce_b\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/5825871567_4d477202ce_b.jpg\" /></p>\r\n\r\n<p>I care. So, what do you think of her, Han? A tremor in the Force. The last time I felt it was in the presence of my old master.&nbsp;But with the blast shield down,&nbsp;I can&rsquo;t even see! How am I supposed to fight? Obi-Wan is here. The Force is with him. But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? You are a part of the Rebel Alliance and a traitor! Take her away!</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.<br />\r\nYou don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>I care. So, what do you think of her, Han?</li>\r\n	<li>You mean it controls your actions?</li>\r\n	<li>Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</li>\r\n	<li>I&rsquo;m trying not to, kid.</li>\r\n</ol>\r\n\r\n<h2>Revenge of the Sith</h2>\r\n\r\n<p><img alt=\"post-image\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-180x300.jpg\" />I can&rsquo;t get involved! I&rsquo;ve got work to do! It&rsquo;s not that I like the Empire, I hate it, but there&rsquo;s nothing I can do about it right now. It&rsquo;s such a long way from here. Leave that to me. Send a distress signal, and inform the Senate that all on board were killed. I&rsquo;m surprised you had the courage to take the responsibility yourself. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip;</p>\r\n\r\n<p>Your eyes can deceive you. Don&rsquo;t trust them. He is here. What?! Hokey religions and ancient weapons are no match for a good blaster at your side, kid. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I&rsquo;m trying not to, kid. I have traced the Rebel spies to her. Now she is my only link to finding their secret base. He is here. You are a part of the Rebel Alliance and a traitor! Take her away! Dantooine. They&rsquo;re on Dantooine.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Still, she&rsquo;s got a lot of spirit. I don&rsquo;t know, what do you think? What!? I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; What good is a reward if you ain&rsquo;t around to use it? Besides, attacking that battle station ain&rsquo;t my idea of courage. It&rsquo;s more like&hellip;suicide.</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You don&rsquo;t believe in the Force, do you? Obi-Wan is here. The Force is with him. I call it luck. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going. What?! The Force is strong with this one. I have you now.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"9FybtVFNSEOxogGzIvHJ_IMG_2226\" src=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/9FybtVFNSEOxogGzIvHJ_IMG_2226.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hey, Luke! May the Force be with you. Kid, I&rsquo;ve flown from one side of this galaxy to the other. I&rsquo;ve seen a lot of strange stuff, but I&rsquo;ve never seen anything to make me believe there&rsquo;s one all-powerful Force controlling everything. There&rsquo;s no mystical energy field that controls my destiny. It&rsquo;s all a lot of simple tricks and nonsense. Remember, a Jedi can feel the Force flowing through him. He is here. Ye-ha! I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>Oh God, my uncle. How am I ever gonna explain this? Look, I ain&rsquo;t in this for your revolution, and I&rsquo;m not in it for you, Princess. I expect to be well paid. I&rsquo;m in it for the money. A tremor in the Force. The last time I felt it was in the presence of my old master.</p>\r\n\r\n<p>All right. Well, take care of yourself, Han.&nbsp;I guess that&rsquo;s what you&rsquo;re best at,&nbsp;ain&rsquo;t it? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is. The plans you refer to will soon be back in our hands.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href=\"http://themes.tielabs.com/wp-content/uploads/sites/3/2015/01/post-image-2.jpg\"><img alt=\"WOW, Nice photo !\" src=\"http://themes.tielabs.com/sahifa5/wp-content/uploads/sites/3/2015/01/post-image-2-200x300.jpg\" /></a></p>\r\n\r\n<p>WOW, Nice photo !</p>\r\n\r\n<p>I need your help, Luke. She needs your help. I&rsquo;m getting too old for this sort of thing. Oh God, my uncle. How am I ever gonna explain this? Hey, Luke! May the Force be with you. No! Alderaan is peaceful. We have no weapons. You can&rsquo;t possibly&hellip; As you wish. Look, I can take you as far as Anchorhead. You can get a transport there to Mos Eisley or wherever you&rsquo;re going.</p>\r\n\r\n<p>I suggest you try it again, Luke. This time, let go your conscious self and act on instinct. Dantooine. They&rsquo;re on Dantooine. You&rsquo;re all clear, kid. Let&rsquo;s blow this thing and go home! I&rsquo;m surprised you had the courage to take the responsibility yourself. I&rsquo;m trying not to, kid.</p>\r\n\r\n<p>I care. So, what do you think of her, Han? Don&rsquo;t underestimate the Force. I don&rsquo;t know what you&rsquo;re talking about. I am a member of the Imperial Senate on a diplomatic mission to Alderaan&ndash; I have traced the Rebel spies to her. Now she is my only link to finding their secret base.</p>\r\n\r\n<p>You&rsquo;re all clear, kid.&nbsp;Let&rsquo;s blow this thing and go home!&nbsp;But with the blast shield down, I can&rsquo;t even see! How am I supposed to fight? Alderaan? I&rsquo;m not going to Alderaan. I&rsquo;ve got to go home. It&rsquo;s late, I&rsquo;m in for it as it is.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891035-01.jpg', 7, NULL, '2016-08-02 21:57:29', '2019-09-13 17:03:26'),
(44, '13000 người đã mua Theme của chúng tôi', 'Đây là bài viết mẫu, nội dung của những bài viết demo đều giống nhau, nó được dùng với mục đích làm ví dụ, các bài viết hiện tại trên trang demo đều có nội dung giống nhau về phần nội dung và mô tả ngắn.', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minhsang2603 để được hỗ trợ.</p>\r\n<p> </p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476893533-02.jpg', 1, NULL, '2016-11-12 18:25:08', '2019-09-13 17:03:26'),
(45, 'Lên trên Công cụ Tìm kiếm Tối ưu hóa các chiến lược!', 'Đây là bài viết mẫu, nội dung của những bài viết demo đều giống nhau, nó được dùng với mục đích làm ví dụ, các bài viết hiện tại trên trang demo đều có nội dung giống nhau về phần nội dung và mô tả ngắn.', '<p>Hi mọi người, &nbsp;dạo gần đ&acirc;y m&igrave;nh kh&ocirc;ng viết blog mới v&igrave; tập trung ph&aacute;t triển Botble CMS. Sau bao cố gắng th&igrave; h&ocirc;m nay m&igrave;nh đ&atilde; c&oacute; thể release version 2.0 với nhiều t&iacute;nh năng mới.</p>\r\n\r\n<p>Ở bản 1.0, m&igrave;nh cảm thấy n&oacute; c&oacute; qu&aacute; nhiều t&iacute;nh năng phức tạp, chỉ ph&ugrave; hợp với hệ thống lớn m&agrave; đa số c&aacute;c website nhỏ kh&ocirc;ng cần đến, v&igrave; vậy m&igrave;nh đ&atilde; đơn giản bớt đi trong phần n&agrave;y v&agrave; tập trung l&agrave;m sao cho Botble CMS dễ sử dụng nhất.</p>\r\n\r\n<p>Botble CMS đang sử dụng Laravel&nbsp;phi&ecirc;n bản mới nhất tại thời điểm n&agrave;y 5.3.</p>\r\n\r\n<h3>Demo:</h3>\r\n\r\n<p>Homepage:&nbsp;<a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n\r\n<p>Admin:&nbsp;<a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n\r\n<p>Account: botble - 159357</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Cấu tr&uacute;c thư mục:</h3>\r\n\r\n<p><img alt=\"core\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" /></p>\r\n\r\n<p>Botble version 2 đ&atilde; được tổ chức th&agrave;nh từng module theo dạng HMVC v&agrave; nằm ho&agrave;n to&agrave;n trong thư mục /core. Những thư mục kh&aacute;c hầu hết được giữ nguy&ecirc;n hiện trạng như bản c&agrave;i mới laravel. Điều n&agrave;y gi&uacute;p dễ d&agrave;ng hơn trong việc cập nhật, n&acirc;ng cấp sau n&agrave;y.</p>\r\n\r\n<p><img alt=\"theme\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" /></p>\r\n\r\n<p>T&aacute;ch biệt thư mục theme v&agrave; chuẩn cấu tr&uacute;c theme.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&igrave;nh xin n&oacute;i sơ qua một số t&iacute;nh năng mới trong bản n&agrave;y.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<h3>Hỗ trợ c&agrave;i đặt.</h3>\r\n	</li>\r\n</ol>\r\n\r\n<p>Nhiều người gặp kh&oacute; khăn trong việc c&agrave;i đặt Botble, đa số đ&oacute; l&agrave; những người mới l&agrave;m quen laravel. V&igrave; vậy m&igrave;nh đ&atilde; viết 1 đoạn script để hỗ trợ c&agrave;i đặt nhanh ch&oacute;ng qua command line.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>2. Tạo 1 module/plugin mới một c&aacute;ch nhanh ch&oacute;ng với CRUD v&agrave; permissions.</h3>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Việc loại bỏ 1 module kh&ocirc;ng cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Do hơi bận rộn n&ecirc;n m&igrave;nh chỉ c&oacute; thể quay một số video hướng dẫn tr&ecirc;n, sẽ bổ sung sau :D</p>\r\n\r\n<h3>3. Theme mới</h3>\r\n\r\n<p>Trong bản cập nhật n&agrave;y, nhờ sự gi&uacute;p đỡ của bạn&nbsp;<a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đ&atilde; được kho&aacute;c l&ecirc;n bộ &aacute;o mới m&agrave; m&igrave;nh đ&aacute;nh gi&aacute; l&agrave; đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n\r\n<p>Một số h&igrave;nh ảnh:</p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>4. Quản l&yacute; plugin</h3>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n\r\n<p>Như c&aacute;c bạn đ&atilde; biết, Wordpress c&oacute; rất nhiều t&iacute;nh năng hay. V&igrave; vậy m&igrave;nh đ&atilde; &aacute;p dụng Filter, Action hook v&agrave; Metabox v&agrave;o Botble để c&oacute; thể dễ d&agrave;ng hơn cho c&aacute;c bạn can thiệp v&agrave;o core sau n&agrave;y.</p>\r\n\r\n<h3>Kết luận:</h3>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; một số t&iacute;nh năng mới của Botble version 2.0, hi vọng c&aacute;c bạn cảm thấy hứng th&uacute; với CMS n&agrave;y.</p>\r\n\r\n<p>C&aacute;c bạn c&oacute; thể mua ủng hộ m&igrave;nh tr&ecirc;n Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc li&ecirc;n hệ trực tiếp với m&igrave;nh qua skype live:minsang2603 để được hỗ trợ.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/nmnkzkiyqsygikfjinsb-20140717-212636-3-660x330.jpg', 1, NULL, '2016-11-12 18:27:06', '2019-09-13 17:03:27');
INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(46, 'Bạn sẽ chọn công ty nào', 'Đây là bài viết mẫu, nội dung của những bài viết demo đều giống nhau, nó được dùng với mục đích làm ví dụ, các bài viết hiện tại trên trang demo đều có nội dung giống nhau về phần nội dung và mô tả ngắn.', '<p>Hi mọi người, &nbsp;dạo gần đ&acirc;y m&igrave;nh kh&ocirc;ng viết blog mới v&igrave; tập trung ph&aacute;t triển Botble CMS. Sau bao cố gắng th&igrave; h&ocirc;m nay m&igrave;nh đ&atilde; c&oacute; thể release version 2.0 với nhiều t&iacute;nh năng mới.</p>\r\n\r\n<p>Ở bản 1.0, m&igrave;nh cảm thấy n&oacute; c&oacute; qu&aacute; nhiều t&iacute;nh năng phức tạp, chỉ ph&ugrave; hợp với hệ thống lớn m&agrave; đa số c&aacute;c website nhỏ kh&ocirc;ng cần đến, v&igrave; vậy m&igrave;nh đ&atilde; đơn giản bớt đi trong phần n&agrave;y v&agrave; tập trung l&agrave;m sao cho Botble CMS dễ sử dụng nhất.</p>\r\n\r\n<p>Botble CMS đang sử dụng Laravel&nbsp;phi&ecirc;n bản mới nhất tại thời điểm n&agrave;y 5.3.</p>\r\n\r\n<h3>Demo:</h3>\r\n\r\n<p>Homepage:&nbsp;<a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n\r\n<p>Admin:&nbsp;<a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n\r\n<p>Account: botble - 159357</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Cấu tr&uacute;c thư mục:</h3>\r\n\r\n<p><img alt=\"core\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" /></p>\r\n\r\n<p>Botble version 2 đ&atilde; được tổ chức th&agrave;nh từng module theo dạng HMVC v&agrave; nằm ho&agrave;n to&agrave;n trong thư mục /core. Những thư mục kh&aacute;c hầu hết được giữ nguy&ecirc;n hiện trạng như bản c&agrave;i mới laravel. Điều n&agrave;y gi&uacute;p dễ d&agrave;ng hơn trong việc cập nhật, n&acirc;ng cấp sau n&agrave;y.</p>\r\n\r\n<p><img alt=\"theme\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" /></p>\r\n\r\n<p>T&aacute;ch biệt thư mục theme v&agrave; chuẩn cấu tr&uacute;c theme.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&igrave;nh xin n&oacute;i sơ qua một số t&iacute;nh năng mới trong bản n&agrave;y.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<h3>Hỗ trợ c&agrave;i đặt.</h3>\r\n	</li>\r\n</ol>\r\n\r\n<p>Nhiều người gặp kh&oacute; khăn trong việc c&agrave;i đặt Botble, đa số đ&oacute; l&agrave; những người mới l&agrave;m quen laravel. V&igrave; vậy m&igrave;nh đ&atilde; viết 1 đoạn script để hỗ trợ c&agrave;i đặt nhanh ch&oacute;ng qua command line.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>2. Tạo 1 module/plugin mới một c&aacute;ch nhanh ch&oacute;ng với CRUD v&agrave; permissions.</h3>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Việc loại bỏ 1 module kh&ocirc;ng cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Do hơi bận rộn n&ecirc;n m&igrave;nh chỉ c&oacute; thể quay một số video hướng dẫn tr&ecirc;n, sẽ bổ sung sau :D</p>\r\n\r\n<h3>3. Theme mới</h3>\r\n\r\n<p>Trong bản cập nhật n&agrave;y, nhờ sự gi&uacute;p đỡ của bạn&nbsp;<a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đ&atilde; được kho&aacute;c l&ecirc;n bộ &aacute;o mới m&agrave; m&igrave;nh đ&aacute;nh gi&aacute; l&agrave; đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n\r\n<p>Một số h&igrave;nh ảnh:</p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>4. Quản l&yacute; plugin</h3>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n\r\n<p>Như c&aacute;c bạn đ&atilde; biết, Wordpress c&oacute; rất nhiều t&iacute;nh năng hay. V&igrave; vậy m&igrave;nh đ&atilde; &aacute;p dụng Filter, Action hook v&agrave; Metabox v&agrave;o Botble để c&oacute; thể dễ d&agrave;ng hơn cho c&aacute;c bạn can thiệp v&agrave;o core sau n&agrave;y.</p>\r\n\r\n<h3>Kết luận:</h3>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; một số t&iacute;nh năng mới của Botble version 2.0, hi vọng c&aacute;c bạn cảm thấy hứng th&uacute; với CMS n&agrave;y.</p>\r\n\r\n<p>C&aacute;c bạn c&oacute; thể mua ủng hộ m&igrave;nh tr&ecirc;n Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc li&ecirc;n hệ trực tiếp với m&igrave;nh qua skype live:minsang2603 để được hỗ trợ.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476893532-01.jpg', 0, NULL, '2016-11-12 18:32:57', '2019-09-13 17:03:27'),
(47, 'Sử dụng xe Đại lý bán hàng Tricks xúc', 'Đây là bài viết mẫu, nội dung của những bài viết demo đều giống nhau, nó được dùng với mục đích làm ví dụ, các bài viết hiện tại trên trang demo đều có nội dung giống nhau về phần nội dung và mô tả ngắn.', '<p>Hi mọi người, &nbsp;dạo gần đ&acirc;y m&igrave;nh kh&ocirc;ng viết blog mới v&igrave; tập trung ph&aacute;t triển Botble CMS. Sau bao cố gắng th&igrave; h&ocirc;m nay m&igrave;nh đ&atilde; c&oacute; thể release version 2.0 với nhiều t&iacute;nh năng mới.</p>\r\n\r\n<p>Ở bản 1.0, m&igrave;nh cảm thấy n&oacute; c&oacute; qu&aacute; nhiều t&iacute;nh năng phức tạp, chỉ ph&ugrave; hợp với hệ thống lớn m&agrave; đa số c&aacute;c website nhỏ kh&ocirc;ng cần đến, v&igrave; vậy m&igrave;nh đ&atilde; đơn giản bớt đi trong phần n&agrave;y v&agrave; tập trung l&agrave;m sao cho Botble CMS dễ sử dụng nhất.</p>\r\n\r\n<p>Botble CMS đang sử dụng Laravel&nbsp;phi&ecirc;n bản mới nhất tại thời điểm n&agrave;y 5.3.</p>\r\n\r\n<h3>Demo:</h3>\r\n\r\n<p>Homepage:&nbsp;<a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n\r\n<p>Admin:&nbsp;<a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n\r\n<p>Account: botble - 159357</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Cấu tr&uacute;c thư mục:</h3>\r\n\r\n<p><img alt=\"core\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" /></p>\r\n\r\n<p>Botble version 2 đ&atilde; được tổ chức th&agrave;nh từng module theo dạng HMVC v&agrave; nằm ho&agrave;n to&agrave;n trong thư mục /core. Những thư mục kh&aacute;c hầu hết được giữ nguy&ecirc;n hiện trạng như bản c&agrave;i mới laravel. Điều n&agrave;y gi&uacute;p dễ d&agrave;ng hơn trong việc cập nhật, n&acirc;ng cấp sau n&agrave;y.</p>\r\n\r\n<p><img alt=\"theme\" src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" /></p>\r\n\r\n<p>T&aacute;ch biệt thư mục theme v&agrave; chuẩn cấu tr&uacute;c theme.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>M&igrave;nh xin n&oacute;i sơ qua một số t&iacute;nh năng mới trong bản n&agrave;y.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<h3>Hỗ trợ c&agrave;i đặt.</h3>\r\n	</li>\r\n</ol>\r\n\r\n<p>Nhiều người gặp kh&oacute; khăn trong việc c&agrave;i đặt Botble, đa số đ&oacute; l&agrave; những người mới l&agrave;m quen laravel. V&igrave; vậy m&igrave;nh đ&atilde; viết 1 đoạn script để hỗ trợ c&agrave;i đặt nhanh ch&oacute;ng qua command line.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>2. Tạo 1 module/plugin mới một c&aacute;ch nhanh ch&oacute;ng với CRUD v&agrave; permissions.</h3>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Việc loại bỏ 1 module kh&ocirc;ng cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"506\" src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Do hơi bận rộn n&ecirc;n m&igrave;nh chỉ c&oacute; thể quay một số video hướng dẫn tr&ecirc;n, sẽ bổ sung sau :D</p>\r\n\r\n<h3>3. Theme mới</h3>\r\n\r\n<p>Trong bản cập nhật n&agrave;y, nhờ sự gi&uacute;p đỡ của bạn&nbsp;<a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đ&atilde; được kho&aacute;c l&ecirc;n bộ &aacute;o mới m&agrave; m&igrave;nh đ&aacute;nh gi&aacute; l&agrave; đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n\r\n<p>Một số h&igrave;nh ảnh:</p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>4. Quản l&yacute; plugin</h3>\r\n\r\n<p><img alt=\"\" src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n\r\n<p>Như c&aacute;c bạn đ&atilde; biết, Wordpress c&oacute; rất nhiều t&iacute;nh năng hay. V&igrave; vậy m&igrave;nh đ&atilde; &aacute;p dụng Filter, Action hook v&agrave; Metabox v&agrave;o Botble để c&oacute; thể dễ d&agrave;ng hơn cho c&aacute;c bạn can thiệp v&agrave;o core sau n&agrave;y.</p>\r\n\r\n<h3>Kết luận:</h3>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y l&agrave; một số t&iacute;nh năng mới của Botble version 2.0, hi vọng c&aacute;c bạn cảm thấy hứng th&uacute; với CMS n&agrave;y.</p>\r\n\r\n<p>C&aacute;c bạn c&oacute; thể mua ủng hộ m&igrave;nh tr&ecirc;n Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc li&ecirc;n hệ trực tiếp với m&igrave;nh qua skype live:minsang2603 để được hỗ trợ.</p>\r\n', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/old-car-660x330.jpg', 4, NULL, '2016-11-12 18:35:25', '2019-09-13 17:03:27'),
(48, 'Chiến lược phát triển phần mềm', 'Chiến lược phát triển phần mềm', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/lock-660x330.jpg', 0, NULL, '2018-04-13 09:40:11', '2019-09-13 17:03:27'),
(49, 'Nền tảng mã nguồn mở PHP', 'Nền tảng', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/7998125906-4489ed8a2f-b-660x330.jpg', 0, NULL, '2018-04-13 09:41:28', '2019-09-13 17:03:27'),
(50, 'Xây dựng website một cách nhanh chóng', 'Nội dung mô tả', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890033-hero03.jpg', 0, NULL, '2018-04-13 09:42:20', '2019-09-13 17:03:27'),
(51, 'Sản phẩm trí tuệ Việt Nam', 'Mô tả', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'news/1476891198-04.jpg', 0, NULL, '2018-04-13 09:43:09', '2019-09-13 17:03:27');
INSERT INTO `posts` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `image`, `views`, `format_type`, `created_at`, `updated_at`) VALUES
(52, 'Cuộc sống tuổi 30', 'Cuộc sống tuổi 30', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890029-hero01.jpg', 0, NULL, '2018-04-13 10:02:20', '2019-09-13 17:03:27'),
(53, 'Hành trình tìm kiếm sự khác biệt', 'Hành trình tìm kiếm sự khác biệt', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476891195-02.jpg', 0, NULL, '2018-04-13 10:02:59', '2019-09-13 17:03:28'),
(54, 'Thế giới động vật muôn màu', 'Thế giới động vật muôn màu', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/7717834982-bbd7e12b8c-b-660x330.jpg', 0, NULL, '2018-04-13 10:03:46', '2019-09-13 17:03:28'),
(55, 'Đi tìm hồi ức tuổi thơ', 'Đi tìm hồi ức tuổi thơ', '<p>Hi mọi người,  dạo gần đây mình không viết blog mới vì tập trung phát triển Botble CMS. Sau bao cố gắng thì hôm nay mình đã có thể release version 2.0 với nhiều tính năng mới.</p>\r\n<p>Ở bản 1.0, mình cảm thấy nó có quá nhiều tính năng phức tạp, chỉ phù hợp với hệ thống lớn mà đa số các website nhỏ không cần đến, vì vậy mình đã đơn giản bớt đi trong phần này và tập trung làm sao cho Botble CMS dễ sử dụng nhất.</p>\r\n<p>Botble CMS đang sử dụng Laravel phiên bản mới nhất tại thời điểm này 5.3.</p>\r\n<h3>Demo:</h3>\r\n<p>Homepage: <a href=\"http://cms.botble.com/\">http://cms.botble.com</a></p>\r\n<p>Admin: <a href=\"http://cms.botble.com/admin\">http://cms.botble.com/admin</a></p>\r\n<p>Account: botble - 159357</p>\r\n<p> </p>\r\n<h3>Cấu trúc thư mục:</h3>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Core-700x328.png\" alt=\"core\" /></p>\r\n<p>Botble version 2 đã được tổ chức thành từng module theo dạng HMVC và nằm hoàn toàn trong thư mục /core. Những thư mục khác hầu hết được giữ nguyên hiện trạng như bản cài mới laravel. Điều này giúp dễ dàng hơn trong việc cập nhật, nâng cấp sau này.</p>\r\n<p><img src=\"http://sangplus.com/wp-content/uploads/2016/10/Theme-700x326.png\" alt=\"theme\" /></p>\r\n<p>Tách biệt thư mục theme và chuẩn cấu trúc theme.</p>\r\n<p> </p>\r\n<p>Mình xin nói sơ qua một số tính năng mới trong bản này.</p>\r\n<ol>\r\n<li>\r\n<h3>Hỗ trợ cài đặt.</h3>\r\n</li>\r\n</ol>\r\n<p>Nhiều người gặp khó khăn trong việc cài đặt Botble, đa số đó là những người mới làm quen laravel. Vì vậy mình đã viết 1 đoạn script để hỗ trợ cài đặt nhanh chóng qua command line.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/DAZe2V_A0wo?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<h3>2. Tạo 1 module/plugin mới một cách nhanh chóng với CRUD và permissions.</h3>\r\n<p><iframe src=\"https://www.youtube.com/embed/8F4wfrS9svs?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Việc loại bỏ 1 module không cần thiết cũng đơn giản hơn bao giờ hết.</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jmex2G4eC18?feature=oembed\" width=\"900\" height=\"506\" frameborder=\"0\"></iframe></p>\r\n<p> </p>\r\n<p>Do hơi bận rộn nên mình chỉ có thể quay một số video hướng dẫn trên, sẽ bổ sung sau :D</p>\r\n<h3>3. Theme mới</h3>\r\n<p>Trong bản cập nhật này, nhờ sự giúp đỡ của bạn <a href=\"http://nghiadev.com/\">Minh Nghĩa</a>, Botble front page đã được khoác lên bộ áo mới mà mình đánh giá là đẹp hơn rất nhiều so với theme mặc định cũ.</p>\r\n<p>Một số hình ảnh:</p>\r\n<p><img src=\"https://lh3.googleusercontent.com/GuJDOdVnDbOVIneN9Qwvo5G_CkX8lx09S0fqifpEvWODI5vFPk4qdvqHITQFbKbdlUoZhB1FMaiTS4-qG2c0qKNGnOoj0oe5Gh9bv9xLg_wp_mWFysuKj9R-WQEufFXzYMrwQ30HRnvebvJqeGCBXhU5RatFHTBbv-8lwoWXZVXd3G1eUSOPKn1S1dVTsoXf7XxzFzN-Vny8uypcJxT2ZenVA1jDQ4doIPky0LHLra08TnMJZ8zh4lMSm8r5PiWXPCpLhBr4OhFTWjYk4dMgo19rggDGJvFZnvFwl682bD0EBqbhITdR4tAni-CTiprbroUuBo5aKX3MFnKIMyVQcc5H5tNuJgtieDuVDe0ZjePxvQzwbG-Vzoz7cEMOLp7G23fTG3L_w3Czpt2p8TzE8nsa8xYdu5yWIetxoYfWRGMYQCHtINnbglYDKuK1wFnXu41rzOO8p4y7NF5rksoAuNleDD9RB24JR3a6J2zhOnvRL5EWlq3HlZbUdn7cybXn3rnJY9rWqBjubFUz8d-lPtObLi4dHzYxT6KfDMRzwMXcPx_RxZVc-6ZOEn2e051DsccRfb-IvurAzppoqnYlfxFEUYZMq8c5s-GNeRKOwqXFhEs4=w655-h324-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/0S7CR_aFNK7QzUEf8Oo6gnwBU1htFdqYLWmX5VQHMZusY47Xyk_jim18FiAwWlVz4WVtn56AMhlhTqtNDMdBpVi2w8pc4yShEYs2-5gr9qIDPxylbCOKfQqj-zzGS8kR7v5xBBph55FE5YFXWEnDQEYo0udhhoNAY-nDpmIi3t3Ho_BqUf8U0E13WDUf-Iu-UQSrvUfFqdqmVubGBuuSx6xXtnj3RBwUcoFdHkFihdXneylcNppQRnTPg3bMOcbhPaJt3JQNQmM3SJIS40fG4W1Yc9ALpK1N7GYt2sRcczWQTQRJveezh-zoPY-HACeQIViWXR_hktNrILo5o-3lhvlDy1kiB60elO3mWKA6bmHZstJ5pqEuBNR6A0DlpBJ-bbtqZBDqMVrjVoromcvucN3ybY3zUOWonD5g1_5kRfRo9C451BAMZ0W1uH4u5pmr3qNTcoxJJ55K_cT4X8ZFuqIV3DOooFuXOYQiXqPFpEgl--px4vvdQycJyhdtgrxb4FBff1vyMskx00rmA33a5DSqoBxt_HzzXqBWNTw6VXj0CLhXft6FQ-ojvWaxMMlzFqmB93heEtSrAczb8nTRIlT7fb6q-2MlCr3LxCjd9VhkVyzL=w1344-h662-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/mv9qalSsV8wrJjeugYDvhdVF-WEVfsNahKVIVg8CZ6NfN9tisVjGPch-VyQG4Ij4fCG6Z_LY5B-uwX30pI2BXBre3SxsWHF_v2pF-CwNGiB9Z5jj-_zEbqL5p2vGj3YRsUG2fY9TO1eAWZ-Pf3YiuS55cPdpc52cprwtRFpGBZ0NjpDAefNZYBHvabj5H_Rgg29U7R8KVvJj95bA_Dj12kqbxZFZB6otTfyikWskXTQa6Vy5yEqv7-KlU96jrowouyIQsXER0FaedD2X6aHPvA6dFhVmK5lJ9LwVr01Ani16vqjOcDf__FCh6o5iPTkLFPCccLPBhtT4kd2AotQCyopvmH8wyewYLFeG6mNi6bEnh35UfPix500VPTviZkW4UVMy5dIOWHalfdO7VXkbGIq--dsO7H9OPQ3BHLA5MKmjmER4Wg1Mi8vxI8xm7Rx6mTo8BQBNixAuVBPjgVIEjgUKJGTOFeXNrNVL685EuWTMVfhJSepjxjSNUtEjkeHxy03NjWd_8JUgfVFpr1Yer_5qGjg7zAFOB0XDbNn_nhl-CRZW_pRj4GQ_NwumV2ICD0IMPxOj17bxbA8tpf8rqWo9x__pgyWeeuYUMBWrnH1NfozG=w1345-h670-no\" alt=\"\" /></p>\r\n<p><img src=\"https://lh3.googleusercontent.com/lF39CW32xGI1aCvNJU7eZmIKn94wkt9qiWqjqsaO7DR60azWq94_SnVPWG3cx0bDY-oX-vaZKCa_hA1jvjpZ7JTLVPo1HtYobNArbvP6HcrsBn0oTLs7I25ChfLsdlMI-GywLSMgrx4paTLJxEWuiqXBxkRGA7S59BH_CQ1w53H3cPvD34k3Ps9HxvTetDwjx6YiVe0JAMXGLHjtUYKDwaAi4pSxVnTRDwS84KSm9dTyhdU-I014QSP_RA6gHR8krt2Ddlwr9CaPWc2yAe214G1_gN1inUoTMmI6mZr3vY9qdB5JVLodTNEGtfqgpzy8s-wAGHhjI7JPsWbsa0UEOzS3WBpzbfiOR66L1ops7PiTRjvhr9HMS5er0a3P_lQUK8r7hq37lDMk9nflkyOkzcFhSvRGP6TS83TJR4X95tcal0uTP6pYPeC0C41IpomtXRalqIB9dx1tAQfBWlY9WRhZADnwLIkWxm5U8vqEhXBq6RQFhRBiWlInMwOI01bmInLvioAwd0bohHtNd2yP_pU-WeCu2slVxpG-HTRacW14vvpcBJKGy2k0sjCnngI1FFb8X7NHSdQy_etGkaggWJ2mfgAvD8DzSfNvScchUhbkcqq4=w1353-h669-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>4. Quản lý plugin</h3>\r\n<p><img src=\"https://lh3.googleusercontent.com/03ZeATliabVof9wqnXaoI4a3m52QOYhC7bPpAyUihUpthcH2yilmrA45oX4w5PbG_99bDvy9tKPD0iOrDCqEpEMR0xfHxczuBxLISD96inWaZW6fHo9YMeHZm_8ULpZSQxK5bvORy9iQ2FBT6xBGuYPeVjVOzNcO839cC0ZjnaTxQUozTm8SA4Fe938L4kT4xeNAVYMHourqELmoC1tjgIr8oq66Wze_loZuY35kBZlX8iFGoFiC4gkjO_yT16q4wpeqRzP7rWOdztqmZpZ5wrbTSusbfi1Dn2ghIABvS4uqrF7gi_JIL9AlSEhnFTHY7QaDH4Q7N8m3r3n2rZWD43iaRIauOm4mXgP1XpaTzZjVjehXLfgG8BNNIq56KldAlroHbGblojOS_gPHuYCvsxk3mckq6oHvGLh4_HyxYl7JAuG0hYxN0sIimEd8GehVV-EqNnbPWAocxytigYnX4SiMndGOtVet6o2XvrDLMlHN8ASKxcO59IJ8Se7hzaCfcJ6wQpl5kvJE4oITxA4R-5_1ezvnZDPGt7jyPuIv1wGsks2YJ1apYIBMQEOhKdlCDkqQC_hBx0vLhq8zxgNgptZKv2opfPGZ9j6KQPqNL3khGoRv=w1352-h666-no\" alt=\"\" /></p>\r\n<p> </p>\r\n<h3>5. Sử dụng một số concept của Wordpress</h3>\r\n<p>Như các bạn đã biết, Wordpress có rất nhiều tính năng hay. Vì vậy mình đã áp dụng Filter, Action hook và Metabox vào Botble để có thể dễ dàng hơn cho các bạn can thiệp vào core sau này.</p>\r\n<h3>Kết luận:</h3>\r\n<p>Trên đây là một số tính năng mới của Botble version 2.0, hi vọng các bạn cảm thấy hứng thú với CMS này.</p>\r\n<p>Các bạn có thể mua ủng hộ mình trên Codecanyon https://codecanyon.net/item/botble-cms-php-platform-based-on-laravel-framework/16928182 hoặc liên hệ trực tiếp với mình qua skype live:minsang2603 để được hỗ trợ.</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 1, 'news/1476890036-hero05.jpg', 1, NULL, '2018-04-13 10:06:40', '2020-04-24 23:50:46'),
(56, 'test', 'test', '<p>test</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', 0, NULL, '2020-04-23 08:12:00', '2020-04-22 08:37:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post_categories`
--

INSERT INTO `post_categories` (`id`, `category_id`, `post_id`) VALUES
(14, 1, 3),
(37, 1, 1),
(48, 1, 2),
(94, 1, 22),
(162, 8, 22),
(164, 6, 23),
(165, 9, 26),
(166, 7, 27),
(167, 7, 28),
(168, 8, 29),
(171, 7, 30),
(172, 8, 31),
(173, 6, 37),
(175, 8, 38),
(176, 7, 39),
(177, 8, 40),
(179, 9, 41),
(181, 7, 42),
(186, 8, 43),
(252, 11, 10),
(255, 11, 13),
(262, 1, 20),
(263, 1, 21),
(265, 7, 45),
(266, 7, 47),
(267, 6, 46),
(279, 1, 8),
(280, 11, 9),
(287, 6, 6),
(289, 1, 5),
(290, 1, 7),
(295, 8, 17),
(302, 6, 18),
(303, 7, 16),
(306, 8, 15),
(308, 1, 19),
(310, 9, 14),
(313, 11, 12),
(314, 9, 11),
(315, 6, 4),
(316, 16, 44),
(318, 17, 48),
(320, 13, 49),
(322, 15, 50),
(333, 15, 51),
(334, 13, 52),
(336, 13, 53),
(337, 16, 53),
(341, 13, 54),
(342, 16, 55),
(350, 12, 56),
(351, 13, 56),
(352, 16, 56),
(353, 14, 56),
(354, 17, 56),
(355, 15, 56),
(356, 18, 56);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_tags`
--

CREATE TABLE `post_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `post_tags`
--

INSERT INTO `post_tags` (`id`, `tag_id`, `post_id`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 1),
(4, 4, 1),
(5, 1, 1),
(6, 5, 6),
(7, 6, 6),
(8, 5, 8),
(9, 6, 8),
(10, 5, 9),
(11, 6, 9),
(12, 5, 10),
(13, 6, 10),
(14, 5, 11),
(15, 6, 11),
(16, 5, 12),
(17, 6, 12),
(18, 5, 13),
(19, 6, 13),
(20, 5, 14),
(21, 6, 14),
(22, 5, 15),
(23, 6, 15),
(24, 5, 16),
(25, 6, 16),
(26, 5, 17),
(27, 6, 17),
(28, 5, 18),
(29, 6, 18),
(30, 5, 19),
(31, 6, 19),
(32, 5, 20),
(33, 6, 20),
(34, 5, 21),
(35, 6, 21),
(36, 7, 22),
(40, 7, 5),
(41, 7, 7),
(42, 7, 49),
(43, 8, 50),
(44, 9, 50),
(92, 10, 51),
(93, 12, 51),
(94, 16, 51),
(95, 17, 51),
(96, 19, 51),
(97, 23, 51),
(98, 26, 56);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `procategories`
--

CREATE TABLE `procategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `icon` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` tinyint(4) NOT NULL DEFAULT 0,
  `type_menu` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_hot` tinyint(4) NOT NULL DEFAULT 0,
  `is_new` tinyint(4) NOT NULL DEFAULT 0,
  `is_featured` tinyint(4) NOT NULL DEFAULT 0,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `procategories`
--

INSERT INTO `procategories` (`id`, `name`, `parent_id`, `description`, `status`, `author_id`, `author_type`, `icon`, `order`, `type_menu`, `is_hot`, `is_new`, `is_featured`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 'Thằng Cha', 0, 'Quần áo', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 1, 0, 1, '2020-04-13 06:20:19', '2020-04-20 10:07:56'),
(2, 'Thằng con thứ nhất', 1, 'Quần áo', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 0, 0, 0, '2020-04-13 06:22:55', '2020-04-20 10:07:56'),
(3, 'Thằng con thứ hai', 1, 'Quần áo', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 0, 0, 0, '2020-04-13 06:23:26', '2020-04-20 10:07:56'),
(5, 'Thằng cháu thứ nhất', 3, NULL, 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 0, 0, 0, '2020-04-13 09:35:25', '2020-04-20 10:07:56'),
(6, 'thằng cháu thứ hai', 3, NULL, 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 0, 0, 0, '2020-04-13 09:35:38', '2020-04-20 10:07:56'),
(7, 'Thằng Cha thứ 2', 0, 'Quần áo', 'published', 1, 'Botble\\ACL\\Models\\User', NULL, 0, '0', 0, 0, 0, 0, '2020-04-13 10:01:10', '2020-04-20 10:07:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `is_featured` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `imagedl` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricecost` decimal(20,0) DEFAULT NULL,
  `pricesell` decimal(20,0) NOT NULL,
  `pricetime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amound` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pricesale` decimal(20,0) DEFAULT NULL,
  `price_sale_start` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_sale_end` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `currency_id` int(10) UNSIGNED DEFAULT NULL,
  `format_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `content`, `status`, `author_id`, `author_type`, `is_featured`, `imagedl`, `images`, `pricecost`, `pricesell`, `pricetime`, `amound`, `option1`, `video`, `pricesale`, `price_sale_start`, `price_sale_end`, `views`, `currency_id`, `format_type`, `created_at`, `updated_at`) VALUES
(1, 'Sản phẩm 1', 'sdsds3434', '<p>dsdsd34343</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', '[]', '20000', '203434', '2020-03-31', '33434', '[]', 'hải dương434343', '343434343', '2020-04-23', '2020-04-25', 0, NULL, NULL, '2020-04-13 08:10:03', '2020-04-13 08:11:43'),
(2, 'Sản phẩm 1(Duplicate)', 'sdsds3434', '<p>dsdsd34343</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', '[]', '20000', '203434', '2020-03-31', '33434', '[]', 'hải dương434343', '343434343', '2020-04-23', '2020-04-25', 0, NULL, NULL, '2020-04-13 08:14:09', '2020-04-13 08:14:20'),
(3, 'Sản phẩm 1(Duplicate)(Duplicate)', 'sdsds3434', '<p>dsdsd34343</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', '[]', '20000', '203434', '2020-03-31', '33434', '[]', 'hải dương434343', '343434343', '2020-04-23', '2020-04-25', 2, NULL, NULL, '2020-04-13 08:14:24', '2020-04-26 00:23:19'),
(4, '2', 'sdsds3434', '<p>dsdsd34343</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', '[]', '20000', '203434', '2020-03-31', '33434', '[]', 'hải dương434343', '343434343', '2020-04-23', '2020-04-25', 2, NULL, NULL, '2020-04-13 08:14:34', '2020-04-25 11:28:37'),
(5, '1', 'sdsds3434', '<p>dsdsd34343</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, 'users/avatar.jpg', '[]', '20000', '203434', '2020-03-31', '33434', '[]', 'hải dương434343', '343434343', '2020-04-23', '2020-04-25', 0, NULL, NULL, '2020-04-25 11:28:48', '2020-04-25 11:29:13'),
(6, 'Sản phẩm 1', 'Sản phẩm 1', '<p>Sản phẩm 1</p>', 'published', 1, 'Botble\\ACL\\Models\\User', 0, NULL, '[\"galleries\\/1476521053-volkswagen-id-paris-motor-show-4k-1280x720.jpg\",\"galleries\\/1476520641-elena-siberian-tigress-4k-1280x720.jpg\",\"galleries\\/1476520418-supergirl-season-2-1280x720.jpg\",\"galleries\\/1476513493-world-of-tanks-football-event-1280x720.jpg\",\"galleries\\/1476513490-tulips-4k-8k-768x1280.jpg\",\"galleries\\/1476513488-spectacular-sunrise-4k-1280x720.jpg\",\"galleries\\/1476513486-power-rangers-red-ranger-4k-1280x720.jpg\",\"galleries\\/1476513484-power-rangers-blue-ranger-4k-1280x720.jpg\",\"galleries\\/1476513483-misty-mountains-1280x720.jpg\",\"galleries\\/1476513478-bmw-x2-paris-auto-show-2016-4k-1280x720.jpg\"]', '25000343443434', '500000434343434', '2020-05-09', NULL, '[\"rgb(190, 77, 97)\",\"rgb(144, 17, 39)\",\"rgb(103, 69, 75)\",\"rgb(105, 26, 40)\"]', 'Sản phẩm 1', NULL, NULL, NULL, 3, 5, NULL, '2020-04-26 01:57:08', '2020-04-26 07:52:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_carts`
--

CREATE TABLE `product_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `carts_id` int(10) UNSIGNED DEFAULT NULL,
  `carts_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carts_amound` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `procategory_id` int(10) UNSIGNED DEFAULT NULL,
  `pro_category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_categories`
--

INSERT INTO `product_categories` (`id`, `procategory_id`, `pro_category_id`, `product_id`) VALUES
(3, NULL, 1, 1),
(4, NULL, 2, 1),
(5, NULL, 1, 2),
(26, NULL, 1, 4),
(28, NULL, 1, 5),
(31, NULL, 1, 3),
(35, NULL, 1, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_colors`
--

CREATE TABLE `product_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `colors_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_colors`
--

INSERT INTO `product_colors` (`id`, `colors_id`, `product_id`) VALUES
(2, 6, 4),
(3, 7, 4),
(5, 9, 4),
(6, 6, 3),
(7, 7, 3),
(8, 8, 3),
(9, 9, 3),
(10, 6, 6),
(11, 9, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_features`
--

CREATE TABLE `product_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `features_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_features`
--

INSERT INTO `product_features` (`id`, `features_id`, `product_id`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 1, 6),
(4, 2, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_orderstatuses`
--

CREATE TABLE `product_orderstatuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderstatuses_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_payments`
--

CREATE TABLE `product_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `payments_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_shippings`
--

CREATE TABLE `product_shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `shippings_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `sizes_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `sizes_id`, `product_id`) VALUES
(6, 10, 4),
(7, 12, 4),
(8, 15, 4),
(9, 10, 3),
(10, 11, 3),
(11, 12, 3),
(12, 13, 3),
(14, 15, 3),
(15, 16, 3),
(16, 10, 6),
(17, 12, 6),
(18, 16, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_stores`
--

CREATE TABLE `product_stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `stores_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_tags`
--

CREATE TABLE `product_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `protag_id` int(10) UNSIGNED DEFAULT NULL,
  `pro_tag_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_tags`
--

INSERT INTO `product_tags` (`id`, `protag_id`, `pro_tag_id`, `product_id`) VALUES
(3, NULL, 1, 4),
(4, NULL, 2, 4),
(5, NULL, 3, 4),
(6, NULL, 4, 4),
(7, NULL, 6, 6),
(8, NULL, 7, 6),
(9, NULL, 8, 6),
(10, NULL, 9, 6),
(11, NULL, 10, 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `protags`
--

CREATE TABLE `protags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `protags`
--

INSERT INTO `protags` (`id`, `name`, `author_id`, `author_type`, `description`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'test', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 'published', '2020-04-22 11:39:27', '2020-04-22 11:39:27'),
(2, 'product tag 2', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(3, 'tag 3', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(4, 'tag 4', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-22 11:50:35', '2020-04-22 11:50:35'),
(5, 'thử nghiệm', 1, 'Botble\\ACL\\Models\\User', NULL, 0, 'published', '2020-04-22 11:51:03', '2020-04-22 11:51:03'),
(6, 'fgfgf', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(7, 'trtrt', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(8, 'ưewe', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(9, 'xâx', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-26 01:57:09', '2020-04-26 01:57:09'),
(10, 'gfgfg', 1, 'Botble\\ACL\\Models\\User', '', 0, 'published', '2020-04-26 01:57:09', '2020-04-26 01:57:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `request_logs`
--

CREATE TABLE `request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `request_logs`
--

INSERT INTO `request_logs` (`id`, `status_code`, `url`, `count`, `user_id`, `referrer`, `created_at`, `updated_at`) VALUES
(1, 404, 'http://video4k.vnn:811/themes/coolbaby/screenshot.png', 1, NULL, NULL, '2020-04-13 05:22:25', '2020-04-13 05:22:25'),
(2, 404, 'http://video4k.vnn:811/listing.html', 2, '[1]', NULL, '2020-04-13 07:30:51', '2020-04-23 00:19:23'),
(3, 404, 'http://video4k.vnn:811/themes/babyshop/screenshot.png', 1, NULL, NULL, '2020-04-13 07:35:57', '2020-04-13 07:35:57'),
(4, 404, 'http://video4k.vnn:811/img/shop/shop_items01.jpg', 10, NULL, NULL, '2020-04-13 07:36:09', '2020-04-13 08:26:37'),
(5, 404, 'http://video4k.vnn:811/users/avatar.jpg', 12, NULL, NULL, '2020-04-13 08:17:16', '2020-04-26 01:46:22'),
(6, 404, 'http://video4k.vnn:811/_ajax_view-product.html?_=1587575187534', 1, '[1]', NULL, '2020-04-22 10:06:35', '2020-04-22 10:06:35'),
(7, 404, 'http://video4k.vnn:811/_ajax_view-product.html?_=1587575187535', 1, '[1]', NULL, '2020-04-22 10:06:39', '2020-04-22 10:06:39'),
(8, 404, 'http://video4k.vnn:811/_ajax_view-product.html?_=1587575187536', 1, '[1]', NULL, '2020-04-22 10:06:40', '2020-04-22 10:06:40'),
(9, 404, 'http://video4k.vnn:811/_ajax_view-product.html?_=1587575187537', 1, '[1]', NULL, '2020-04-22 10:06:45', '2020-04-22 10:06:45'),
(10, 404, 'http://video4k.vnn:811/h%E1%BA%A3i%20d%C6%B0%C6%A1ng434343', 9, '[1]', NULL, '2020-04-22 12:13:56', '2020-04-26 01:46:22'),
(11, 404, 'http://video4k.vnn:811/api/verify_license', 9, NULL, NULL, '2020-04-23 12:04:11', '2020-04-23 12:33:24'),
(12, 404, 'http://video4k.vnn:811/api/deactivate_license', 5, NULL, NULL, '2020-04-23 12:04:22', '2020-04-23 12:44:54'),
(13, 404, 'http://video4k.vnn:811/admin/settings/license/activate', 1, NULL, NULL, '2020-04-23 12:23:57', '2020-04-23 12:23:57'),
(14, 404, 'http://video4k.vnn:811/api/v1api/deactivate_license', 4, NULL, NULL, '2020-04-23 12:41:14', '2020-04-23 12:43:58'),
(15, 404, 'http://flex.vnn:811/api/activate_license', 6, NULL, NULL, '2020-04-24 01:26:57', '2020-04-24 01:30:14'),
(16, 404, 'http://flex.vnn:811/api/deactivate_license', 4, NULL, NULL, '2020-04-24 01:32:19', '2020-04-24 01:36:44'),
(17, 404, 'http://flex.vnn:811/api/verify_license', 2, NULL, NULL, '2020-04-24 01:32:32', '2020-04-24 01:32:40'),
(18, 404, 'http://video4k.vnn:811/vendor/core/js/js-validation.js', 5, NULL, NULL, '2020-04-24 01:52:03', '2020-04-24 07:06:57'),
(19, 404, 'http://video4k.vnn:811/platform/plugins/product/public/jscolor/colorpicker.js', 5, NULL, NULL, '2020-04-24 08:31:06', '2020-04-24 08:32:33'),
(20, 404, 'http://video4k.vnn:811/news/1476890033-hero03.jpg', 1, NULL, NULL, '2020-04-25 07:15:19', '2020-04-25 07:15:19'),
(21, 404, 'http://video4k.vnn:811/news/1476891198-04.jpg', 1, NULL, NULL, '2020-04-25 07:15:19', '2020-04-25 07:15:19'),
(22, 404, 'http://video4k.vnn:811/admin/system/logs', 1, NULL, NULL, '2020-04-25 10:07:39', '2020-04-25 10:07:39'),
(23, 404, 'http://video4k.vnn:811/admin/system', 1, NULL, NULL, '2020-04-25 10:07:43', '2020-04-25 10:07:43'),
(24, 404, 'http://video4k.vnn:811/vendor/core/plugins/product/css/store-product-admin.css', 25, NULL, NULL, '2020-04-25 10:36:07', '2020-04-25 23:38:50'),
(25, 404, 'http://video4k.vnn:811/vendor/core/plugins/product/js/store-product-admin.js', 19, NULL, NULL, '2020-04-25 10:36:07', '2020-04-25 11:16:10'),
(26, 404, 'http://video4k.vnn:811/vendor/core/plugins/product/css/store-product.css', 45, NULL, NULL, '2020-04-25 12:27:48', '2020-04-26 08:03:29'),
(27, 404, 'http://video4k.vnn:811/S%E1%BA%A3n%20ph%E1%BA%A9m%201', 37, '[1]', NULL, '2020-04-26 01:58:03', '2020-04-26 08:03:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `revisions`
--

CREATE TABLE `revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `revisions`
--

INSERT INTO `revisions` (`id`, `revisionable_type`, `revisionable_id`, `user_id`, `key`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(1, 'Botble\\Blog\\Models\\Post', 44, 1, 'featured', '0', '1', '2018-04-13 09:38:58', '2018-04-13 09:38:58'),
(2, 'Botble\\Blog\\Models\\Post', 48, 1, 'image', NULL, 'https://s3-ap-southeast-1.amazonaws.com/botble/cms/news/lock-660x330.jpg', '2018-04-13 09:40:18', '2018-04-13 09:40:18'),
(3, 'Botble\\Blog\\Models\\Post', 49, 1, 'featured', '0', '1', '2018-04-13 09:41:32', '2018-04-13 09:41:32'),
(4, 'Botble\\Blog\\Models\\Post', 50, 1, 'featured', '0', '1', '2018-04-13 09:42:27', '2018-04-13 09:42:27'),
(5, 'Botble\\Blog\\Models\\Post', 54, 1, 'featured', '1', '0', '2018-04-13 10:04:12', '2018-04-13 10:04:12'),
(6, 'Botble\\Blog\\Models\\Post', 54, 1, 'featured', '0', '1', '2018-04-13 10:04:20', '2018-04-13 10:04:20'),
(7, 'Botble\\Page\\Models\\Page', 17, 1, 'description', 'Đây là trang liên hệ', NULL, '2020-03-10 19:33:16', '2020-03-10 19:33:16'),
(8, 'Botble\\Product\\Models\\Product', 1, 1, 'imagedl', NULL, 'users/avatar.jpg', '2020-04-13 08:10:46', '2020-04-13 08:10:46'),
(9, 'Botble\\Product\\Models\\Product', 1, 1, 'description', 'sdsds', 'sdsds3434', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(10, 'Botble\\Product\\Models\\Product', 1, 1, 'pricesell', '20', '203434', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(11, 'Botble\\Product\\Models\\Product', 1, 1, 'amound', NULL, '33434', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(12, 'Botble\\Product\\Models\\Product', 1, 1, 'video', 'hải dương', 'hải dương434343', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(13, 'Botble\\Product\\Models\\Product', 1, 1, 'pricesale', NULL, '343434343', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(14, 'Botble\\Product\\Models\\Product', 1, 1, 'price_sale_start', NULL, '2020-04-23', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(15, 'Botble\\Product\\Models\\Product', 1, 1, 'price_sale_end', NULL, '2020-04-25', '2020-04-13 08:11:43', '2020-04-13 08:11:43'),
(16, 'Botble\\Product\\Models\\Product', 2, 1, 'images', NULL, '[]', '2020-04-13 08:14:20', '2020-04-13 08:14:20'),
(17, 'Botble\\Product\\Models\\Product', 2, 1, 'colors', NULL, '[]', '2020-04-13 08:14:20', '2020-04-13 08:14:20'),
(18, 'Botble\\Product\\Models\\Product', 2, 1, 'sizes', NULL, '[]', '2020-04-13 08:14:20', '2020-04-13 08:14:20'),
(19, 'Botble\\Product\\Models\\Product', 3, 1, 'images', NULL, '[]', '2020-04-13 08:14:30', '2020-04-13 08:14:30'),
(20, 'Botble\\Product\\Models\\Product', 3, 1, 'colors', NULL, '[]', '2020-04-13 08:14:30', '2020-04-13 08:14:30'),
(21, 'Botble\\Product\\Models\\Product', 3, 1, 'sizes', NULL, '[]', '2020-04-13 08:14:30', '2020-04-13 08:14:30'),
(22, 'Botble\\Product\\Models\\Product', 4, 1, 'images', NULL, '[]', '2020-04-13 08:14:39', '2020-04-13 08:14:39'),
(23, 'Botble\\Product\\Models\\Product', 4, 1, 'colors', NULL, '[]', '2020-04-13 08:14:39', '2020-04-13 08:14:39'),
(24, 'Botble\\Product\\Models\\Product', 4, 1, 'sizes', NULL, '[]', '2020-04-13 08:14:39', '2020-04-13 08:14:39'),
(25, 'Botble\\Blog\\Models\\Post', 56, 1, 'created_at', '2020-04-22 15:12:17', '2020-04-23 15:12:00', '2020-04-22 08:37:41', '2020-04-22 08:37:41'),
(26, 'Botble\\Product\\Models\\Product', 4, 1, 'colors', '[]', '[\"1\"]', '2020-04-24 08:22:21', '2020-04-24 08:22:21'),
(27, 'Botble\\Product\\Models\\Product', 4, 1, 'option1', '[\"6\",\"7\",\"9\"]', '[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\"]', '2020-04-24 08:59:04', '2020-04-24 08:59:04'),
(28, 'Botble\\Product\\Models\\Product', 4, 1, 'option1', '[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\"]', '[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\",\"rgb(255, 192, 203)\"]', '2020-04-24 09:00:36', '2020-04-24 09:00:36'),
(29, 'Botble\\Product\\Models\\Product', 4, 1, 'option1', '[\"rgb(150, 74, 87)\",\"rgb(152, 86, 97)\",\"rgb(198, 13, 45)\",\"rgb(255, 192, 203)\"]', '[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\"]', '2020-04-24 09:05:27', '2020-04-24 09:05:27'),
(30, 'Botble\\Product\\Models\\Product', 4, 1, 'option1', '[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\"]', '[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\",\"rgb(255, 192, 203)\"]', '2020-04-24 09:06:33', '2020-04-24 09:06:33'),
(31, 'Botble\\Product\\Models\\Product', 4, 1, 'option1', '[\"rgb(255, 192, 203)\",\"rgb(75, 25, 34)\",\"rgb(196, 97, 114)\",\"rgb(229, 54, 85)\",\"rgb(255, 192, 203)\"]', '[]', '2020-04-24 09:33:50', '2020-04-24 09:33:50'),
(32, 'Botble\\Product\\Models\\Product', 4, 1, 'name', 'Sản phẩm 1(Duplicate)(Duplicate)(Duplicate)', '3', '2020-04-25 11:27:42', '2020-04-25 11:27:42'),
(33, 'Botble\\Product\\Models\\Product', 4, 1, 'name', '3', '1', '2020-04-25 11:28:11', '2020-04-25 11:28:11'),
(34, 'Botble\\Product\\Models\\Product', 4, 1, 'name', '1', '2', '2020-04-25 11:28:37', '2020-04-25 11:28:37'),
(35, 'Botble\\Product\\Models\\Product', 5, 1, 'name', '2(Duplicate)', '1', '2020-04-25 11:29:13', '2020-04-25 11:29:13'),
(36, 'Botble\\Product\\Models\\Product', 5, 1, 'images', NULL, '[]', '2020-04-25 11:29:13', '2020-04-25 11:29:13'),
(37, 'Botble\\Product\\Models\\Product', 5, 1, 'option1', NULL, '[]', '2020-04-25 11:29:13', '2020-04-25 11:29:13'),
(38, 'Botble\\Product\\Models\\Product', 6, 1, 'currency_id', NULL, '5', '2020-04-26 07:45:05', '2020-04-26 07:45:05'),
(39, 'Botble\\Product\\Models\\Product', 6, 1, 'pricecost', '25000', '25000343443434', '2020-04-26 07:52:50', '2020-04-26 07:52:50'),
(40, 'Botble\\Product\\Models\\Product', 6, 1, 'pricesell', '500000', '500000434343434', '2020-04-26 07:52:50', '2020-04-26 07:52:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `description`, `is_default`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'administrators', 'Administrators', '{\"analytics.general\":true,\"analytics.page\":true,\"analytics.browser\":true,\"analytics.referrer\":true,\"backups.list\":true,\"backups.create\":true,\"backups.restore\":true,\"backups.delete\":true,\"block.list\":true,\"block.create\":true,\"block.edit\":true,\"block.delete\":true,\"categories.list\":true,\"categories.create\":true,\"categories.edit\":true,\"categories.delete\":true,\"contacts.list\":true,\"contacts.create\":true,\"contacts.edit\":true,\"contacts.delete\":true,\"custom-fields.list\":true,\"custom-fields.create\":true,\"custom-fields.edit\":true,\"custom-fields.delete\":true,\"dashboard.index\":true,\"galleries.list\":true,\"galleries.create\":true,\"galleries.edit\":true,\"galleries.delete\":true,\"languages.list\":true,\"languages.create\":true,\"languages.edit\":true,\"languages.delete\":true,\"logs.list\":true,\"logs.delete\":true,\"media.index\":true,\"files.list\":true,\"files.create\":true,\"files.edit\":true,\"files.trash\":true,\"files.delete\":true,\"folders.list\":true,\"folders.create\":true,\"folders.edit\":true,\"folders.trash\":true,\"folders.delete\":true,\"member.list\":true,\"member.create\":true,\"member.edit\":true,\"member.delete\":true,\"menus.list\":true,\"menus.create\":true,\"menus.edit\":true,\"menus.delete\":true,\"pages.list\":true,\"pages.create\":true,\"pages.edit\":true,\"pages.delete\":true,\"plugins.list\":true,\"posts.list\":true,\"posts.create\":true,\"posts.edit\":true,\"posts.delete\":true,\"roles.list\":true,\"roles.create\":true,\"roles.edit\":true,\"roles.delete\":true,\"settings.options\":true,\"tags.list\":true,\"tags.create\":true,\"tags.edit\":true,\"tags.delete\":true,\"translations.list\":true,\"translations.create\":true,\"translations.edit\":true,\"translations.delete\":true,\"users.list\":true,\"users.create\":true,\"users.edit\":true,\"users.delete\":true,\"widgets.list\":true}', 'Highest role in system', 0, 1, 1, '2018-01-17 18:00:45', '2018-03-14 21:49:45'),
(2, 'members', 'Members', '{\"analytics.general\":true,\"analytics.page\":true,\"analytics.browser\":true,\"analytics.referrer\":true,\"block.list\":true,\"block.create\":true,\"block.edit\":true,\"block.delete\":true,\"categories.list\":true,\"categories.create\":true,\"categories.edit\":true,\"categories.delete\":true,\"contacts.list\":true,\"contacts.create\":true,\"contacts.edit\":true,\"contacts.delete\":true,\"custom-fields.list\":true,\"custom-fields.create\":true,\"custom-fields.edit\":true,\"custom-fields.delete\":true,\"dashboard.index\":true,\"galleries.list\":true,\"galleries.create\":true,\"galleries.edit\":true,\"galleries.delete\":true,\"media.index\":true,\"files.list\":true,\"files.create\":true,\"files.edit\":true,\"files.trash\":true,\"files.delete\":true,\"folders.list\":true,\"folders.create\":true,\"folders.edit\":true,\"folders.trash\":true,\"folders.delete\":true,\"menus.list\":true,\"menus.create\":true,\"menus.edit\":true,\"menus.delete\":true,\"pages.list\":true,\"pages.create\":true,\"pages.edit\":true,\"pages.delete\":true,\"posts.list\":true,\"posts.create\":true,\"posts.edit\":true,\"posts.delete\":true,\"tags.list\":true,\"tags.create\":true,\"tags.edit\":true,\"tags.delete\":true}', 'Member role', 1, 1, 1, '2018-01-17 18:01:32', '2018-03-14 21:50:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_users`
--

CREATE TABLE `role_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_users`
--

INSERT INTO `role_users` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(3, 1, 1, '2018-01-18 08:11:34', '2018-01-18 08:11:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'site_title', 'PHP platform based on Laravel Framework'),
(2, 'copyright', '© 2016 Botble Technologies. All right reserved. Designed by <a href=\"http://nghiadev.com\" target=\"_blank\">Nghia Minh</a>'),
(3, 'admin_email', 'botble.cms@gmail.com'),
(6, 'seo_title', 'Botble Platform'),
(7, 'seo_description', 'Botble Platform - PHP platform base on Laravel Framework'),
(8, 'seo_keywords', 'botble, botble team, botble platform, php platform, php framework, web development'),
(9, 'contact_address', 'Elinext Building, 37 Phan Xich Long, ward 3, Phu Nhuan district, Ho Chi Minh, Vietnam'),
(10, 'contact_email', 'sangnguyenplus@gmail.com'),
(11, 'email_support', 'sangnguyenplus@gmail.com'),
(12, 'contact_phone', '+84988606928'),
(13, 'contact_hotline', '+84988606928'),
(14, 'enable_captcha', '0'),
(15, 'about', '<p>Content here</p>\r\n'),
(16, 'show_admin_bar', '0'),
(17, 'theme', 'ripple'),
(18, 'enable_change_admin_theme', '1'),
(19, 'enable_multi_language_in_admin', '1'),
(20, 'enable_https', '0'),
(21, 'google_plus', 'https://plus.google.com'),
(22, 'facebook', 'https://www.facebook.com/botble.technologies'),
(23, 'twitter', 'https://twitter.com/botble'),
(24, 'enable_cache', '0'),
(25, 'cache_time', '10'),
(26, 'cache_time_site_map', '3600'),
(27, 'language_hide_default', '1'),
(28, 'language_switcher_display', 'dropdown'),
(29, 'language_display', 'all'),
(30, 'language_hide_languages', '[]'),
(31, 'theme-ripple-site_title', 'Botble Technologies'),
(34, 'theme-ripple-copyright', '© 2019 Botble Technologies. All right reserved. Designed by Nghia Minh.'),
(35, 'theme-newstv-copyright', '© Copyright 2017 Botble Technologies. All Rights Reserved.'),
(36, 'theme-newstv-theme_color', 'red'),
(38, 'theme-newstv-logo', 'https://s3-ap-southeast-1.amazonaws.com/botble/cms/logo/logo.png'),
(39, 'rich_editor', 'ckeditor'),
(41, 'admin_title', 'Botble Technologies'),
(44, 'activated_plugins', '[\"analytics\",\"audit-log\",\"backup\",\"captcha\",\"language\",\"request-log\",\"social-login\",\"block\",\"custom-field\",\"member\",\"contact\",\"blog\",\"gallery\",\"translation\",\"impersonate\",\"log-viewer\",\"post-scheduler\",\"maintenance-mode\",\"simple-slider\",\"product\",\"cookie-consent\"]'),
(45, 'theme-ripple-vi-copyright', '© 2019 Botble Technologies. Tất cả quyền đã được bảo hộ. Thiết kế bởi Minh Nghĩa.'),
(47, 'theme-newstv-vi-copyright', '© 2017 Botble Technologies. Tất cả quyền đã được bảo hộ.'),
(48, 'theme-newstv-vi-theme_color', 'red'),
(49, 'theme-newstv-vi-top_banner', '/themes/newstv/assets/images/banner.png'),
(51, 'time_zone', 'UTC'),
(52, 'optimize_page_speed_enable', '0'),
(53, 'enable_send_error_reporting_via_email', '1'),
(54, 'default_admin_theme', 'default'),
(55, 'cache_admin_menu_enable', '0'),
(56, 'language_show_default_item_if_current_version_not_existed', '1'),
(57, 'show_site_name', '0'),
(62, 'captcha_site_key', 'no-captcha-site-key'),
(63, 'captcha_secret', 'no-captcha-secret'),
(64, 'social_utilities_enable', '1'),
(85, 'submit', 'save'),
(106, 'social_utilities_facebook_url', 'botble.technologies'),
(107, 'social_utilities_twitter_url', 'sangnguyen2603'),
(108, 'social_utilities_google-plus_url', 'sangnguyen2603'),
(109, 'social_utilities_linkedin_url', 'sangnguyen2603'),
(110, 'social_utilities_pinterest_url', 'sangnguyen2603'),
(111, 'theme-ripple-show_site_name', '0'),
(112, 'theme-ripple-seo_title', 'Botble Technologies'),
(113, 'theme-ripple-seo_description', 'Botble Platform - PHP platform base on Laravel Framework'),
(118, 'theme-ripple-primary_font', 'Poppins'),
(120, 'theme-ripple-vi-site_title', 'PHP platform based on Laravel Framework'),
(121, 'theme-ripple-vi-show_site_name', '0'),
(122, 'theme-ripple-vi-seo_title', 'Botble Technologies'),
(124, 'theme-ripple-vi-primary_font', 'Roboto'),
(126, 'theme-ripple-vi-seo_description', 'PHP platform based on Laravel Framework'),
(138, 'theme-newstv-site_title', 'Botble Technologies'),
(139, 'theme-newstv-show_site_name', '0'),
(140, 'theme-newstv-seo_title', 'Botble Technologies'),
(142, 'theme-newstv-top_banner', 'ads/728x90.jpg'),
(143, 'theme-ripple-site_description', 'A young team in Vietnam'),
(144, 'theme-ripple-address', 'Go Vap District, HCM City, Vietnam'),
(145, 'theme-ripple-website', 'https://botble.com'),
(146, 'theme-ripple-contact_email', 'botble.cms@gmail.com'),
(151, 'theme-ripple-vi-site_description', 'Một nhóm trẻ tại Việt Nam'),
(152, 'theme-ripple-vi-address', 'Quận Gò Vấp, TP. Hồ Chí Minh, Việt Nam'),
(153, 'theme-ripple-vi-website', 'https://botble.com'),
(154, 'theme-ripple-vi-contact_email', 'botble.cms@gmail.com'),
(159, 'membership_authorization_at', '2020-04-22 14:22:34'),
(160, 'theme-ripple-favicon', 'logo/facebook.png'),
(161, 'theme-ripple-logo', ''),
(162, 'theme-ripple-facebook', ''),
(163, 'theme-ripple-twitter', ''),
(164, 'theme-ripple-youtube', ''),
(175, 'simple_slider_using_assets', '1'),
(179, 'cookie_consent_enable', '1'),
(227, 'licensed_to', '123456789'),
(228, 'payment_paypal_name', 'Pay online via PayPal'),
(229, 'payment_paypal_client_id', '123456'),
(231, 'payment_paypal_mode', '0'),
(232, 'payment_paypal_status', '1'),
(233, 'payment_stripe_name', 'Pay online via Stripe'),
(234, 'payment_stripe_client_id', '456456'),
(236, 'payment_stripe_status', '1'),
(247, 'currencies_is_default', '0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shippings`
--

CREATE TABLE `shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `simple_sliders`
--

CREATE TABLE `simple_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `simple_sliders`
--

INSERT INTO `simple_sliders` (`id`, `name`, `key`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'main slider', 'main-slider', 'main-slider', 'published', '2020-04-23 00:02:22', '2020-04-23 00:02:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `simple_slider_items`
--

CREATE TABLE `simple_slider_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `simple_slider_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_slider` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `content_des2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_des3` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_des1` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `simple_slider_items`
--

INSERT INTO `simple_slider_items` (`id`, `simple_slider_id`, `title`, `image`, `link`, `description`, `type_slider`, `content_des2`, `content_des3`, `content_des1`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 'Trang chủ', '1476890029-hero01-540x360.jpg', 'https://benhvienhoabinh.vn/nguoi-tham-gia-bao-hiem-can-biet.html', 'd', 1, 'd', 'd', 'd', 0, '2020-04-23 00:08:42', '2020-04-23 00:08:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_sizes` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `code_sizes`, `status`, `created_at`, `updated_at`) VALUES
(10, 'XXL', '#fff', 'published', '2020-04-24 09:27:19', '2020-04-24 09:27:19'),
(11, 'XL', '#fff', 'published', '2020-04-24 09:27:27', '2020-04-24 09:27:27'),
(12, 'X', '#fff', 'published', '2020-04-24 09:27:33', '2020-04-24 09:27:33'),
(13, 'XX', '#fff', 'published', '2020-04-24 09:27:40', '2020-04-24 09:27:40'),
(14, 'L', '#fff', 'published', '2020-04-24 09:27:48', '2020-04-24 09:27:48'),
(15, 'M', '#fff', 'published', '2020-04-24 09:27:54', '2020-04-24 09:27:54'),
(16, 'S', '#fff', 'published', '2020-04-24 09:28:01', '2020-04-24 09:28:01');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slugs`
--

CREATE TABLE `slugs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prefix` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slugs`
--

INSERT INTO `slugs` (`id`, `key`, `reference_id`, `reference_type`, `created_at`, `updated_at`, `prefix`) VALUES
(1, '13000-people-have-bought-our-theme', 4, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(2, 'top-search-engine-optimization-strategies', 5, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(3, 'which-company-would-you-choose', 6, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(4, 'used-car-dealer-sales-tricks-exposed', 7, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(5, '20-ways-to-sell-your-product-faster', 8, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(6, 'the-secrets-of-rich-and-famous-writers', 9, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(7, 'imagine-losing-20-pounds-in-14-days', 10, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(8, 'are-you-still-using-that-slow-old-typewriter', 11, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(9, 'a-skin-cream-thats-proven-to-work', 12, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(10, '10-reasons-to-start-your-own-profitable-website', 13, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(11, 'simple-ways-to-reduce', 14, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(12, 'apple-imac-with-retina-5k-display-review', 15, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(13, '10-reasons-to-start-your-own-profitable-website-1', 16, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(14, 'unlock-the-secrets-of-selling-high-ticket-items', 17, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(15, '10000-web-site-visitors-in-one-monthguaranteed', 18, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(16, 'are-you-still-using-that-slow-old-typewriter-1', 19, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(17, 'a-skin-cream-thats-proven-to-work-1', 20, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(18, 'top-search-engine-optimization-strategies-1', 21, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(19, '13000-nguoi-da-mua-theme-cua-chung-toi', 44, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(20, 'len-tren-cong-cu-tim-kiem-toi-uu-hoa-cac-chien-luoc', 45, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(21, 'ban-se-chon-cong-ty-nao', 46, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(22, 'su-dung-xe-dai-ly-ban-hang-tricks-xuc', 47, 'Botble\\Blog\\Models\\Post', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(23, 'uncategorized', 1, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(24, 'events', 6, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(25, 'projects', 7, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(26, 'portfolio', 8, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(27, 'business', 9, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(28, 'resources', 10, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(29, 'new-update', 11, 'Botble\\Blog\\Models\\Category', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(30, 'botble', 5, 'Botble\\Blog\\Models\\Tag', '2017-11-30 18:26:09', '2019-10-26 17:52:18', 'tag'),
(31, 'botble-cms', 6, 'Botble\\Blog\\Models\\Tag', '2017-11-30 18:26:09', '2019-10-26 17:52:18', 'tag'),
(32, 'contact', 1, 'Botble\\Page\\Models\\Page', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(33, 'lien-he', 17, 'Botble\\Page\\Models\\Page', '2017-11-30 18:26:09', '2017-11-30 18:26:09', ''),
(34, 'photography', 1, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(35, 'selfie', 2, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(36, 'new-day', 3, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(37, 'morning', 4, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(38, 'happy-day', 5, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(39, 'perfect', 6, 'Botble\\Gallery\\Models\\Gallery', '2017-11-30 18:26:09', '2019-10-26 17:56:54', 'gallery'),
(40, 'chua-phan-loai', 12, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:02:12', '2018-04-13 09:02:12', ''),
(41, 'su-kien', 13, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:04:30', '2018-04-13 09:04:30', ''),
(42, 'doanh-nghiep', 14, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:04:49', '2018-04-13 09:04:49', ''),
(43, 'tin-tuc-cap-nhat', 15, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:05:06', '2018-04-13 09:05:06', ''),
(44, 'du-an', 16, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:05:23', '2018-04-13 09:05:23', ''),
(45, 'dau-tu', 17, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:06:44', '2018-04-13 09:06:44', ''),
(46, 'nguon-luc', 18, 'Botble\\Blog\\Models\\Category', '2018-04-13 09:08:01', '2018-04-13 09:08:01', ''),
(47, 'chien-luoc-phat-trien-phan-mem', 48, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:40:11', '2018-04-13 09:40:11', ''),
(48, 'nen-tang-ma-nguon-mo-php', 49, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:41:28', '2018-04-13 09:41:28', ''),
(49, 'nen-tang-ma-nguon-mo-php-1', 7, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:41:28', '2019-10-26 17:52:18', 'tag'),
(50, 'xay-dung-website-mot-cach-nhanh-chong', 50, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:42:20', '2018-04-13 09:42:20', ''),
(51, 'xay-dung-website-mot-cach-nhanh-chong-1', 8, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:42:20', '2019-10-26 17:52:18', 'tag'),
(52, 'xay-dung-website-mot-cach-nhanh-chong-2', 9, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:42:20', '2019-10-26 17:52:18', 'tag'),
(53, 'san-pham-tri-tue-viet-nam', 51, 'Botble\\Blog\\Models\\Post', '2018-04-13 09:43:09', '2018-04-13 09:43:09', ''),
(54, 'san-pham-tri-tue-viet-nam-1', 10, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:43:09', '2019-10-26 17:52:18', 'tag'),
(55, 'tai-ve', 11, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:45:22', '2019-10-26 17:52:18', 'tag'),
(58, 'tag-su-kien', 24, 'Botble\\Blog\\Models\\Tag', '2018-04-13 09:59:52', '2019-10-26 17:52:18', 'tag'),
(59, 'popular', 25, 'Botble\\Blog\\Models\\Tag', '2018-04-13 10:00:35', '2019-10-26 17:52:18', 'tag'),
(60, 'cuoc-song-tuoi-30', 52, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:02:20', '2018-04-13 10:02:20', ''),
(61, 'hanh-trinh-tim-kiem-su-khac-biet', 53, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:02:59', '2018-04-13 10:02:59', ''),
(62, 'the-gioi-dong-vat-muon-mau', 54, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:03:46', '2018-04-13 10:03:46', ''),
(63, 'di-tim-hoi-uc-tuoi-tho', 55, 'Botble\\Blog\\Models\\Post', '2018-04-13 10:06:40', '2018-04-13 10:06:40', ''),
(64, 'nhiep-anh', 7, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:44:54', '2019-10-26 17:56:54', 'gallery'),
(65, 'thien-nhien', 8, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:45:11', '2019-10-26 17:56:54', 'gallery'),
(66, 'ngay-moi', 9, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:45:36', '2019-10-26 17:56:54', 'gallery'),
(67, 'buoi-sang', 10, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:45:42', '2019-10-26 17:56:54', 'gallery'),
(68, 'ngay-hanh-phuc', 11, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:45:46', '2019-10-26 17:56:54', 'gallery'),
(69, 'hoan-hao', 12, 'Botble\\Gallery\\Models\\Gallery', '2018-05-11 07:45:51', '2019-10-26 17:56:54', 'gallery'),
(70, 'quan-ao', 1, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:20:19', '2020-04-13 06:20:19', ''),
(71, 'quan-ao-tre-em', 2, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:23:22', '2020-04-13 06:23:22', ''),
(72, 'quan-ao-so-sinh', 3, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 06:23:41', '2020-04-13 06:23:41', ''),
(73, 'san-pham-1', 1, 'Botble\\Product\\Models\\Product', '2020-04-13 08:10:03', '2020-04-13 08:10:03', ''),
(74, 'san-pham-1duplicate', 2, 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:20', '2020-04-13 08:14:20', ''),
(75, 'san-pham-1duplicateduplicate', 3, 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:30', '2020-04-13 08:14:30', ''),
(76, 'san-pham-1duplicateduplicateduplicate', 4, 'Botble\\Product\\Models\\Product', '2020-04-13 08:14:39', '2020-04-13 08:14:39', ''),
(78, 'thang-chau-thu-nhat', 5, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 09:35:25', '2020-04-13 09:35:25', ''),
(79, 'thang-chau-thu-hai', 6, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 09:35:38', '2020-04-13 09:35:38', ''),
(80, 'thang-cha-thu-2', 7, 'Botble\\Product\\Models\\ProCategory', '2020-04-13 10:01:19', '2020-04-13 10:01:19', ''),
(81, 'test', 56, 'Botble\\Blog\\Models\\Post', '2020-04-22 08:12:17', '2020-04-22 08:12:17', ''),
(82, 'jhjhj', 26, 'Botble\\Blog\\Models\\Tag', '2020-04-22 08:12:17', '2020-04-22 08:12:17', 'tag'),
(83, 'test', 1, 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:39:27', '2020-04-22 11:39:27', 'protag'),
(84, 'product-tag-2', 2, 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35', 'protag'),
(85, 'tag-3', 3, 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35', 'protag'),
(86, 'tag-4', 4, 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:50:35', '2020-04-22 11:50:35', 'protag'),
(87, 'thu-nghiem', 5, 'Botble\\Product\\Models\\ProTag', '2020-04-22 11:51:03', '2020-04-22 11:51:03', 'protag'),
(89, '1587842323', 6, 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:02', '2020-04-25 12:18:43', ''),
(90, '1587742033', 7, 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:13', '2020-04-24 08:27:13', ''),
(91, 'mau-xanh', 8, 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:23', '2020-04-24 08:27:23', ''),
(92, 'mau-trang', 9, 'Botble\\Product\\Models\\Color', '2020-04-24 08:27:34', '2020-04-24 08:27:34', ''),
(93, 'xxl', 10, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:19', '2020-04-24 09:27:19', ''),
(94, '1587745647', 11, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:27', '2020-04-24 09:27:27', ''),
(95, '1587745653', 12, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:33', '2020-04-24 09:27:33', ''),
(96, '1587745660', 13, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:40', '2020-04-24 09:27:40', ''),
(97, '1587745668', 14, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:48', '2020-04-24 09:27:48', ''),
(98, '1587745674', 15, 'Botble\\Product\\Models\\Size', '2020-04-24 09:27:54', '2020-04-24 09:27:54', ''),
(99, '1587745681', 16, 'Botble\\Product\\Models\\Size', '2020-04-24 09:28:01', '2020-04-24 09:28:01', ''),
(100, 'fsf', 5, 'Botble\\Product\\Models\\Product', '2020-04-25 11:29:13', '2020-04-25 11:29:13', ''),
(101, '1587842682', 1, 'Botble\\Product\\Models\\Features', '2020-04-25 12:24:42', '2020-04-25 12:24:42', ''),
(102, '1587842688', 2, 'Botble\\Product\\Models\\Features', '2020-04-25 12:24:48', '2020-04-25 12:24:48', ''),
(103, 'sarn-ph', 6, 'Botble\\Product\\Models\\Product', '2020-04-26 01:57:08', '2020-04-26 01:57:08', ''),
(104, 'fgfgf', 6, 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09', 'protag'),
(105, 'trtrt', 7, 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09', 'protag'),
(106, 'uewe', 8, 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09', 'protag'),
(107, 'xax', 9, 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09', 'protag'),
(108, 'gfgfg', 10, 'Botble\\Product\\Models\\ProTag', '2020-04-26 01:57:09', '2020-04-26 01:57:09', 'protag');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `stores`
--

CREATE TABLE `stores` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `stores`
--

INSERT INTO `stores` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Kho 1', 'published', '2020-04-24 12:10:07', '2020-04-24 12:10:07');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `store_products`
--

CREATE TABLE `store_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `store_products`
--

INSERT INTO `store_products` (`id`, `name`, `key`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'test product', 'o-ok', 'rêr', 'published', '2020-04-25 10:03:13', '2020-04-25 10:04:54'),
(2, 'Kho 1', 'kho 1', 'kho 1', 'published', '2020-04-25 10:36:06', '2020-04-25 10:36:06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `store_product_items`
--

CREATE TABLE `store_product_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_product_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `store_product_items`
--

INSERT INTO `store_product_items` (`id`, `store_product_id`, `title`, `image`, `link`, `description`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 'fsf', '1476890029-hero01-540x360.jpg', 'ssdsd', 'sdsds', 0, '2020-04-25 10:14:01', '2020-04-25 10:14:01'),
(2, 2, 'dfdf', NULL, 'dfdf', 'dfdfd', 34, '2020-04-25 10:52:30', '2020-04-25 10:52:30'),
(3, 2, 'Thêm trường biến tấu khác các page', NULL, 'https://benhvienhoabinh.vn/nguoi-tham-gia-bao-hiem-can-biet.html', '4456645', 0, '2020-04-25 11:08:56', '2020-04-25 11:08:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tags`
--

INSERT INTO `tags` (`id`, `name`, `author_id`, `author_type`, `description`, `status`, `created_at`, `updated_at`) VALUES
(5, 'download', 1, 'Botble\\ACL\\Models\\User', '', 'published', '2016-08-02 21:38:34', '2016-09-27 09:30:37'),
(6, 'event', 1, 'Botble\\ACL\\Models\\User', '', 'published', '2016-08-02 21:38:34', '2016-09-27 09:30:50'),
(7, 'nổi bật', 1, 'Botble\\ACL\\Models\\User', NULL, 'published', '2018-04-13 09:41:28', '2018-04-13 10:00:09'),
(11, 'tải về', 1, 'Botble\\ACL\\Models\\User', NULL, 'published', '2018-04-13 09:45:22', '2018-04-13 09:45:22'),
(24, 'sự kiện', 1, 'Botble\\ACL\\Models\\User', NULL, 'published', '2018-04-13 09:59:52', '2018-04-13 09:59:52'),
(25, 'popular', 1, 'Botble\\ACL\\Models\\User', NULL, 'published', '2018-04-13 10:00:35', '2018-04-13 10:00:35'),
(26, 'jhjhj', 1, 'Botble\\ACL\\Models\\User', '', 'published', '2020-04-22 08:12:17', '2020-04-22 08:12:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `translations`
--

CREATE TABLE `translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `translations`
--

INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'auth', 'failed', 'These credentials do not match our records.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(2, 1, 'en', 'auth', 'throttle', 'Too many login attempts. Please try again in :seconds seconds.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(3, 1, 'en', 'pagination', 'previous', '&laquo; Previous', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(4, 1, 'en', 'pagination', 'next', 'Next &raquo;', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(5, 1, 'en', 'passwords', 'reset', 'Your password has been reset!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(6, 1, 'en', 'passwords', 'sent', 'We have emailed your password reset link!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(7, 1, 'en', 'passwords', 'throttled', 'Please wait before retrying.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(8, 1, 'en', 'passwords', 'token', 'This password reset token is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(9, 1, 'en', 'passwords', 'user', 'We can\'t find a user with that email address.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(10, 1, 'en', 'validation', 'accepted', 'The :attribute must be accepted.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(11, 1, 'en', 'validation', 'active_url', 'The :attribute is not a valid URL.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(12, 1, 'en', 'validation', 'after', 'The :attribute must be a date after :date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(13, 1, 'en', 'validation', 'after_or_equal', 'The :attribute must be a date after or equal to :date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(14, 1, 'en', 'validation', 'alpha', 'The :attribute may only contain letters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(15, 1, 'en', 'validation', 'alpha_dash', 'The :attribute may only contain letters, numbers, dashes and underscores.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(16, 1, 'en', 'validation', 'alpha_num', 'The :attribute may only contain letters and numbers.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(17, 1, 'en', 'validation', 'array', 'The :attribute must be an array.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(18, 1, 'en', 'validation', 'before', 'The :attribute must be a date before :date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(19, 1, 'en', 'validation', 'before_or_equal', 'The :attribute must be a date before or equal to :date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(20, 1, 'en', 'validation', 'between.numeric', 'The :attribute must be between :min and :max.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(21, 1, 'en', 'validation', 'between.file', 'The :attribute must be between :min and :max kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(22, 1, 'en', 'validation', 'between.string', 'The :attribute must be between :min and :max characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(23, 1, 'en', 'validation', 'between.array', 'The :attribute must have between :min and :max items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(24, 1, 'en', 'validation', 'boolean', 'The :attribute field must be true or false.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(25, 1, 'en', 'validation', 'confirmed', 'The :attribute confirmation does not match.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(26, 1, 'en', 'validation', 'date', 'The :attribute is not a valid date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(27, 1, 'en', 'validation', 'date_equals', 'The :attribute must be a date equal to :date.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(28, 1, 'en', 'validation', 'date_format', 'The :attribute does not match the format :format.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(29, 1, 'en', 'validation', 'different', 'The :attribute and :other must be different.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(30, 1, 'en', 'validation', 'digits', 'The :attribute must be :digits digits.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(31, 1, 'en', 'validation', 'digits_between', 'The :attribute must be between :min and :max digits.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(32, 1, 'en', 'validation', 'dimensions', 'The :attribute has invalid image dimensions.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(33, 1, 'en', 'validation', 'distinct', 'The :attribute field has a duplicate value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(34, 1, 'en', 'validation', 'email', 'The :attribute must be a valid email address.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(35, 1, 'en', 'validation', 'ends_with', 'The :attribute must end with one of the following: :values.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(36, 1, 'en', 'validation', 'exists', 'The selected :attribute is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(37, 1, 'en', 'validation', 'file', 'The :attribute must be a file.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(38, 1, 'en', 'validation', 'filled', 'The :attribute field must have a value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(39, 1, 'en', 'validation', 'gt.numeric', 'The :attribute must be greater than :value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(40, 1, 'en', 'validation', 'gt.file', 'The :attribute must be greater than :value kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(41, 1, 'en', 'validation', 'gt.string', 'The :attribute must be greater than :value characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(42, 1, 'en', 'validation', 'gt.array', 'The :attribute must have more than :value items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(43, 1, 'en', 'validation', 'gte.numeric', 'The :attribute must be greater than or equal :value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(44, 1, 'en', 'validation', 'gte.file', 'The :attribute must be greater than or equal :value kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(45, 1, 'en', 'validation', 'gte.string', 'The :attribute must be greater than or equal :value characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(46, 1, 'en', 'validation', 'gte.array', 'The :attribute must have :value items or more.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(47, 1, 'en', 'validation', 'image', 'The :attribute must be an image.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(48, 1, 'en', 'validation', 'in', 'The selected :attribute is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(49, 1, 'en', 'validation', 'in_array', 'The :attribute field does not exist in :other.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(50, 1, 'en', 'validation', 'integer', 'The :attribute must be an integer.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(51, 1, 'en', 'validation', 'ip', 'The :attribute must be a valid IP address.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(52, 1, 'en', 'validation', 'ipv4', 'The :attribute must be a valid IPv4 address.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(53, 1, 'en', 'validation', 'ipv6', 'The :attribute must be a valid IPv6 address.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(54, 1, 'en', 'validation', 'json', 'The :attribute must be a valid JSON string.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(55, 1, 'en', 'validation', 'lt.numeric', 'The :attribute must be less than :value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(56, 1, 'en', 'validation', 'lt.file', 'The :attribute must be less than :value kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(57, 1, 'en', 'validation', 'lt.string', 'The :attribute must be less than :value characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(58, 1, 'en', 'validation', 'lt.array', 'The :attribute must have less than :value items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(59, 1, 'en', 'validation', 'lte.numeric', 'The :attribute must be less than or equal :value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(60, 1, 'en', 'validation', 'lte.file', 'The :attribute must be less than or equal :value kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(61, 1, 'en', 'validation', 'lte.string', 'The :attribute must be less than or equal :value characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(62, 1, 'en', 'validation', 'lte.array', 'The :attribute must not have more than :value items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(63, 1, 'en', 'validation', 'max.numeric', 'The :attribute may not be greater than :max.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(64, 1, 'en', 'validation', 'max.file', 'The :attribute may not be greater than :max kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(65, 1, 'en', 'validation', 'max.string', 'The :attribute may not be greater than :max characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(66, 1, 'en', 'validation', 'max.array', 'The :attribute may not have more than :max items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(67, 1, 'en', 'validation', 'mimes', 'The :attribute must be a file of type: :values.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(68, 1, 'en', 'validation', 'mimetypes', 'The :attribute must be a file of type: :values.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(69, 1, 'en', 'validation', 'min.numeric', 'The :attribute must be at least :min.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(70, 1, 'en', 'validation', 'min.file', 'The :attribute must be at least :min kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(71, 1, 'en', 'validation', 'min.string', 'The :attribute must be at least :min characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(72, 1, 'en', 'validation', 'min.array', 'The :attribute must have at least :min items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(73, 1, 'en', 'validation', 'not_in', 'The selected :attribute is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(74, 1, 'en', 'validation', 'not_regex', 'The :attribute format is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(75, 1, 'en', 'validation', 'numeric', 'The :attribute must be a number.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(76, 1, 'en', 'validation', 'password', 'The password is incorrect.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(77, 1, 'en', 'validation', 'present', 'The :attribute field must be present.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(78, 1, 'en', 'validation', 'regex', 'The :attribute format is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(79, 1, 'en', 'validation', 'required', 'The :attribute field is required.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(80, 1, 'en', 'validation', 'required_if', 'The :attribute field is required when :other is :value.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(81, 1, 'en', 'validation', 'required_unless', 'The :attribute field is required unless :other is in :values.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(82, 1, 'en', 'validation', 'required_with', 'The :attribute field is required when :values is present.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(83, 1, 'en', 'validation', 'required_with_all', 'The :attribute field is required when :values are present.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(84, 1, 'en', 'validation', 'required_without', 'The :attribute field is required when :values is not present.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(85, 1, 'en', 'validation', 'required_without_all', 'The :attribute field is required when none of :values are present.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(86, 1, 'en', 'validation', 'same', 'The :attribute and :other must match.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(87, 1, 'en', 'validation', 'size.numeric', 'The :attribute must be :size.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(88, 1, 'en', 'validation', 'size.file', 'The :attribute must be :size kilobytes.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(89, 1, 'en', 'validation', 'size.string', 'The :attribute must be :size characters.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(90, 1, 'en', 'validation', 'size.array', 'The :attribute must contain :size items.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(91, 1, 'en', 'validation', 'starts_with', 'The :attribute must start with one of the following: :values.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(92, 1, 'en', 'validation', 'string', 'The :attribute must be a string.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(93, 1, 'en', 'validation', 'timezone', 'The :attribute must be a valid zone.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(94, 1, 'en', 'validation', 'unique', 'The :attribute has already been taken.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(95, 1, 'en', 'validation', 'uploaded', 'The :attribute failed to upload.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(96, 1, 'en', 'validation', 'url', 'The :attribute format is invalid.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(97, 1, 'en', 'validation', 'uuid', 'The :attribute must be a valid UUID.', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(98, 1, 'en', 'validation', 'custom.attribute-name.rule-name', 'custom-message', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(99, 1, 'en', 'core/acl/api', 'api_clients', 'API Clients', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(100, 1, 'en', 'core/acl/api', 'create_new_client', 'Create new client', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(101, 1, 'en', 'core/acl/api', 'create_new_client_success', 'Create new client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(102, 1, 'en', 'core/acl/api', 'edit_client', 'Edit client \":name\"', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(103, 1, 'en', 'core/acl/api', 'edit_client_success', 'Updated client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(104, 1, 'en', 'core/acl/api', 'delete_success', 'Deleted client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(105, 1, 'en', 'core/acl/api', 'confirm_delete_title', 'Confirm delete client \":name\"', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(106, 1, 'en', 'core/acl/api', 'confirm_delete_description', 'Do you really want to delete client \":name\"?', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(107, 1, 'en', 'core/acl/api', 'cancel_delete', 'No', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(108, 1, 'en', 'core/acl/api', 'continue_delete', 'Yes, let\'s delete it!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(109, 1, 'en', 'core/acl/api', 'name', 'Name', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(110, 1, 'en', 'core/acl/api', 'cancel', 'Cancel', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(111, 1, 'en', 'core/acl/api', 'save', 'Save', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(112, 1, 'en', 'core/acl/api', 'edit', 'Edit', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(113, 1, 'en', 'core/acl/api', 'delete', 'Delete', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(114, 1, 'en', 'core/acl/api', 'client_id', 'Client ID', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(115, 1, 'en', 'core/acl/api', 'secret', 'Secret', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(116, 0, 'en', 'core/acl/auth', 'login.username', 'Username', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(117, 0, 'en', 'core/acl/auth', 'login.email', 'Email', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(118, 0, 'en', 'core/acl/auth', 'login.password', 'Password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(119, 0, 'en', 'core/acl/auth', 'login.title', 'User Login', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(120, 0, 'en', 'core/acl/auth', 'login.remember', 'Remember me?', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(121, 0, 'en', 'core/acl/auth', 'login.login', 'Sign in', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(122, 0, 'en', 'core/acl/auth', 'login.placeholder.username', 'Please enter your username', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(123, 0, 'en', 'core/acl/auth', 'login.placeholder.email', 'Please enter your email', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(124, 0, 'en', 'core/acl/auth', 'login.success', 'Login successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(125, 0, 'en', 'core/acl/auth', 'login.fail', 'Wrong username or password.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(126, 0, 'en', 'core/acl/auth', 'login.not_active', 'Your account has not been activated yet!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(127, 0, 'en', 'core/acl/auth', 'login.banned', 'This account is banned.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(128, 0, 'en', 'core/acl/auth', 'login.logout_success', 'Logout successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(129, 0, 'en', 'core/acl/auth', 'login.dont_have_account', 'You don\'t have account on this system, please contact administrator for more information!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(130, 0, 'en', 'core/acl/auth', 'forgot_password.title', 'Forgot Password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(131, 0, 'en', 'core/acl/auth', 'forgot_password.message', '<p>Have you forgotten your password?</p><p>Please enter your email account. System will send a email with active link to reset your password.</p>', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(132, 0, 'en', 'core/acl/auth', 'forgot_password.submit', 'Submit', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(133, 0, 'en', 'core/acl/auth', 'reset.new_password', 'New password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(134, 0, 'en', 'core/acl/auth', 'reset.password_confirmation', 'Confirm new password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(135, 0, 'en', 'core/acl/auth', 'reset.email', 'Email', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(136, 0, 'en', 'core/acl/auth', 'reset.title', 'Reset your password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(137, 0, 'en', 'core/acl/auth', 'reset.update', 'Update', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(138, 0, 'en', 'core/acl/auth', 'reset.wrong_token', 'This link is invalid or expired. Please try using reset form again.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(139, 0, 'en', 'core/acl/auth', 'reset.user_not_found', 'This username is not exist.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(140, 0, 'en', 'core/acl/auth', 'reset.success', 'Reset password successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(141, 0, 'en', 'core/acl/auth', 'reset.fail', 'Token is invalid, the reset password link has been expired!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(142, 0, 'en', 'core/acl/auth', 'reset.reset.title', 'Email reset password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(143, 0, 'en', 'core/acl/auth', 'reset.send.success', 'A email was sent to your email account. Please check and complete this action.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(144, 0, 'en', 'core/acl/auth', 'reset.send.fail', 'Can not send email in this time. Please try again later.', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(145, 0, 'en', 'core/acl/auth', 'reset.new-password', 'New password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(146, 0, 'en', 'core/acl/auth', 'email.reminder.title', 'Email reset password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(147, 0, 'en', 'core/acl/auth', 'password_confirmation', 'Password confirm', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(148, 0, 'en', 'core/acl/auth', 'failed', 'Failed', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(149, 0, 'en', 'core/acl/auth', 'throttle', 'Throttle', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(150, 0, 'en', 'core/acl/auth', 'not_member', 'Not a member yet?', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(151, 0, 'en', 'core/acl/auth', 'register_now', 'Register now', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(152, 0, 'en', 'core/acl/auth', 'lost_your_password', 'Lost your password?', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(153, 0, 'en', 'core/acl/auth', 'login_title', 'Admin', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(154, 0, 'en', 'core/acl/auth', 'login_via_social', 'Login with social networks', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(155, 0, 'en', 'core/acl/auth', 'back_to_login', 'Back to login page', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(156, 0, 'en', 'core/acl/auth', 'sign_in_below', 'Sign In Below', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(157, 0, 'en', 'core/acl/auth', 'languages', 'Languages', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(158, 0, 'en', 'core/acl/auth', 'reset_password', 'Reset Password', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(159, 0, 'en', 'core/acl/permissions', 'notices.role_in_use', 'Cannot delete this role, it is still in use!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(160, 0, 'en', 'core/acl/permissions', 'notices.role_delete_belong_user', 'You are not able to delete this role; it belongs to someone else!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(161, 0, 'en', 'core/acl/permissions', 'notices.role_edit_belong_user', 'You are not able to edit this role; it belongs to someone else!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(162, 0, 'en', 'core/acl/permissions', 'notices.delete_global_role', 'You are not allowed to delete global roles!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(163, 0, 'en', 'core/acl/permissions', 'notices.delete_success', 'The selected role was deleted successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(164, 0, 'en', 'core/acl/permissions', 'notices.modified_success', 'The selected role was modified successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(165, 0, 'en', 'core/acl/permissions', 'notices.create_success', 'The new role was successfully created', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(166, 0, 'en', 'core/acl/permissions', 'notices.duplicated_success', 'The selected role was duplicated successfully', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(167, 0, 'en', 'core/acl/permissions', 'notices.no_select', 'Please select at least one record to take this action!', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(168, 0, 'en', 'core/acl/permissions', 'notices.not_found', 'Role not found', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(169, 0, 'en', 'core/acl/permissions', 'list', 'List Permissions', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(170, 0, 'en', 'core/acl/permissions', 'name', 'Name', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(171, 0, 'en', 'core/acl/permissions', 'current_role', 'Current Role', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(172, 0, 'en', 'core/acl/permissions', 'no_role_assigned', 'No role assigned', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(173, 0, 'en', 'core/acl/permissions', 'role_assigned', 'Assigned Role', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(174, 0, 'en', 'core/acl/permissions', 'create_role', 'Create New Role', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(175, 0, 'en', 'core/acl/permissions', 'role_name', 'Name', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(176, 0, 'en', 'core/acl/permissions', 'role_description', 'Description', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(177, 0, 'en', 'core/acl/permissions', 'permission_flags', 'Permission Flags', '2020-03-29 20:06:53', '2020-04-24 09:38:34'),
(178, 0, 'en', 'core/acl/permissions', 'cancel', 'Cancel', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(179, 0, 'en', 'core/acl/permissions', 'reset', 'Reset', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(180, 0, 'en', 'core/acl/permissions', 'save', 'Save', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(181, 0, 'en', 'core/acl/permissions', 'global_role_msg', 'This is a global role and cannot be modified.  You can use the Duplicate button to make a copy of this role that you can then modify.', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(182, 0, 'en', 'core/acl/permissions', 'details', 'Details', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(183, 0, 'en', 'core/acl/permissions', 'duplicate', 'Duplicate', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(184, 0, 'en', 'core/acl/permissions', 'all', 'All Permissions', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(185, 0, 'en', 'core/acl/permissions', 'list_role', 'List Roles', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(186, 0, 'en', 'core/acl/permissions', 'created_on', 'Created On', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(187, 0, 'en', 'core/acl/permissions', 'created_by', 'Created By', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(188, 0, 'en', 'core/acl/permissions', 'actions', 'Actions', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(189, 0, 'en', 'core/acl/permissions', 'role_in_use', 'Cannot delete this role, it is still in use!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(190, 0, 'en', 'core/acl/permissions', 'role_delete_belong_user', 'You are not able to delete this role; it belongs to someone else!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(191, 0, 'en', 'core/acl/permissions', 'delete_global_role', 'Can not delete global role', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(192, 0, 'en', 'core/acl/permissions', 'delete_success', 'Delete role successfully', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(193, 0, 'en', 'core/acl/permissions', 'no_select', 'Please select at least one role to take this action!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(194, 0, 'en', 'core/acl/permissions', 'not_found', 'No role found!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(195, 0, 'en', 'core/acl/permissions', 'role_edit_belong_user', 'You are not able to edit this role; it belongs to someone else!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(196, 0, 'en', 'core/acl/permissions', 'modified_success', 'Modified successfully', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(197, 0, 'en', 'core/acl/permissions', 'create_success', 'Create successfully', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(198, 0, 'en', 'core/acl/permissions', 'duplicated_success', 'Duplicated successfully', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(199, 0, 'en', 'core/acl/permissions', 'options', 'Options', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(200, 0, 'en', 'core/acl/permissions', 'access_denied_message', 'You are not allowed to do this action', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(201, 0, 'en', 'core/acl/permissions', 'roles', 'Roles', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(202, 0, 'en', 'core/acl/permissions', 'role_permission', 'Roles and Permissions', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(203, 0, 'en', 'core/acl/permissions', 'user_management', 'User Management', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(204, 0, 'en', 'core/acl/permissions', 'super_user_management', 'Super User Management', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(205, 0, 'en', 'core/acl/reminders', 'password', 'Passwords must be at least six characters and match the confirmation.', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(206, 0, 'en', 'core/acl/reminders', 'user', 'We can\'t find a user with that e-mail address.', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(207, 0, 'en', 'core/acl/reminders', 'token', 'This password reset token is invalid.', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(208, 0, 'en', 'core/acl/reminders', 'sent', 'Password reminder sent!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(209, 0, 'en', 'core/acl/reminders', 'reset', 'Password has been reset!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(210, 0, 'en', 'core/acl/users', 'delete_user_logged_in', 'Can\'t delete this user. This user is logged on!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(211, 0, 'en', 'core/acl/users', 'no_select', 'Please select at least one record to take this action!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(212, 0, 'en', 'core/acl/users', 'lock_user_logged_in', 'Can\'t lock this user. This user is logged on!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(213, 0, 'en', 'core/acl/users', 'update_success', 'Update status successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(214, 0, 'en', 'core/acl/users', 'save_setting_failed', 'Something went wrong when save your setting', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(215, 0, 'en', 'core/acl/users', 'not_found', 'User not found', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(216, 0, 'en', 'core/acl/users', 'email_exist', 'That email address already belongs to an existing account', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(217, 0, 'en', 'core/acl/users', 'username_exist', 'That username address already belongs to an existing account', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(218, 0, 'en', 'core/acl/users', 'update_profile_success', 'Your profile changes were successfully saved', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(219, 0, 'en', 'core/acl/users', 'password_update_success', 'Password successfully changed', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(220, 0, 'en', 'core/acl/users', 'current_password_not_valid', 'Current password is not valid', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(221, 0, 'en', 'core/acl/users', 'user_exist_in', 'User is already a member', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(222, 0, 'en', 'core/acl/users', 'email', 'Email', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(223, 0, 'en', 'core/acl/users', 'role', 'Role', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(224, 0, 'en', 'core/acl/users', 'username', 'Username', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(225, 0, 'en', 'core/acl/users', 'last_name', 'Last Name', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(226, 0, 'en', 'core/acl/users', 'first_name', 'First Name', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(227, 0, 'en', 'core/acl/users', 'message', 'Message', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(228, 0, 'en', 'core/acl/users', 'cancel_btn', 'Cancel', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(229, 0, 'en', 'core/acl/users', 'change_password', 'Change password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(230, 0, 'en', 'core/acl/users', 'current_password', 'Current password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(231, 0, 'en', 'core/acl/users', 'new_password', 'New Password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(232, 0, 'en', 'core/acl/users', 'confirm_new_password', 'Confirm New Password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(233, 0, 'en', 'core/acl/users', 'password', 'Password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(234, 0, 'en', 'core/acl/users', 'save', 'Save', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(235, 0, 'en', 'core/acl/users', 'cannot_delete', 'User could not be deleted', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(236, 0, 'en', 'core/acl/users', 'deleted', 'User deleted', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(237, 0, 'en', 'core/acl/users', 'last_login', 'Last Login', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(238, 0, 'en', 'core/acl/users', 'error_update_profile_image', 'Error when update profile image', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(239, 0, 'en', 'core/acl/users', 'email_reminder_template', '<h3>Hello :name</h3><p>The system has received a request to restore the password for your account, to complete this task please click the link below.</p><p><a href=\":link\">Reset password now</a></p><p>If not you ask recover password, please ignore this email.</p><p>This email is valid for 60 minutes after receiving the email.</p>', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(240, 0, 'en', 'core/acl/users', 'change_profile_image', 'Change Profile Image', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(241, 0, 'en', 'core/acl/users', 'new_image', 'New Image', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(242, 0, 'en', 'core/acl/users', 'loading', 'Loading', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(243, 0, 'en', 'core/acl/users', 'close', 'Close', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(244, 0, 'en', 'core/acl/users', 'update', 'Update', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(245, 0, 'en', 'core/acl/users', 'read_image_failed', 'Failed to read the image file', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(246, 0, 'en', 'core/acl/users', 'users', 'Users', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(247, 0, 'en', 'core/acl/users', 'update_avatar_success', 'Update profile image successfully!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(248, 0, 'en', 'core/acl/users', 'info.title', 'User profile', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(249, 0, 'en', 'core/acl/users', 'info.first_name', 'First Name', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(250, 0, 'en', 'core/acl/users', 'info.last_name', 'Last Name', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(251, 0, 'en', 'core/acl/users', 'info.email', 'Email', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(252, 0, 'en', 'core/acl/users', 'info.second_email', 'Secondary Email', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(253, 0, 'en', 'core/acl/users', 'info.address', 'Address', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(254, 0, 'en', 'core/acl/users', 'info.second_address', 'Secondary Address', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(255, 0, 'en', 'core/acl/users', 'info.birth_day', 'Date of birth', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(256, 0, 'en', 'core/acl/users', 'info.job', 'Job Position', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(257, 0, 'en', 'core/acl/users', 'info.mobile_number', 'Mobile Number', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(258, 0, 'en', 'core/acl/users', 'info.second_mobile_number', 'Secondary Phone', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(259, 0, 'en', 'core/acl/users', 'info.interes', 'Interests', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(260, 0, 'en', 'core/acl/users', 'info.about', 'About', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(261, 0, 'en', 'core/acl/users', 'gender.title', 'Gender', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(262, 0, 'en', 'core/acl/users', 'gender.male', 'Male', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(263, 0, 'en', 'core/acl/users', 'gender.female', 'Female', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(264, 0, 'en', 'core/acl/users', 'total_users', 'Total users', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(265, 0, 'en', 'core/acl/users', 'statuses.activated', 'Activated', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(266, 0, 'en', 'core/acl/users', 'statuses.deactivated', 'Deactivated', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(267, 0, 'en', 'core/acl/users', 'make_super', 'Make super', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(268, 0, 'en', 'core/acl/users', 'remove_super', 'Remove super', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(269, 0, 'en', 'core/acl/users', 'is_super', 'Is super?', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(270, 0, 'en', 'core/acl/users', 'email_placeholder', 'Ex: example@gmail.com', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(271, 0, 'en', 'core/acl/users', 'password_confirmation', 'Re-type password', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(272, 0, 'en', 'core/acl/users', 'select_role', 'Select role', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(273, 0, 'en', 'core/acl/users', 'create_new_user', 'Create a new user', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(274, 0, 'en', 'core/acl/users', 'cannot_delete_super_user', 'Permission denied. Cannot delete a super user!', '2020-03-29 20:06:53', '2020-04-24 09:38:35'),
(275, 1, 'vi', 'core/acl/api', 'api_clients', 'API Clients', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(276, 1, 'vi', 'core/acl/api', 'create_new_client', 'Create new client', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(277, 1, 'vi', 'core/acl/api', 'create_new_client_success', 'Create new client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(278, 1, 'vi', 'core/acl/api', 'edit_client', 'Edit client \":name\"', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(279, 1, 'vi', 'core/acl/api', 'edit_client_success', 'Updated client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(280, 1, 'vi', 'core/acl/api', 'delete_success', 'Deleted client successfully!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(281, 1, 'vi', 'core/acl/api', 'confirm_delete_title', 'Confirm delete client \":name\"', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(282, 1, 'vi', 'core/acl/api', 'confirm_delete_description', 'Do you really want to delete client \":name\"?', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(283, 1, 'vi', 'core/acl/api', 'cancel_delete', 'No', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(284, 1, 'vi', 'core/acl/api', 'continue_delete', 'Yes, let\'s delete it!', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(285, 1, 'vi', 'core/acl/api', 'name', 'Name', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(286, 1, 'vi', 'core/acl/api', 'cancel', 'Cancel', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(287, 1, 'vi', 'core/acl/api', 'save', 'Save', '2020-03-29 20:06:53', '2020-03-29 20:06:53'),
(288, 1, 'vi', 'core/acl/api', 'edit', 'Edit', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(289, 1, 'vi', 'core/acl/api', 'delete', 'Delete', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(290, 1, 'vi', 'core/acl/api', 'client_id', 'Client ID', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(291, 1, 'vi', 'core/acl/api', 'secret', 'Secret', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(292, 0, 'vi', 'core/acl/auth', 'login.username', 'Tên truy cập', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(293, 0, 'vi', 'core/acl/auth', 'login.password', 'Mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(294, 0, 'vi', 'core/acl/auth', 'login.title', 'Đăng nhập vào quản trị', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(295, 0, 'vi', 'core/acl/auth', 'login.remember', 'Nhớ mật khẩu?', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(296, 0, 'vi', 'core/acl/auth', 'login.login', 'Đăng nhập', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(297, 0, 'vi', 'core/acl/auth', 'login.placeholder.username', 'Vui lòng nhập tên truy cập', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(298, 0, 'vi', 'core/acl/auth', 'login.placeholder.email', 'Vui lòng nhập email', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(299, 0, 'vi', 'core/acl/auth', 'login.success', 'Đăng nhập thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(300, 0, 'vi', 'core/acl/auth', 'login.fail', 'Sai tên truy cập hoặc mật khẩu.', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(301, 0, 'vi', 'core/acl/auth', 'login.not_active', 'Tài khoản của bạn chưa được kích hoạt!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(302, 0, 'vi', 'core/acl/auth', 'login.banned', 'Tài khoản này đã bị khóa.', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(303, 0, 'vi', 'core/acl/auth', 'login.logout_success', 'Đăng xuất thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(304, 0, 'vi', 'core/acl/auth', 'login.dont_have_account', 'Bạn không có tài khoản trong hệ thống, vui lòng liên hệ quản trị viên!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(305, 0, 'vi', 'core/acl/auth', 'login.email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(306, 0, 'vi', 'core/acl/auth', 'forgot_password.title', 'Quên mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(307, 0, 'vi', 'core/acl/auth', 'forgot_password.message', '<p>Quên mật khẩu?</p><p>Vui lòng nhập email đăng nhập tài khoản của bạn để hệ thống gửi liên kết khôi phục mật khẩu.</p>', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(308, 0, 'vi', 'core/acl/auth', 'forgot_password.submit', 'Hoàn tất', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(309, 0, 'vi', 'core/acl/auth', 'reset.new_password', 'Mật khẩu mới', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(310, 0, 'vi', 'core/acl/auth', 'reset.password_confirmation', 'Xác nhận mật khẩu mới', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(311, 0, 'vi', 'core/acl/auth', 'reset.title', 'Khôi phục mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(312, 0, 'vi', 'core/acl/auth', 'reset.update', 'Cập nhật', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(313, 0, 'vi', 'core/acl/auth', 'reset.wrong_token', 'Liên kết này không chính xác hoặc đã hết hiệu lực, vui lòng yêu cầu khôi phục mật khẩu lại!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(314, 0, 'vi', 'core/acl/auth', 'reset.user_not_found', 'Tên đăng nhập không tồn tại.', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(315, 0, 'vi', 'core/acl/auth', 'reset.success', 'Khôi phục mật khẩu thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(316, 0, 'vi', 'core/acl/auth', 'reset.fail', 'Token không hợp lệ hoặc liên kết khôi phục mật khẩu đã hết thời gian hiệu lực!', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(317, 0, 'vi', 'core/acl/auth', 'reset.reset.title', 'Email khôi phục mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(318, 0, 'vi', 'core/acl/auth', 'reset.send.success', 'Một email khôi phục mật khẩu đã được gửi tới email của bạn, vui lòng kiểm tra và hoàn tất yêu cầu.', '2020-03-29 20:06:54', '2020-04-24 09:38:35'),
(319, 0, 'vi', 'core/acl/auth', 'reset.send.fail', 'Không thể gửi email trong lúc này. Vui lòng thực hiện lại sau.', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(320, 0, 'vi', 'core/acl/auth', 'reset.new-password', 'Mật khẩu mới', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(321, 0, 'vi', 'core/acl/auth', 'reset.email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(322, 0, 'vi', 'core/acl/auth', 'email.reminder.title', 'Email khôi phục mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(323, 0, 'vi', 'core/acl/auth', 'failed', 'Không thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(324, 0, 'vi', 'core/acl/auth', 'password_confirmation', 'Xác nhận mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(325, 0, 'vi', 'core/acl/auth', 'throttle', 'Throttle', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(326, 0, 'vi', 'core/acl/auth', 'back_to_login', 'Quay lại trang đăng nhập', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(327, 0, 'vi', 'core/acl/auth', 'login_title', 'Đăng nhập vào quản trị', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(328, 0, 'vi', 'core/acl/auth', 'login_via_social', 'Đăng nhập thông qua mạng xã hội', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(329, 0, 'vi', 'core/acl/auth', 'lost_your_password', 'Quên mật khẩu?', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(330, 0, 'vi', 'core/acl/auth', 'not_member', 'Chưa là thành viên?', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(331, 0, 'vi', 'core/acl/auth', 'register_now', 'Đăng ký ngay', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(332, 0, 'vi', 'core/acl/auth', 'sign_in_below', 'Đăng nhập', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(333, 0, 'vi', 'core/acl/auth', 'languages', 'Ngôn ngữ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(334, 0, 'vi', 'core/acl/auth', 'reset_password', 'Khôi phục mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(335, 0, 'vi', 'core/acl/permissions', 'notices.role_in_use', 'Không thể xóa quyền người dùng này vì nó đang được sử dụng!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(336, 0, 'vi', 'core/acl/permissions', 'notices.role_delete_belong_user', 'Không thể xóa quyền hạn này vì nó thuộc về người khác!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(337, 0, 'vi', 'core/acl/permissions', 'notices.role_edit_belong_user', 'Bạn không được phép sửa quyền này vì nó thuộc về người khác', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(338, 0, 'vi', 'core/acl/permissions', 'notices.delete_global_role', 'Bạn không thể xóa quyền người dùng hệ thống!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(339, 0, 'vi', 'core/acl/permissions', 'notices.delete_success', 'Quyền người dùng đã được xóa!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(340, 0, 'vi', 'core/acl/permissions', 'notices.modified_success', 'Quyền người dùng đã được cập nhật thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(341, 0, 'vi', 'core/acl/permissions', 'notices.create_success', 'Quyền người dùng mới đã được tạo thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(342, 0, 'vi', 'core/acl/permissions', 'notices.duplicated_success', 'Quyền người dùng đã được sao chép thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(343, 0, 'vi', 'core/acl/permissions', 'notices.no_select', 'Hãy chọn ít nhất một quyền người dùng để thực hiện hành động này!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(344, 0, 'vi', 'core/acl/permissions', 'notices.not_found', 'Không tìm thấy quyền người dùng này', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(345, 0, 'vi', 'core/acl/permissions', 'list', 'Danh sách phân quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(346, 0, 'vi', 'core/acl/permissions', 'name', 'Tên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(347, 0, 'vi', 'core/acl/permissions', 'current_role', 'Quyền hiện tại', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(348, 0, 'vi', 'core/acl/permissions', 'no_role_assigned', 'Không có quyền hạn nào', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(349, 0, 'vi', 'core/acl/permissions', 'role_assigned', 'Quyền đã được gán', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(350, 0, 'vi', 'core/acl/permissions', 'create_role', 'Tạo quyền mới', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(351, 0, 'vi', 'core/acl/permissions', 'role_name', 'Tên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(352, 0, 'vi', 'core/acl/permissions', 'role_description', 'Mô tả', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(353, 0, 'vi', 'core/acl/permissions', 'permission_flags', 'Cờ đánh dấu quyền hạn', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(354, 0, 'vi', 'core/acl/permissions', 'cancel', 'Hủy bỏ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(355, 0, 'vi', 'core/acl/permissions', 'reset', 'Làm lại', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(356, 0, 'vi', 'core/acl/permissions', 'save', 'Lưu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(357, 0, 'vi', 'core/acl/permissions', 'global_role_msg', 'Đây là một phân quyền toàn cục và không thể thay đổi.  Bạn có thể sử dụng nút \"Nhân bản\" để tạo một bản sao chép cho phân quyền này và chỉnh sửa.', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(358, 0, 'vi', 'core/acl/permissions', 'details', 'Chi tiết', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(359, 0, 'vi', 'core/acl/permissions', 'duplicate', 'Nhân bản', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(360, 0, 'vi', 'core/acl/permissions', 'all', 'Tất cả phân quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(361, 0, 'vi', 'core/acl/permissions', 'list_role', 'Danh sách quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(362, 0, 'vi', 'core/acl/permissions', 'created_on', 'Ngày tạo', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(363, 0, 'vi', 'core/acl/permissions', 'created_by', 'Được tạo bởi', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(364, 0, 'vi', 'core/acl/permissions', 'actions', 'Tác vụ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(365, 0, 'vi', 'core/acl/permissions', 'create_success', 'Tạo thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(366, 0, 'vi', 'core/acl/permissions', 'delete_global_role', 'Không thể xóa quyền hệ thống', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(367, 0, 'vi', 'core/acl/permissions', 'delete_success', 'Xóa quyền thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(368, 0, 'vi', 'core/acl/permissions', 'duplicated_success', 'Nhân bản thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(369, 0, 'vi', 'core/acl/permissions', 'modified_success', 'Sửa thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(370, 0, 'vi', 'core/acl/permissions', 'no_select', 'Hãy chọn ít nhất một quyền để thực hiện hành động này!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(371, 0, 'vi', 'core/acl/permissions', 'not_found', 'Không tìm thấy quyền thành viên nào!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(372, 0, 'vi', 'core/acl/permissions', 'options', 'Tùy chọn', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(373, 0, 'vi', 'core/acl/permissions', 'role_delete_belong_user', 'Không thể xóa quyền hạn này vì nó thuộc về người khác!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(374, 0, 'vi', 'core/acl/permissions', 'role_edit_belong_user', 'Bạn không được phép sửa quyền này vì nó thuộc về người khác', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(375, 0, 'vi', 'core/acl/permissions', 'role_in_use', 'Không thể xóa quyền người dùng này vì nó đang được sử dụng!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(376, 0, 'vi', 'core/acl/permissions', 'access_denied_message', 'Bạn không có quyền sử dụng chức năng này!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(377, 0, 'vi', 'core/acl/permissions', 'roles', 'Quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(378, 0, 'vi', 'core/acl/permissions', 'role_permission', 'Nhóm và phân quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(379, 0, 'vi', 'core/acl/permissions', 'user_management', 'Quản lý người dùng hệ thống', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(380, 0, 'vi', 'core/acl/permissions', 'super_user_management', 'Quản lý người dùng cấp cao', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(381, 0, 'vi', 'core/acl/reminders', 'password', 'Mật khẩu phải ít nhất 6 kí tự và trùng khớp với mật khẩu xác nhận.', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(382, 0, 'vi', 'core/acl/reminders', 'user', 'Hệ thống không thể tìm thấy tài khoản với email này.', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(383, 0, 'vi', 'core/acl/reminders', 'token', 'Mã khôi phục mật khẩu này không hợp lệ.', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(384, 0, 'vi', 'core/acl/reminders', 'sent', 'Liên kết khôi phục mật khẩu đã được gửi!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(385, 0, 'vi', 'core/acl/reminders', 'reset', 'Mật khẩu đã được thay đổi!', '2020-03-29 20:06:54', '2020-04-24 09:38:36');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(386, 0, 'vi', 'core/acl/users', 'delete_user_logged_in', 'Không thể xóa người dùng đang đăng nhập hệ thống!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(387, 0, 'vi', 'core/acl/users', 'no_select', 'Hãy chọn ít nhất một trường để thực hiện hành động này!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(388, 0, 'vi', 'core/acl/users', 'lock_user_logged_in', 'Không thể khóa người dùng đang đăng nhập hệ thống!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(389, 0, 'vi', 'core/acl/users', 'update_success', 'Cập nhật trạng thái thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(390, 0, 'vi', 'core/acl/users', 'save_setting_failed', 'Có lỗi xảy ra trong quá trình lưu cài đặt của người dùng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(391, 0, 'vi', 'core/acl/users', 'not_found', 'Không tìm thấy người dùng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(392, 0, 'vi', 'core/acl/users', 'email_exist', 'Email này đã được sử dụng bởi người dùng khác trong hệ thống', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(393, 0, 'vi', 'core/acl/users', 'username_exist', 'Tên đăng nhập này đã được sử dụng bởi người dùng khác trong hệ thống', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(394, 0, 'vi', 'core/acl/users', 'update_profile_success', 'Thông tin tài khoản của bạn đã được cập nhật thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(395, 0, 'vi', 'core/acl/users', 'password_update_success', 'Cập nhật mật khẩu thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(396, 0, 'vi', 'core/acl/users', 'current_password_not_valid', 'Mật khẩu hiện tại không chính xác', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(397, 0, 'vi', 'core/acl/users', 'user_exist_in', 'Người dùng đã là thành viên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(398, 0, 'vi', 'core/acl/users', 'email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(399, 0, 'vi', 'core/acl/users', 'username', 'Tên đăng nhập', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(400, 0, 'vi', 'core/acl/users', 'role', 'Phân quyền', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(401, 0, 'vi', 'core/acl/users', 'first_name', 'Họ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(402, 0, 'vi', 'core/acl/users', 'last_name', 'Tên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(403, 0, 'vi', 'core/acl/users', 'message', 'Thông điệp', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(404, 0, 'vi', 'core/acl/users', 'cancel_btn', 'Hủy bỏ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(405, 0, 'vi', 'core/acl/users', 'password', 'Mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(406, 0, 'vi', 'core/acl/users', 'new_password', 'Mật khẩu mới', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(407, 0, 'vi', 'core/acl/users', 'save', 'Lưu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(408, 0, 'vi', 'core/acl/users', 'confirm_new_password', 'Xác nhận mật khẩu mới', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(409, 0, 'vi', 'core/acl/users', 'deleted', 'Xóa thành viên thành công', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(410, 0, 'vi', 'core/acl/users', 'cannot_delete', 'Không thể xóa thành viên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(411, 0, 'vi', 'core/acl/users', 'list', 'Danh sách thành viên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(412, 0, 'vi', 'core/acl/users', 'last_login', 'Lần cuối đăng nhập', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(413, 0, 'vi', 'core/acl/users', 'error_update_profile_image', 'Có lỗi trong quá trình đổi ảnh đại diện', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(414, 0, 'vi', 'core/acl/users', 'email_reminder_template', '<h3>Xin chào :name</h3><p>Hệ thống vừa nhận được yêu cầu khôi phục mật khẩu cho tài khoản của bạn, để hoàn tất tác vụ này vui lòng click vào đường link bên dưới.</p><p><a href=\":link\">Khôi phục mật khẩu</a></p><p>Nếu không phải bạn yêu cầu khôi phục mật khẩu, vui lòng bỏ qua email này.</p><p>Email này có giá trị trong vòng 60 phút kể từ lúc nhận được email.</p>', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(415, 0, 'vi', 'core/acl/users', 'change_profile_image', 'Thay đổi ảnh đại diện', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(416, 0, 'vi', 'core/acl/users', 'new_image', 'Ảnh mới', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(417, 0, 'vi', 'core/acl/users', 'loading', 'Đang tải', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(418, 0, 'vi', 'core/acl/users', 'close', 'Đóng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(419, 0, 'vi', 'core/acl/users', 'update', 'Cập nhật', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(420, 0, 'vi', 'core/acl/users', 'read_image_failed', 'Không đọc được nội dung của hình ảnh', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(421, 0, 'vi', 'core/acl/users', 'update_avatar_success', 'Cập nhật ảnh đại diện thành công!', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(422, 0, 'vi', 'core/acl/users', 'users', 'Quản trị viên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(423, 0, 'vi', 'core/acl/users', 'info.title', 'Thông tin người dùng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(424, 0, 'vi', 'core/acl/users', 'info.first_name', 'Họ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(425, 0, 'vi', 'core/acl/users', 'info.last_name', 'Tên', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(426, 0, 'vi', 'core/acl/users', 'info.email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(427, 0, 'vi', 'core/acl/users', 'info.second_email', 'Email dự phòng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(428, 0, 'vi', 'core/acl/users', 'info.address', 'Địa chỉ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(429, 0, 'vi', 'core/acl/users', 'info.second_address', 'Địa chỉ dự phòng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(430, 0, 'vi', 'core/acl/users', 'info.birth_day', 'Ngày sinh', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(431, 0, 'vi', 'core/acl/users', 'info.job', 'Nghề nghiệp', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(432, 0, 'vi', 'core/acl/users', 'info.mobile_number', 'Số điện thoại di động', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(433, 0, 'vi', 'core/acl/users', 'info.second_mobile_number', 'Số điện thoại dự phòng', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(434, 0, 'vi', 'core/acl/users', 'info.interes', 'Sở thích', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(435, 0, 'vi', 'core/acl/users', 'info.about', 'Giới thiệu', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(436, 0, 'vi', 'core/acl/users', 'gender.title', 'Giới tính', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(437, 0, 'vi', 'core/acl/users', 'gender.male', 'nam', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(438, 0, 'vi', 'core/acl/users', 'gender.female', 'nữ', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(439, 0, 'vi', 'core/acl/users', 'statuses.activated', 'Đang hoạt động', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(440, 0, 'vi', 'core/acl/users', 'statuses.deactivated', 'Đã khoá', '2020-03-29 20:06:54', '2020-04-24 09:38:36'),
(441, 0, 'vi', 'core/acl/users', 'change_password', 'Thay đổi mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(442, 0, 'vi', 'core/acl/users', 'current_password', 'Mật khẩu hiện tại', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(443, 0, 'vi', 'core/acl/users', 'make_super', 'Thiết lập quyền cao nhất', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(444, 0, 'vi', 'core/acl/users', 'remove_super', 'Loại bỏ quyền cao nhất', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(445, 0, 'vi', 'core/acl/users', 'is_super', 'Quyền cao nhất?', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(446, 0, 'vi', 'core/acl/users', 'email_placeholder', 'Ex: example@gmail.com', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(447, 0, 'vi', 'core/acl/users', 'password_confirmation', 'Nhập lại mật khẩu', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(448, 0, 'vi', 'core/acl/users', 'select_role', 'Chọn nhóm', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(449, 0, 'vi', 'core/acl/users', 'create_new_user', 'Tạo tài khoản quản trị viên mới', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(450, 0, 'vi', 'core/acl/users', 'cannot_delete_super_user', 'Vượt quyền hạn, không thể xoá quản trị viên cấp cao nhất!', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(451, 0, 'en', 'core/base/base', 'yes', 'Yes', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(452, 0, 'en', 'core/base/base', 'no', 'No', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(453, 0, 'en', 'core/base/base', 'is_default', 'Is default?', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(454, 0, 'en', 'core/base/base', 'proc_close_disabled_error', 'Function proc_close() is disabled. Please contact your hosting provider to enable\n    it. Or you can add to .env: CAN_EXECUTE_COMMAND=false to disable this feature.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(455, 0, 'en', 'core/base/cache', 'cache_management', 'Cache management', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(456, 0, 'en', 'core/base/cache', 'cache_commands', 'Clear cache commands', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(457, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.title', 'Clear all CMS cache', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(458, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.description', 'Clear CMS caching: database caching, static blocks... Run this command when you don\'t see the changes after updating data.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(459, 0, 'en', 'core/base/cache', 'commands.clear_cms_cache.success_msg', 'Cache cleaned', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(460, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.title', 'Refresh compiled views', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(461, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.description', 'Clear compiled views to make views up to date.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(462, 0, 'en', 'core/base/cache', 'commands.refresh_compiled_views.success_msg', 'Cache view refreshed', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(463, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.title', 'Clear config cache', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(464, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.description', 'You might need to refresh the config caching when you change something on production environment.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(465, 0, 'en', 'core/base/cache', 'commands.clear_config_cache.success_msg', 'Config cache cleaned', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(466, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.title', 'Clear route cache', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(467, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.description', 'Clear cache routing.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(468, 0, 'en', 'core/base/cache', 'commands.clear_route_cache.success_msg', 'The route cache has been cleaned', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(469, 0, 'en', 'core/base/cache', 'commands.clear_log.title', 'Clear log', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(470, 0, 'en', 'core/base/cache', 'commands.clear_log.description', 'Clear system log files', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(471, 0, 'en', 'core/base/cache', 'commands.clear_log.success_msg', 'The system log has been cleaned', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(472, 0, 'en', 'core/base/enums', 'statuses.draft', 'Draft', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(473, 0, 'en', 'core/base/enums', 'statuses.pending', 'Pending', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(474, 0, 'en', 'core/base/enums', 'statuses.published', 'Published', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(475, 0, 'en', 'core/base/errors', '401_title', 'Permission Denied', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(476, 0, 'en', 'core/base/errors', '401_msg', '<li>You have not been granted access to the section by the administrator.</li>\n	                <li>You may have the wrong account type.</li>\n	                <li>You are not authorized to view the requested resource.</li>\n	                <li>Your subscription may have expired.</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(477, 0, 'en', 'core/base/errors', '404_title', 'Page could not be found', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(478, 0, 'en', 'core/base/errors', '404_msg', '<li>The page you requested does not exist.</li>\n	                <li>The link you clicked is no longer.</li>\n	                <li>The page may have moved to a new location.</li>\n	                <li>An error may have occurred.</li>\n	                <li>You are not authorized to view the requested resource.</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(479, 0, 'en', 'core/base/errors', '500_title', 'Page could not be loaded', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(480, 0, 'en', 'core/base/errors', '500_msg', '<li>The page you requested does not exist.</li>\n	                <li>The link you clicked is no longer.</li>\n	                <li>The page may have moved to a new location.</li>\n	                <li>An error may have occurred.</li>\n	                <li>You are not authorized to view the requested resource.</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(481, 0, 'en', 'core/base/errors', 'reasons', 'This may have occurred because of several reasons', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(482, 1, 'en', 'core/base/errors', 'try_again', 'Please try again in a few minutes, or alternatively return to the homepage by <a href=\"http://botble.local/admin\">clicking here</a>.', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(483, 0, 'en', 'core/base/forms', 'choose_image', 'Choose image', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(484, 0, 'en', 'core/base/forms', 'actions', 'Actions', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(485, 0, 'en', 'core/base/forms', 'save', 'Save', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(486, 0, 'en', 'core/base/forms', 'save_and_continue', 'Save & Edit', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(487, 0, 'en', 'core/base/forms', 'image', 'Image', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(488, 0, 'en', 'core/base/forms', 'image_placeholder', 'Insert path of image or click upload button', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(489, 0, 'en', 'core/base/forms', 'create', 'Create', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(490, 0, 'en', 'core/base/forms', 'edit', 'Edit', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(491, 0, 'en', 'core/base/forms', 'permalink', 'Permalink', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(492, 0, 'en', 'core/base/forms', 'ok', 'OK', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(493, 0, 'en', 'core/base/forms', 'cancel', 'Cancel', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(494, 0, 'en', 'core/base/forms', 'character_remain', 'character(s) remain', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(495, 0, 'en', 'core/base/forms', 'template', 'Template', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(496, 0, 'en', 'core/base/forms', 'choose_file', 'Choose file', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(497, 0, 'en', 'core/base/forms', 'file', 'File', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(498, 0, 'en', 'core/base/forms', 'content', 'Content', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(499, 0, 'en', 'core/base/forms', 'description', 'Description', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(500, 0, 'en', 'core/base/forms', 'name', 'Name', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(501, 0, 'en', 'core/base/forms', 'slug', 'Slug', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(502, 0, 'en', 'core/base/forms', 'title', 'Title', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(503, 0, 'en', 'core/base/forms', 'value', 'Value', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(504, 0, 'en', 'core/base/forms', 'name_placeholder', 'Name', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(505, 0, 'en', 'core/base/forms', 'alias_placeholder', 'Alias', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(506, 0, 'en', 'core/base/forms', 'description_placeholder', 'Short description', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(507, 0, 'en', 'core/base/forms', 'parent', 'Parent', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(508, 0, 'en', 'core/base/forms', 'icon', 'Icon', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(509, 0, 'en', 'core/base/forms', 'icon_placeholder', 'Ex: fa fa-home', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(510, 0, 'en', 'core/base/forms', 'order_by', 'Order by', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(511, 0, 'en', 'core/base/forms', 'order_by_placeholder', 'Order by', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(512, 0, 'en', 'core/base/forms', 'is_featured', 'Is featured?', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(513, 0, 'en', 'core/base/forms', 'is_default', 'Is default?', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(514, 0, 'en', 'core/base/forms', 'update', 'Update', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(515, 0, 'en', 'core/base/forms', 'publish', 'Publish', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(516, 0, 'en', 'core/base/forms', 'remove_image', 'Remove image', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(517, 0, 'en', 'core/base/forms', 'remove_file', 'Remove file', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(518, 0, 'en', 'core/base/forms', 'order', 'Order', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(519, 0, 'en', 'core/base/forms', 'alias', 'Alias', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(520, 0, 'en', 'core/base/forms', 'basic_information', 'Basic information', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(521, 0, 'en', 'core/base/forms', 'short_code', 'Shortcode', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(522, 0, 'en', 'core/base/forms', 'add_short_code', 'Add a shortcode', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(523, 0, 'en', 'core/base/forms', 'add', 'Add', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(524, 0, 'en', 'core/base/forms', 'link', 'Link', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(525, 0, 'en', 'core/base/forms', 'show_hide_editor', 'Show/Hide Editor', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(526, 0, 'en', 'core/base/forms', 'basic_info_title', 'Basic information', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(527, 0, 'en', 'core/base/layouts', 'platform_admin', 'Platform Administration', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(528, 0, 'en', 'core/base/layouts', 'dashboard', 'Dashboard', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(529, 0, 'en', 'core/base/layouts', 'appearance', 'Appearance', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(530, 0, 'en', 'core/base/layouts', 'menu', 'Menu', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(531, 0, 'en', 'core/base/layouts', 'widgets', 'Widgets', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(532, 0, 'en', 'core/base/layouts', 'plugins', 'Plugins', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(533, 0, 'en', 'core/base/layouts', 'settings', 'Settings', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(534, 0, 'en', 'core/base/layouts', 'setting_general', 'General', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(535, 0, 'en', 'core/base/layouts', 'setting_email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(536, 0, 'en', 'core/base/layouts', 'system_information', 'System information', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(537, 0, 'en', 'core/base/layouts', 'theme', 'Theme', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(538, 0, 'en', 'core/base/layouts', 'copyright', 'Copyright :year &copy; :company. Version: <span>:version</span>', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(539, 0, 'en', 'core/base/layouts', 'profile', 'Profile', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(540, 0, 'en', 'core/base/layouts', 'logout', 'Logout', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(541, 0, 'en', 'core/base/layouts', 'no_search_result', 'No results found, please try with different keywords.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(542, 0, 'en', 'core/base/layouts', 'home', 'Home', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(543, 0, 'en', 'core/base/layouts', 'search', 'Search', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(544, 0, 'en', 'core/base/layouts', 'add_new', 'Add new', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(545, 0, 'en', 'core/base/layouts', 'n_a', 'N/A', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(546, 0, 'en', 'core/base/layouts', 'page_loaded_time', 'Page loaded in', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(547, 0, 'en', 'core/base/layouts', 'view_website', 'View website', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(548, 0, 'en', 'core/base/mail', 'send-fail', 'Send email failed', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(549, 0, 'en', 'core/base/notices', 'create_success_message', 'Created successfully', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(550, 0, 'en', 'core/base/notices', 'update_success_message', 'Updated successfully', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(551, 0, 'en', 'core/base/notices', 'delete_success_message', 'Deleted successfully', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(552, 0, 'en', 'core/base/notices', 'success_header', 'Success!', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(553, 0, 'en', 'core/base/notices', 'error_header', 'Error!', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(554, 0, 'en', 'core/base/notices', 'cannot_delete', 'Can not delete this record!', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(555, 0, 'en', 'core/base/notices', 'no_select', 'Please select at least one record to perform this action!', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(556, 0, 'en', 'core/base/notices', 'processing_request', 'We are processing your request.', '2020-03-29 20:06:54', '2020-04-24 09:38:37'),
(557, 0, 'en', 'core/base/notices', 'error', 'Error!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(558, 0, 'en', 'core/base/notices', 'success', 'Success!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(559, 0, 'en', 'core/base/notices', 'info', 'Info!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(560, 0, 'en', 'core/base/notices', 'enum.validate_message', 'The :attribute value you have entered is invalid.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(561, 0, 'en', 'core/base/system', 'no_select', 'Please select at least one record to take this action!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(562, 0, 'en', 'core/base/system', 'cannot_find_user', 'Unable to find specified user', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(563, 0, 'en', 'core/base/system', 'supper_revoked', 'Super user access revoked', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(564, 0, 'en', 'core/base/system', 'cannot_revoke_yourself', 'Can not revoke supper user access permission yourself!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(565, 0, 'en', 'core/base/system', 'cant_remove_supper', 'You don\'t has permission to remove this super user', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(566, 0, 'en', 'core/base/system', 'cant_find_user_with_email', 'Unable to find user with specified email address', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(567, 0, 'en', 'core/base/system', 'supper_granted', 'Super user access granted', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(568, 0, 'en', 'core/base/system', 'delete_log_success', 'Delete log file successfully!', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(569, 0, 'en', 'core/base/system', 'get_member_success', 'Member list retrieved successfully', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(570, 0, 'en', 'core/base/system', 'error_occur', 'The following errors occurred', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(571, 0, 'en', 'core/base/system', 'user_management', 'User Management', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(572, 0, 'en', 'core/base/system', 'user_management_description', 'Manage users.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(573, 0, 'en', 'core/base/system', 'role_and_permission', 'Roles and Permissions', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(574, 0, 'en', 'core/base/system', 'role_and_permission_description', 'Manage the available roles.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(575, 0, 'en', 'core/base/system', 'user.list_super', 'List Super Users', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(576, 0, 'en', 'core/base/system', 'user.email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(577, 0, 'en', 'core/base/system', 'user.last_login', 'Last Login', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(578, 0, 'en', 'core/base/system', 'user.username', 'Username', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(579, 0, 'en', 'core/base/system', 'user.add_user', 'Add Super User', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(580, 0, 'en', 'core/base/system', 'user.cancel', 'Cancel', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(581, 0, 'en', 'core/base/system', 'user.create', 'Create', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(582, 0, 'en', 'core/base/system', 'options.features', 'Feature Access Control', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(583, 0, 'en', 'core/base/system', 'options.feature_description', 'Manage the available.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(584, 0, 'en', 'core/base/system', 'options.manage_super', 'Super User Management', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(585, 0, 'en', 'core/base/system', 'options.manage_super_description', 'Add/remove super users.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(586, 0, 'en', 'core/base/system', 'options.info', 'System information', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(587, 0, 'en', 'core/base/system', 'options.info_description', 'All information about current system configuration.', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(588, 0, 'en', 'core/base/system', 'info.title', 'System information', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(589, 0, 'en', 'core/base/system', 'info.cache', 'Cache', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(590, 0, 'en', 'core/base/system', 'info.locale', 'Active locale', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(591, 0, 'en', 'core/base/system', 'info.environment', 'Environment', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(592, 1, 'en', 'core/base/system', 'disabled_in_demo_mode', 'You can not do it in demo mode!', '2020-03-29 20:06:54', '2020-03-29 20:06:54'),
(593, 0, 'en', 'core/base/system', 'report_description', 'Please share this information for troubleshooting', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(594, 0, 'en', 'core/base/system', 'get_system_report', 'Get System Report', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(595, 0, 'en', 'core/base/system', 'system_environment', 'System Environment', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(596, 0, 'en', 'core/base/system', 'framework_version', 'Framework Version', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(597, 0, 'en', 'core/base/system', 'timezone', 'Timezone', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(598, 0, 'en', 'core/base/system', 'debug_mode', 'Debug Mode', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(599, 0, 'en', 'core/base/system', 'storage_dir_writable', 'Storage Dir Writable', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(600, 0, 'en', 'core/base/system', 'cache_dir_writable', 'Cache Dir Writable', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(601, 0, 'en', 'core/base/system', 'app_size', 'App Size', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(602, 0, 'en', 'core/base/system', 'server_environment', 'Server Environment', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(603, 0, 'en', 'core/base/system', 'php_version', 'PHP Version', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(604, 0, 'en', 'core/base/system', 'server_software', 'Server Software', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(605, 0, 'en', 'core/base/system', 'server_os', 'Server OS', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(606, 0, 'en', 'core/base/system', 'database', 'Database', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(607, 0, 'en', 'core/base/system', 'ssl_installed', 'SSL Installed', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(608, 0, 'en', 'core/base/system', 'cache_driver', 'Cache Driver', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(609, 0, 'en', 'core/base/system', 'session_driver', 'Session Driver', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(610, 0, 'en', 'core/base/system', 'queue_connection', 'Queue Connection', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(611, 0, 'en', 'core/base/system', 'mbstring_ext', 'Mbstring Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(612, 0, 'en', 'core/base/system', 'openssl_ext', 'OpenSSL Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(613, 0, 'en', 'core/base/system', 'pdo_ext', 'PDO Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(614, 0, 'en', 'core/base/system', 'curl_ext', 'CURL Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(615, 0, 'en', 'core/base/system', 'exif_ext', 'Exif Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(616, 0, 'en', 'core/base/system', 'file_info_ext', 'File info Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(617, 0, 'en', 'core/base/system', 'tokenizer_ext', 'Tokenizer Ext', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(618, 0, 'en', 'core/base/system', 'extra_stats', 'Extra Stats', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(619, 0, 'en', 'core/base/system', 'installed_packages', 'Installed Packages and their version numbers', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(620, 0, 'en', 'core/base/system', 'extra_information', 'Extra Information', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(621, 0, 'en', 'core/base/system', 'copy_report', 'Copy Report', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(622, 0, 'en', 'core/base/system', 'package_name', 'Package Name', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(623, 0, 'en', 'core/base/system', 'dependency_name', 'Dependency Name', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(624, 0, 'en', 'core/base/system', 'version', 'Version', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(625, 0, 'en', 'core/base/system', 'cms_version', 'CMS Version', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(626, 0, 'en', 'core/base/tables', 'id', 'ID', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(627, 0, 'en', 'core/base/tables', 'name', 'Name', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(628, 0, 'en', 'core/base/tables', 'slug', 'Slug', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(629, 0, 'en', 'core/base/tables', 'title', 'Title', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(630, 0, 'en', 'core/base/tables', 'order_by', 'Order By', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(631, 0, 'en', 'core/base/tables', 'order', 'Order', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(632, 0, 'en', 'core/base/tables', 'status', 'Status', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(633, 0, 'en', 'core/base/tables', 'created_at', 'Created At', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(634, 0, 'en', 'core/base/tables', 'updated_at', 'Updated At', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(635, 0, 'en', 'core/base/tables', 'description', 'Description', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(636, 0, 'en', 'core/base/tables', 'operations', 'Operations', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(637, 0, 'en', 'core/base/tables', 'url', 'URL', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(638, 0, 'en', 'core/base/tables', 'author', 'Author', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(639, 0, 'en', 'core/base/tables', 'notes', 'Notes', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(640, 0, 'en', 'core/base/tables', 'column', 'Column', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(641, 0, 'en', 'core/base/tables', 'origin', 'Origin', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(642, 0, 'en', 'core/base/tables', 'after_change', 'After changes', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(643, 0, 'en', 'core/base/tables', 'views', 'Views', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(644, 0, 'en', 'core/base/tables', 'browser', 'Browser', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(645, 0, 'en', 'core/base/tables', 'session', 'Session', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(646, 0, 'en', 'core/base/tables', 'activated', 'activated', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(647, 0, 'en', 'core/base/tables', 'deactivated', 'deactivated', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(648, 0, 'en', 'core/base/tables', 'is_featured', 'Is featured', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(649, 0, 'en', 'core/base/tables', 'edit', 'Edit', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(650, 0, 'en', 'core/base/tables', 'delete', 'Delete', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(651, 0, 'en', 'core/base/tables', 'restore', 'Restore', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(652, 0, 'en', 'core/base/tables', 'activate', 'Activate', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(653, 0, 'en', 'core/base/tables', 'deactivate', 'Deactivate', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(654, 0, 'en', 'core/base/tables', 'excel', 'Excel', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(655, 0, 'en', 'core/base/tables', 'export', 'Export', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(656, 0, 'en', 'core/base/tables', 'csv', 'CSV', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(657, 0, 'en', 'core/base/tables', 'pdf', 'PDF', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(658, 0, 'en', 'core/base/tables', 'print', 'Print', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(659, 0, 'en', 'core/base/tables', 'reset', 'Reset', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(660, 0, 'en', 'core/base/tables', 'reload', 'Reload', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(661, 0, 'en', 'core/base/tables', 'display', 'Display', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(662, 0, 'en', 'core/base/tables', 'all', 'All', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(663, 0, 'en', 'core/base/tables', 'add_new', 'Add New', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(664, 0, 'en', 'core/base/tables', 'action', 'Actions', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(665, 0, 'en', 'core/base/tables', 'delete_entry', 'Delete', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(666, 0, 'en', 'core/base/tables', 'view', 'View Detail', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(667, 0, 'en', 'core/base/tables', 'save', 'Save', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(668, 0, 'en', 'core/base/tables', 'show_from', 'Show from', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(669, 0, 'en', 'core/base/tables', 'to', 'to', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(670, 0, 'en', 'core/base/tables', 'in', 'in', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(671, 0, 'en', 'core/base/tables', 'records', 'records', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(672, 0, 'en', 'core/base/tables', 'no_data', 'No data to display', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(673, 0, 'en', 'core/base/tables', 'no_record', 'No record', '2020-03-29 20:06:54', '2020-04-24 09:38:38'),
(674, 0, 'en', 'core/base/tables', 'confirm_delete', 'Confirm delete', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(675, 0, 'en', 'core/base/tables', 'confirm_delete_msg', 'Do you really want to delete this record?', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(676, 0, 'en', 'core/base/tables', 'confirm_delete_many_msg', 'Do you really want to delete selected record(s)?', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(677, 0, 'en', 'core/base/tables', 'cancel', 'Cancel', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(678, 0, 'en', 'core/base/tables', 'template', 'Template', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(679, 0, 'en', 'core/base/tables', 'email', 'Email', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(680, 0, 'en', 'core/base/tables', 'last_login', 'Last login', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(681, 0, 'en', 'core/base/tables', 'shortcode', 'Shortcode', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(682, 0, 'en', 'core/base/tables', 'image', 'Image', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(683, 0, 'en', 'core/base/tables', 'bulk_changes', 'Bulk changes', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(684, 0, 'en', 'core/base/tables', 'submit', 'Submit', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(685, 0, 'en', 'core/base/tabs', 'detail', 'Detail', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(686, 0, 'en', 'core/base/tabs', 'file', 'Files', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(687, 0, 'en', 'core/base/tabs', 'record_note', 'Record Note', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(688, 0, 'en', 'core/base/tabs', 'revision', 'Revision History', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(689, 0, 'vi', 'core/base/base', 'yes', 'Có', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(690, 0, 'vi', 'core/base/base', 'no', 'Không', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(691, 0, 'vi', 'core/base/base', 'is_default', 'Mặc định?', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(692, 0, 'vi', 'core/base/base', 'proc_close_disabled_error', 'Hàm proc_close() đã bị tắt. Vui lòng liên hệ nhà cung cấp hosting để mở hàm này. Hoặc có thể thêm vào .env: CAN_EXECUTE_COMMAND=false để tắt tính năng này.', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(693, 0, 'vi', 'core/base/cache', 'cache_management', 'Quản lý bộ nhớ đệm', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(694, 0, 'vi', 'core/base/cache', 'cache_commands', 'Các lệnh xoá bộ nhớ đệm cơ bản', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(695, 0, 'vi', 'core/base/cache', 'commands.clear_cms_cache.title', 'Xóa tất cả bộ đệm hiện có của ứng dụng', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(696, 0, 'vi', 'core/base/cache', 'commands.clear_cms_cache.description', 'Xóa các bộ nhớ đệm của ứng dụng: cơ sở dữ liệu, nội dung tĩnh... Chạy lệnh này khi bạn thử cập nhật dữ liệu nhưng giao diện không thay đổi', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(697, 0, 'vi', 'core/base/cache', 'commands.clear_cms_cache.success_msg', 'Bộ đệm đã được xóa', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(698, 0, 'vi', 'core/base/cache', 'commands.refresh_compiled_views.title', 'Làm mới bộ đệm giao diện', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(699, 0, 'vi', 'core/base/cache', 'commands.refresh_compiled_views.description', 'Làm mới bộ đệm giao diện giúp phần giao diện luôn mới nhất', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(700, 0, 'vi', 'core/base/cache', 'commands.refresh_compiled_views.success_msg', 'Bộ đệm giao diện đã được làm mới', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(701, 0, 'vi', 'core/base/cache', 'commands.clear_config_cache.title', 'Xóa bộ nhớ đệm của phần cấu hình', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(702, 0, 'vi', 'core/base/cache', 'commands.clear_config_cache.description', 'Bạn cần làm mới bộ đệm cấu hình khi bạn tạo ra sự thay đổi nào đó ở môi trường thành phẩm.', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(703, 0, 'vi', 'core/base/cache', 'commands.clear_config_cache.success_msg', 'Bộ đệm cấu hình đã được xóa', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(704, 0, 'vi', 'core/base/cache', 'commands.clear_route_cache.title', 'Xoá cache đường dẫn', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(705, 0, 'vi', 'core/base/cache', 'commands.clear_route_cache.description', 'Cần thực hiện thao tác này khi thấy không xuất hiện đường dẫn mới.', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(706, 0, 'vi', 'core/base/cache', 'commands.clear_route_cache.success_msg', 'Bộ đệm điều hướng đã bị xóa', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(707, 0, 'vi', 'core/base/cache', 'commands.clear_log.description', 'Xoá lịch sử lỗi', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(708, 0, 'vi', 'core/base/cache', 'commands.clear_log.success_msg', 'Lịch sử lỗi đã được làm sạch', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(709, 0, 'vi', 'core/base/cache', 'commands.clear_log.title', 'Xoá lịch sử lỗi', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(710, 0, 'vi', 'core/base/enums', 'statuses.draft', 'Bản nháp', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(711, 0, 'vi', 'core/base/enums', 'statuses.pending', 'Đang chờ xử lý', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(712, 0, 'vi', 'core/base/enums', 'statuses.publish', 'Đã xuất bản', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(713, 0, 'vi', 'core/base/errors', '401_title', 'Bạn không có quyền truy cập trang này', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(714, 0, 'vi', 'core/base/errors', '401_msg', '<li>Bạn không được cấp quyền truy cập bởi quản trị viên.</li>\n	                <li>Bạn sử dụng sai loại tài khoản.</li>\n	                <li>Bạn không được cấp quyền để có thể truy cập trang này</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(715, 0, 'vi', 'core/base/errors', '404_title', 'Không tìm thấy trang yêu cầu', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(716, 0, 'vi', 'core/base/errors', '404_msg', '<li>Trang bạn yêu cầu không tồn tại.</li>\n	                <li>Liên kết bạn vừa nhấn không còn được sử dụng.</li>\n	                <li>Trang này có thể đã được chuyển sang vị trí khác.</li>\n	                <li>Có thể có lỗi xảy ra.</li>\n	                <li>Bạn không được cấp quyền để có thể truy cập trang này</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(717, 0, 'vi', 'core/base/errors', '500_title', 'Không thể tải trang', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(718, 0, 'vi', 'core/base/errors', '500_msg', '<li>Trang bạn yêu cầu không tồn tại.</li>\n	                <li>Liên kết bạn vừa nhấn không còn được sử dụng.</li>\n	                <li>Trang này có thể đã được chuyển sang vị trí khác.</li>\n	                <li>Có thể có lỗi xảy ra.</li>\n	                <li>Bạn không được cấp quyền để có thể truy cập trang này</li>', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(719, 0, 'vi', 'core/base/errors', 'reasons', 'Điều này có thể xảy ra vì nhiều lý do.', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(720, 0, 'vi', 'core/base/errors', 'try_again', 'Vui lòng thử lại trong vài phút, hoặc quay trở lại trang chủ bằng cách <a href=\"http://cms.local/admin\"> nhấn vào đây </a>.', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(721, 0, 'vi', 'core/base/forms', 'choose_image', 'Chọn ảnh', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(722, 0, 'vi', 'core/base/forms', 'actions', 'Tác vụ', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(723, 0, 'vi', 'core/base/forms', 'save', 'Lưu', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(724, 0, 'vi', 'core/base/forms', 'save_and_continue', 'Lưu & chỉnh sửa', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(725, 0, 'vi', 'core/base/forms', 'image', 'Hình ảnh', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(726, 0, 'vi', 'core/base/forms', 'image_placeholder', 'Chèn đường dẫn hình ảnh hoặc nhấn nút để chọn hình', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(727, 0, 'vi', 'core/base/forms', 'create', 'Tạo mới', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(728, 0, 'vi', 'core/base/forms', 'edit', 'Sửa', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(729, 0, 'vi', 'core/base/forms', 'permalink', 'Đường dẫn', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(730, 0, 'vi', 'core/base/forms', 'ok', 'OK', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(731, 0, 'vi', 'core/base/forms', 'cancel', 'Hủy bỏ', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(732, 0, 'vi', 'core/base/forms', 'character_remain', 'kí tự còn lại', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(733, 0, 'vi', 'core/base/forms', 'template', 'Template', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(734, 0, 'vi', 'core/base/forms', 'choose_file', 'Chọn tập tin', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(735, 0, 'vi', 'core/base/forms', 'file', 'Tập tin', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(736, 0, 'vi', 'core/base/forms', 'content', 'Nội dung', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(737, 0, 'vi', 'core/base/forms', 'description', 'Mô tả', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(738, 0, 'vi', 'core/base/forms', 'name', 'Tên', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(739, 0, 'vi', 'core/base/forms', 'name_placeholder', 'Nhập tên', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(740, 0, 'vi', 'core/base/forms', 'description_placeholder', 'Mô tả ngắn', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(741, 0, 'vi', 'core/base/forms', 'parent', 'Cha', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(742, 0, 'vi', 'core/base/forms', 'icon', 'Biểu tượng', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(743, 0, 'vi', 'core/base/forms', 'order_by', 'Sắp xếp', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(744, 0, 'vi', 'core/base/forms', 'order_by_placeholder', 'Sắp xếp', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(745, 0, 'vi', 'core/base/forms', 'slug', 'Slug', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(746, 0, 'vi', 'core/base/forms', 'is_featured', 'Nổi bật?', '2020-03-29 20:06:54', '2020-04-24 09:38:39'),
(747, 0, 'vi', 'core/base/forms', 'is_default', 'Mặc định?', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(748, 0, 'vi', 'core/base/forms', 'icon_placeholder', 'Ví dụ: fa fa-home', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(749, 0, 'vi', 'core/base/forms', 'update', 'Cập nhật', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(750, 0, 'vi', 'core/base/forms', 'publish', 'Xuất bản', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(751, 0, 'vi', 'core/base/forms', 'remove_image', 'Xoá ảnh', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(752, 0, 'vi', 'core/base/forms', 'add', 'Thêm', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(753, 0, 'vi', 'core/base/forms', 'add_short_code', 'Thêm shortcode', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(754, 0, 'vi', 'core/base/forms', 'alias', 'Mã thay thế', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(755, 0, 'vi', 'core/base/forms', 'alias_placeholder', 'Mã thay thế', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(756, 0, 'vi', 'core/base/forms', 'basic_information', 'Thông tin cơ bản', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(757, 0, 'vi', 'core/base/forms', 'link', 'Liên kết', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(758, 0, 'vi', 'core/base/forms', 'order', 'Thứ tự', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(759, 0, 'vi', 'core/base/forms', 'short_code', 'Shortcode', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(760, 0, 'vi', 'core/base/forms', 'title', 'Tiêu đề', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(761, 0, 'vi', 'core/base/forms', 'value', 'Giá trị', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(762, 0, 'vi', 'core/base/forms', 'show_hide_editor', 'Ẩn/Hiện trình soạn thảo', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(763, 0, 'vi', 'core/base/forms', 'basic_info_title', 'Thông tin cơ bản', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(764, 0, 'vi', 'core/base/layouts', 'platform_admin', 'Quản trị hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(765, 0, 'vi', 'core/base/layouts', 'dashboard', 'Bảng điều khiển', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(766, 0, 'vi', 'core/base/layouts', 'appearance', 'Hiển thị', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(767, 0, 'vi', 'core/base/layouts', 'menu', 'Trình đơn', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(768, 0, 'vi', 'core/base/layouts', 'widgets', 'Tiện ích', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(769, 0, 'vi', 'core/base/layouts', 'theme_options', 'Tuỳ chọn giao diện', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(770, 0, 'vi', 'core/base/layouts', 'plugins', 'Tiện ích mở rộng', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(771, 0, 'vi', 'core/base/layouts', 'settings', 'Cài đặt', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(772, 0, 'vi', 'core/base/layouts', 'setting_general', 'Cơ bản', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(773, 0, 'vi', 'core/base/layouts', 'system_information', 'Thông tin hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(774, 0, 'vi', 'core/base/layouts', 'theme', 'Giao diện', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(775, 0, 'vi', 'core/base/layouts', 'copyright', 'Bản quyền :year &copy; :company. Phiên bản: <span>:version</span>', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(776, 0, 'vi', 'core/base/layouts', 'profile', 'Thông tin cá nhân', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(777, 0, 'vi', 'core/base/layouts', 'logout', 'Đăng xuất', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(778, 0, 'vi', 'core/base/layouts', 'no_search_result', 'Không có kết quả tìm kiếm, hãy thử lại với từ khóa khác.', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(779, 0, 'vi', 'core/base/layouts', 'home', 'Trang chủ', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(780, 0, 'vi', 'core/base/layouts', 'search', 'Tìm kiếm', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(781, 0, 'vi', 'core/base/layouts', 'add_new', 'Thêm mới', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(782, 0, 'vi', 'core/base/layouts', 'n_a', 'N/A', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(783, 0, 'vi', 'core/base/layouts', 'page_loaded_time', 'Trang tải xong trong', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(784, 0, 'vi', 'core/base/layouts', 'view_website', 'Xem trang ngoài', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(785, 0, 'vi', 'core/base/layouts', 'setting_email', 'Email', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(786, 0, 'vi', 'core/base/mail', 'send-fail', 'Gửi email không thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(787, 0, 'vi', 'core/base/mail', 'happy_birthday', 'Chúc mừng sinh nhật!', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(788, 0, 'vi', 'core/base/notices', 'create_success_message', 'Tạo thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(789, 0, 'vi', 'core/base/notices', 'update_success_message', 'Cập nhật thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:39');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(790, 0, 'vi', 'core/base/notices', 'delete_success_message', 'Xóa thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(791, 0, 'vi', 'core/base/notices', 'success_header', 'Thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(792, 0, 'vi', 'core/base/notices', 'error_header', 'Lỗi!', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(793, 0, 'vi', 'core/base/notices', 'no_select', 'Chọn ít nhất 1 trường để thực hiện hành động này!', '2020-03-29 20:06:55', '2020-04-24 09:38:39'),
(794, 0, 'vi', 'core/base/notices', 'cannot_delete', 'Không thể xóa bản ghi này', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(795, 0, 'vi', 'core/base/notices', 'processing_request', 'Hệ thống đang xử lý yêu cầu.', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(796, 0, 'vi', 'core/base/notices', 'error', 'Lỗi!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(797, 0, 'vi', 'core/base/notices', 'success', 'Thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(798, 0, 'vi', 'core/base/notices', 'info', 'Thông tin!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(799, 0, 'vi', 'core/base/system', 'no_select', 'Hãy chọn ít nhất 1 trường để thực hiện hành động này!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(800, 0, 'vi', 'core/base/system', 'cannot_find_user', 'Không thể tải được thông tin người dùng', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(801, 0, 'vi', 'core/base/system', 'supper_revoked', 'Quyền quản trị viên cao nhất đã được gỡ bỏ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(802, 0, 'vi', 'core/base/system', 'cannot_revoke_yourself', 'Không thể gỡ bỏ quyền quản trị cấp cao của chính bạn!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(803, 0, 'vi', 'core/base/system', 'cant_remove_supper', 'Bạn không có quyền xóa quản trị viên cấp cao', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(804, 0, 'vi', 'core/base/system', 'cant_find_user_with_email', 'Không thể tìm thấy người dùng với email này', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(805, 0, 'vi', 'core/base/system', 'supper_granted', 'Quyền quản trị cao nhất đã được gán', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(806, 0, 'vi', 'core/base/system', 'delete_log_success', 'Xóa tập tin log thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(807, 0, 'vi', 'core/base/system', 'get_member_success', 'Hiển thị danh sách thành viên thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(808, 0, 'vi', 'core/base/system', 'error_occur', 'Có lỗi dưới đây', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(809, 0, 'vi', 'core/base/system', 'role_and_permission', 'Phân quyền hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(810, 0, 'vi', 'core/base/system', 'role_and_permission_description', 'Quản lý những phân quyền trong hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(811, 0, 'vi', 'core/base/system', 'user.list_super', 'Danh sách quản trị viên cấp cao', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(812, 0, 'vi', 'core/base/system', 'user.username', 'Tên đăng nhập', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(813, 0, 'vi', 'core/base/system', 'user.email', 'Email', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(814, 0, 'vi', 'core/base/system', 'user.last_login', 'Đăng nhập lần cuối	', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(815, 0, 'vi', 'core/base/system', 'user.add_user', 'Thêm quản trị viên cấp cao', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(816, 0, 'vi', 'core/base/system', 'user.cancel', 'Hủy bỏ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(817, 0, 'vi', 'core/base/system', 'user.create', 'Thêm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(818, 0, 'vi', 'core/base/system', 'options.features', 'Kiểm soát truy cập các tính năng', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(819, 0, 'vi', 'core/base/system', 'options.feature_description', 'Bật/tắt các tính năng.', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(820, 0, 'vi', 'core/base/system', 'options.manage_super', 'Quản lý quản trị viên cấp cao', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(821, 0, 'vi', 'core/base/system', 'options.manage_super_description', 'Thêm/xóa quản trị viên cấp cao', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(822, 0, 'vi', 'core/base/system', 'options.info', 'Thông tin hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(823, 0, 'vi', 'core/base/system', 'options.info_description', 'Những thông tin về cấu hình hiện tại của hệ thống.', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(824, 0, 'vi', 'core/base/system', 'info.title', 'Thông tin hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(825, 0, 'vi', 'core/base/system', 'info.cache', 'Bộ nhớ đệm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(826, 0, 'vi', 'core/base/system', 'info.environment', 'Môi trường', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(827, 0, 'vi', 'core/base/system', 'info.locale', 'Ngôn ngữ hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(828, 0, 'vi', 'core/base/system', 'user_management', 'Quản lý thành viên', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(829, 0, 'vi', 'core/base/system', 'user_management_description', 'Quản lý người dùng trong hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(830, 0, 'vi', 'core/base/system', 'app_size', 'Kích thước ứng dụng', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(831, 0, 'vi', 'core/base/system', 'cache_dir_writable', 'Có thể ghi bộ nhớ đệm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(832, 0, 'vi', 'core/base/system', 'cache_driver', 'Loại lưu trữ bộ nhớ đệm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(833, 0, 'vi', 'core/base/system', 'copy_report', 'Sao chép báo cáo', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(834, 0, 'vi', 'core/base/system', 'curl_ext', 'CURL Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(835, 0, 'vi', 'core/base/system', 'database', 'Hệ thống dữ liệu', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(836, 0, 'vi', 'core/base/system', 'debug_mode', 'Chế độ sửa lỗi', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(837, 0, 'vi', 'core/base/system', 'dependency_name', 'Tên gói', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(838, 0, 'vi', 'core/base/system', 'exif_ext', 'Exif Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(839, 0, 'vi', 'core/base/system', 'extra_information', 'Thông tin mở rộng', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(840, 0, 'vi', 'core/base/system', 'extra_stats', 'Thống kê thêm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(841, 0, 'vi', 'core/base/system', 'file_info_ext', 'File info Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(842, 0, 'vi', 'core/base/system', 'framework_version', 'Phiên bản framework', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(843, 0, 'vi', 'core/base/system', 'get_system_report', 'Lấy thông tin hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(844, 0, 'vi', 'core/base/system', 'installed_packages', 'Các gói đã cài đặt và phiên bản', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(845, 0, 'vi', 'core/base/system', 'mbstring_ext', 'Mbstring Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(846, 0, 'vi', 'core/base/system', 'openssl_ext', 'OpenSSL Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(847, 0, 'vi', 'core/base/system', 'package_name', 'Tên gói', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(848, 0, 'vi', 'core/base/system', 'pdo_ext', 'PDO Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(849, 0, 'vi', 'core/base/system', 'php_version', 'Phiên bản PHP', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(850, 0, 'vi', 'core/base/system', 'report_description', 'Vui lòng chia sẻ thông tin này nhằm mục đích điều tra nguyên nhân gây lỗi', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(851, 0, 'vi', 'core/base/system', 'server_environment', 'Môi trường máy chủ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(852, 0, 'vi', 'core/base/system', 'server_os', 'Hệ điều hành của máy chủ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(853, 0, 'vi', 'core/base/system', 'server_software', 'Phần mềm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(854, 0, 'vi', 'core/base/system', 'session_driver', 'Loại lưu trữ phiên làm việc', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(855, 0, 'vi', 'core/base/system', 'ssl_installed', 'Đã cài đặt chứng chỉ SSL', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(856, 0, 'vi', 'core/base/system', 'storage_dir_writable', 'Có thể lưu trữ dữ liệu tạm', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(857, 0, 'vi', 'core/base/system', 'system_environment', 'Môi trường hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(858, 0, 'vi', 'core/base/system', 'timezone', 'Múi giờ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(859, 0, 'vi', 'core/base/system', 'tokenizer_ext', 'Tokenizer Ext', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(860, 0, 'vi', 'core/base/system', 'version', 'Phiên bản', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(861, 0, 'vi', 'core/base/system', 'cms_version', 'Phiên bản CMS', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(862, 0, 'vi', 'core/base/tables', 'filter_enabled', 'Tìm kiếm nâng cao đã được kích hoạt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(863, 0, 'vi', 'core/base/tables', 'id', 'ID', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(864, 0, 'vi', 'core/base/tables', 'name', 'Tên', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(865, 0, 'vi', 'core/base/tables', 'order_by', 'Thứ tự', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(866, 0, 'vi', 'core/base/tables', 'status', 'Trạng thái', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(867, 0, 'vi', 'core/base/tables', 'created_at', 'Ngày tạo', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(868, 0, 'vi', 'core/base/tables', 'updated_at', 'Ngày cập nhật', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(869, 0, 'vi', 'core/base/tables', 'operations', 'Tác vụ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(870, 0, 'vi', 'core/base/tables', 'loading_data', 'Đang tải dữ liệu từ server', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(871, 0, 'vi', 'core/base/tables', 'url', 'URL', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(872, 0, 'vi', 'core/base/tables', 'author', 'Người tạo', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(873, 0, 'vi', 'core/base/tables', 'column', 'Cột', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(874, 0, 'vi', 'core/base/tables', 'origin', 'Bản cũ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(875, 0, 'vi', 'core/base/tables', 'after_change', 'Sau khi thay đổi', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(876, 0, 'vi', 'core/base/tables', 'notes', 'Ghi chú', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(877, 0, 'vi', 'core/base/tables', 'activated', 'kích hoạt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(878, 0, 'vi', 'core/base/tables', 'browser', 'Trình duyệt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(879, 0, 'vi', 'core/base/tables', 'deactivated', 'hủy kích hoạt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(880, 0, 'vi', 'core/base/tables', 'description', 'Mô tả', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(881, 0, 'vi', 'core/base/tables', 'session', 'Phiên', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(882, 0, 'vi', 'core/base/tables', 'views', 'Lượt xem', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(883, 0, 'vi', 'core/base/tables', 'restore', 'Khôi phục', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(884, 0, 'vi', 'core/base/tables', 'edit', 'Sửa', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(885, 0, 'vi', 'core/base/tables', 'delete', 'Xóa', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(886, 0, 'vi', 'core/base/tables', 'action', 'Hành động', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(887, 0, 'vi', 'core/base/tables', 'activate', 'Kích hoạt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(888, 0, 'vi', 'core/base/tables', 'add_new', 'Thêm mới', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(889, 0, 'vi', 'core/base/tables', 'all', 'Tất cả', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(890, 0, 'vi', 'core/base/tables', 'cancel', 'Hủy bỏ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(891, 0, 'vi', 'core/base/tables', 'confirm_delete', 'Xác nhận xóa', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(892, 0, 'vi', 'core/base/tables', 'confirm_delete_msg', 'Bạn có chắc chắn muốn xóa bản ghi này?', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(893, 0, 'vi', 'core/base/tables', 'csv', 'CSV', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(894, 0, 'vi', 'core/base/tables', 'deactivate', 'Hủy kích hoạt', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(895, 0, 'vi', 'core/base/tables', 'delete_entry', 'Xóa bản ghi', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(896, 0, 'vi', 'core/base/tables', 'display', 'Hiển thị', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(897, 0, 'vi', 'core/base/tables', 'excel', 'Excel', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(898, 0, 'vi', 'core/base/tables', 'export', 'Xuất bản', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(899, 0, 'vi', 'core/base/tables', 'filter', 'Nhập từ khóa...', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(900, 0, 'vi', 'core/base/tables', 'filtered_from', 'được lọc từ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(901, 0, 'vi', 'core/base/tables', 'in', 'trong tổng số', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(902, 0, 'vi', 'core/base/tables', 'loading', 'Đang tải dữ liệu từ hệ thống', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(903, 0, 'vi', 'core/base/tables', 'no_data', 'Không có dữ liệu để hiển thị', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(904, 0, 'vi', 'core/base/tables', 'no_record', 'Không có dữ liệu', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(905, 0, 'vi', 'core/base/tables', 'pdf', 'PDF', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(906, 0, 'vi', 'core/base/tables', 'print', 'In', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(907, 0, 'vi', 'core/base/tables', 'records', 'bản ghi', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(908, 0, 'vi', 'core/base/tables', 'reload', 'Tải lại', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(909, 0, 'vi', 'core/base/tables', 'reset', 'Làm mới', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(910, 0, 'vi', 'core/base/tables', 'save', 'Lưu', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(911, 0, 'vi', 'core/base/tables', 'show_from', 'Hiển thị từ', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(912, 0, 'vi', 'core/base/tables', 'template', 'Giao diện', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(913, 0, 'vi', 'core/base/tables', 'to', 'đến', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(914, 0, 'vi', 'core/base/tables', 'view', 'Xem chi tiết', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(915, 0, 'vi', 'core/base/tables', 'email', 'Email', '2020-03-29 20:06:55', '2020-04-24 09:38:40'),
(916, 0, 'vi', 'core/base/tables', 'image', 'Hình ảnh', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(917, 0, 'vi', 'core/base/tables', 'is_featured', 'Nổi bật', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(918, 0, 'vi', 'core/base/tables', 'last_login', 'Lần cuối đăng nhập', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(919, 0, 'vi', 'core/base/tables', 'order', 'Thứ tự', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(920, 0, 'vi', 'core/base/tables', 'shortcode', 'Shortcode', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(921, 0, 'vi', 'core/base/tables', 'slug', 'Slug', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(922, 0, 'vi', 'core/base/tables', 'title', 'Tiêu đề', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(923, 0, 'vi', 'core/base/tabs', 'detail', 'Chi tiết', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(924, 0, 'vi', 'core/base/tabs', 'file', 'Tập tin', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(925, 0, 'vi', 'core/base/tabs', 'record_note', 'Ghi chú', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(926, 0, 'vi', 'core/base/tabs', 'revision', 'Lịch sử thay đổi', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(927, 0, 'en', 'core/dashboard/dashboard', 'update_position_success', 'Update widget position successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(928, 0, 'en', 'core/dashboard/dashboard', 'hide_success', 'Update widget successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(929, 0, 'en', 'core/dashboard/dashboard', 'confirm_hide', 'Are you sure?', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(930, 0, 'en', 'core/dashboard/dashboard', 'hide_message', 'Do you really want to hide this widget? It will be disappear on Dashboard!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(931, 0, 'en', 'core/dashboard/dashboard', 'confirm_hide_btn', 'Yes, hide this widget', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(932, 0, 'en', 'core/dashboard/dashboard', 'cancel_hide_btn', 'Cancel', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(933, 0, 'en', 'core/dashboard/dashboard', 'collapse_expand', 'Collapse/Expand', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(934, 0, 'en', 'core/dashboard/dashboard', 'hide', 'Hide', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(935, 0, 'en', 'core/dashboard/dashboard', 'reload', 'Reload', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(936, 0, 'en', 'core/dashboard/dashboard', 'save_setting_success', 'Save widget settings successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(937, 0, 'en', 'core/dashboard/dashboard', 'widget_not_exists', 'Widget is not exits!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(938, 0, 'en', 'core/dashboard/dashboard', 'manage_widgets', 'Manage Widgets', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(939, 0, 'en', 'core/dashboard/dashboard', 'fullscreen', 'Full screen', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(940, 0, 'en', 'core/dashboard/dashboard', 'title', 'Dashboard', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(941, 0, 'vi', 'core/dashboard/dashboard', 'cancel_hide_btn', 'Hủy bỏ', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(942, 0, 'vi', 'core/dashboard/dashboard', 'collapse_expand', 'Mở rộng', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(943, 0, 'vi', 'core/dashboard/dashboard', 'confirm_hide', 'Bạn có chắc?', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(944, 0, 'vi', 'core/dashboard/dashboard', 'confirm_hide_btn', 'Vâng, ẩn tiện ích này', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(945, 0, 'vi', 'core/dashboard/dashboard', 'hide', 'Ẩn', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(946, 0, 'vi', 'core/dashboard/dashboard', 'hide_message', 'Bạn có chắc chắn muốn ẩn tiện ích này? Nó sẽ không được hiển thị trên trang chủ nữa!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(947, 0, 'vi', 'core/dashboard/dashboard', 'hide_success', 'Ẩn tiện ích thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(948, 0, 'vi', 'core/dashboard/dashboard', 'manage_widgets', 'Quản lý tiện ích', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(949, 0, 'vi', 'core/dashboard/dashboard', 'reload', 'Làm mới', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(950, 0, 'vi', 'core/dashboard/dashboard', 'save_setting_success', 'Lưu tiện ích thành công', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(951, 0, 'vi', 'core/dashboard/dashboard', 'update_position_success', 'Cập nhật vị trí tiện ích thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(952, 0, 'vi', 'core/dashboard/dashboard', 'widget_not_exists', 'Tiện ích này không tồn tại!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(953, 0, 'vi', 'core/dashboard/dashboard', 'fullscreen', 'Toàn màn hình', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(954, 0, 'vi', 'core/dashboard/dashboard', 'title', 'Trang quản trị', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(955, 0, 'en', 'core/media/media', 'filter', 'Filter', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(956, 0, 'en', 'core/media/media', 'everything', 'Everything', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(957, 0, 'en', 'core/media/media', 'image', 'Image', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(958, 0, 'en', 'core/media/media', 'video', 'Video', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(959, 0, 'en', 'core/media/media', 'document', 'Document', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(960, 0, 'en', 'core/media/media', 'view_in', 'View in', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(961, 0, 'en', 'core/media/media', 'all_media', 'All media', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(962, 0, 'en', 'core/media/media', 'trash', 'Trash', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(963, 0, 'en', 'core/media/media', 'recent', 'Recent', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(964, 0, 'en', 'core/media/media', 'favorites', 'Favorites', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(965, 0, 'en', 'core/media/media', 'upload', 'Upload', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(966, 0, 'en', 'core/media/media', 'add_from', 'Add from', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(967, 0, 'en', 'core/media/media', 'youtube', 'Youtube', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(968, 0, 'en', 'core/media/media', 'vimeo', 'Vimeo', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(969, 0, 'en', 'core/media/media', 'vine', 'Vine', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(970, 0, 'en', 'core/media/media', 'daily_motion', 'Dailymotion', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(971, 0, 'en', 'core/media/media', 'create_folder', 'Create folder', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(972, 0, 'en', 'core/media/media', 'refresh', 'Refresh', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(973, 0, 'en', 'core/media/media', 'empty_trash', 'Empty trash', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(974, 0, 'en', 'core/media/media', 'search_file_and_folder', 'Search file and folder', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(975, 0, 'en', 'core/media/media', 'sort', 'Sort', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(976, 0, 'en', 'core/media/media', 'file_name_asc', 'File name - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(977, 0, 'en', 'core/media/media', 'file_name_desc', 'File name - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(978, 0, 'en', 'core/media/media', 'created_date_asc', 'Created date - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(979, 0, 'en', 'core/media/media', 'created_date_desc', 'Created_date - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(980, 0, 'en', 'core/media/media', 'uploaded_date_asc', 'Uploaded date - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(981, 0, 'en', 'core/media/media', 'uploaded_date_desc', 'Uploaded date - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(982, 0, 'en', 'core/media/media', 'size_asc', 'Size - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(983, 0, 'en', 'core/media/media', 'size_desc', 'Size - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(984, 0, 'en', 'core/media/media', 'actions', 'Actions', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(985, 0, 'en', 'core/media/media', 'nothing_is_selected', 'Nothing is selected', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(986, 0, 'en', 'core/media/media', 'insert', 'Insert', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(987, 0, 'en', 'core/media/media', 'coming_soon', 'Coming soon', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(988, 0, 'en', 'core/media/media', 'add_from_youtube', 'Add from youtube', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(989, 0, 'en', 'core/media/media', 'playlist', 'Playlist', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(990, 0, 'en', 'core/media/media', 'add_url', 'Add URL', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(991, 0, 'en', 'core/media/media', 'folder_name', 'Folder name', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(992, 0, 'en', 'core/media/media', 'create', 'Create', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(993, 0, 'en', 'core/media/media', 'rename', 'Rename', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(994, 0, 'en', 'core/media/media', 'close', 'Close', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(995, 0, 'en', 'core/media/media', 'save_changes', 'Save changes', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(996, 0, 'en', 'core/media/media', 'move_to_trash', 'Move items to trash', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(997, 0, 'en', 'core/media/media', 'confirm_trash', 'Are you sure you want to move these items to trash?', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(998, 0, 'en', 'core/media/media', 'confirm', 'Confirm', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(999, 0, 'en', 'core/media/media', 'confirm_delete', 'Delete item(s)', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1000, 0, 'en', 'core/media/media', 'confirm_delete_description', 'Your request cannot rollback. Are you sure you wanna delete these items?', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1001, 0, 'en', 'core/media/media', 'empty_trash_title', 'Empty trash', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1002, 0, 'en', 'core/media/media', 'empty_trash_description', 'Your request cannot rollback.Are you sure you wanna remove all items in trash?', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1003, 0, 'en', 'core/media/media', 'up_level', 'Up one level', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1004, 0, 'en', 'core/media/media', 'upload_progress', 'Upload progress', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1005, 0, 'en', 'core/media/media', 'folder_created', 'Folder is created successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1006, 0, 'en', 'core/media/media', 'gallery', 'Media gallery', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1007, 0, 'en', 'core/media/media', 'trash_error', 'Error when delete selected item(s)', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1008, 0, 'en', 'core/media/media', 'trash_success', 'Moved selected item(s) to trash successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1009, 0, 'en', 'core/media/media', 'restore_error', 'Error when restore selected item(s)', '2020-03-29 20:06:55', '2020-04-24 09:38:41'),
(1010, 0, 'en', 'core/media/media', 'restore_success', 'Restore selected item(s) successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1011, 0, 'en', 'core/media/media', 'copy_success', 'Copied selected item(s) successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1012, 0, 'en', 'core/media/media', 'delete_success', 'Deleted selected item(s) successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1013, 0, 'en', 'core/media/media', 'favorite_success', 'Favorite selected item(s) successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1014, 0, 'en', 'core/media/media', 'remove_favorite_success', 'Remove selected item(s) from favorites successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1015, 0, 'en', 'core/media/media', 'rename_error', 'Error when rename item(s)', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1016, 0, 'en', 'core/media/media', 'rename_success', 'Rename selected item(s) successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1017, 0, 'en', 'core/media/media', 'empty_trash_success', 'Empty trash successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1018, 0, 'en', 'core/media/media', 'invalid_action', 'Invalid action!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1019, 0, 'en', 'core/media/media', 'file_not_exists', 'File is not exists!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1020, 0, 'en', 'core/media/media', 'download_file_error', 'Error when downloading files!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1021, 0, 'en', 'core/media/media', 'missing_zip_archive_extension', 'Please enable ZipArchive extension to download file!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1022, 0, 'en', 'core/media/media', 'can_not_download_file', 'Can not download this file!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1023, 0, 'en', 'core/media/media', 'invalid_request', 'Invalid request!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1024, 0, 'en', 'core/media/media', 'add_success', 'Add item successfully!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1025, 0, 'en', 'core/media/media', 'file_too_big', 'File too big. Max file upload is :size bytes', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1026, 0, 'en', 'core/media/media', 'can_not_detect_file_type', 'File type is not allowed or can not detect file type!', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1027, 0, 'en', 'core/media/media', 'upload_failed', 'The file is NOT uploaded completely. The server allows max upload file size is :size . Please check your file size OR try to upload again in case of having network errors', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1028, 0, 'en', 'core/media/media', 'menu_name', 'Media', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1029, 0, 'en', 'core/media/media', 'add', 'Add media', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1030, 0, 'en', 'core/media/media', 'javascript.name', 'Name', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1031, 0, 'en', 'core/media/media', 'javascript.url', 'Url', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1032, 0, 'en', 'core/media/media', 'javascript.full_url', 'Full url', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1033, 0, 'en', 'core/media/media', 'javascript.size', 'Size', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1034, 0, 'en', 'core/media/media', 'javascript.mime_type', 'Type', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1035, 0, 'en', 'core/media/media', 'javascript.created_at', 'Uploaded at', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1036, 0, 'en', 'core/media/media', 'javascript.updated_at', 'Modified at', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1037, 0, 'en', 'core/media/media', 'javascript.nothing_selected', 'Nothing is selected', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1038, 0, 'en', 'core/media/media', 'javascript.visit_link', 'Open link', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1039, 0, 'en', 'core/media/media', 'javascript.add_from.youtube.original_msg', 'Please input Youtube URL', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1040, 0, 'en', 'core/media/media', 'javascript.add_from.youtube.no_api_key_msg', 'Please specify the Youtube API key', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1041, 0, 'en', 'core/media/media', 'javascript.add_from.youtube.invalid_url_msg', 'Your link is not belongs to Youtube', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1042, 0, 'en', 'core/media/media', 'javascript.add_from.youtube.error_msg', 'Some error occurred...', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1043, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.icon', 'fas fa-cloud-upload-alt', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1044, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.title', 'Drop files and folders here', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1045, 0, 'en', 'core/media/media', 'javascript.no_item.all_media.message', 'Or use the upload button above', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1046, 0, 'en', 'core/media/media', 'javascript.no_item.trash.icon', 'fas fa-trash-alt', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1047, 0, 'en', 'core/media/media', 'javascript.no_item.trash.title', 'There is nothing in your trash currently', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1048, 0, 'en', 'core/media/media', 'javascript.no_item.trash.message', 'Delete files to move them to trash automatically. Delete files from trash to remove them permanently', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1049, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.icon', 'fas fa-star', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1050, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.title', 'You have not added anything to your favorites yet', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1051, 0, 'en', 'core/media/media', 'javascript.no_item.favorites.message', 'Add files to favorites to easily find them later', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1052, 0, 'en', 'core/media/media', 'javascript.no_item.recent.icon', 'far fa-clock', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1053, 0, 'en', 'core/media/media', 'javascript.no_item.recent.title', 'You did not opened anything yet', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1054, 0, 'en', 'core/media/media', 'javascript.no_item.recent.message', 'All recent files that you opened will be appeared here', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1055, 0, 'en', 'core/media/media', 'javascript.no_item.default.icon', 'fas fa-sync', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1056, 0, 'en', 'core/media/media', 'javascript.no_item.default.title', 'No items', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1057, 0, 'en', 'core/media/media', 'javascript.no_item.default.message', 'This directory has no item', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1058, 0, 'en', 'core/media/media', 'javascript.clipboard.success', 'These file links has been copied to clipboard', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1059, 0, 'en', 'core/media/media', 'javascript.message.error_header', 'Error', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1060, 0, 'en', 'core/media/media', 'javascript.message.success_header', 'Success', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1061, 0, 'en', 'core/media/media', 'javascript.download.error', 'No files selected or cannot download these files', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1062, 0, 'en', 'core/media/media', 'javascript.actions_list.basic.preview', 'Preview', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1063, 0, 'en', 'core/media/media', 'javascript.actions_list.file.copy_link', 'Copy link', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1064, 0, 'en', 'core/media/media', 'javascript.actions_list.file.rename', 'Rename', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1065, 0, 'en', 'core/media/media', 'javascript.actions_list.file.make_copy', 'Make a copy', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1066, 0, 'en', 'core/media/media', 'javascript.actions_list.user.favorite', 'Add to favorite', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1067, 0, 'en', 'core/media/media', 'javascript.actions_list.user.remove_favorite', 'Remove favorite', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1068, 0, 'en', 'core/media/media', 'javascript.actions_list.other.download', 'Download', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1069, 0, 'en', 'core/media/media', 'javascript.actions_list.other.trash', 'Move to trash', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1070, 0, 'en', 'core/media/media', 'javascript.actions_list.other.delete', 'Delete permanently', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1071, 0, 'en', 'core/media/media', 'javascript.actions_list.other.restore', 'Restore', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1072, 0, 'en', 'core/media/media', 'name_invalid', 'The folder name has invalid character(s).', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1073, 0, 'vi', 'core/media/media', 'filter', 'Lọc', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1074, 0, 'vi', 'core/media/media', 'everything', 'Tất cả', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1075, 0, 'vi', 'core/media/media', 'image', 'Hình ảnh', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1076, 0, 'vi', 'core/media/media', 'video', 'Phim', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1077, 0, 'vi', 'core/media/media', 'document', 'Tài liệu', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1078, 0, 'vi', 'core/media/media', 'view_in', 'Chế độ xem', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1079, 0, 'vi', 'core/media/media', 'all_media', 'Tất cả tập tin', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1080, 0, 'vi', 'core/media/media', 'trash', 'Thùng rác', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1081, 0, 'vi', 'core/media/media', 'recent', 'Gần đây', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1082, 0, 'vi', 'core/media/media', 'favorites', 'Được gắn dấu sao', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1083, 0, 'vi', 'core/media/media', 'upload', 'Tải lên', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1084, 0, 'vi', 'core/media/media', 'add_from', 'Thêm từ', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1085, 0, 'vi', 'core/media/media', 'youtube', 'Youtube', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1086, 0, 'vi', 'core/media/media', 'vimeo', 'Vimeo', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1087, 0, 'vi', 'core/media/media', 'vine', 'Vine', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1088, 0, 'vi', 'core/media/media', 'daily_motion', 'Dailymotion', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1089, 0, 'vi', 'core/media/media', 'create_folder', 'Tạo thư mục', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1090, 0, 'vi', 'core/media/media', 'refresh', 'Làm mới', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1091, 0, 'vi', 'core/media/media', 'empty_trash', 'Dọn thùng rác', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1092, 0, 'vi', 'core/media/media', 'search_file_and_folder', 'Tìm kiếm tập tin và thư mục', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1093, 0, 'vi', 'core/media/media', 'sort', 'Sắp xếp', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1094, 0, 'vi', 'core/media/media', 'file_name_asc', 'Tên tập tin - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1095, 0, 'vi', 'core/media/media', 'file_name_desc', 'Tên tập tin - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1096, 0, 'vi', 'core/media/media', 'created_date_asc', 'Ngày tạo - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1097, 0, 'vi', 'core/media/media', 'created_date_desc', 'Ngày tạo - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1098, 0, 'vi', 'core/media/media', 'uploaded_date_asc', 'Ngày tải lên - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1099, 0, 'vi', 'core/media/media', 'uploaded_date_desc', 'Ngày tải lên - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1100, 0, 'vi', 'core/media/media', 'size_asc', 'Kích thước - ASC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1101, 0, 'vi', 'core/media/media', 'size_desc', 'Kích thước - DESC', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1102, 0, 'vi', 'core/media/media', 'actions', 'Hành động', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1103, 0, 'vi', 'core/media/media', 'nothing_is_selected', 'Không có tập tin nào được chọn', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1104, 0, 'vi', 'core/media/media', 'insert', 'Chèn', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1105, 0, 'vi', 'core/media/media', 'coming_soon', 'Đang phát triển', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1106, 0, 'vi', 'core/media/media', 'add_from_youtube', 'Thêm từ youtube', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1107, 0, 'vi', 'core/media/media', 'playlist', 'Playlist', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1108, 0, 'vi', 'core/media/media', 'add_url', 'Thêm đường dẫn', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1109, 0, 'vi', 'core/media/media', 'folder_name', 'Tên thư mục', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1110, 0, 'vi', 'core/media/media', 'create', 'Tạo', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1111, 0, 'vi', 'core/media/media', 'rename', 'Đổi tên', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1112, 0, 'vi', 'core/media/media', 'close', 'Đóng', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1113, 0, 'vi', 'core/media/media', 'save_changes', 'Lưu thay đổi', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1114, 0, 'vi', 'core/media/media', 'move_to_trash', 'Đưa vào thùng rác', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1115, 0, 'vi', 'core/media/media', 'confirm_trash', 'Bạn có chắc chắn muốn bỏ những tập tin này vào thùng rác?', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1116, 0, 'vi', 'core/media/media', 'confirm', 'Xác nhận', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1117, 0, 'vi', 'core/media/media', 'confirm_delete', 'Xóa tập tin', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1118, 0, 'vi', 'core/media/media', 'confirm_delete_description', 'Hành động này không thể khôi phục. Bạn có chắc chắn muốn xóa các tập tin này?', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1119, 0, 'vi', 'core/media/media', 'empty_trash_title', 'Dọn sạch thùng rác', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1120, 0, 'vi', 'core/media/media', 'empty_trash_description', 'Hành động này không thể khôi phục. Bạn có chắc chắn muốn dọn sạch thùng rác?', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1121, 0, 'vi', 'core/media/media', 'up_level', 'Quay lại một cấp', '2020-03-29 20:06:55', '2020-04-24 09:38:42'),
(1122, 0, 'vi', 'core/media/media', 'upload_progress', 'Tiến trình tải lên', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1123, 0, 'vi', 'core/media/media', 'name_reserved', 'Tên này không được phép đặt', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1124, 0, 'vi', 'core/media/media', 'folder_created', 'Tạo thư mục thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1125, 0, 'vi', 'core/media/media', 'gallery', 'Thư viện tập tin', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1126, 0, 'vi', 'core/media/media', 'trash_error', 'Có lỗi khi xóa tập tin/thư mục', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1127, 0, 'vi', 'core/media/media', 'trash_success', 'Xóa tập tin/thư mục thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1128, 0, 'vi', 'core/media/media', 'restore_error', 'Có lỗi trong quá trình khôi phục', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1129, 0, 'vi', 'core/media/media', 'restore_success', 'Khôi phục thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1130, 0, 'vi', 'core/media/media', 'copy_success', 'Sao chép thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1131, 0, 'vi', 'core/media/media', 'delete_success', 'Xóa thành công!', '2020-03-29 20:06:55', '2020-04-24 09:38:43'),
(1132, 0, 'vi', 'core/media/media', 'favorite_success', 'Thêm dấu sao thành công!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1133, 0, 'vi', 'core/media/media', 'remove_favorite_success', 'Bỏ dấu sao thành công!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1134, 0, 'vi', 'core/media/media', 'rename_error', 'Có lỗi trong quá trình đổi tên', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1135, 0, 'vi', 'core/media/media', 'rename_success', 'Đổi tên thành công!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1136, 0, 'vi', 'core/media/media', 'invalid_action', 'Hành động không hợp lệ!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1137, 0, 'vi', 'core/media/media', 'file_not_exists', 'Tập tin không tồn tại!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1138, 0, 'vi', 'core/media/media', 'download_file_error', 'Có lỗi trong quá trình tải xuống tập tin!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1139, 0, 'vi', 'core/media/media', 'missing_zip_archive_extension', 'Hãy bật ZipArchive extension để tải tập tin!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1140, 0, 'vi', 'core/media/media', 'can_not_download_file', 'Không thể tải tập tin!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1141, 0, 'vi', 'core/media/media', 'invalid_request', 'Yêu cầu không hợp lệ!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1142, 0, 'vi', 'core/media/media', 'add_success', 'Thêm thành công!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1143, 0, 'vi', 'core/media/media', 'file_too_big', 'Tập tin quá lớn. Giới hạn tải lên là :size bytes', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1144, 0, 'vi', 'core/media/media', 'can_not_detect_file_type', 'Loại tập tin không hợp lệ hoặc không thể xác định loại tập tin!', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1145, 0, 'vi', 'core/media/media', 'upload_failed', 'The file is NOT uploaded completely. The server allows max upload file size is :size . Please check your file size OR try to upload again in case of having network errors', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1146, 0, 'vi', 'core/media/media', 'menu_name', 'Quản lý tập tin', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1147, 0, 'vi', 'core/media/media', 'add', 'Thêm tập tin', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1148, 0, 'vi', 'core/media/media', 'javascript.name', 'Tên', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1149, 0, 'vi', 'core/media/media', 'javascript.url', 'Đường dẫn', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1150, 0, 'vi', 'core/media/media', 'javascript.full_url', 'Đường dẫn tuyệt đối', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1151, 0, 'vi', 'core/media/media', 'javascript.size', 'Kích thước', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1152, 0, 'vi', 'core/media/media', 'javascript.mime_type', 'Loại', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1153, 0, 'vi', 'core/media/media', 'javascript.created_at', 'Được tải lên lúc', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1154, 0, 'vi', 'core/media/media', 'javascript.updated_at', 'Được chỉnh sửa lúc', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1155, 0, 'vi', 'core/media/media', 'javascript.nothing_selected', 'Bạn chưa chọn tập tin nào', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1156, 0, 'vi', 'core/media/media', 'javascript.visit_link', 'Mở liên kết', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1157, 0, 'vi', 'core/media/media', 'javascript.add_from.youtube.original_msg', 'Vui lòng nhập chính xác đường dẫn Youtube', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1158, 0, 'vi', 'core/media/media', 'javascript.add_from.youtube.no_api_key_msg', 'Vui lòng khai báo API key của Youtube', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1159, 0, 'vi', 'core/media/media', 'javascript.add_from.youtube.invalid_url_msg', 'Liên kết này không dẫn đến Youtube', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1160, 0, 'vi', 'core/media/media', 'javascript.add_from.youtube.error_msg', 'Có lỗi xảy ra trong tiến trình thực hiện...', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1161, 0, 'vi', 'core/media/media', 'javascript.no_item.all_media.icon', 'fas fa-cloud-upload-alt', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1162, 0, 'vi', 'core/media/media', 'javascript.no_item.all_media.title', 'Bạn có thể kéo thả tập tin vào đây để tải lên', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1163, 0, 'vi', 'core/media/media', 'javascript.no_item.all_media.message', 'Hoặc có thể bấm nút Tải lên ở phía trên', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1164, 0, 'vi', 'core/media/media', 'javascript.no_item.trash.icon', 'fas fa-trash-alt', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1165, 0, 'vi', 'core/media/media', 'javascript.no_item.trash.title', 'Hiện tại không có tập tin nào trong thùng rác', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1166, 0, 'vi', 'core/media/media', 'javascript.no_item.trash.message', 'Xóa tập tin sẽ đem tập tin lưu vào thùng rác. Xóa tập tin trong thùng rác sẽ xóa vĩnh viễn.', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1167, 0, 'vi', 'core/media/media', 'javascript.no_item.favorites.icon', 'fas fa-star', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1168, 0, 'vi', 'core/media/media', 'javascript.no_item.favorites.title', 'Bạn chưa đặt tập tin nào vào mục yêu thích', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1169, 0, 'vi', 'core/media/media', 'javascript.no_item.favorites.message', 'Thêm tập tin vào mục yêu thích để tìm kiếm chúng dễ dàng sau này.', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1170, 0, 'vi', 'core/media/media', 'javascript.no_item.recent.icon', 'far fa-clock', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1171, 0, 'vi', 'core/media/media', 'javascript.no_item.recent.title', 'Bạn chưa mở tập tin nào.', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1172, 0, 'vi', 'core/media/media', 'javascript.no_item.recent.message', 'Mục này hiển thị các tập tin bạn đã xem gần đây.', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1173, 0, 'vi', 'core/media/media', 'javascript.no_item.default.icon', 'fas fa-sync', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1174, 0, 'vi', 'core/media/media', 'javascript.no_item.default.title', 'Thư mục trống', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1175, 0, 'vi', 'core/media/media', 'javascript.no_item.default.message', 'Thư mục này chưa có tập tin nào', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1176, 0, 'vi', 'core/media/media', 'javascript.clipboard.success', 'Đường dẫn của các tập tin đã được sao chép vào clipboard', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1177, 0, 'vi', 'core/media/media', 'javascript.message.error_header', 'Lỗi', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1178, 0, 'vi', 'core/media/media', 'javascript.message.success_header', 'Thành công', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1179, 0, 'vi', 'core/media/media', 'javascript.download.error', 'Bạn chưa chọn tập tin nào hoặc tập tin này không cho phép tải về', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1180, 0, 'vi', 'core/media/media', 'javascript.actions_list.basic.preview', 'Xem trước', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1181, 0, 'vi', 'core/media/media', 'javascript.actions_list.file.copy_link', 'Sao chép đường dẫn', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1182, 0, 'vi', 'core/media/media', 'javascript.actions_list.file.rename', 'Đổi tên', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1183, 0, 'vi', 'core/media/media', 'javascript.actions_list.file.make_copy', 'Nhân bản', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1184, 0, 'vi', 'core/media/media', 'javascript.actions_list.user.favorite', 'Yêu thích', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1185, 0, 'vi', 'core/media/media', 'javascript.actions_list.user.remove_favorite', 'Xóa khỏi mục yêu thích', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1186, 0, 'vi', 'core/media/media', 'javascript.actions_list.other.download', 'Tải xuống', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1187, 0, 'vi', 'core/media/media', 'javascript.actions_list.other.trash', 'Chuyển vào thùng rác', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1188, 0, 'vi', 'core/media/media', 'javascript.actions_list.other.delete', 'Xóa hoàn toàn', '2020-03-29 20:06:56', '2020-04-24 09:38:43');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1189, 0, 'vi', 'core/media/media', 'javascript.actions_list.other.restore', 'Khôi phục', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1190, 0, 'vi', 'core/media/media', 'empty_trash_success', 'Dọn sạch thùng rác thành công', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1191, 0, 'vi', 'core/media/media', 'name_invalid', 'Tên thư mục chứa ký tự không hợp lệ', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1192, 0, 'en', 'core/setting/setting', 'title', 'Settings', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1193, 0, 'en', 'core/setting/setting', 'email_setting_title', 'Email settings', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1194, 0, 'en', 'core/setting/setting', 'general.theme', 'Theme', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1195, 0, 'en', 'core/setting/setting', 'general.description', 'Setting site information', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1196, 0, 'en', 'core/setting/setting', 'general.title', 'General', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1197, 0, 'en', 'core/setting/setting', 'general.general_block', 'General Information', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1198, 0, 'en', 'core/setting/setting', 'general.rich_editor', 'Rich Editor', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1199, 0, 'en', 'core/setting/setting', 'general.site_title', 'Site title', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1200, 0, 'en', 'core/setting/setting', 'general.admin_email', 'Admin Email', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1201, 0, 'en', 'core/setting/setting', 'general.seo_block', 'SEO Configuration', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1202, 0, 'en', 'core/setting/setting', 'general.seo_title', 'SEO Title', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1203, 0, 'en', 'core/setting/setting', 'general.seo_description', 'SEO Description', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1204, 0, 'en', 'core/setting/setting', 'general.webmaster_tools_block', 'Google Webmaster Tools', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1205, 0, 'en', 'core/setting/setting', 'general.google_site_verification', 'Google site verification', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1206, 0, 'en', 'core/setting/setting', 'general.placeholder.site_title', 'Site Title (maximum 120 characters)', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1207, 0, 'en', 'core/setting/setting', 'general.placeholder.admin_email', 'Admin Email', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1208, 0, 'en', 'core/setting/setting', 'general.placeholder.seo_title', 'SEO Title (maximum 120 characters)', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1209, 0, 'en', 'core/setting/setting', 'general.placeholder.seo_description', 'SEO Description (maximum 120 characters)', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1210, 0, 'en', 'core/setting/setting', 'general.placeholder.google_analytics', 'Google Analytics', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1211, 0, 'en', 'core/setting/setting', 'general.placeholder.google_site_verification', 'Google Site Verification', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1212, 0, 'en', 'core/setting/setting', 'general.cache_admin_menu', 'Cache admin menu?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1213, 0, 'en', 'core/setting/setting', 'general.enable_send_error_reporting_via_email', 'Enable to send error reporting via email?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1214, 0, 'en', 'core/setting/setting', 'general.optimize_page_speed', 'Optimize page speed (minify HTML output, inline CSS, remove comments ..)', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1215, 0, 'en', 'core/setting/setting', 'general.time_zone', 'Timezone', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1216, 0, 'en', 'core/setting/setting', 'general.default_admin_theme', 'Default admin theme', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1217, 0, 'en', 'core/setting/setting', 'general.enable_change_admin_theme', 'Enable change admin theme?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1218, 0, 'en', 'core/setting/setting', 'general.enable_multi_language_in_admin', 'Enable multi language in admin area?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1219, 0, 'en', 'core/setting/setting', 'general.enable', 'Enable', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1220, 0, 'en', 'core/setting/setting', 'general.disable', 'Disable', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1221, 0, 'en', 'core/setting/setting', 'general.enable_cache', 'Enable cache?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1222, 0, 'en', 'core/setting/setting', 'general.cache_time', 'Cache time', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1223, 0, 'en', 'core/setting/setting', 'general.cache_time_site_map', 'Cache Time Site map', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1224, 0, 'en', 'core/setting/setting', 'general.admin_logo', 'Admin logo', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1225, 0, 'en', 'core/setting/setting', 'general.admin_favicon', 'Admin favicon', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1226, 0, 'en', 'core/setting/setting', 'general.admin_title', 'Admin title', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1227, 0, 'en', 'core/setting/setting', 'general.admin_title_placeholder', 'Title show to tab of browser', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1228, 0, 'en', 'core/setting/setting', 'general.cache_block', 'Cache', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1229, 0, 'en', 'core/setting/setting', 'general.admin_appearance_title', 'Admin appearance', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1230, 0, 'en', 'core/setting/setting', 'general.admin_appearance_description', 'Setting admin appearance such as editor, language...', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1231, 0, 'en', 'core/setting/setting', 'general.seo_block_description', 'Setting site title, site meta description, site keyword for optimize SEO', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1232, 0, 'en', 'core/setting/setting', 'general.webmaster_tools_description', 'Google Webmaster Tools (GWT) is free software that helps you manage the technical side of your website', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1233, 0, 'en', 'core/setting/setting', 'general.cache_description', 'Config cache for system for optimize speed', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1234, 0, 'en', 'core/setting/setting', 'general.yes', 'Yes', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1235, 0, 'en', 'core/setting/setting', 'general.no', 'No', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1236, 0, 'en', 'core/setting/setting', 'general.show_on_front', 'Your homepage displays', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1237, 0, 'en', 'core/setting/setting', 'general.select', '— Select —', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1238, 0, 'en', 'core/setting/setting', 'general.show_site_name', 'Show site name after page title, separate with \"-\"?', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1239, 0, 'en', 'core/setting/setting', 'email.subject', 'Subject', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1240, 0, 'en', 'core/setting/setting', 'email.content', 'Content', '2020-03-29 20:06:56', '2020-04-24 09:38:43'),
(1241, 0, 'en', 'core/setting/setting', 'email.title', 'Setting for email template', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1242, 0, 'en', 'core/setting/setting', 'email.description', 'Email template using HTML & system variables.', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1243, 0, 'en', 'core/setting/setting', 'email.reset_to_default', 'Reset to default', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1244, 0, 'en', 'core/setting/setting', 'email.back', 'Back to settings', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1245, 0, 'en', 'core/setting/setting', 'email.reset_success', 'Reset back to default successfully', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1246, 0, 'en', 'core/setting/setting', 'email.confirm_reset', 'Confirm reset email template?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1247, 0, 'en', 'core/setting/setting', 'email.confirm_message', 'Do you really want to reset this email template to default?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1248, 0, 'en', 'core/setting/setting', 'email.continue', 'Continue', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1249, 0, 'en', 'core/setting/setting', 'email.sender_name', 'Sender name', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1250, 0, 'en', 'core/setting/setting', 'email.sender_name_placeholder', 'Name', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1251, 0, 'en', 'core/setting/setting', 'email.sender_email', 'Sender email', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1252, 0, 'en', 'core/setting/setting', 'email.driver', 'Driver', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1253, 0, 'en', 'core/setting/setting', 'email.port', 'Port', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1254, 0, 'en', 'core/setting/setting', 'email.port_placeholder', 'Ex: 587', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1255, 0, 'en', 'core/setting/setting', 'email.host', 'Host', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1256, 0, 'en', 'core/setting/setting', 'email.host_placeholder', 'Ex: smtp.gmail.com', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1257, 0, 'en', 'core/setting/setting', 'email.username', 'Username', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1258, 0, 'en', 'core/setting/setting', 'email.username_placeholder', 'Username to login to mail server', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1259, 0, 'en', 'core/setting/setting', 'email.password', 'Password', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1260, 0, 'en', 'core/setting/setting', 'email.password_placeholder', 'Password to login to mail server', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1261, 0, 'en', 'core/setting/setting', 'email.encryption', 'Encryption', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1262, 0, 'en', 'core/setting/setting', 'email.mail_gun_domain', 'Domain', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1263, 0, 'en', 'core/setting/setting', 'email.mail_gun_domain_placeholder', 'Domain', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1264, 0, 'en', 'core/setting/setting', 'email.mail_gun_secret', 'Secret', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1265, 0, 'en', 'core/setting/setting', 'email.mail_gun_secret_placeholder', 'Secret', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1266, 0, 'en', 'core/setting/setting', 'email.template_title', 'Email templates', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1267, 0, 'en', 'core/setting/setting', 'email.template_description', 'Base templates for all emails', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1268, 0, 'en', 'core/setting/setting', 'email.template_header', 'Email template header', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1269, 0, 'en', 'core/setting/setting', 'email.template_header_description', 'Template for header of emails', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1270, 0, 'en', 'core/setting/setting', 'email.template_footer', 'Email template footer', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1271, 0, 'en', 'core/setting/setting', 'email.template_footer_description', 'Template for footer of emails', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1272, 0, 'en', 'core/setting/setting', 'media.title', 'Media', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1273, 0, 'en', 'core/setting/setting', 'media.driver', 'Driver', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1274, 0, 'en', 'core/setting/setting', 'media.description', 'Settings for media', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1275, 0, 'en', 'core/setting/setting', 'media.aws_access_key_id', 'AWS Access Key ID', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1276, 0, 'en', 'core/setting/setting', 'media.aws_secret_key', 'AWS Secret Key', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1277, 0, 'en', 'core/setting/setting', 'media.aws_default_region', 'AWS Default Region', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1278, 0, 'en', 'core/setting/setting', 'media.aws_bucket', 'AWS Bucket', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1279, 0, 'en', 'core/setting/setting', 'media.aws_url', 'AWS URL', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1280, 0, 'en', 'core/setting/setting', 'license.purchase_code', 'Purchase code', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1281, 0, 'en', 'core/setting/setting', 'license.buyer', 'Buyer', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1282, 0, 'en', 'core/setting/setting', 'field_type_not_exists', 'This field type does not exist', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1283, 0, 'en', 'core/setting/setting', 'save_settings', 'Save settings', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1284, 0, 'en', 'core/setting/setting', 'template', 'Template', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1285, 0, 'en', 'core/setting/setting', 'description', 'Description', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1286, 0, 'en', 'core/setting/setting', 'enable', 'Enable', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1287, 0, 'en', 'core/setting/setting', 'send', 'Send', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1288, 0, 'en', 'core/setting/setting', 'test_email_description', 'To send test email, please make sure you are updated configuration to send mail!', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1289, 0, 'en', 'core/setting/setting', 'test_email_input_placeholder', 'Enter the email which you want to send test email.', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1290, 0, 'en', 'core/setting/setting', 'test_email_modal_title', 'Send a test email', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1291, 0, 'en', 'core/setting/setting', 'test_send_mail', 'Send test mail', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1292, 0, 'vi', 'core/setting/setting', 'title', 'Cài đặt', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1293, 0, 'vi', 'core/setting/setting', 'general.theme', 'Giao diện', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1294, 0, 'vi', 'core/setting/setting', 'general.description', 'Cấu hình những thông tin cài đặt website.', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1295, 0, 'vi', 'core/setting/setting', 'general.title', 'Thông tin chung', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1296, 0, 'vi', 'core/setting/setting', 'general.general_block', 'Thông tin chung', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1297, 0, 'vi', 'core/setting/setting', 'general.site_title', 'Tên trang', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1298, 0, 'vi', 'core/setting/setting', 'general.admin_email', 'Email quản trị viên', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1299, 0, 'vi', 'core/setting/setting', 'general.seo_block', 'Cấu hình SEO', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1300, 0, 'vi', 'core/setting/setting', 'general.seo_title', 'Tiêu đề SEO', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1301, 0, 'vi', 'core/setting/setting', 'general.seo_description', 'Mô tả SEO', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1302, 0, 'vi', 'core/setting/setting', 'general.seo_keywords', 'Từ khoá SEO', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1303, 0, 'vi', 'core/setting/setting', 'general.webmaster_tools_block', 'Google Webmaster Tools', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1304, 0, 'vi', 'core/setting/setting', 'general.google_site_verification', 'Google site verification', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1305, 0, 'vi', 'core/setting/setting', 'general.enable_captcha', 'Sử dụng Captcha?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1306, 0, 'vi', 'core/setting/setting', 'general.contact_block', 'Thông tin liên hệ', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1307, 0, 'vi', 'core/setting/setting', 'general.placeholder.site_title', 'Tên trang (tối đa 120 kí tự)', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1308, 0, 'vi', 'core/setting/setting', 'general.placeholder.admin_email', 'Admin Email', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1309, 0, 'vi', 'core/setting/setting', 'general.placeholder.google_analytics', 'Ví dụ: UA-42767940-2', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1310, 0, 'vi', 'core/setting/setting', 'general.placeholder.google_site_verification', 'Mã xác nhận trang web dùng cho Google Webmaster Tool', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1311, 0, 'vi', 'core/setting/setting', 'general.placeholder.seo_title', 'Tiêu đề SEO (tối đa 120 kí tự)', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1312, 0, 'vi', 'core/setting/setting', 'general.placeholder.seo_description', 'Mô tả SEO (tối đa 120 kí tự)', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1313, 0, 'vi', 'core/setting/setting', 'general.placeholder.seo_keywords', 'Từ khoá SEO (tối đa 60 kí tự, phân cách từ khóa bằng dấu phẩy)', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1314, 0, 'vi', 'core/setting/setting', 'general.enable_change_admin_theme', 'Cho phép thay đổi giao diện quản trị?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1315, 0, 'vi', 'core/setting/setting', 'general.enable_multi_language_in_admin', 'Cho phép thay đổi ngôn ngữ trang quản trị?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1316, 0, 'vi', 'core/setting/setting', 'general.enable', 'Bật', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1317, 0, 'vi', 'core/setting/setting', 'general.disable', 'Tắt', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1318, 0, 'vi', 'core/setting/setting', 'general.enable_cache', 'Bật bộ nhớ đệm?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1319, 0, 'vi', 'core/setting/setting', 'general.cache_time', 'Thời gian lưu bộ nhớ đệm', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1320, 0, 'vi', 'core/setting/setting', 'general.cache_time_site_map', 'Thời gian lưu sơ đồ trang trong bộ nhớ đệm', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1321, 0, 'vi', 'core/setting/setting', 'general.admin_logo', 'Logo trang quản trị', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1322, 0, 'vi', 'core/setting/setting', 'general.admin_title', 'Tiêu đề trang quản trị', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1323, 0, 'vi', 'core/setting/setting', 'general.admin_title_placeholder', 'Tiêu đề hiển thị trên thẻ trình duyệt', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1324, 0, 'vi', 'core/setting/setting', 'general.rich_editor', 'Bộ soạn thảo văn bản', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1325, 0, 'vi', 'core/setting/setting', 'general.cache_block', 'Bộ nhớ đệm', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1326, 0, 'vi', 'core/setting/setting', 'general.yes', 'Bật', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1327, 0, 'vi', 'core/setting/setting', 'general.no', 'Tắt', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1328, 0, 'vi', 'core/setting/setting', 'email.subject', 'Tiêu đề', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1329, 0, 'vi', 'core/setting/setting', 'email.content', 'Nội dung', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1330, 0, 'vi', 'core/setting/setting', 'email.title', 'Cấu hình email template', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1331, 0, 'vi', 'core/setting/setting', 'email.description', 'Cấu trúc file template sử dụng HTML và các biến trong hệ thống.', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1332, 0, 'vi', 'core/setting/setting', 'email.reset_to_default', 'Khôi phục về mặc định', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1333, 0, 'vi', 'core/setting/setting', 'email.back', 'Trở lại trang cài đặt', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1334, 0, 'vi', 'core/setting/setting', 'email.reset_success', 'Khôi phục mặc định thành công', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1335, 0, 'vi', 'core/setting/setting', 'email.confirm_reset', 'Xác nhận khôi phục mặc định?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1336, 0, 'vi', 'core/setting/setting', 'email.confirm_message', 'Bạn có chắc chắn muốn khôi phục về bản mặc định?', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1337, 0, 'vi', 'core/setting/setting', 'email.continue', 'Tiếp tục', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1338, 0, 'vi', 'core/setting/setting', 'email.sender_name', 'Tên người gửi', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1339, 0, 'vi', 'core/setting/setting', 'email.sender_name_placeholder', 'Tên', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1340, 0, 'vi', 'core/setting/setting', 'email.sender_email', 'Email của người gửi', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1341, 0, 'vi', 'core/setting/setting', 'email.driver', 'Loại', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1342, 0, 'vi', 'core/setting/setting', 'email.port', 'Cổng', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1343, 0, 'vi', 'core/setting/setting', 'email.port_placeholder', 'Ví dụ: 587', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1344, 0, 'vi', 'core/setting/setting', 'email.host', 'Máy chủ', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1345, 0, 'vi', 'core/setting/setting', 'email.host_placeholder', 'Ví dụ: smtp.gmail.com', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1346, 0, 'vi', 'core/setting/setting', 'email.username', 'Tên đăng nhập', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1347, 0, 'vi', 'core/setting/setting', 'email.username_placeholder', 'Tên đăng nhập vào máy chủ mail', '2020-03-29 20:06:56', '2020-04-24 09:38:44'),
(1348, 0, 'vi', 'core/setting/setting', 'email.password', 'Mật khẩu', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1349, 0, 'vi', 'core/setting/setting', 'email.password_placeholder', 'Mật khẩu đăng nhập vào máy chủ mail', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1350, 0, 'vi', 'core/setting/setting', 'email.encryption', 'Mã hoá', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1351, 0, 'vi', 'core/setting/setting', 'email.mail_gun_domain', 'Tên miền', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1352, 0, 'vi', 'core/setting/setting', 'email.mail_gun_domain_placeholder', 'Tên miền', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1353, 0, 'vi', 'core/setting/setting', 'email.mail_gun_secret', 'Chuỗi bí mật', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1354, 0, 'vi', 'core/setting/setting', 'email.mail_gun_secret_placeholder', 'Chuỗi bí mật', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1355, 0, 'vi', 'core/setting/setting', 'email.template_title', 'Mẫu giao diện email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1356, 0, 'vi', 'core/setting/setting', 'email.template_description', 'Giao diện mặc định cho tất cả email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1357, 0, 'vi', 'core/setting/setting', 'email.template_header', 'Mẫu cho phần trên của email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1358, 0, 'vi', 'core/setting/setting', 'email.template_header_description', 'Phần trên của tất cả email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1359, 0, 'vi', 'core/setting/setting', 'email.template_footer', 'Mẫu cho phần dưới của email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1360, 0, 'vi', 'core/setting/setting', 'email.template_footer_description', 'Phần dưới của tất cả email', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1361, 0, 'vi', 'core/setting/setting', 'save_settings', 'Lưu cài đặt', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1362, 0, 'vi', 'core/setting/setting', 'template', 'Mẫu', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1363, 0, 'vi', 'core/setting/setting', 'description', 'Mô tả', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1364, 0, 'vi', 'core/setting/setting', 'enable', 'Bật', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1365, 0, 'vi', 'core/setting/setting', 'test_send_mail', 'Gửi thử nghiệm', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1366, 0, 'en', 'core/table/general', 'operations', 'Operations', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1367, 0, 'en', 'core/table/general', 'loading_data', 'Loading data from server', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1368, 0, 'en', 'core/table/general', 'display', 'Display', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1369, 0, 'en', 'core/table/general', 'all', 'All', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1370, 0, 'en', 'core/table/general', 'edit_entry', 'Edit', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1371, 0, 'en', 'core/table/general', 'delete_entry', 'Delete', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1372, 0, 'en', 'core/table/general', 'show_from', 'Showing from', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1373, 0, 'en', 'core/table/general', 'to', 'to', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1374, 0, 'en', 'core/table/general', 'in', 'in', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1375, 0, 'en', 'core/table/general', 'records', 'records', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1376, 0, 'en', 'core/table/general', 'no_data', 'No data to display', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1377, 0, 'en', 'core/table/general', 'no_record', 'No record', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1378, 0, 'en', 'core/table/general', 'loading', 'Loading data from server', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1379, 0, 'en', 'core/table/general', 'confirm_delete', 'Confirm delete', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1380, 0, 'en', 'core/table/general', 'confirm_delete_msg', 'Do you really want to delete this record?', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1381, 0, 'en', 'core/table/general', 'cancel', 'Cancel', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1382, 0, 'en', 'core/table/general', 'delete', 'Delete', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1383, 0, 'en', 'core/table/general', 'close', 'Close', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1384, 0, 'en', 'core/table/general', 'contains', 'Contains', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1385, 0, 'en', 'core/table/general', 'is_equal_to', 'Is equal to', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1386, 0, 'en', 'core/table/general', 'greater_than', 'Greater than', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1387, 0, 'en', 'core/table/general', 'less_than', 'Less than', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1388, 0, 'en', 'core/table/general', 'value', 'Value', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1389, 0, 'en', 'core/table/general', 'select_field', 'Select field', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1390, 0, 'en', 'core/table/general', 'reset', 'Reset', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1391, 0, 'en', 'core/table/general', 'add_additional_filter', 'Add additional filter', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1392, 0, 'en', 'core/table/general', 'apply', 'Apply', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1393, 0, 'en', 'core/table/general', 'filters', 'Filters', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1394, 0, 'en', 'core/table/general', 'bulk_change', 'Bulk changes', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1395, 0, 'en', 'core/table/general', 'select_option', 'Select option', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1396, 0, 'en', 'core/table/general', 'bulk_actions', 'Bulk Actions', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1397, 0, 'en', 'core/table/general', 'save_bulk_change_success', 'Update data for selected record(s) successfully!', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1398, 0, 'en', 'core/table/general', 'please_select_record', 'Please select at least one record to perform this action!', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1399, 0, 'en', 'core/table/general', 'filtered', '(filtered from _MAX_ total records)', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1400, 0, 'en', 'core/table/general', 'search', 'Search...', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1401, 0, 'vi', 'core/table/general', 'operations', 'Hành động', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1402, 0, 'vi', 'core/table/general', 'loading_data', 'Đang tải dữ liệu từ hệ thống', '2020-03-29 20:06:56', '2020-04-24 09:38:45'),
(1403, 0, 'vi', 'core/table/general', 'display', 'Hiển thị', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1404, 0, 'vi', 'core/table/general', 'all', 'Tất cả', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1405, 0, 'vi', 'core/table/general', 'edit_entry', 'Sửa', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1406, 0, 'vi', 'core/table/general', 'delete_entry', 'Xoá', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1407, 0, 'vi', 'core/table/general', 'show_from', 'Hiển thị từ', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1408, 0, 'vi', 'core/table/general', 'to', 'đến', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1409, 0, 'vi', 'core/table/general', 'in', 'trong tổng số', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1410, 0, 'vi', 'core/table/general', 'records', 'bản ghi', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1411, 0, 'vi', 'core/table/general', 'no_data', 'Không có dữ liệu để hiển thị', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1412, 0, 'vi', 'core/table/general', 'no_record', 'không có bản ghi nào', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1413, 0, 'vi', 'core/table/general', 'loading', 'Đang tải dữ liệu từ hệ thống', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1414, 0, 'vi', 'core/table/general', 'confirm_delete', 'Xác nhận xoá', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1415, 0, 'vi', 'core/table/general', 'confirm_delete_msg', 'Bạn có chắc chắn muốn xoá bản ghi này?', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1416, 0, 'vi', 'core/table/general', 'cancel', 'Huỷ bỏ', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1417, 0, 'vi', 'core/table/general', 'delete', 'Xoá', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1418, 0, 'vi', 'core/table/general', 'close', 'Đóng', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1419, 0, 'vi', 'core/table/general', 'is_equal_to', 'Bằng với', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1420, 0, 'vi', 'core/table/general', 'greater_than', 'Lớn hơn', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1421, 0, 'vi', 'core/table/general', 'less_than', 'Nhỏ hơn', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1422, 0, 'vi', 'core/table/general', 'value', 'Giá trị', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1423, 0, 'vi', 'core/table/general', 'select_field', 'Chọn trường', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1424, 0, 'vi', 'core/table/general', 'reset', 'Làm mới', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1425, 0, 'vi', 'core/table/general', 'add_additional_filter', 'Thêm bộ lọc', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1426, 0, 'vi', 'core/table/general', 'apply', 'Áp dụng', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1427, 0, 'vi', 'core/table/general', 'select_option', 'Lựa chọn', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1428, 0, 'vi', 'core/table/general', 'filters', 'Lọc dữ liệu', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1429, 0, 'vi', 'core/table/general', 'bulk_change', 'Thay đổi hàng loạt', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1430, 0, 'vi', 'core/table/general', 'bulk_actions', 'Hành động', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1431, 0, 'vi', 'core/table/general', 'contains', 'Bao gồm', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1432, 0, 'vi', 'core/table/general', 'filtered', '(đã được lọc từ _MAX_ bản ghi)', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1433, 0, 'en', 'packages/installer/installer', 'title', 'Installer', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1434, 0, 'en', 'packages/installer/installer', 'next', 'Next Step', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1435, 0, 'en', 'packages/installer/installer', 'back', 'Previous', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1436, 0, 'en', 'packages/installer/installer', 'finish', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1437, 0, 'en', 'packages/installer/installer', 'forms.errorTitle', 'The following errors occurred:', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1438, 0, 'en', 'packages/installer/installer', 'welcome.templateTitle', 'Botble CMS', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1439, 0, 'en', 'packages/installer/installer', 'welcome.title', 'Botble CMS', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1440, 0, 'en', 'packages/installer/installer', 'welcome.message', 'Welcome to Botble CMS. Before getting started, we need some information on the database. You will need to know the following items before proceeding.', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1441, 0, 'en', 'packages/installer/installer', 'welcome.next', 'Let\'s go', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1442, 0, 'en', 'packages/installer/installer', 'requirements.templateTitle', 'Step 1 | Server Requirements', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1443, 0, 'en', 'packages/installer/installer', 'requirements.title', 'Server Requirements', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1444, 0, 'en', 'packages/installer/installer', 'requirements.next', 'Check Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1445, 0, 'en', 'packages/installer/installer', 'permissions.templateTitle', 'Step 2 | Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1446, 0, 'en', 'packages/installer/installer', 'permissions.title', 'Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1447, 0, 'en', 'packages/installer/installer', 'permissions.next', 'Configure Environment', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1448, 0, 'en', 'packages/installer/installer', 'environment.wizard.templateTitle', 'Environment Settings', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1449, 0, 'en', 'packages/installer/installer', 'environment.wizard.title', 'Environment Settings', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1450, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.name_required', 'An environment name is required.', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1451, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.app_name_label', 'Site title', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1452, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.app_name_placeholder', 'Site title', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1453, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.app_url_label', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1454, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.app_url_placeholder', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1455, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_connection_label', 'Database Connection', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1456, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_mysql', 'MySQL', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1457, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_sqlite', 'SQLite', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1458, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_pgsql', 'PostgreSQL', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1459, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_host_label', 'Database host', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1460, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_host_placeholder', 'Database host', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1461, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_port_label', 'Database port', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1462, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_port_placeholder', 'Database port', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1463, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_name_label', 'Database name', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1464, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_name_placeholder', 'Database name', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1465, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_username_label', 'Database username', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1466, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_username_placeholder', 'Database username', '2020-03-29 20:06:57', '2020-04-24 09:38:45'),
(1467, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_password_label', 'Database password', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1468, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.db_password_placeholder', 'Database password', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1469, 0, 'en', 'packages/installer/installer', 'environment.wizard.form.buttons.install', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1470, 0, 'en', 'packages/installer/installer', 'environment.classic.templateTitle', 'Step 3 | Environment Settings | Classic Editor', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1471, 0, 'en', 'packages/installer/installer', 'environment.classic.title', 'Classic Environment Editor', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1472, 0, 'en', 'packages/installer/installer', 'environment.classic.save', 'Save .env', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1473, 0, 'en', 'packages/installer/installer', 'environment.classic.back', 'Use Form Wizard', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1474, 0, 'en', 'packages/installer/installer', 'environment.classic.install', 'Save and Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1475, 0, 'en', 'packages/installer/installer', 'environment.success', 'Your .env file settings have been saved.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1476, 0, 'en', 'packages/installer/installer', 'environment.errors', 'Unable to save the .env file, Please create it manually.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1477, 0, 'en', 'packages/installer/installer', 'install', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1478, 0, 'en', 'packages/installer/installer', 'final.title', 'Installation Finished', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1479, 0, 'en', 'packages/installer/installer', 'final.templateTitle', 'Installation Finished', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1480, 0, 'en', 'packages/installer/installer', 'final.finished', 'Application has been successfully installed.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1481, 0, 'en', 'packages/installer/installer', 'final.migration', 'Migration & Seed Console Output:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1482, 0, 'en', 'packages/installer/installer', 'final.console', 'Application Console Output:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1483, 0, 'en', 'packages/installer/installer', 'final.log', 'Installation Log Entry:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1484, 0, 'en', 'packages/installer/installer', 'final.env', 'Final .env File:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1485, 0, 'en', 'packages/installer/installer', 'final.exit', 'Click here to exit', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1486, 0, 'vi', 'packages/installer/installer', 'title', 'Installer', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1487, 0, 'vi', 'packages/installer/installer', 'next', 'Next Step', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1488, 0, 'vi', 'packages/installer/installer', 'back', 'Previous', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1489, 0, 'vi', 'packages/installer/installer', 'finish', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1490, 0, 'vi', 'packages/installer/installer', 'forms.errorTitle', 'The following errors occurred:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1491, 0, 'vi', 'packages/installer/installer', 'welcome.templateTitle', 'Botble CMS', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1492, 0, 'vi', 'packages/installer/installer', 'welcome.title', 'Botble CMS', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1493, 0, 'vi', 'packages/installer/installer', 'welcome.message', 'Welcome to Botble CMS. Before getting started, we need some information on the database. You will need to know the following items before proceeding.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1494, 0, 'vi', 'packages/installer/installer', 'welcome.next', 'Let\'s go', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1495, 0, 'vi', 'packages/installer/installer', 'requirements.templateTitle', 'Step 1 | Server Requirements', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1496, 0, 'vi', 'packages/installer/installer', 'requirements.title', 'Server Requirements', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1497, 0, 'vi', 'packages/installer/installer', 'requirements.next', 'Check Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1498, 0, 'vi', 'packages/installer/installer', 'permissions.templateTitle', 'Step 2 | Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1499, 0, 'vi', 'packages/installer/installer', 'permissions.title', 'Permissions', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1500, 0, 'vi', 'packages/installer/installer', 'permissions.next', 'Configure Environment', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1501, 0, 'vi', 'packages/installer/installer', 'environment.wizard.templateTitle', 'Environment Settings', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1502, 0, 'vi', 'packages/installer/installer', 'environment.wizard.title', 'Environment Settings', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1503, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.name_required', 'An environment name is required.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1504, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.app_name_label', 'Site title', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1505, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.app_name_placeholder', 'Site title', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1506, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.app_url_label', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1507, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.app_url_placeholder', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1508, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_connection_label', 'Database Connection', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1509, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_mysql', 'MySQL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1510, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_sqlite', 'SQLite', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1511, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_connection_label_pgsql', 'PostgreSQL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1512, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_host_label', 'Database host', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1513, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_host_placeholder', 'Database host', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1514, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_port_label', 'Database port', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1515, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_port_placeholder', 'Database port', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1516, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_name_label', 'Database name', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1517, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_name_placeholder', 'Database name', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1518, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_username_label', 'Database username', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1519, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_username_placeholder', 'Database username', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1520, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_password_label', 'Database password', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1521, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.db_password_placeholder', 'Database password', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1522, 0, 'vi', 'packages/installer/installer', 'environment.wizard.form.buttons.install', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1523, 0, 'vi', 'packages/installer/installer', 'environment.classic.templateTitle', 'Step 3 | Environment Settings | Classic Editor', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1524, 0, 'vi', 'packages/installer/installer', 'environment.classic.title', 'Classic Environment Editor', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1525, 0, 'vi', 'packages/installer/installer', 'environment.classic.save', 'Save .env', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1526, 0, 'vi', 'packages/installer/installer', 'environment.classic.back', 'Use Form Wizard', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1527, 0, 'vi', 'packages/installer/installer', 'environment.classic.install', 'Save and Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1528, 0, 'vi', 'packages/installer/installer', 'environment.success', 'Your .env file settings have been saved.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1529, 0, 'vi', 'packages/installer/installer', 'environment.errors', 'Unable to save the .env file, Please create it manually.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1530, 0, 'vi', 'packages/installer/installer', 'install', 'Install', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1531, 0, 'vi', 'packages/installer/installer', 'final.title', 'Installation Finished', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1532, 0, 'vi', 'packages/installer/installer', 'final.templateTitle', 'Installation Finished', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1533, 0, 'vi', 'packages/installer/installer', 'final.finished', 'Application has been successfully installed.', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1534, 0, 'vi', 'packages/installer/installer', 'final.migration', 'Migration & Seed Console Output:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1535, 0, 'vi', 'packages/installer/installer', 'final.console', 'Application Console Output:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1536, 0, 'vi', 'packages/installer/installer', 'final.log', 'Installation Log Entry:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1537, 0, 'vi', 'packages/installer/installer', 'final.env', 'Final .env File:', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1538, 0, 'vi', 'packages/installer/installer', 'final.exit', 'Click here to exit', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1539, 0, 'en', 'packages/menu/menu', 'name', 'Menus', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1540, 0, 'en', 'packages/menu/menu', 'key_name', 'Menu name (key: :key)', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1541, 0, 'en', 'packages/menu/menu', 'basic_info', 'Basic information', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1542, 0, 'en', 'packages/menu/menu', 'add_to_menu', 'Add to menu', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1543, 0, 'en', 'packages/menu/menu', 'custom_link', 'Custom link', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1544, 0, 'en', 'packages/menu/menu', 'add_link', 'Add link', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1545, 0, 'en', 'packages/menu/menu', 'structure', 'Menu structure', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1546, 0, 'en', 'packages/menu/menu', 'remove', 'Remove', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1547, 0, 'en', 'packages/menu/menu', 'cancel', 'Cancel', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1548, 0, 'en', 'packages/menu/menu', 'title', 'Title', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1549, 0, 'en', 'packages/menu/menu', 'icon', 'Icon', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1550, 0, 'en', 'packages/menu/menu', 'url', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1551, 0, 'en', 'packages/menu/menu', 'target', 'Target', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1552, 0, 'en', 'packages/menu/menu', 'css_class', 'CSS class', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1553, 0, 'en', 'packages/menu/menu', 'self_open_link', 'Open link directly', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1554, 0, 'en', 'packages/menu/menu', 'blank_open_link', 'Open link in new tab', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1555, 0, 'en', 'packages/menu/menu', 'create', 'Create menu', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1556, 0, 'en', 'packages/menu/menu', 'edit', 'Edit menu', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1557, 0, 'vi', 'packages/menu/menu', 'name', 'Trình đơn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1558, 0, 'vi', 'packages/menu/menu', 'cancel', 'Hủy bỏ', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1559, 0, 'vi', 'packages/menu/menu', 'add_link', 'Thêm liên kết', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1560, 0, 'vi', 'packages/menu/menu', 'add_to_menu', 'Thêm vào trình đơn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1561, 0, 'vi', 'packages/menu/menu', 'basic_info', 'Thông tin cơ bản', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1562, 0, 'vi', 'packages/menu/menu', 'blank_open_link', 'Mở liên kết trong tab mới', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1563, 0, 'vi', 'packages/menu/menu', 'css_class', 'CSS class', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1564, 0, 'vi', 'packages/menu/menu', 'custom_link', 'Liên kết tùy chọn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1565, 0, 'vi', 'packages/menu/menu', 'icon', 'Biểu tượng', '2020-03-29 20:06:57', '2020-04-24 09:38:46');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1566, 0, 'vi', 'packages/menu/menu', 'key_name', 'Tên menu (key::key)', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1567, 0, 'vi', 'packages/menu/menu', 'remove', 'Xóa', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1568, 0, 'vi', 'packages/menu/menu', 'self_open_link', 'Mở liên kết trong tab hiện tại', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1569, 0, 'vi', 'packages/menu/menu', 'structure', 'Cấu trúc trình đơn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1570, 0, 'vi', 'packages/menu/menu', 'target', 'Target', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1571, 0, 'vi', 'packages/menu/menu', 'title', 'Tiêu đề', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1572, 0, 'vi', 'packages/menu/menu', 'url', 'URL', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1573, 0, 'vi', 'packages/menu/menu', 'create', 'Tạo trình đơn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1574, 0, 'vi', 'packages/menu/menu', 'edit', 'Sửa trình đơn', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1575, 0, 'en', 'packages/optimize/optimize', 'settings.title', 'Optimize page speed', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1576, 0, 'en', 'packages/optimize/optimize', 'settings.description', 'Minify HTML output, inline CSS, remove comments...', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1577, 0, 'en', 'packages/optimize/optimize', 'settings.enable', 'Enable optimize page speed?', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1578, 0, 'vi', 'packages/optimize/optimize', 'settings.title', 'Optimize page speed', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1579, 0, 'vi', 'packages/optimize/optimize', 'settings.description', 'Minify HTML output, inline CSS, remove comments...', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1580, 0, 'vi', 'packages/optimize/optimize', 'settings.enable', 'Enable optimize page speed?', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1581, 0, 'en', 'packages/page/pages', 'model', 'Page', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1582, 0, 'en', 'packages/page/pages', 'models', 'Pages', '2020-03-29 20:06:57', '2020-04-24 09:38:46'),
(1583, 0, 'en', 'packages/page/pages', 'list', 'List Pages', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1584, 0, 'en', 'packages/page/pages', 'create', 'Create new page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1585, 0, 'en', 'packages/page/pages', 'edit', 'Edit page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1586, 0, 'en', 'packages/page/pages', 'form.name', 'Name', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1587, 0, 'en', 'packages/page/pages', 'form.name_placeholder', 'Page\'s name (Maximum 120 characters)', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1588, 0, 'en', 'packages/page/pages', 'form.content', 'Content', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1589, 0, 'en', 'packages/page/pages', 'form.note', 'Note content', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1590, 0, 'en', 'packages/page/pages', 'notices.no_select', 'Please select at least one record to take this action!', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1591, 0, 'en', 'packages/page/pages', 'notices.update_success_message', 'Update successfully', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1592, 0, 'en', 'packages/page/pages', 'cannot_delete', 'Page could not be deleted', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1593, 0, 'en', 'packages/page/pages', 'deleted', 'Page deleted', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1594, 0, 'en', 'packages/page/pages', 'pages', 'Pages', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1595, 0, 'en', 'packages/page/pages', 'menu', 'Pages', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1596, 0, 'en', 'packages/page/pages', 'menu_name', 'Pages', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1597, 0, 'en', 'packages/page/pages', 'edit_this_page', 'Edit this page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1598, 0, 'en', 'packages/page/pages', 'total_pages', 'Total pages', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1599, 0, 'en', 'packages/page/pages', 'settings.show_on_front', 'Your homepage displays', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1600, 0, 'en', 'packages/page/pages', 'settings.select', '— Select —', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1601, 0, 'en', 'packages/page/pages', 'settings.title', 'Page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1602, 0, 'en', 'packages/page/pages', 'settings.description', 'Setting for page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1603, 0, 'en', 'packages/page/pages', 'front_page', 'Front Page', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1604, 0, 'vi', 'packages/page/pages', 'model', 'Trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1605, 0, 'vi', 'packages/page/pages', 'models', 'Trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1606, 0, 'vi', 'packages/page/pages', 'list', 'Danh sách trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1607, 0, 'vi', 'packages/page/pages', 'create', 'Thêm trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1608, 0, 'vi', 'packages/page/pages', 'edit', 'Sửa trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1609, 0, 'vi', 'packages/page/pages', 'form.name', 'Tiêu đề trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1610, 0, 'vi', 'packages/page/pages', 'form.note', 'Nội dung ghi chú', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1611, 0, 'vi', 'packages/page/pages', 'form.name_placeholder', 'Tên trang (tối đa 120 kí tự)', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1612, 0, 'vi', 'packages/page/pages', 'form.content', 'Nội dung', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1613, 0, 'vi', 'packages/page/pages', 'notices.no_select', 'Chọn ít nhất 1 trang để thực hiện hành động này!', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1614, 0, 'vi', 'packages/page/pages', 'notices.update_success_message', 'Cập nhật thành công', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1615, 0, 'vi', 'packages/page/pages', 'deleted', 'Xóa trang thành công', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1616, 0, 'vi', 'packages/page/pages', 'cannot_delete', 'Không thể xóa trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1617, 0, 'vi', 'packages/page/pages', 'menu', 'Trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1618, 0, 'vi', 'packages/page/pages', 'menu_name', 'Trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1619, 0, 'vi', 'packages/page/pages', 'edit_this_page', 'Sửa trang này', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1620, 0, 'vi', 'packages/page/pages', 'pages', 'Trang', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1621, 0, 'en', 'packages/plugin-management/plugin', 'enabled', 'Enabled', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1622, 0, 'en', 'packages/plugin-management/plugin', 'deactivated', 'Deactivated', '2020-03-29 20:06:57', '2020-04-24 09:38:47'),
(1623, 0, 'en', 'packages/plugin-management/plugin', 'activated', 'Activated', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1624, 0, 'en', 'packages/plugin-management/plugin', 'activate', 'Activate', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1625, 0, 'en', 'packages/plugin-management/plugin', 'deactivate', 'Deactivate', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1626, 0, 'en', 'packages/plugin-management/plugin', 'author', 'By', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1627, 0, 'en', 'packages/plugin-management/plugin', 'update_plugin_status_success', 'Update plugin successfully', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1628, 0, 'en', 'packages/plugin-management/plugin', 'plugins', 'Plugins', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1629, 0, 'en', 'packages/plugin-management/plugin', 'missing_required_plugins', 'Please activate plugin(s): :plugins before activate this plugin!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1630, 0, 'en', 'packages/plugin-management/plugin', 'remove', 'Remove', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1631, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_success', 'Remove plugin successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1632, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin', 'Remove plugin', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1633, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_confirm_message', 'Do you really want to remove this plugin?', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1634, 0, 'en', 'packages/plugin-management/plugin', 'remove_plugin_confirm_yes', 'Yes, remove it!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1635, 0, 'en', 'packages/plugin-management/plugin', 'total_plugins', 'Total plugins', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1636, 0, 'en', 'packages/plugin-management/plugin', 'invalid_plugin', 'This plugin is not a valid plugin, please check it again!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1637, 0, 'en', 'packages/plugin-management/plugin', 'version', 'Version', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1638, 0, 'vi', 'packages/plugin-management/plugin', 'activate', 'Kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1639, 0, 'vi', 'packages/plugin-management/plugin', 'author', 'Tác giả', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1640, 0, 'vi', 'packages/plugin-management/plugin', 'version', 'Phiên bản', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1641, 0, 'vi', 'packages/plugin-management/plugin', 'activated', 'Đã kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1642, 0, 'vi', 'packages/plugin-management/plugin', 'deactivate', 'Hủy kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1643, 0, 'vi', 'packages/plugin-management/plugin', 'deactivated', 'Đã vô hiệu', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1644, 0, 'vi', 'packages/plugin-management/plugin', 'enabled', 'Kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1645, 0, 'vi', 'packages/plugin-management/plugin', 'invalid_plugin', 'Gói mở rộng không hợp lệ', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1646, 0, 'vi', 'packages/plugin-management/plugin', 'update_plugin_status_success', 'Cập nhật trạng thái gói mở rộng thành công', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1647, 0, 'vi', 'packages/plugin-management/plugin', 'missing_required_plugins', 'Vui lòng kích hoạt các gói mở rộng :plugins trước khi kích hoạt gói này', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1648, 0, 'vi', 'packages/plugin-management/plugin', 'plugins', 'Gói mở rộng', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1649, 0, 'vi', 'packages/plugin-management/plugin', 'remove', 'Xoá', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1650, 0, 'vi', 'packages/plugin-management/plugin', 'remove_plugin_success', 'Xoá thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1651, 0, 'vi', 'packages/plugin-management/plugin', 'remove_plugin', 'Xoá gói mở rộng', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1652, 0, 'vi', 'packages/plugin-management/plugin', 'remove_plugin_confirm_message', 'Bạn có chắc chắn muốn xoá plugin này?', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1653, 0, 'vi', 'packages/plugin-management/plugin', 'remove_plugin_confirm_yes', 'Có, xoá!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1654, 0, 'vi', 'packages/plugin-management/plugin', 'total_plugins', 'Tất cả plugins', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1655, 0, 'en', 'packages/seo-helper/seo-helper', 'meta_box_header', 'Search Engine Optimize', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1656, 0, 'en', 'packages/seo-helper/seo-helper', 'edit_seo_meta', 'Edit SEO meta', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1657, 0, 'en', 'packages/seo-helper/seo-helper', 'default_description', 'Setup meta title & description to make your site easy to discovered on search engines such as Google', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1658, 0, 'en', 'packages/seo-helper/seo-helper', 'seo_title', 'SEO Title', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1659, 0, 'en', 'packages/seo-helper/seo-helper', 'seo_description', 'SEO description', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1660, 0, 'vi', 'packages/seo-helper/seo-helper', 'meta_box_header', 'Tối ưu hoá bộ máy tìm kiếm (SEO)', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1661, 0, 'vi', 'packages/seo-helper/seo-helper', 'edit_seo_meta', 'Chỉnh sửa SEO', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1662, 0, 'vi', 'packages/seo-helper/seo-helper', 'default_description', 'Thiết lập các thẻ mô tả giúp người dùng dễ dàng tìm thấy trên công cụ tìm kiếm như Google.', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1663, 0, 'vi', 'packages/seo-helper/seo-helper', 'seo_title', 'Tiêu đề trang', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1664, 0, 'vi', 'packages/seo-helper/seo-helper', 'seo_description', 'Mô tả trang', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1665, 0, 'en', 'packages/theme/theme', 'name', 'Themes', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1666, 0, 'en', 'packages/theme/theme', 'theme', 'Theme', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1667, 0, 'en', 'packages/theme/theme', 'author', 'Author', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1668, 0, 'en', 'packages/theme/theme', 'version', 'Version', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1669, 0, 'en', 'packages/theme/theme', 'description', 'Description', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1670, 0, 'en', 'packages/theme/theme', 'active_success', 'Active theme successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1671, 0, 'en', 'packages/theme/theme', 'active', 'Active', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1672, 0, 'en', 'packages/theme/theme', 'activated', 'Activated', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1673, 0, 'en', 'packages/theme/theme', 'theme_options', 'Theme options', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1674, 0, 'en', 'packages/theme/theme', 'save_changes', 'Save Changes', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1675, 0, 'en', 'packages/theme/theme', 'developer_mode', 'Developer Mode Enabled', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1676, 0, 'en', 'packages/theme/theme', 'custom_css', 'Custom CSS', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1677, 0, 'en', 'packages/theme/theme', 'remove_theme_success', 'Remove theme successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1678, 0, 'en', 'packages/theme/theme', 'theme_is_note_existed', 'This theme is not existed!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1679, 0, 'en', 'packages/theme/theme', 'remove', 'Remove', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1680, 0, 'en', 'packages/theme/theme', 'remove_theme', 'Remove theme', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1681, 0, 'en', 'packages/theme/theme', 'remove_theme_confirm_message', 'Do you really want to remove this theme?', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1682, 0, 'en', 'packages/theme/theme', 'remove_theme_confirm_yes', 'Yes, remove it!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1683, 0, 'en', 'packages/theme/theme', 'total_themes', 'Total themes', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1684, 0, 'en', 'packages/theme/theme', 'show_admin_bar', 'Show admin bar (When admin logged in, still show admin bar in website)?', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1685, 0, 'en', 'packages/theme/theme', 'settings.title', 'Theme', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1686, 0, 'en', 'packages/theme/theme', 'settings.description', 'Setting for theme', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1687, 0, 'vi', 'packages/theme/theme', 'name', 'Giao diện', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1688, 0, 'vi', 'packages/theme/theme', 'activated', 'Đã kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1689, 0, 'vi', 'packages/theme/theme', 'active', 'Kích hoạt', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1690, 0, 'vi', 'packages/theme/theme', 'active_success', 'Kích hoạt giao diện thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1691, 0, 'vi', 'packages/theme/theme', 'author', 'Tác giả', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1692, 0, 'vi', 'packages/theme/theme', 'description', 'Mô tả', '2020-03-29 20:06:58', '2020-04-24 09:38:47'),
(1693, 0, 'vi', 'packages/theme/theme', 'theme', 'Giao diện', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1694, 0, 'vi', 'packages/theme/theme', 'theme_options', 'Tuỳ chọn giao diện', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1695, 0, 'vi', 'packages/theme/theme', 'version', 'Phiên bản', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1696, 0, 'vi', 'packages/theme/theme', 'save_changes', 'Lưu thay đổi', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1697, 0, 'vi', 'packages/theme/theme', 'developer_mode', 'Đang kích hoạt chế độ thử nghiệm', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1698, 0, 'vi', 'packages/theme/theme', 'custom_css', 'Tuỳ chỉnh CSS', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1699, 0, 'en', 'packages/widget/global', 'name', 'Widgets', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1700, 0, 'en', 'packages/widget/global', 'create', 'New widget', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1701, 0, 'en', 'packages/widget/global', 'edit', 'Edit widget', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1702, 0, 'en', 'packages/widget/global', 'delete', 'Delete', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1703, 0, 'en', 'packages/widget/global', 'available', 'Available Widgets', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1704, 0, 'en', 'packages/widget/global', 'instruction', 'To activate a widget drag it to a sidebar or click on it. To deactivate a widget and delete its settings, drag it back.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1705, 0, 'en', 'packages/widget/global', 'number_tag_display', 'Number tags will be display', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1706, 0, 'en', 'packages/widget/global', 'number_post_display', 'Number posts will be display', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1707, 0, 'en', 'packages/widget/global', 'select_menu', 'Select Menu', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1708, 0, 'en', 'packages/widget/global', 'widget_text', 'Text', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1709, 0, 'en', 'packages/widget/global', 'widget_text_description', 'Arbitrary text or HTML.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1710, 0, 'en', 'packages/widget/global', 'widget_recent_post', 'Recent Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1711, 0, 'en', 'packages/widget/global', 'widget_recent_post_description', 'Recent posts widget.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1712, 0, 'en', 'packages/widget/global', 'widget_custom_menu', 'Custom Menu', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1713, 0, 'en', 'packages/widget/global', 'widget_custom_menu_description', 'Add a custom menu to your widget area.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1714, 0, 'en', 'packages/widget/global', 'widget_tag', 'Tags', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1715, 0, 'en', 'packages/widget/global', 'widget_tag_description', 'Popular tags', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1716, 0, 'en', 'packages/widget/global', 'save_success', 'Save widget successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1717, 0, 'en', 'packages/widget/global', 'delete_success', 'Delete widget successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1718, 0, 'vi', 'packages/widget/global', 'name', 'Widgets', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1719, 0, 'vi', 'packages/widget/global', 'create', 'New widget', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1720, 0, 'vi', 'packages/widget/global', 'edit', 'Edit widget', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1721, 0, 'vi', 'packages/widget/global', 'available', 'Tiện ích có sẵn', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1722, 0, 'vi', 'packages/widget/global', 'delete', 'Xóa', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1723, 0, 'vi', 'packages/widget/global', 'instruction', 'Để kích hoạt tiện ích, hãy kéo nó vào sidebar hoặc nhấn vào nó. Để hủy kích hoạt tiện ích và xóa các thiết lập của tiện ích, kéo nó quay trở lại.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1724, 0, 'vi', 'packages/widget/global', 'number_post_display', 'Số bài viết sẽ hiển thị', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1725, 0, 'vi', 'packages/widget/global', 'number_tag_display', 'Số thẻ sẽ hiển thị', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1726, 0, 'vi', 'packages/widget/global', 'select_menu', 'Lựa chọn trình đơn', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1727, 0, 'vi', 'packages/widget/global', 'widget_custom_menu', 'Menu tùy chỉnh', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1728, 0, 'vi', 'packages/widget/global', 'widget_custom_menu_description', 'Thêm menu tùy chỉnh vào khu vực tiện ích của bạn', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1729, 0, 'vi', 'packages/widget/global', 'widget_recent_post', 'Bài viết gần đây', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1730, 0, 'vi', 'packages/widget/global', 'widget_recent_post_description', 'Tiện ích bài viết gần đây', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1731, 0, 'vi', 'packages/widget/global', 'widget_tag', 'Thẻ', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1732, 0, 'vi', 'packages/widget/global', 'widget_tag_description', 'Thẻ phổ biến, sử dụng nhiều', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1733, 0, 'vi', 'packages/widget/global', 'widget_text', 'Văn bản', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1734, 0, 'vi', 'packages/widget/global', 'widget_text_description', 'Văn bản tùy ý hoặc HTML.', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1735, 0, 'vi', 'packages/widget/global', 'delete_success', 'Xoá tiện ích thành công', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1736, 0, 'vi', 'packages/widget/global', 'save_success', 'Lưu tiện ích thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1737, 0, 'en', 'plugins/analytics/analytics', 'sessions', 'Sessions', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1738, 0, 'en', 'plugins/analytics/analytics', 'visitors', 'Visitors', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1739, 0, 'en', 'plugins/analytics/analytics', 'pageviews', 'Pageviews', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1740, 0, 'en', 'plugins/analytics/analytics', 'bounce_rate', 'Bounce Rate', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1741, 0, 'en', 'plugins/analytics/analytics', 'page_session', 'Pages/Session', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1742, 0, 'en', 'plugins/analytics/analytics', 'avg_duration', 'Avg. Duration', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1743, 0, 'en', 'plugins/analytics/analytics', 'percent_new_session', 'Percent new session', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1744, 0, 'en', 'plugins/analytics/analytics', 'new_users', 'New visitors', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1745, 0, 'en', 'plugins/analytics/analytics', 'visits', 'visits', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1746, 0, 'en', 'plugins/analytics/analytics', 'views', 'views', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1747, 0, 'en', 'plugins/analytics/analytics', 'view_id_not_specified', 'You must provide a valid view id. The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1748, 0, 'en', 'plugins/analytics/analytics', 'credential_is_not_valid', 'Analytics credentials is not valid. The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1749, 0, 'en', 'plugins/analytics/analytics', 'start_date_can_not_before_end_date', 'Start date :start_date cannot be after end date :end_date', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1750, 0, 'en', 'plugins/analytics/analytics', 'wrong_configuration', 'To view analytics you\'ll need to get a google analytics client id and add it to your settings. <br /> You also need JSON credential data. <br /> The document here: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1751, 0, 'en', 'plugins/analytics/analytics', 'settings.title', 'Google Analytics', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1752, 0, 'en', 'plugins/analytics/analytics', 'settings.description', 'Config Credentials for Google Analytics', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1753, 0, 'en', 'plugins/analytics/analytics', 'settings.tracking_code', 'Tracking ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1754, 0, 'en', 'plugins/analytics/analytics', 'settings.tracking_code_placeholder', 'Example: GA-12586526-8', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1755, 0, 'en', 'plugins/analytics/analytics', 'settings.view_id', 'View ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1756, 0, 'en', 'plugins/analytics/analytics', 'settings.view_id_description', 'Google Analytics View ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1757, 0, 'en', 'plugins/analytics/analytics', 'settings.json_credential', 'Service Account Credentials', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1758, 0, 'en', 'plugins/analytics/analytics', 'settings.json_credential_description', 'Service Account Credentials', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1759, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_page', 'Top Most Visit Pages', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1760, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_browser', 'Top Browsers', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1761, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_referrer', 'Top Referrers', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1762, 0, 'en', 'plugins/analytics/analytics', 'widget_analytics_general', 'Site Analytics', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1763, 0, 'vi', 'plugins/analytics/analytics', 'avg_duration', 'Trung bình', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1764, 0, 'vi', 'plugins/analytics/analytics', 'bounce_rate', 'Tỉ lệ thoát', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1765, 0, 'vi', 'plugins/analytics/analytics', 'page_session', 'Trang/Phiên', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1766, 0, 'vi', 'plugins/analytics/analytics', 'pageviews', 'Lượt xem', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1767, 0, 'vi', 'plugins/analytics/analytics', 'sessions', 'Phiên', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1768, 0, 'vi', 'plugins/analytics/analytics', 'views', 'lượt xem', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1769, 0, 'vi', 'plugins/analytics/analytics', 'visitors', 'Người truy cập', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1770, 0, 'vi', 'plugins/analytics/analytics', 'visits', 'lượt ghé thăm', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1771, 0, 'vi', 'plugins/analytics/analytics', 'credential_is_not_valid', 'Thông tin cài đặt không hợp lệ. Tài liệu hướng dẫn tại đây: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1772, 0, 'vi', 'plugins/analytics/analytics', 'new_users', 'Lượt khách mới', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1773, 0, 'vi', 'plugins/analytics/analytics', 'percent_new_session', 'Tỉ lệ khách mới', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1774, 0, 'vi', 'plugins/analytics/analytics', 'start_date_can_not_before_end_date', 'Ngày bắt đầu :start_date không thể sau ngày kết thúc :end_date', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1775, 0, 'vi', 'plugins/analytics/analytics', 'view_id_not_specified', 'Bạn phải cung cấp View ID hợp lê. Tài liệu hướng dẫn tại đây: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1776, 0, 'vi', 'plugins/analytics/analytics', 'wrong_configuration', 'Để xem dữ liệu thống kê Google Analytics, bạn cần lấy thông tin Client ID và thêm nó vào trong phần cài đặt. Bạn cũng cần thông tin xác thực dạng JSON. Tài liệu hướng dẫn tại đây: <a href=\"https://docs.botble.com/cms/:version/plugin-analytics\" target=\"_blank\">https://docs.botble.com/cms/:version/plugin-analytics</a>', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1777, 0, 'vi', 'plugins/analytics/analytics', 'settings.title', 'Google Analytics', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1778, 0, 'vi', 'plugins/analytics/analytics', 'settings.description', 'Config Credentials for Google Analytics', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1779, 0, 'vi', 'plugins/analytics/analytics', 'settings.tracking_code', 'Tracking Code', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1780, 0, 'vi', 'plugins/analytics/analytics', 'settings.tracking_code_placeholder', 'Example: GA-12586526-8', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1781, 0, 'vi', 'plugins/analytics/analytics', 'settings.view_id', 'View ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1782, 0, 'vi', 'plugins/analytics/analytics', 'settings.view_id_description', 'Google Analytics View ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1783, 0, 'vi', 'plugins/analytics/analytics', 'settings.json_credential', 'Service Account Credentials', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1784, 0, 'vi', 'plugins/analytics/analytics', 'settings.json_credential_description', 'Service Account Credentials', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1785, 0, 'vi', 'plugins/analytics/analytics', 'widget_analytics_browser', 'Trình duyệt truy cập nhiều', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1786, 0, 'vi', 'plugins/analytics/analytics', 'widget_analytics_general', 'Thống kê truy cập', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1787, 0, 'vi', 'plugins/analytics/analytics', 'widget_analytics_page', 'Trang được xem nhiều nhất', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1788, 0, 'vi', 'plugins/analytics/analytics', 'widget_analytics_referrer', 'Trang giới thiệu nhiều', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1789, 0, 'en', 'plugins/api/api', 'api_clients', 'API Clients', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1790, 0, 'en', 'plugins/api/api', 'create_new_client', 'Create new client', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1791, 0, 'en', 'plugins/api/api', 'create_new_client_success', 'Create new client successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1792, 0, 'en', 'plugins/api/api', 'edit_client', 'Edit client \":name\"', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1793, 0, 'en', 'plugins/api/api', 'edit_client_success', 'Updated client successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1794, 0, 'en', 'plugins/api/api', 'delete_success', 'Deleted client successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1795, 0, 'en', 'plugins/api/api', 'confirm_delete_title', 'Confirm delete client \":name\"', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1796, 0, 'en', 'plugins/api/api', 'confirm_delete_description', 'Do you really want to delete client \":name\"?', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1797, 0, 'en', 'plugins/api/api', 'cancel_delete', 'No', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1798, 0, 'en', 'plugins/api/api', 'continue_delete', 'Yes, let\'s delete it!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1799, 0, 'en', 'plugins/api/api', 'name', 'Name', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1800, 0, 'en', 'plugins/api/api', 'cancel', 'Cancel', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1801, 0, 'en', 'plugins/api/api', 'save', 'Save', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1802, 0, 'en', 'plugins/api/api', 'edit', 'Edit', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1803, 0, 'en', 'plugins/api/api', 'delete', 'Delete', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1804, 0, 'en', 'plugins/api/api', 'client_id', 'Client ID', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1805, 0, 'en', 'plugins/api/api', 'secret', 'Secret', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1806, 0, 'vi', 'plugins/api/api', 'api_clients', 'API Clients', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1807, 0, 'vi', 'plugins/api/api', 'create_new_client', 'Tạo client mới', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1808, 0, 'vi', 'plugins/api/api', 'create_new_client_success', 'Tạo client mới thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1809, 0, 'vi', 'plugins/api/api', 'edit_client', 'Sửa client \":name\"', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1810, 0, 'vi', 'plugins/api/api', 'edit_client_success', 'Cập nhật client thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1811, 0, 'vi', 'plugins/api/api', 'delete_success', 'Xoá client thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:48'),
(1812, 0, 'vi', 'plugins/api/api', 'confirm_delete_title', 'Xoá client \":name\"', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1813, 0, 'vi', 'plugins/api/api', 'confirm_delete_description', 'Bạn có chắc chắn muốn xoá client \":name\"?', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1814, 0, 'vi', 'plugins/api/api', 'cancel_delete', 'Không', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1815, 0, 'vi', 'plugins/api/api', 'continue_delete', 'Có, tiếp tục xoá!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1816, 0, 'vi', 'plugins/api/api', 'name', 'Tên', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1817, 0, 'vi', 'plugins/api/api', 'cancel', 'Huỷ bỏ', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1818, 0, 'vi', 'plugins/api/api', 'save', 'Lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1819, 0, 'vi', 'plugins/api/api', 'edit', 'Sửa', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1820, 0, 'vi', 'plugins/api/api', 'delete', 'Xoá', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1821, 0, 'vi', 'plugins/api/api', 'client_id', 'Client ID', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1822, 0, 'vi', 'plugins/api/api', 'secret', 'Chuỗi bí mật', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1823, 0, 'en', 'plugins/audit-log/history', 'name', 'Activities Logs', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1824, 0, 'en', 'plugins/audit-log/history', 'created', 'created', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1825, 0, 'en', 'plugins/audit-log/history', 'updated', 'updated', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1826, 0, 'en', 'plugins/audit-log/history', 'deleted', 'deleted', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1827, 0, 'en', 'plugins/audit-log/history', 'logged in', 'logged in', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1828, 0, 'en', 'plugins/audit-log/history', 'logged out', 'logged out', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1829, 0, 'en', 'plugins/audit-log/history', 'changed password', 'changed password', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1830, 0, 'en', 'plugins/audit-log/history', 'updated profile', 'updated profile', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1831, 0, 'en', 'plugins/audit-log/history', 'attached', 'attached', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1832, 0, 'en', 'plugins/audit-log/history', 'shared', 'shared', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1833, 0, 'en', 'plugins/audit-log/history', 'to the system', 'to the system', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1834, 0, 'en', 'plugins/audit-log/history', 'of the system', 'of the system', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1835, 0, 'en', 'plugins/audit-log/history', 'menu', 'menu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1836, 0, 'en', 'plugins/audit-log/history', 'post', 'post', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1837, 0, 'en', 'plugins/audit-log/history', 'page', 'page', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1838, 0, 'en', 'plugins/audit-log/history', 'category', 'category', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1839, 0, 'en', 'plugins/audit-log/history', 'tag', 'tag', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1840, 0, 'en', 'plugins/audit-log/history', 'user', 'user', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1841, 0, 'en', 'plugins/audit-log/history', 'contact', 'contact', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1842, 0, 'en', 'plugins/audit-log/history', 'backup', 'backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1843, 0, 'en', 'plugins/audit-log/history', 'custom-field', 'custom field', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1844, 0, 'en', 'plugins/audit-log/history', 'widget_audit_logs', 'Activities Logs', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1845, 0, 'en', 'plugins/audit-log/history', 'action', 'Action', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1846, 0, 'en', 'plugins/audit-log/history', 'user_agent', 'User Agent', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1847, 0, 'vi', 'plugins/audit-log/history', 'name', 'Lịch sử hoạt động', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1848, 0, 'vi', 'plugins/audit-log/history', 'created', ' đã tạo', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1849, 0, 'vi', 'plugins/audit-log/history', 'updated', ' đã cập nhật', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1850, 0, 'vi', 'plugins/audit-log/history', 'deleted', ' đã xóa', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1851, 0, 'vi', 'plugins/audit-log/history', 'attached', 'đính kèm', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1852, 0, 'vi', 'plugins/audit-log/history', 'backup', 'sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1853, 0, 'vi', 'plugins/audit-log/history', 'category', 'danh mục', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1854, 0, 'vi', 'plugins/audit-log/history', 'changed password', 'thay đổi mật khẩu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1855, 0, 'vi', 'plugins/audit-log/history', 'contact', 'liên hệ', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1856, 0, 'vi', 'plugins/audit-log/history', 'custom-field', 'khung mở rộng', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1857, 0, 'vi', 'plugins/audit-log/history', 'logged in', 'đăng nhập', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1858, 0, 'vi', 'plugins/audit-log/history', 'logged out', 'đăng xuất', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1859, 0, 'vi', 'plugins/audit-log/history', 'menu', 'trình đơn', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1860, 0, 'vi', 'plugins/audit-log/history', 'of the system', 'khỏi hệ thống', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1861, 0, 'vi', 'plugins/audit-log/history', 'page', 'trang', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1862, 0, 'vi', 'plugins/audit-log/history', 'post', 'bài viết', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1863, 0, 'vi', 'plugins/audit-log/history', 'shared', 'đã chia sẻ', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1864, 0, 'vi', 'plugins/audit-log/history', 'tag', 'thẻ', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1865, 0, 'vi', 'plugins/audit-log/history', 'to the system', 'vào hệ thống', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1866, 0, 'vi', 'plugins/audit-log/history', 'updated profile', 'cập nhật tài khoản', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1867, 0, 'vi', 'plugins/audit-log/history', 'user', 'thành viên', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1868, 0, 'vi', 'plugins/audit-log/history', 'widget_audit_logs', 'Lịch sử hoạt động', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1869, 0, 'en', 'plugins/backup/backup', 'name', 'Backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1870, 0, 'en', 'plugins/backup/backup', 'backup_description', 'Backup database and uploads folder.', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1871, 0, 'en', 'plugins/backup/backup', 'create_backup_success', 'Created backup successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1872, 0, 'en', 'plugins/backup/backup', 'delete_backup_success', 'Delete backup successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1873, 0, 'en', 'plugins/backup/backup', 'restore_backup_success', 'Restore backup successfully!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1874, 0, 'en', 'plugins/backup/backup', 'generate_btn', 'Generate backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1875, 0, 'en', 'plugins/backup/backup', 'create', 'Create backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1876, 0, 'en', 'plugins/backup/backup', 'restore', 'Restore backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1877, 0, 'en', 'plugins/backup/backup', 'create_btn', 'Create', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1878, 0, 'en', 'plugins/backup/backup', 'restore_btn', 'Restore', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1879, 0, 'en', 'plugins/backup/backup', 'restore_confirm_msg', 'Do you really want to restore this revision?', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1880, 0, 'en', 'plugins/backup/backup', 'download_uploads_folder', 'Download backup of uploads folder', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1881, 0, 'en', 'plugins/backup/backup', 'download_database', 'Download backup of database', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1882, 0, 'en', 'plugins/backup/backup', 'restore_tooltip', 'Restore this backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1883, 0, 'en', 'plugins/backup/backup', 'demo_alert', 'Hi guest, if you see demo site is destroyed, please help me <a href=\":link\">go here</a> and restore demo site to the latest revision! Thank you so much!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1884, 0, 'en', 'plugins/backup/backup', 'menu_name', 'Backups', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1885, 0, 'en', 'plugins/backup/backup', 'settings.title', 'Backup', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1886, 0, 'en', 'plugins/backup/backup', 'settings.description', 'Settings for backup function', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1887, 0, 'en', 'plugins/backup/backup', 'settings.backup_mysql_execute_path', 'MySQL execute path', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1888, 0, 'en', 'plugins/backup/backup', 'settings.backup_mysql_execute_path_placeholder', 'Default: /usr/mysql/bin. Ignore it if you don\'t sure about it', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1889, 0, 'vi', 'plugins/backup/backup', 'backup_description', 'Sao lưu cơ sở dữ liệu và thư mục /uploads', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1890, 0, 'vi', 'plugins/backup/backup', 'create', 'Tạo bản sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1891, 0, 'vi', 'plugins/backup/backup', 'create_backup_success', 'Tạo bản sao lưu thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1892, 0, 'vi', 'plugins/backup/backup', 'create_btn', 'Tạo', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1893, 0, 'vi', 'plugins/backup/backup', 'delete_backup_success', 'Xóa bản sao lưu thành công!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1894, 0, 'vi', 'plugins/backup/backup', 'generate_btn', 'Tạo sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1895, 0, 'vi', 'plugins/backup/backup', 'name', 'Sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1896, 0, 'vi', 'plugins/backup/backup', 'restore', 'Khôi phục bản sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1897, 0, 'vi', 'plugins/backup/backup', 'restore_backup_success', 'Khôi phục bản sao lưu thành công', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1898, 0, 'vi', 'plugins/backup/backup', 'restore_btn', 'Khôi phục', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1899, 0, 'vi', 'plugins/backup/backup', 'restore_confirm_msg', 'Bạn có chắc chắn muốn khôi phục hệ thống về thời điểm tạo bản sao lưu này?', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1900, 0, 'vi', 'plugins/backup/backup', 'restore_tooltip', 'Khôi phục bản sao lưu này', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1901, 0, 'vi', 'plugins/backup/backup', 'download_database', 'Tải về bản sao lưu CSDL', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1902, 0, 'vi', 'plugins/backup/backup', 'download_uploads_folder', 'Tải về bản sao lưu thư mục uploads', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1903, 0, 'vi', 'plugins/backup/backup', 'menu_name', 'Sao lưu', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1904, 0, 'vi', 'plugins/backup/backup', 'demo_alert', 'Chào khách, nếu bạn thấy trang demo bị phá hoại, hãy tới <a href=\":link\">trang sao lưu</a> và khôi phục bản sao lưu cuối cùng. Cảm ơn bạn nhiều!', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1905, 0, 'en', 'plugins/block/block', 'name', 'Block', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1906, 0, 'en', 'plugins/block/block', 'create', 'New block', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1907, 0, 'en', 'plugins/block/block', 'edit', 'Edit block', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1908, 0, 'en', 'plugins/block/block', 'menu', 'Static Blocks', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1909, 0, 'vi', 'plugins/block/block', 'name', 'Nội dung tĩnh', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1910, 0, 'vi', 'plugins/block/block', 'create', 'Thêm nội dung tĩnh', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1911, 0, 'vi', 'plugins/block/block', 'edit', 'Sửa nội dung tĩnh', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1912, 0, 'vi', 'plugins/block/block', 'list', 'Danh sách nội dung tĩnh', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1913, 0, 'vi', 'plugins/block/block', 'menu', 'Nội dung tĩnh', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1914, 0, 'en', 'plugins/blog/base', 'menu_name', 'Blog', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1915, 0, 'en', 'plugins/blog/base', 'blog_page', 'Blog Page', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1916, 0, 'en', 'plugins/blog/categories', 'create', 'Create new category', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1917, 0, 'en', 'plugins/blog/categories', 'edit', 'Edit category', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1918, 0, 'en', 'plugins/blog/categories', 'menu', 'Categories', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1919, 0, 'en', 'plugins/blog/categories', 'edit_this_category', 'Edit this category', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1920, 0, 'en', 'plugins/blog/categories', 'menu_name', 'Categories', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1921, 0, 'en', 'plugins/blog/categories', 'none', 'None', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1922, 0, 'en', 'plugins/blog/member', 'dob', 'Born :date', '2020-03-29 20:06:58', '2020-04-24 09:38:49'),
(1923, 0, 'en', 'plugins/blog/member', 'draft_posts', 'Draft Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1924, 0, 'en', 'plugins/blog/member', 'pending_posts', 'Pending Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1925, 0, 'en', 'plugins/blog/member', 'published_posts', 'Published Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1926, 0, 'en', 'plugins/blog/member', 'posts', 'Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1927, 0, 'en', 'plugins/blog/member', 'write_post', 'Write a post', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1928, 0, 'en', 'plugins/blog/posts', 'model', 'Post', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1929, 0, 'en', 'plugins/blog/posts', 'models', 'Posts', '2020-03-29 20:06:58', '2020-04-24 09:38:50'),
(1930, 0, 'en', 'plugins/blog/posts', 'create', 'Create new post', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1931, 0, 'en', 'plugins/blog/posts', 'edit', 'Edit post', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1932, 0, 'en', 'plugins/blog/posts', 'form.name', 'Name', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1933, 0, 'en', 'plugins/blog/posts', 'form.name_placeholder', 'Post\'s name (Maximum :c characters)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1934, 0, 'en', 'plugins/blog/posts', 'form.description', 'Description', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1935, 0, 'en', 'plugins/blog/posts', 'form.description_placeholder', 'Short description for post (Maximum :c characters)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1936, 0, 'en', 'plugins/blog/posts', 'form.categories', 'Categories', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1937, 0, 'en', 'plugins/blog/posts', 'form.tags', 'Tags', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1938, 0, 'en', 'plugins/blog/posts', 'form.tags_placeholder', 'Tags', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1939, 0, 'en', 'plugins/blog/posts', 'form.content', 'Content', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1940, 0, 'en', 'plugins/blog/posts', 'form.is_featured', 'Is featured?', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1941, 0, 'en', 'plugins/blog/posts', 'form.note', 'Note content', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1942, 0, 'en', 'plugins/blog/posts', 'form.format_type', 'Format', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1943, 0, 'en', 'plugins/blog/posts', 'cannot_delete', 'Post could not be deleted', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1944, 0, 'en', 'plugins/blog/posts', 'post_deleted', 'Post deleted', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1945, 0, 'en', 'plugins/blog/posts', 'posts', 'Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1946, 0, 'en', 'plugins/blog/posts', 'edit_this_post', 'Edit this post', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1947, 0, 'en', 'plugins/blog/posts', 'no_new_post_now', 'There is no new post now!', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1948, 0, 'en', 'plugins/blog/posts', 'menu_name', 'Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1949, 0, 'en', 'plugins/blog/posts', 'widget_posts_recent', 'Recent Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1950, 0, 'en', 'plugins/blog/posts', 'categories', 'Categories', '2020-03-29 20:06:59', '2020-04-24 09:38:50');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1951, 0, 'en', 'plugins/blog/posts', 'author', 'Author', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1952, 0, 'en', 'plugins/blog/settings', 'title', 'Blog', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1953, 0, 'en', 'plugins/blog/settings', 'description', 'Settings for Blog plugin', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1954, 0, 'en', 'plugins/blog/settings', 'select', '-- Select --', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1955, 0, 'en', 'plugins/blog/settings', 'blog_page_id', 'Blog page', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1956, 0, 'en', 'plugins/blog/tags', 'model', 'Tag', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1957, 0, 'en', 'plugins/blog/tags', 'models', 'Tags', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1958, 0, 'en', 'plugins/blog/tags', 'form.name', 'Name', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1959, 0, 'en', 'plugins/blog/tags', 'form.name_placeholder', 'Tag\'s name (Maximum 120 characters)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1960, 0, 'en', 'plugins/blog/tags', 'form.description', 'Description', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1961, 0, 'en', 'plugins/blog/tags', 'form.description_placeholder', 'Short description for tag (Maximum 400 characters)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1962, 0, 'en', 'plugins/blog/tags', 'form.categories', 'Categories', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1963, 0, 'en', 'plugins/blog/tags', 'notices.no_select', 'Please select at least one tag to take this action!', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1964, 0, 'en', 'plugins/blog/tags', 'create', 'Create new tag', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1965, 0, 'en', 'plugins/blog/tags', 'edit', 'Edit tag', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1966, 0, 'en', 'plugins/blog/tags', 'cannot_delete', 'Tag could not be deleted', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1967, 0, 'en', 'plugins/blog/tags', 'deleted', 'Tag deleted', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1968, 0, 'en', 'plugins/blog/tags', 'menu', 'Tags', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1969, 0, 'en', 'plugins/blog/tags', 'edit_this_tag', 'Edit this tag', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1970, 0, 'en', 'plugins/blog/tags', 'menu_name', 'Tags', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1971, 0, 'vi', 'plugins/blog/base', 'menu_name', 'Blog', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1972, 0, 'vi', 'plugins/blog/categories', 'model', 'Danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1973, 0, 'vi', 'plugins/blog/categories', 'models', 'Danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1974, 0, 'vi', 'plugins/blog/categories', 'list', 'Danh sách danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1975, 0, 'vi', 'plugins/blog/categories', 'create', 'Thêm danh mục mới', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1976, 0, 'vi', 'plugins/blog/categories', 'edit', 'Sửa danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1977, 0, 'vi', 'plugins/blog/categories', 'menu_name', 'Danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1978, 0, 'vi', 'plugins/blog/categories', 'edit_this_category', 'Sửa danh mục này', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1979, 0, 'vi', 'plugins/blog/categories', 'menu', 'Danh mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1980, 0, 'vi', 'plugins/blog/member', 'dob', 'Born :date', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1981, 0, 'vi', 'plugins/blog/member', 'draft_posts', 'Draft Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1982, 0, 'vi', 'plugins/blog/member', 'pending_posts', 'Pending Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1983, 0, 'vi', 'plugins/blog/member', 'published_posts', 'Published Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1984, 0, 'vi', 'plugins/blog/member', 'posts', 'Posts', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1985, 0, 'vi', 'plugins/blog/member', 'write_post', 'Write a post', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1986, 0, 'vi', 'plugins/blog/posts', 'model', 'Bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1987, 0, 'vi', 'plugins/blog/posts', 'models', 'Bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1988, 0, 'vi', 'plugins/blog/posts', 'list', 'Danh sách bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1989, 0, 'vi', 'plugins/blog/posts', 'create', 'Thêm bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1990, 0, 'vi', 'plugins/blog/posts', 'edit', 'Sửa bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1991, 0, 'vi', 'plugins/blog/posts', 'form.name', 'Tên', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1992, 0, 'vi', 'plugins/blog/posts', 'form.name_placeholder', 'Tên bài viết (Tối đa 120 kí tự)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1993, 0, 'vi', 'plugins/blog/posts', 'form.description', 'Mô tả', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1994, 0, 'vi', 'plugins/blog/posts', 'form.description_placeholder', 'Mô tả ngắn cho bài viết (Tối đa 400 kí tự)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1995, 0, 'vi', 'plugins/blog/posts', 'form.categories', 'Chuyên mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1996, 0, 'vi', 'plugins/blog/posts', 'form.tags', 'Từ khóa', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1997, 0, 'vi', 'plugins/blog/posts', 'form.tags_placeholder', 'Thêm từ khóa', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1998, 0, 'vi', 'plugins/blog/posts', 'form.content', 'Nội dung', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(1999, 0, 'vi', 'plugins/blog/posts', 'form.featured', 'Bài viết nổi bật?', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2000, 0, 'vi', 'plugins/blog/posts', 'form.note', 'Nội dung ghi chú', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2001, 0, 'vi', 'plugins/blog/posts', 'form.format_type', 'Định dạng', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2002, 0, 'vi', 'plugins/blog/posts', 'post_deleted', 'Xóa bài viết thành công', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2003, 0, 'vi', 'plugins/blog/posts', 'cannot_delete', 'Không thể xóa bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2004, 0, 'vi', 'plugins/blog/posts', 'menu_name', 'Bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2005, 0, 'vi', 'plugins/blog/posts', 'edit_this_post', 'Sửa bài viết này', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2006, 0, 'vi', 'plugins/blog/posts', 'no_new_post_now', 'Hiện tại không có bài viết mới!', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2007, 0, 'vi', 'plugins/blog/posts', 'posts', 'Bài viết', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2008, 0, 'vi', 'plugins/blog/posts', 'widget_posts_recent', 'Bài viết gần đây', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2009, 0, 'vi', 'plugins/blog/posts', 'author', 'Tác giả', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2010, 0, 'vi', 'plugins/blog/settings', 'title', 'Blog', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2011, 0, 'vi', 'plugins/blog/settings', 'description', 'Settings for Blog plugin', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2012, 0, 'vi', 'plugins/blog/settings', 'select', '-- Select --', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2013, 0, 'vi', 'plugins/blog/settings', 'blog_page_id', 'Blog page', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2014, 0, 'vi', 'plugins/blog/tags', 'model', 'Thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2015, 0, 'vi', 'plugins/blog/tags', 'models', 'Thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2016, 0, 'vi', 'plugins/blog/tags', 'list', 'Danh sách thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2017, 0, 'vi', 'plugins/blog/tags', 'create', 'Thêm thẻ mới', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2018, 0, 'vi', 'plugins/blog/tags', 'edit', 'Sửa thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2019, 0, 'vi', 'plugins/blog/tags', 'form.name', 'Tên', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2020, 0, 'vi', 'plugins/blog/tags', 'form.name_placeholder', 'Tên thẻ (Tối đa 120 kí tự)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2021, 0, 'vi', 'plugins/blog/tags', 'form.description', 'Mô tả', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2022, 0, 'vi', 'plugins/blog/tags', 'form.description_placeholder', 'Mô tả ngắn cho tag (Tối đa 400 kí tự)', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2023, 0, 'vi', 'plugins/blog/tags', 'form.categories', 'Chuyên mục', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2024, 0, 'vi', 'plugins/blog/tags', 'notices.no_select', 'Chọn ít nhất 1 bài viết để thực hiện hành động này!', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2025, 0, 'vi', 'plugins/blog/tags', 'cannot_delete', 'Không thể xóa thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2026, 0, 'vi', 'plugins/blog/tags', 'deleted', 'Xóa thẻ thành công', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2027, 0, 'vi', 'plugins/blog/tags', 'menu_name', 'Thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2028, 0, 'vi', 'plugins/blog/tags', 'edit_this_tag', 'Sửa thẻ này', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2029, 0, 'vi', 'plugins/blog/tags', 'menu', 'Thẻ', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2030, 0, 'en', 'plugins/captcha/captcha', 'settings.title', 'Captcha', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2031, 0, 'en', 'plugins/captcha/captcha', 'settings.description', 'Settings for Google Captcha', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2032, 0, 'en', 'plugins/captcha/captcha', 'settings.captcha_site_key', 'Captcha Site Key', '2020-03-29 20:06:59', '2020-04-24 09:38:50'),
(2033, 0, 'en', 'plugins/captcha/captcha', 'settings.captcha_secret', 'Captcha Secret', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2034, 0, 'en', 'plugins/captcha/captcha', 'settings.enable_captcha', 'Enable Captcha, show Captcha in contact form?', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2035, 0, 'en', 'plugins/captcha/captcha', 'settings.helper', 'Go here to get credentials https://www.google.com/recaptcha/admin#list. Please select reCaptcha v2.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2036, 0, 'vi', 'plugins/captcha/captcha', 'settings.title', 'Captcha', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2037, 0, 'vi', 'plugins/captcha/captcha', 'settings.description', 'Cài đặt cho Google Captcha', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2038, 0, 'vi', 'plugins/captcha/captcha', 'settings.captcha_site_key', 'Captcha Site Key', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2039, 0, 'vi', 'plugins/captcha/captcha', 'settings.captcha_secret', 'Captcha Secret', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2040, 0, 'vi', 'plugins/captcha/captcha', 'settings.enable_captcha', 'Bật Captcha, hiển thị Captcha trong form liên hệ?', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2041, 0, 'vi', 'plugins/captcha/captcha', 'settings.helper', 'Truy cập https://www.google.com/recaptcha/admin#list để lấy thông tin về Site key và Secret. Hãy chọn reCaptcha v2.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2042, 0, 'en', 'plugins/contact/contact', 'menu', 'Contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2043, 0, 'en', 'plugins/contact/contact', 'model', 'Contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2044, 0, 'en', 'plugins/contact/contact', 'models', 'Contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2045, 0, 'en', 'plugins/contact/contact', 'edit', 'View contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2046, 0, 'en', 'plugins/contact/contact', 'tables.phone', 'Phone', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2047, 0, 'en', 'plugins/contact/contact', 'tables.email', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2048, 0, 'en', 'plugins/contact/contact', 'tables.full_name', 'Full Name', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2049, 0, 'en', 'plugins/contact/contact', 'tables.time', 'Time', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2050, 0, 'en', 'plugins/contact/contact', 'tables.address', 'Address', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2051, 0, 'en', 'plugins/contact/contact', 'tables.subject', 'Subject', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2052, 0, 'en', 'plugins/contact/contact', 'tables.content', 'Content', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2053, 0, 'en', 'plugins/contact/contact', 'contact_information', 'Contact information', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2054, 0, 'en', 'plugins/contact/contact', 'replies', 'Replies', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2055, 0, 'en', 'plugins/contact/contact', 'email.header', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2056, 0, 'en', 'plugins/contact/contact', 'email.title', 'New contact from your site', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2057, 0, 'en', 'plugins/contact/contact', 'email.success', 'Send message successfully!', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2058, 0, 'en', 'plugins/contact/contact', 'email.failed', 'Can\'t send message on this time, please try again later!', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2059, 0, 'en', 'plugins/contact/contact', 'name.required', 'Name is required', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2060, 0, 'en', 'plugins/contact/contact', 'email.required', 'Email is required', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2061, 0, 'en', 'plugins/contact/contact', 'email.email', 'The email address is not valid', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2062, 0, 'en', 'plugins/contact/contact', 'content.required', 'Content is required', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2063, 0, 'en', 'plugins/contact/contact', 'g-recaptcha-response.required', 'Please confirm you are not a robot before sending the message.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2064, 0, 'en', 'plugins/contact/contact', 'g-recaptcha-response.captcha', 'You are not confirm robot yet.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2065, 0, 'en', 'plugins/contact/contact', 'contact_sent_from', 'This contact information sent from', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2066, 0, 'en', 'plugins/contact/contact', 'sender', 'Sender', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2067, 0, 'en', 'plugins/contact/contact', 'sender_email', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2068, 0, 'en', 'plugins/contact/contact', 'sender_address', 'Address', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2069, 0, 'en', 'plugins/contact/contact', 'sender_phone', 'Phone', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2070, 0, 'en', 'plugins/contact/contact', 'message_content', 'Message content', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2071, 0, 'en', 'plugins/contact/contact', 'sent_from', 'Email sent from', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2072, 0, 'en', 'plugins/contact/contact', 'form_name', 'Name', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2073, 0, 'en', 'plugins/contact/contact', 'form_email', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2074, 0, 'en', 'plugins/contact/contact', 'form_address', 'Address', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2075, 0, 'en', 'plugins/contact/contact', 'form_subject', 'Subject', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2076, 0, 'en', 'plugins/contact/contact', 'form_phone', 'Phone', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2077, 0, 'en', 'plugins/contact/contact', 'form_message', 'Message', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2078, 0, 'en', 'plugins/contact/contact', 'confirm_not_robot', 'Please confirm you are not robot', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2079, 0, 'en', 'plugins/contact/contact', 'required_field', 'The field with (<span style=\"color: red\">*</span>) is required.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2080, 0, 'en', 'plugins/contact/contact', 'send_btn', 'Send message', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2081, 0, 'en', 'plugins/contact/contact', 'new_msg_notice', 'You have <span class=\"bold\">:count</span> New Messages', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2082, 0, 'en', 'plugins/contact/contact', 'view_all', 'View all', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2083, 0, 'en', 'plugins/contact/contact', 'statuses.read', 'Read', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2084, 0, 'en', 'plugins/contact/contact', 'statuses.unread', 'UnRead', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2085, 0, 'en', 'plugins/contact/contact', 'phone', 'Phone', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2086, 0, 'en', 'plugins/contact/contact', 'address', 'Address', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2087, 0, 'en', 'plugins/contact/contact', 'message', 'Message', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2088, 0, 'en', 'plugins/contact/contact', 'settings.email.title', 'Contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2089, 0, 'en', 'plugins/contact/contact', 'settings.email.description', 'Contact email configuration', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2090, 0, 'en', 'plugins/contact/contact', 'settings.email.templates.notice_title', 'Send notice to administrator', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2091, 0, 'en', 'plugins/contact/contact', 'settings.email.templates.notice_description', 'Email template to send notice to administrator when system get new contact', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2092, 0, 'vi', 'plugins/contact/contact', 'menu', 'Liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2093, 0, 'vi', 'plugins/contact/contact', 'model', 'Liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2094, 0, 'vi', 'plugins/contact/contact', 'models', 'Liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2095, 0, 'vi', 'plugins/contact/contact', 'list', 'Danh sách liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2096, 0, 'vi', 'plugins/contact/contact', 'edit', 'Xem liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2097, 0, 'vi', 'plugins/contact/contact', 'tables.phone', 'Điện thoại', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2098, 0, 'vi', 'plugins/contact/contact', 'tables.email', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2099, 0, 'vi', 'plugins/contact/contact', 'tables.full_name', 'Họ tên', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2100, 0, 'vi', 'plugins/contact/contact', 'tables.time', 'Thời gian', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2101, 0, 'vi', 'plugins/contact/contact', 'tables.address', 'Địa địa chỉ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2102, 0, 'vi', 'plugins/contact/contact', 'tables.subject', 'Tiêu đề', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2103, 0, 'vi', 'plugins/contact/contact', 'tables.content', 'Nội dung', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2104, 0, 'vi', 'plugins/contact/contact', 'form.is_read', 'Đã xem?', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2105, 0, 'vi', 'plugins/contact/contact', 'form.status', 'Trạng thái', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2106, 0, 'vi', 'plugins/contact/contact', 'notices.no_select', 'Chọn ít nhất 1 liên hệ để thực hiện hành động này!', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2107, 0, 'vi', 'plugins/contact/contact', 'notices.update_success_message', 'Cập nhật thành công', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2108, 0, 'vi', 'plugins/contact/contact', 'cannot_delete', 'Không thể xóa liên hệ này', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2109, 0, 'vi', 'plugins/contact/contact', 'deleted', 'Liên hệ đã được xóa', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2110, 0, 'vi', 'plugins/contact/contact', 'contact_information', 'Thông tin liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2111, 0, 'vi', 'plugins/contact/contact', 'email.title', 'Thông tin liên hệ mới', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2112, 0, 'vi', 'plugins/contact/contact', 'email.success', 'Gửi tin nhắn thành công!', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2113, 0, 'vi', 'plugins/contact/contact', 'email.failed', 'Có lỗi trong quá trình gửi tin nhắn!', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2114, 0, 'vi', 'plugins/contact/contact', 'email.required', 'Email không được để trống', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2115, 0, 'vi', 'plugins/contact/contact', 'email.email', 'Địa chỉ email không hợp lệ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2116, 0, 'vi', 'plugins/contact/contact', 'email.header', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2117, 0, 'vi', 'plugins/contact/contact', 'name.required', 'Họ tên không được để trống', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2118, 0, 'vi', 'plugins/contact/contact', 'content.required', 'Nội dung tin nhắn không được để trống', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2119, 0, 'vi', 'plugins/contact/contact', 'g-recaptcha-response.required', 'Hãy xác minh không phải là robot trước khi gửi tin nhắn.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2120, 0, 'vi', 'plugins/contact/contact', 'g-recaptcha-response.captcha', 'Bạn chưa xác minh không phải là robot thành công.', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2121, 0, 'vi', 'plugins/contact/contact', 'confirm_not_robot', 'Xác nhận không phải người máy', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2122, 0, 'vi', 'plugins/contact/contact', 'contact_sent_from', 'Liên hệ này được gửi từ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2123, 0, 'vi', 'plugins/contact/contact', 'form_address', 'Địa chỉ', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2124, 0, 'vi', 'plugins/contact/contact', 'form_email', 'Thư điện tử', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2125, 0, 'vi', 'plugins/contact/contact', 'form_message', 'Thông điệp', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2126, 0, 'vi', 'plugins/contact/contact', 'form_name', 'Họ tên', '2020-03-29 20:06:59', '2020-04-24 09:38:51'),
(2127, 0, 'vi', 'plugins/contact/contact', 'form_phone', 'Số điện thoại', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2128, 0, 'vi', 'plugins/contact/contact', 'form_subject', 'Tiêu đề', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2129, 0, 'vi', 'plugins/contact/contact', 'message_content', 'Nội dung thông điệp', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2130, 0, 'vi', 'plugins/contact/contact', 'required_field', 'Những trường có dấu (<span style=\"color: red\">*</span>) là bắt buộc.', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2131, 0, 'vi', 'plugins/contact/contact', 'send_btn', 'Gửi tin nhắn', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2132, 0, 'vi', 'plugins/contact/contact', 'sender', 'Người gửi', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2133, 0, 'vi', 'plugins/contact/contact', 'sender_address', 'Địa chỉ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2134, 0, 'vi', 'plugins/contact/contact', 'sender_email', 'Thư điện tử', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2135, 0, 'vi', 'plugins/contact/contact', 'sender_phone', 'Số điện thoại', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2136, 0, 'vi', 'plugins/contact/contact', 'sent_from', 'Thư được gửi từ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2137, 0, 'vi', 'plugins/contact/contact', 'mark_as_read', 'Đánh dấu đã đọc', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2138, 0, 'vi', 'plugins/contact/contact', 'mark_as_unread', 'Đánh dấu chưa đọc', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2139, 0, 'vi', 'plugins/contact/contact', 'address', 'Địa chỉ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2140, 0, 'vi', 'plugins/contact/contact', 'message', 'Liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2141, 0, 'vi', 'plugins/contact/contact', 'new_msg_notice', 'Bạn có <span class=\"bold\">:count</span> tin nhắn mới', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2142, 0, 'vi', 'plugins/contact/contact', 'phone', 'Điện thoại', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2143, 0, 'vi', 'plugins/contact/contact', 'statuses.read', 'Đã đọc', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2144, 0, 'vi', 'plugins/contact/contact', 'statuses.unread', 'Chưa đọc', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2145, 0, 'vi', 'plugins/contact/contact', 'view_all', 'Xem tất cả', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2146, 0, 'vi', 'plugins/contact/contact', 'settings.email.title', 'Liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2147, 0, 'vi', 'plugins/contact/contact', 'settings.email.description', 'Cấu hình thông tin cho mục liên hệ', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2148, 0, 'vi', 'plugins/contact/contact', 'settings.email.templates.notice_title', 'Thông báo tới admin', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2149, 0, 'vi', 'plugins/contact/contact', 'settings.email.templates.notice_description', 'Mẫu nội dung email gửi tới admin khi có liên hệ mới', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2150, 0, 'en', 'plugins/custom-field/base', 'admin_menu.title', 'Custom fields', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2151, 0, 'en', 'plugins/custom-field/base', 'page_title', 'Custom fields', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2152, 0, 'en', 'plugins/custom-field/base', 'all_field_groups', 'All field groups', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2153, 0, 'en', 'plugins/custom-field/base', 'form.create_field_group', 'Create field group', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2154, 0, 'en', 'plugins/custom-field/base', 'form.edit_field_group', 'Edit field group', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2155, 0, 'en', 'plugins/custom-field/base', 'form.field_items_information', 'Field items information', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2156, 0, 'en', 'plugins/custom-field/base', 'form.repeater_fields', 'Repeater', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2157, 0, 'en', 'plugins/custom-field/base', 'form.add_field', 'Add field', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2158, 0, 'en', 'plugins/custom-field/base', 'form.remove_field', 'Remove field', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2159, 0, 'en', 'plugins/custom-field/base', 'form.close_field', 'Close field', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2160, 0, 'en', 'plugins/custom-field/base', 'form.field_label', 'Label', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2161, 0, 'en', 'plugins/custom-field/base', 'form.field_label_helper', 'This is the title of field item. It will be shown on edit pages.', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2162, 0, 'en', 'plugins/custom-field/base', 'form.field_name', 'Field name', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2163, 0, 'en', 'plugins/custom-field/base', 'form.field_name_helper', 'The alias of field item. Accepted numbers, characters and underscore.', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2164, 0, 'en', 'plugins/custom-field/base', 'form.field_type', 'Field type', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2165, 0, 'en', 'plugins/custom-field/base', 'form.field_type_helper', 'Please select the type of this field.', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2166, 0, 'en', 'plugins/custom-field/base', 'form.field_instructions', 'Field instructions', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2167, 0, 'en', 'plugins/custom-field/base', 'form.field_instructions_helper', 'HThe instructions guide for user easier know what they need to input.', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2168, 0, 'en', 'plugins/custom-field/base', 'form.default_value', 'Default value', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2169, 0, 'en', 'plugins/custom-field/base', 'form.default_value_helper', 'The default value of field when leave it blank', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2170, 0, 'en', 'plugins/custom-field/base', 'form.placeholder', 'Placeholder', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2171, 0, 'en', 'plugins/custom-field/base', 'form.placeholder_helper', 'Placeholder text', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2172, 0, 'en', 'plugins/custom-field/base', 'form.rows', 'Rows', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2173, 0, 'en', 'plugins/custom-field/base', 'form.rows_helper', 'Rows of this textarea', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2174, 0, 'en', 'plugins/custom-field/base', 'form.toolbar', 'Toolbar', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2175, 0, 'en', 'plugins/custom-field/base', 'form.toolbar_helper', 'The toolbar when use editor', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2176, 0, 'en', 'plugins/custom-field/base', 'form.toolbar_basic', 'Basic', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2177, 0, 'en', 'plugins/custom-field/base', 'form.toolbar_full', 'Full', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2178, 0, 'en', 'plugins/custom-field/base', 'form.choices', 'Choices', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2179, 0, 'en', 'plugins/custom-field/base', 'form.choices_helper', 'Enter each choice on a new line.<br>For more control, you may specify both a value and label like this:<br>red: Red<br>blue: Blue', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2180, 0, 'en', 'plugins/custom-field/base', 'form.button_label', 'Button for repeater', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2181, 0, 'en', 'plugins/custom-field/base', 'form.groups.basic', 'Basic', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2182, 0, 'en', 'plugins/custom-field/base', 'form.groups.content', 'Content', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2183, 0, 'en', 'plugins/custom-field/base', 'form.groups.choice', 'Choices', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2184, 0, 'en', 'plugins/custom-field/base', 'form.groups.other', 'Other', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2185, 0, 'en', 'plugins/custom-field/base', 'form.types.text', 'Text field', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2186, 0, 'en', 'plugins/custom-field/base', 'form.types.textarea', 'Textarea', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2187, 0, 'en', 'plugins/custom-field/base', 'form.types.number', 'Number', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2188, 0, 'en', 'plugins/custom-field/base', 'form.types.email', 'Email', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2189, 0, 'en', 'plugins/custom-field/base', 'form.types.password', 'Password', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2190, 0, 'en', 'plugins/custom-field/base', 'form.types.wysiwyg', 'WYSIWYG editor', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2191, 0, 'en', 'plugins/custom-field/base', 'form.types.image', 'Image', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2192, 0, 'en', 'plugins/custom-field/base', 'form.types.file', 'File', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2193, 0, 'en', 'plugins/custom-field/base', 'form.types.select', 'Select', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2194, 0, 'en', 'plugins/custom-field/base', 'form.types.checkbox', 'Checkbox', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2195, 0, 'en', 'plugins/custom-field/base', 'form.types.radio', 'Radio', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2196, 0, 'en', 'plugins/custom-field/base', 'form.types.repeater', 'Repeater', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2197, 0, 'en', 'plugins/custom-field/base', 'form.rules.rules', 'Display rules', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2198, 0, 'en', 'plugins/custom-field/base', 'form.rules.rules_helper', 'Show this field group if', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2199, 0, 'en', 'plugins/custom-field/base', 'form.rules.add_rule_group', 'Add rule group', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2200, 0, 'en', 'plugins/custom-field/base', 'form.rules.is_equal_to', 'Equal to', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2201, 0, 'en', 'plugins/custom-field/base', 'form.rules.is_not_equal_to', 'Not equal to', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2202, 0, 'en', 'plugins/custom-field/base', 'form.rules.and', 'And', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2203, 0, 'en', 'plugins/custom-field/base', 'form.rules.or', 'Or', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2204, 0, 'en', 'plugins/custom-field/base', 'import', 'Import', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2205, 0, 'en', 'plugins/custom-field/base', 'export', 'Export', '2020-03-29 20:06:59', '2020-04-24 09:38:52'),
(2206, 0, 'en', 'plugins/custom-field/permissions', 'view-custom-fields', 'View custom fields', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2207, 0, 'en', 'plugins/custom-field/permissions', 'create-field-groups', 'Create field groups', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2208, 0, 'en', 'plugins/custom-field/permissions', 'edit-field-groups', 'Edit field groups', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2209, 0, 'en', 'plugins/custom-field/permissions', 'delete-field-groups', 'Delete field groups', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2210, 0, 'en', 'plugins/custom-field/rules', 'groups.basic', 'Basic', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2211, 0, 'en', 'plugins/custom-field/rules', 'groups.other', 'Other', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2212, 0, 'en', 'plugins/custom-field/rules', 'groups.blog', 'Blog', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2213, 0, 'en', 'plugins/custom-field/rules', 'logged_in_user', 'Logged in user', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2214, 0, 'en', 'plugins/custom-field/rules', 'logged_in_user_has_role', 'Logged in has role', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2215, 0, 'en', 'plugins/custom-field/rules', 'page_template', 'Page template', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2216, 0, 'en', 'plugins/custom-field/rules', 'page', 'Page', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2217, 0, 'en', 'plugins/custom-field/rules', 'model_name', 'Model name', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2218, 0, 'en', 'plugins/custom-field/rules', 'model_name_page', 'Page', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2219, 0, 'en', 'plugins/custom-field/rules', 'category', 'Category', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2220, 0, 'en', 'plugins/custom-field/rules', 'post_with_related_category', 'Post with related category', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2221, 0, 'en', 'plugins/custom-field/rules', 'model_name_post', 'Post (blog)', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2222, 0, 'en', 'plugins/custom-field/rules', 'model_name_category', 'Category (blog)', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2223, 0, 'en', 'plugins/custom-field/rules', 'post_format', 'Post format', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2224, 0, 'vi', 'plugins/custom-field/base', 'admin_menu.title', 'Trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2225, 0, 'vi', 'plugins/custom-field/base', 'page_title', 'Trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2226, 0, 'vi', 'plugins/custom-field/base', 'all_field_groups', 'Tất cả nhóm trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2227, 0, 'vi', 'plugins/custom-field/base', 'form.create_field_group', 'Thêm nhóm trường mới', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2228, 0, 'vi', 'plugins/custom-field/base', 'form.edit_field_group', 'Chỉnh sửa trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2229, 0, 'vi', 'plugins/custom-field/base', 'form.field_items_information', 'Thiết đặt các mục con của trường này', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2230, 0, 'vi', 'plugins/custom-field/base', 'form.repeater_fields', 'Bộ lặp', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2231, 0, 'vi', 'plugins/custom-field/base', 'form.add_field', 'Thêm trường', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2232, 0, 'vi', 'plugins/custom-field/base', 'form.remove_field', 'Xóa trường này', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2233, 0, 'vi', 'plugins/custom-field/base', 'form.close_field', 'Thu nhỏ trường này lại', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2234, 0, 'vi', 'plugins/custom-field/base', 'form.field_label', 'Nhãn', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2235, 0, 'vi', 'plugins/custom-field/base', 'form.field_label_helper', 'Đây là tiêu đề của từng trường, xuất hiện ở các trang chỉnh sửa', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2236, 0, 'vi', 'plugins/custom-field/base', 'form.field_name', 'Tên truy nhập trường', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2237, 0, 'vi', 'plugins/custom-field/base', 'form.field_name_helper', 'Tên truy nhập của trường. Chỉ chấp nhận ký tự thường và dấu gạch dưới', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2238, 0, 'vi', 'plugins/custom-field/base', 'form.field_type', 'Kiểu trường', '2020-03-29 20:07:00', '2020-04-24 09:38:52'),
(2239, 0, 'vi', 'plugins/custom-field/base', 'form.field_type_helper', 'Vui lòng chọn một kiểu phù hợp cho bạn', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2240, 0, 'vi', 'plugins/custom-field/base', 'form.field_instructions', 'Hướng dẫn nhập liệu cho trường', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2241, 0, 'vi', 'plugins/custom-field/base', 'form.field_instructions_helper', 'Hướng dẫn nhập liệu từng trường cho người nhập liệu. Hiển thị ở các trang chỉnh sửa', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2242, 0, 'vi', 'plugins/custom-field/base', 'form.default_value', 'Giá trị mặc định', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2243, 0, 'vi', 'plugins/custom-field/base', 'form.default_value_helper', 'Tự động khởi tạo khi trường bị để trống', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2244, 0, 'vi', 'plugins/custom-field/base', 'form.placeholder', 'Mô tả trường', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2245, 0, 'vi', 'plugins/custom-field/base', 'form.placeholder_helper', 'Xuất hiện ngay bên trong mục nhập khi trường bị để trống', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2246, 0, 'vi', 'plugins/custom-field/base', 'form.rows', 'Số dòng', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2247, 0, 'vi', 'plugins/custom-field/base', 'form.rows_helper', 'Số dòng được khởi tạo khi sử dụng trường textarea', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2248, 0, 'vi', 'plugins/custom-field/base', 'form.toolbar', 'Thanh công cụ', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2249, 0, 'vi', 'plugins/custom-field/base', 'form.toolbar_helper', 'Tùy chỉnh kiểu thanh công cụ khi sử dụng trình nhập liệu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2250, 0, 'vi', 'plugins/custom-field/base', 'form.toolbar_basic', 'Thanh công cụ đơn giản', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2251, 0, 'vi', 'plugins/custom-field/base', 'form.toolbar_full', 'Thanh công cụ đầy đủ', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2252, 0, 'vi', 'plugins/custom-field/base', 'form.choices', 'Các mục lựa chọn', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2253, 0, 'vi', 'plugins/custom-field/base', 'form.choices_helper', 'Nhập mỗi lựa chọn trên một dòng mới. <br> Để quản lý tốt hơn, bạn có thể cần phải xác định rõ cả nhãn và giá trị lựa chọn như sau: <br> red: Red <br> blue: Blue', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2254, 0, 'vi', 'plugins/custom-field/base', 'form.button_label', 'Nhãn cho nút thêm mới bộ lặp', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2255, 0, 'vi', 'plugins/custom-field/base', 'form.groups.basic', 'Cơ bản', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2256, 0, 'vi', 'plugins/custom-field/base', 'form.groups.content', 'Nội dung', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2257, 0, 'vi', 'plugins/custom-field/base', 'form.groups.choice', 'Lựa chọn', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2258, 0, 'vi', 'plugins/custom-field/base', 'form.groups.other', 'Khác', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2259, 0, 'vi', 'plugins/custom-field/base', 'form.types.text', 'Văn bản', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2260, 0, 'vi', 'plugins/custom-field/base', 'form.types.textarea', 'Văn bản nhiều dòng', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2261, 0, 'vi', 'plugins/custom-field/base', 'form.types.number', 'Số', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2262, 0, 'vi', 'plugins/custom-field/base', 'form.types.email', 'Thư điện tử', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2263, 0, 'vi', 'plugins/custom-field/base', 'form.types.password', 'Mật khẩu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2264, 0, 'vi', 'plugins/custom-field/base', 'form.types.wysiwyg', 'Trình nhập liệu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2265, 0, 'vi', 'plugins/custom-field/base', 'form.types.image', 'Hình ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2266, 0, 'vi', 'plugins/custom-field/base', 'form.types.file', 'Tập tin', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2267, 0, 'vi', 'plugins/custom-field/base', 'form.types.select', 'Thanh lựa chọn', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2268, 0, 'vi', 'plugins/custom-field/base', 'form.types.checkbox', 'Checkbox', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2269, 0, 'vi', 'plugins/custom-field/base', 'form.types.radio', 'Radio', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2270, 0, 'vi', 'plugins/custom-field/base', 'form.types.repeater', 'Bộ lặp', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2271, 0, 'vi', 'plugins/custom-field/base', 'form.rules.rules', 'Luật hiển thị', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2272, 0, 'vi', 'plugins/custom-field/base', 'form.rules.rules_helper', 'Hiển thị nhóm trường này nếu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2273, 0, 'vi', 'plugins/custom-field/base', 'form.rules.add_rule_group', 'Thêm nhóm luật mới', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2274, 0, 'vi', 'plugins/custom-field/base', 'form.rules.is_equal_to', 'Tương đương', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2275, 0, 'vi', 'plugins/custom-field/base', 'form.rules.is_not_equal_to', 'Khác với', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2276, 0, 'vi', 'plugins/custom-field/base', 'form.rules.and', 'Và', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2277, 0, 'vi', 'plugins/custom-field/base', 'form.rules.or', 'Hoặc', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2278, 0, 'vi', 'plugins/custom-field/base', 'import', 'Nhập dữ liệu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2279, 0, 'vi', 'plugins/custom-field/base', 'export', 'Xuất dữ liệu', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2280, 0, 'vi', 'plugins/custom-field/permissions', 'view-custom-fields', 'Xem trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2281, 0, 'vi', 'plugins/custom-field/permissions', 'create-field-groups', 'Tạo các nhóm trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2282, 0, 'vi', 'plugins/custom-field/permissions', 'edit-field-groups', 'Chỉnh sửa các nhóm trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2283, 0, 'vi', 'plugins/custom-field/permissions', 'delete-field-groups', 'Xóa các nhóm trường tùy chỉnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2284, 0, 'vi', 'plugins/custom-field/rules', 'groups.basic', 'Cơ bản', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2285, 0, 'vi', 'plugins/custom-field/rules', 'groups.other', 'Khác', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2286, 0, 'vi', 'plugins/custom-field/rules', 'groups.blog', 'Blog', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2287, 0, 'vi', 'plugins/custom-field/rules', 'logged_in_user', 'Người đăng nhập hiện tại', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2288, 0, 'vi', 'plugins/custom-field/rules', 'logged_in_user_has_role', 'Người đăng nhập hiện tại có vai trò', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2289, 0, 'vi', 'plugins/custom-field/rules', 'page_template', 'Giao diện trang tĩnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2290, 0, 'vi', 'plugins/custom-field/rules', 'page', 'Trang tĩnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2291, 0, 'vi', 'plugins/custom-field/rules', 'model_name', 'Tên model', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2292, 0, 'vi', 'plugins/custom-field/rules', 'model_name_page', 'Trang tĩnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2293, 0, 'vi', 'plugins/custom-field/rules', 'category', 'Danh mục bài viết', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2294, 0, 'vi', 'plugins/custom-field/rules', 'post_with_related_category', 'Bài viết có danh mục', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2295, 0, 'vi', 'plugins/custom-field/rules', 'model_name_post', 'Bài viết (blog)', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2296, 0, 'vi', 'plugins/custom-field/rules', 'model_name_category', 'Danh mục (blog)', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2297, 0, 'en', 'plugins/gallery/gallery', 'create', 'Create new gallery', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2298, 0, 'en', 'plugins/gallery/gallery', 'edit', 'Edit gallery', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2299, 0, 'en', 'plugins/gallery/gallery', 'galleries', 'Galleries', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2300, 0, 'en', 'plugins/gallery/gallery', 'item', 'Gallery item', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2301, 0, 'en', 'plugins/gallery/gallery', 'select_image', 'Select images', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2302, 0, 'en', 'plugins/gallery/gallery', 'reset', 'Reset gallery', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2303, 0, 'en', 'plugins/gallery/gallery', 'update_photo_description', 'Update photo\'s description', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2304, 0, 'en', 'plugins/gallery/gallery', 'update_photo_description_placeholder', 'Photo\'s description...', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2305, 0, 'en', 'plugins/gallery/gallery', 'delete_photo', 'Delete this photo', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2306, 0, 'en', 'plugins/gallery/gallery', 'gallery_box', 'Gallery images', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2307, 0, 'en', 'plugins/gallery/gallery', 'by', 'By', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2308, 0, 'en', 'plugins/gallery/gallery', 'menu_name', 'Galleries', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2309, 0, 'en', 'plugins/gallery/gallery', 'gallery_images', 'Gallery images', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2310, 0, 'en', 'plugins/gallery/gallery', 'add_gallery_short_code', 'Add a gallery', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2311, 0, 'vi', 'plugins/gallery/gallery', 'create', 'Tạo mới thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2312, 0, 'vi', 'plugins/gallery/gallery', 'edit', 'Sửa thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2313, 0, 'vi', 'plugins/gallery/gallery', 'list', 'Thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2314, 0, 'vi', 'plugins/gallery/gallery', 'delete_photo', 'Xóa ảnh này', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2315, 0, 'vi', 'plugins/gallery/gallery', 'galleries', 'Thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2316, 0, 'vi', 'plugins/gallery/gallery', 'item', 'Ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2317, 0, 'vi', 'plugins/gallery/gallery', 'reset', 'Làm mới thư viện', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2318, 0, 'vi', 'plugins/gallery/gallery', 'select_image', 'Lựa chọn hình ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2319, 0, 'vi', 'plugins/gallery/gallery', 'update_photo_description', 'Cập nhật mô tả cho hình ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2320, 0, 'vi', 'plugins/gallery/gallery', 'update_photo_description_placeholder', 'Mô tả của hình ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2321, 0, 'vi', 'plugins/gallery/gallery', 'by', 'Bởi', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2322, 0, 'vi', 'plugins/gallery/gallery', 'gallery_box', 'Thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2323, 0, 'vi', 'plugins/gallery/gallery', 'menu_name', 'Thư viện ảnh', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2324, 0, 'en', 'plugins/language/language', 'name', 'Languages', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2325, 0, 'en', 'plugins/language/language', 'choose_language', 'Choose a language', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2326, 0, 'en', 'plugins/language/language', 'select_language', 'Select language', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2327, 0, 'en', 'plugins/language/language', 'choose_language_helper', 'You can choose a language in the list or directly edit it below.', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2328, 0, 'en', 'plugins/language/language', 'full_name', 'Full name', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2329, 0, 'en', 'plugins/language/language', 'full_name_helper', 'The name is how it is displayed on your site (for example: English).', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2330, 0, 'en', 'plugins/language/language', 'locale', 'Locale', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2331, 0, 'en', 'plugins/language/language', 'locale_helper', 'Laravel Locale for the language (for example: <code>en</code>).', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2332, 0, 'en', 'plugins/language/language', 'language_code', 'Language code', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2333, 0, 'en', 'plugins/language/language', 'language_code_helper', 'Language code - preferably 2-letters ISO 639-1 (for example: en)', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2334, 0, 'en', 'plugins/language/language', 'text_direction', 'Text direction', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2335, 0, 'en', 'plugins/language/language', 'text_direction_helper', 'Choose the text direction for the language', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2336, 0, 'en', 'plugins/language/language', 'left_to_right', 'Left to right', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2337, 0, 'en', 'plugins/language/language', 'right_to_left', 'Right to left', '2020-03-29 20:07:00', '2020-04-24 09:38:53');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(2338, 0, 'en', 'plugins/language/language', 'flag', 'Flag', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2339, 0, 'en', 'plugins/language/language', 'flag_helper', 'Choose a flag for the language.', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2340, 0, 'en', 'plugins/language/language', 'order', 'Order', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2341, 0, 'en', 'plugins/language/language', 'order_helper', 'Position of the language in the language switcher', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2342, 0, 'en', 'plugins/language/language', 'add_new_language', 'Add new language', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2343, 0, 'en', 'plugins/language/language', 'code', 'Code', '2020-03-29 20:07:00', '2020-04-24 09:38:53'),
(2344, 0, 'en', 'plugins/language/language', 'default_language', 'Default language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2345, 0, 'en', 'plugins/language/language', 'actions', 'Actions', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2346, 0, 'en', 'plugins/language/language', 'translations', 'Translations', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2347, 0, 'en', 'plugins/language/language', 'edit', 'Edit', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2348, 0, 'en', 'plugins/language/language', 'edit_tooltip', 'Edit this language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2349, 0, 'en', 'plugins/language/language', 'delete', 'Delete', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2350, 0, 'en', 'plugins/language/language', 'delete_tooltip', 'Delete this language and all its associated data', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2351, 0, 'en', 'plugins/language/language', 'choose_default_language', 'Choose :language as default language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2352, 0, 'en', 'plugins/language/language', 'current_language', 'Current record\'s language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2353, 0, 'en', 'plugins/language/language', 'edit_related', 'Edit related language for this record', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2354, 0, 'en', 'plugins/language/language', 'add_language_for_item', 'Add other language version for this record', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2355, 0, 'en', 'plugins/language/language', 'settings', 'Settings', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2356, 0, 'en', 'plugins/language/language', 'language_hide_default', 'Hide default language from URL?', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2357, 0, 'en', 'plugins/language/language', 'language_display_flag_only', 'Flag only', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2358, 0, 'en', 'plugins/language/language', 'language_display_name_only', 'Name only', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2359, 0, 'en', 'plugins/language/language', 'language_display_all', 'Display all flag and name', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2360, 0, 'en', 'plugins/language/language', 'language_display', 'Language display', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2361, 0, 'en', 'plugins/language/language', 'switcher_display', 'Switcher language display', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2362, 0, 'en', 'plugins/language/language', 'language_switcher_display_dropdown', 'Dropdown', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2363, 0, 'en', 'plugins/language/language', 'language_switcher_display_list', 'List', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2364, 0, 'en', 'plugins/language/language', 'current_language_edit_notification', 'You are editing \"<strong class=\"current_language_text\">:language</strong>\" version', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2365, 0, 'en', 'plugins/language/language', 'confirm-change-language', 'Confirm change language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2366, 0, 'en', 'plugins/language/language', 'confirm-change-language-message', 'Do you really want to change language to \"<strong class=\"change_to_language_text\"></strong>\" ? This action will be override \"<strong class=\"change_to_language_text\"></strong>\" version if it\'s existed!', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2367, 0, 'en', 'plugins/language/language', 'confirm-change-language-btn', 'Confirm change', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2368, 0, 'en', 'plugins/language/language', 'hide_languages', 'Hide languages', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2369, 0, 'en', 'plugins/language/language', 'hide_languages_description', 'You can completely hide content in specific languages from visitors and search engines, but still view it yourself. This allows reviewing translations that are in progress.', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2370, 0, 'en', 'plugins/language/language', 'hide_languages_helper_display_hidden', '{0} All languages are currently displayed.|{1} :language is currently hidden to visitors.|[2, Inf] :language are currently hidden to visitors.', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2371, 0, 'en', 'plugins/language/language', 'show_all', 'Show all', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2372, 0, 'en', 'plugins/language/language', 'change_language', 'Language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2373, 0, 'en', 'plugins/language/language', 'language_show_default_item_if_current_version_not_existed', 'Show item in default language if it is not existed in current language', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2374, 0, 'vi', 'plugins/language/language', 'name', 'Ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2375, 0, 'vi', 'plugins/language/language', 'choose_language', 'Chọn một ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2376, 0, 'vi', 'plugins/language/language', 'select_language', 'Chọn ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2377, 0, 'vi', 'plugins/language/language', 'choose_language_helper', 'Bạn có thể chọn 1 ngôn ngữ trong danh sách hoặc nhập trực tiếp nội dung xuống các mục dưới', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2378, 0, 'vi', 'plugins/language/language', 'full_name', 'Tên đầy đủ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2379, 0, 'vi', 'plugins/language/language', 'full_name_helper', 'Tên ngôn ngữ sẽ được hiển thị trên website (ví dụ: Tiếng Anh).', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2380, 0, 'vi', 'plugins/language/language', 'locale', 'Locale', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2381, 0, 'vi', 'plugins/language/language', 'locale_helper', 'Laravel Locale cho ngôn ngữ này (ví dụ: <code>en</code>).', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2382, 0, 'vi', 'plugins/language/language', 'language_code', 'Mã ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2383, 0, 'vi', 'plugins/language/language', 'language_code_helper', 'Mã ngôn ngữ - ưu tiên dạng 2-kí tự theo chuẩn ISO 639-1 (ví dụ: en)', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2384, 0, 'vi', 'plugins/language/language', 'text_direction', 'Hướng viết chữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2385, 0, 'vi', 'plugins/language/language', 'text_direction_helper', 'Chọn hướng viết chữ cho ngôn ngữ này', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2386, 0, 'vi', 'plugins/language/language', 'left_to_right', 'Trái qua phải', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2387, 0, 'vi', 'plugins/language/language', 'right_to_left', 'Phải qua trái', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2388, 0, 'vi', 'plugins/language/language', 'flag', 'Cờ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2389, 0, 'vi', 'plugins/language/language', 'flag_helper', 'Chọn 1 cờ cho ngôn ngữ này', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2390, 0, 'vi', 'plugins/language/language', 'order', 'Sắp xếp', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2391, 0, 'vi', 'plugins/language/language', 'order_helper', 'Vị trí của ngôn ngữ hiển thị trong mục chuyển đổi ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2392, 0, 'vi', 'plugins/language/language', 'add_new_language', 'Thêm ngôn ngữ mới', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2393, 0, 'vi', 'plugins/language/language', 'code', 'Mã', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2394, 0, 'vi', 'plugins/language/language', 'default_language', 'Ngôn ngữ mặc định', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2395, 0, 'vi', 'plugins/language/language', 'actions', 'Hành động', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2396, 0, 'vi', 'plugins/language/language', 'translations', 'Dịch', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2397, 0, 'vi', 'plugins/language/language', 'edit', 'Sửa', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2398, 0, 'vi', 'plugins/language/language', 'edit_tooltip', 'Sửa ngôn ngữ này', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2399, 0, 'vi', 'plugins/language/language', 'delete', 'Xóa', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2400, 0, 'vi', 'plugins/language/language', 'delete_tooltip', 'Xóa ngôn ngữ này và các dữ liệu liên quan', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2401, 0, 'vi', 'plugins/language/language', 'choose_default_language', 'Chọn :language làm ngôn ngữ mặc định', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2402, 0, 'vi', 'plugins/language/language', 'add_language_for_item', 'Thêm ngôn ngữ khác cho bản ghi này', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2403, 0, 'vi', 'plugins/language/language', 'current_language', 'Ngôn ngữ hiện tại của bản ghi', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2404, 0, 'vi', 'plugins/language/language', 'edit_related', 'Sửa bản ngôn ngữ khác của bản ghi này', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2405, 0, 'vi', 'plugins/language/language', 'confirm-change-language', 'Xác nhận thay đổi ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2406, 0, 'vi', 'plugins/language/language', 'confirm-change-language-btn', 'Xác nhận thay đổi', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2407, 0, 'vi', 'plugins/language/language', 'confirm-change-language-message', 'Bạn có chắc chắn muốn thay đổi ngôn ngữ sang tiếng \"<strong class=\"change_to_language_text\"></strong>\" ? Điều này sẽ ghi đè bản ghi tiếng \"<strong class=\"change_to_language_text\"></strong>\" nếu nó đã tồn tại!', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2408, 0, 'vi', 'plugins/language/language', 'current_language_edit_notification', 'Bạn đang chỉnh sửa phiên bản tiếng \"<strong class=\"current_language_text\">:language</strong>\"', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2409, 0, 'vi', 'plugins/language/language', 'hide_languages', 'Ẩn ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2410, 0, 'vi', 'plugins/language/language', 'hide_languages_description', 'Bạn có thể hoàn toàn ẩn ngôn ngữ cụ thể đối với người truy cập và công cụ tìm kiếm, nhưng sẽ vẫn hiển thị trong trang quản trị. Điều này cho phép bạn biết những ngôn ngữ nào đang được xử lý.', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2411, 0, 'vi', 'plugins/language/language', 'hide_languages_helper_display_hidden', '{0} Tất cả ngôn ngữ đang được hiển thị.|{1} :language đang bị ẩn đối với người truy cập.|[2, Inf]  :language đang bị ẩn đối với người truy cập.', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2412, 0, 'vi', 'plugins/language/language', 'language_display', 'Hiển thị ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2413, 0, 'vi', 'plugins/language/language', 'language_display_all', 'Hiển thị cả cờ và tên ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2414, 0, 'vi', 'plugins/language/language', 'language_display_flag_only', 'Chỉ hiển thị cờ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2415, 0, 'vi', 'plugins/language/language', 'language_display_name_only', 'Chỉ hiển thị tên', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2416, 0, 'vi', 'plugins/language/language', 'language_hide_default', 'Ẩn ngôn ngữ mặc định trên URL', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2417, 0, 'vi', 'plugins/language/language', 'language_switcher_display_dropdown', 'Dropdown', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2418, 0, 'vi', 'plugins/language/language', 'language_switcher_display_list', 'Danh sách', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2419, 0, 'vi', 'plugins/language/language', 'settings', 'Cài đặt', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2420, 0, 'vi', 'plugins/language/language', 'switcher_display', 'Hiển thị bộ chuyển đổi ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2421, 0, 'vi', 'plugins/language/language', 'change_language', 'Ngôn ngữ', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2422, 0, 'vi', 'plugins/language/language', 'show_all', 'Hiển thị tất cả', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2423, 0, 'en', 'plugins/log-viewer/general', 'all', 'All', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2424, 0, 'en', 'plugins/log-viewer/general', 'date', 'Date', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2425, 0, 'en', 'plugins/log-viewer/general', 'name', 'System logs', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2426, 0, 'en', 'plugins/log-viewer/levels', 'all', 'All', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2427, 0, 'en', 'plugins/log-viewer/levels', 'emergency', 'Emergency', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2428, 0, 'en', 'plugins/log-viewer/levels', 'alert', 'Alert', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2429, 0, 'en', 'plugins/log-viewer/levels', 'critical', 'Critical', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2430, 0, 'en', 'plugins/log-viewer/levels', 'error', 'Error', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2431, 0, 'en', 'plugins/log-viewer/levels', 'warning', 'Warning', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2432, 0, 'en', 'plugins/log-viewer/levels', 'notice', 'Notice', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2433, 0, 'en', 'plugins/log-viewer/levels', 'info', 'Info', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2434, 0, 'en', 'plugins/log-viewer/levels', 'debug', 'Debug', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2435, 0, 'en', 'plugins/log-viewer/log-viewer', 'system_logs', 'System Logs', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2436, 0, 'en', 'plugins/log-viewer/log-viewer', 'system_logs_description', 'View system errors log.', '2020-03-29 20:07:00', '2020-04-24 09:38:54'),
(2437, 0, 'en', 'plugins/log-viewer/log-viewer', 'name', 'LogViewers', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2438, 0, 'en', 'plugins/log-viewer/log-viewer', 'list', 'List LogViewer', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2439, 0, 'en', 'plugins/log-viewer/log-viewer', 'levels', 'Levels', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2440, 0, 'en', 'plugins/log-viewer/log-viewer', 'no_error', 'There is no error now.', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2441, 0, 'en', 'plugins/log-viewer/log-viewer', 'entries', 'entries', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2442, 0, 'en', 'plugins/log-viewer/log-viewer', 'actions', 'Actions', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2443, 0, 'en', 'plugins/log-viewer/log-viewer', 'delete_log_file', 'Delete log file', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2444, 0, 'en', 'plugins/log-viewer/log-viewer', 'loading', 'Loading...', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2445, 0, 'en', 'plugins/log-viewer/log-viewer', 'delete_button', 'Delete file', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2446, 0, 'en', 'plugins/log-viewer/log-viewer', 'confirm_delete_msg', 'Are you sure you want to <span class=\"label label-danger\">DELETE</span> this log file <span class=\"label label-primary\"><span class=\"log_date\">:date</span></span> ?', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2447, 0, 'en', 'plugins/log-viewer/log-viewer', 'download', 'Download', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2448, 0, 'en', 'plugins/log-viewer/log-viewer', 'delete', 'Delete', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2449, 0, 'en', 'plugins/log-viewer/log-viewer', 'file_path', 'File path', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2450, 0, 'en', 'plugins/log-viewer/log-viewer', 'log_entries', 'Log entries', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2451, 0, 'en', 'plugins/log-viewer/log-viewer', 'size', 'Size', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2452, 0, 'en', 'plugins/log-viewer/log-viewer', 'page', 'Page', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2453, 0, 'en', 'plugins/log-viewer/log-viewer', 'of', 'of', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2454, 0, 'en', 'plugins/log-viewer/log-viewer', 'env', 'Env', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2455, 0, 'en', 'plugins/log-viewer/log-viewer', 'level', 'Level', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2456, 0, 'en', 'plugins/log-viewer/log-viewer', 'time', 'Time', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2457, 0, 'en', 'plugins/log-viewer/log-viewer', 'header', 'Header', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2458, 0, 'en', 'plugins/log-viewer/log-viewer', 'stack', 'Stack', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2459, 0, 'en', 'plugins/log-viewer/log-viewer', 'log_info', 'Log info', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2460, 0, 'en', 'plugins/log-viewer/log-viewer', 'menu_name', 'System logs', '2020-03-29 20:07:00', '2020-04-24 09:38:55'),
(2461, 1, 'vi', 'plugins/log-viewer/general', 'all', 'Tất cả', '2020-03-29 20:07:00', '2020-03-29 20:07:00'),
(2462, 1, 'vi', 'plugins/log-viewer/general', 'date', 'Ngày', '2020-03-29 20:07:00', '2020-03-29 20:07:00'),
(2463, 1, 'vi', 'plugins/log-viewer/general', 'name', 'Lỗi hệ thống', '2020-03-29 20:07:00', '2020-03-29 20:07:00'),
(2464, 1, 'vi', 'plugins/log-viewer/levels', 'all', 'Tất cả', '2020-03-29 20:07:00', '2020-03-29 20:07:00'),
(2465, 1, 'vi', 'plugins/log-viewer/levels', 'emergency', 'Khẩn cấp', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2466, 1, 'vi', 'plugins/log-viewer/levels', 'alert', 'Báo động', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2467, 1, 'vi', 'plugins/log-viewer/levels', 'critical', 'Nguy kịch', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2468, 1, 'vi', 'plugins/log-viewer/levels', 'error', 'Lỗi', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2469, 1, 'vi', 'plugins/log-viewer/levels', 'warning', 'Cảnh báo', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2470, 1, 'vi', 'plugins/log-viewer/levels', 'notice', 'Chú ý', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2471, 1, 'vi', 'plugins/log-viewer/levels', 'info', 'Thông tin', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2472, 1, 'vi', 'plugins/log-viewer/levels', 'debug', 'Gỡ lỗi', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2473, 1, 'vi', 'plugins/log-viewer/log-viewer', 'name', 'LogViewers', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2474, 1, 'vi', 'plugins/log-viewer/log-viewer', 'list', 'List LogViewer', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2475, 1, 'vi', 'plugins/log-viewer/log-viewer', 'actions', 'Hành động', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2476, 1, 'vi', 'plugins/log-viewer/log-viewer', 'confirm_delete_msg', 'Bạn có chắc muốn <span class=\"label label-danger\">XÓA</span> tập tin này <span class=\"label label-primary\"><span class=\"log_date\">:date</span></span> ?', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2477, 1, 'vi', 'plugins/log-viewer/log-viewer', 'delete', 'Xóa', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2478, 1, 'vi', 'plugins/log-viewer/log-viewer', 'delete_button', 'Xóa tập tin', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2479, 1, 'vi', 'plugins/log-viewer/log-viewer', 'delete_log_file', 'Xóa tập tin', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2480, 1, 'vi', 'plugins/log-viewer/log-viewer', 'download', 'Tải xuống', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2481, 1, 'vi', 'plugins/log-viewer/log-viewer', 'entries', 'bản ghi', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2482, 1, 'vi', 'plugins/log-viewer/log-viewer', 'env', 'Môi trường', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2483, 1, 'vi', 'plugins/log-viewer/log-viewer', 'file_path', 'Đường dẫn', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2484, 1, 'vi', 'plugins/log-viewer/log-viewer', 'header', 'Header', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2485, 1, 'vi', 'plugins/log-viewer/log-viewer', 'level', 'Cấp độ', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2486, 1, 'vi', 'plugins/log-viewer/log-viewer', 'levels', 'Cấp độ', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2487, 1, 'vi', 'plugins/log-viewer/log-viewer', 'loading', 'Đang tải...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2488, 1, 'vi', 'plugins/log-viewer/log-viewer', 'log_entries', 'Bản ghi nhật ký', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2489, 1, 'vi', 'plugins/log-viewer/log-viewer', 'log_info', 'Bản ghi thông tin', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2490, 1, 'vi', 'plugins/log-viewer/log-viewer', 'no_error', 'Hiện tại không có lỗi trong hệ thống', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2491, 1, 'vi', 'plugins/log-viewer/log-viewer', 'of', 'trong tổng số', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2492, 1, 'vi', 'plugins/log-viewer/log-viewer', 'page', 'Trang', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2493, 1, 'vi', 'plugins/log-viewer/log-viewer', 'size', 'Kích thước', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2494, 1, 'vi', 'plugins/log-viewer/log-viewer', 'stack', 'Chi tiết', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2495, 1, 'vi', 'plugins/log-viewer/log-viewer', 'time', 'Thời gian', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2496, 1, 'vi', 'plugins/log-viewer/log-viewer', 'menu_name', 'Lỗi hệ thống', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2497, 1, 'vi', 'plugins/log-viewer/log-viewer', 'system_logs', 'Lịch sử lỗi hệ thống', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2498, 1, 'vi', 'plugins/log-viewer/log-viewer', 'system_logs_description', 'Xem lịch sử lỗi hệ thống', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2499, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'maintenance_mode', 'Maintenance mode', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2500, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'message', 'Message', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2501, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'message_placeholder', 'A message for your users', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2502, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'retry_time', 'Retry Time', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2503, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'retry_time_placeholder', 'Set the Retry-After header', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2504, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'secs', 'secs', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2505, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'allowed_ip_address', 'Allowed IP Addresses', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2506, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'allowed_your_current_ip', 'Allow your current IP', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2507, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'allowed_your_current_ip_helper', 'If you uncheck this and do not add your IP address above you will lose access to this site as well', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2508, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'enable_maintenance_mode', 'Enable maintenance mode', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2509, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'disable_maintenance_mode', 'Disable maintenance mode', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2510, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'application_live', 'Application is now live', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2511, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'application_down', 'Application is now in maintenance mode', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2512, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'notice_enable', 'Your website is currently in Maintenance Mode', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2513, 0, 'en', 'plugins/maintenance-mode/maintenance-mode', 'notice_disable', 'Your website is live now', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2514, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'maintenance_mode', 'Chế độ bảo trì', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2515, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'message', 'Tin nhắn', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2516, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'message_placeholder', 'Tin nhắn hiển thị cho khách ghé thăm', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2517, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'retry_time', 'Thời gian thử lại', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2518, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'retry_time_placeholder', 'Thiết lập Retry-After header', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2519, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'secs', 'giây', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2520, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'allowed_ip_address', 'Cho phép các địa chỉ IP có thể truy cập', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2521, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'allowed_your_current_ip', 'Cho phép địa chỉ IP hiện tại của bạn', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2522, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'allowed_your_current_ip_helper', 'Nếu bạn không lựa chọn tuỳ chọn này và không thêm IP của bạn vào mục cho phép phía trên, bạn cũng không thể truy cập được trang này', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2523, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'enable_maintenance_mode', 'Bật chế độ bảo trì', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2524, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'disable_maintenance_mode', 'Tắt chế độ bảo trì', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2525, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'application_live', 'Ứng dụng đang hoạt động', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2526, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'application_down', 'Ứng dụng hiện đang ở chế độ bảo trì', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2527, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'notice_enable', 'Website của bạn đang ở chế độ bảo trì', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2528, 0, 'vi', 'plugins/maintenance-mode/maintenance-mode', 'notice_disable', 'Website của bạn đang hoạt động bình thường', '2020-03-29 20:07:01', '2020-04-24 09:38:55'),
(2529, 0, 'en', 'plugins/request-log/request-log', 'name', 'Request Logs', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2530, 0, 'en', 'plugins/request-log/request-log', 'status_code', 'Status Code', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2531, 0, 'en', 'plugins/request-log/request-log', 'no_request_error', 'No request error now!', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2532, 0, 'en', 'plugins/request-log/request-log', 'widget_request_errors', 'Request Errors', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2533, 0, 'vi', 'plugins/request-log/request-log', 'name', 'Lịch sử lỗi', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2534, 0, 'vi', 'plugins/request-log/request-log', 'status_code', 'Mã lỗi', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2535, 0, 'vi', 'plugins/request-log/request-log', 'no_request_error', 'Hiện tại không có lỗi nào cả!', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2536, 0, 'vi', 'plugins/request-log/request-log', 'widget_request_errors', 'Liên kết bị hỏng', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2537, 0, 'en', 'plugins/social-login/social-login', 'settings.title', 'Social Login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2538, 0, 'en', 'plugins/social-login/social-login', 'settings.description', 'Configure social login options', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2539, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.title', 'Facebook login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2540, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.description', 'Enable/disable & configure app credentials for Facebook login', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2541, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2542, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2543, 0, 'en', 'plugins/social-login/social-login', 'settings.facebook.helper', 'Please go to https://developers.facebook.com to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2544, 0, 'en', 'plugins/social-login/social-login', 'settings.google.title', 'Google login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2545, 0, 'en', 'plugins/social-login/social-login', 'settings.google.description', 'Enable/disable & configure app credentials for Google login', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2546, 0, 'en', 'plugins/social-login/social-login', 'settings.google.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2547, 0, 'en', 'plugins/social-login/social-login', 'settings.google.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2548, 0, 'en', 'plugins/social-login/social-login', 'settings.google.helper', 'Please go to https://console.developers.google.com/apis/dashboard to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:57'),
(2549, 0, 'en', 'plugins/social-login/social-login', 'settings.github.title', 'Github login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2550, 0, 'en', 'plugins/social-login/social-login', 'settings.github.description', 'Enable/disable & configure app credentials for Github login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2551, 0, 'en', 'plugins/social-login/social-login', 'settings.github.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2552, 0, 'en', 'plugins/social-login/social-login', 'settings.github.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2553, 0, 'en', 'plugins/social-login/social-login', 'settings.github.helper', 'Please go to https://github.com/settings/developers to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2554, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.title', 'Linkedin login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2555, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.description', 'Enable/disable & configure app credentials for Linkedin login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2556, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2557, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2558, 0, 'en', 'plugins/social-login/social-login', 'settings.linkedin.helper', 'Please go to https://www.linkedin.com/developers/apps/new to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2559, 0, 'en', 'plugins/social-login/social-login', 'settings.enable', 'Enable?', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2560, 0, 'en', 'plugins/social-login/social-login', 'menu', 'Social Login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2561, 0, 'vi', 'plugins/social-login/social-login', 'settings.title', 'Social Login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2562, 0, 'vi', 'plugins/social-login/social-login', 'settings.description', 'Configure social login options', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2563, 0, 'vi', 'plugins/social-login/social-login', 'settings.facebook.title', 'Facebook login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2564, 0, 'vi', 'plugins/social-login/social-login', 'settings.facebook.description', 'Enable/disable & configure app credentials for Facebook login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2565, 0, 'vi', 'plugins/social-login/social-login', 'settings.facebook.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2566, 0, 'vi', 'plugins/social-login/social-login', 'settings.facebook.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2567, 0, 'vi', 'plugins/social-login/social-login', 'settings.facebook.helper', 'Please go to https://developers.facebook.com to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2568, 0, 'vi', 'plugins/social-login/social-login', 'settings.google.title', 'Google login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2569, 0, 'vi', 'plugins/social-login/social-login', 'settings.google.description', 'Enable/disable & configure app credentials for Google login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2570, 0, 'vi', 'plugins/social-login/social-login', 'settings.google.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2571, 0, 'vi', 'plugins/social-login/social-login', 'settings.google.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2572, 0, 'vi', 'plugins/social-login/social-login', 'settings.google.helper', 'Please go to https://console.developers.google.com/apis/dashboard to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2573, 0, 'vi', 'plugins/social-login/social-login', 'settings.github.title', 'Github login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2574, 0, 'vi', 'plugins/social-login/social-login', 'settings.github.description', 'Enable/disable & configure app credentials for Github login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2575, 0, 'vi', 'plugins/social-login/social-login', 'settings.github.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2576, 0, 'vi', 'plugins/social-login/social-login', 'settings.github.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2577, 0, 'vi', 'plugins/social-login/social-login', 'settings.github.helper', 'Please go to https://github.com/settings/developers to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2578, 0, 'vi', 'plugins/social-login/social-login', 'settings.linkedin.title', 'Linkedin login settings', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2579, 0, 'vi', 'plugins/social-login/social-login', 'settings.linkedin.description', 'Enable/disable & configure app credentials for Linkedin login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2580, 0, 'vi', 'plugins/social-login/social-login', 'settings.linkedin.app_id', 'App ID', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2581, 0, 'vi', 'plugins/social-login/social-login', 'settings.linkedin.app_secret', 'App Secret', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2582, 0, 'vi', 'plugins/social-login/social-login', 'settings.linkedin.helper', 'Please go to https://www.linkedin.com/developers/apps/new to create new app update App ID, App Secret. Callback URL is :callback', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2583, 0, 'vi', 'plugins/social-login/social-login', 'settings.enable', 'Enable?', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2584, 0, 'vi', 'plugins/social-login/social-login', 'menu', 'Social Login', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2585, 0, 'en', 'plugins/translation/translation', 'translations', 'Translations', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2586, 0, 'en', 'plugins/translation/translation', 'translations_description', 'Translate all words in system.', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2587, 0, 'en', 'plugins/translation/translation', 'export_warning', 'Warning, translations are not visible until they are exported back to the resources/lang file, using \'php artisan cms:translations:export\' command or publish button.', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2588, 1, 'en', 'plugins/translation/translation', 'import_done', 'Done importing, processed <strong class=\"counter\">N</strong> items! Reload this page to refresh the groups!', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2589, 0, 'en', 'plugins/translation/translation', 'translation_manager', 'Translations Manager', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2590, 1, 'en', 'plugins/translation/translation', 'done_searching', 'Done searching for translations, found <strong class=\"counter\">N</strong> items!', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2591, 0, 'en', 'plugins/translation/translation', 'done_publishing', 'Done publishing the translations for group', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2592, 0, 'en', 'plugins/translation/translation', 'append_translation', 'Append new translations', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2593, 0, 'en', 'plugins/translation/translation', 'replace_translation', 'Replace existing translations', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2594, 1, 'en', 'plugins/translation/translation', 'loading', 'Loading...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2595, 0, 'en', 'plugins/translation/translation', 'import_group', 'Import group', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2596, 1, 'en', 'plugins/translation/translation', 'confirm_scan_translation', 'Are you sure you want to scan you app folder? All found translation keys will be added to the database.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2597, 1, 'en', 'plugins/translation/translation', 'searching', 'Searching...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2598, 1, 'en', 'plugins/translation/translation', 'find_translation_files', 'Find translations in files', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2599, 0, 'en', 'plugins/translation/translation', 'confirm_publish_group', 'Are you sure you want to publish the translations group \":group\"? This will overwrite existing language files.', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2600, 1, 'en', 'plugins/translation/translation', 'publishing', 'Publishing..', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2601, 0, 'en', 'plugins/translation/translation', 'publish_translations', 'Publish translations', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2602, 0, 'en', 'plugins/translation/translation', 'back', 'Back', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2603, 1, 'en', 'plugins/translation/translation', 'add_key_description', 'Add 1 key per line, without the group prefix', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2604, 1, 'en', 'plugins/translation/translation', 'add_key_button', 'Add keys', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2605, 0, 'en', 'plugins/translation/translation', 'total', 'Total', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2606, 0, 'en', 'plugins/translation/translation', 'changed', 'changed', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2607, 1, 'en', 'plugins/translation/translation', 'key', 'Key', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2608, 0, 'en', 'plugins/translation/translation', 'edit_title', 'Enter translation', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2609, 1, 'en', 'plugins/translation/translation', 'confirm_delete_key', 'Are you sure you want to delete the translations for :key ?', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2610, 1, 'en', 'plugins/translation/translation', 'choose_group_msg', 'Choose a group to display the group translations. If no groups are visible, make sure you have run the migrations and imported the translations.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2611, 0, 'en', 'plugins/translation/translation', 'choose_a_group', 'Choose a group', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2612, 1, 'vi', 'plugins/translation/translation', 'add_key_button', 'Thêm khóa', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2613, 1, 'vi', 'plugins/translation/translation', 'add_key_description', 'Mỗi khóa trên mỗi dòng, trừ tiền tố của nhóm', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2614, 0, 'vi', 'plugins/translation/translation', 'append_translation', 'Tiếp nối bản dịch', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2615, 0, 'vi', 'plugins/translation/translation', 'back', 'Quay lại', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2616, 0, 'vi', 'plugins/translation/translation', 'changed', 'thay đổi', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2617, 0, 'vi', 'plugins/translation/translation', 'choose_a_group', 'Chọn một nhóm', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2618, 0, 'vi', 'plugins/translation/translation', 'choose_group_msg', 'Chọn một nhóm để hiển thị nhóm dịch thuật. Nếu nhóm không có sẵn, hãy chắc chắn là bạn đã chạy migrations và nhập dữ liệu dịch thuật.', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2619, 1, 'vi', 'plugins/translation/translation', 'confirm_delete_key', 'Bạn có chắc muốn xóa dịch thuật cho :key?', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2620, 0, 'vi', 'plugins/translation/translation', 'confirm_publish_group', 'Bạn có chắc muốn xuất bản nhóm \":group\"? Điều này sẽ ghi đè tập tin ngôn ngữ hiện tại.', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2621, 1, 'vi', 'plugins/translation/translation', 'confirm_scan_translation', 'Bạn có chắc muốn quét thư mục app? Tất cả khóa dịch thuật tìm thấy sẽ được thêm vào cơ sở dữ liệu.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2622, 0, 'vi', 'plugins/translation/translation', 'done_publishing', 'Xuất bản nhóm dịch thuật thành công', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2623, 1, 'vi', 'plugins/translation/translation', 'done_searching', 'Tìm kiếm dịch thuật xong, tìm thấy <strong class=\"counter\">N</strong> bản ghi!', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2624, 0, 'vi', 'plugins/translation/translation', 'edit_title', 'Nhập nội dung dịch', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2625, 0, 'vi', 'plugins/translation/translation', 'export_warning', 'Cảnh báo, bản dịch sẽ không có sẵn cho đến khi chúng được xuất bản lại vào thư mục /resources/lang, sử dụng lệnh \'php artisan cms:translations:export\' hoặc sử dụng nút xuất bản', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2626, 1, 'vi', 'plugins/translation/translation', 'find_translation_files', 'Tìm thấy tập tin dịch thuật', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2627, 1, 'vi', 'plugins/translation/translation', 'import_done', 'Nhập hoàn thành, đã xử lý <strong class=\"counter\">N</strong> bản ghi!  ', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2628, 0, 'vi', 'plugins/translation/translation', 'import_group', 'Nhập nhóm', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2629, 1, 'vi', 'plugins/translation/translation', 'key', 'Khóa', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2630, 1, 'vi', 'plugins/translation/translation', 'loading', 'Đang tải...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2631, 0, 'vi', 'plugins/translation/translation', 'publish_translations', 'Xuất bản dịch thuật', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2632, 1, 'vi', 'plugins/translation/translation', 'publishing', 'Đang xuất bản...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2633, 0, 'vi', 'plugins/translation/translation', 'replace_translation', 'Thay thế bản dịch hiện tại', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2634, 1, 'vi', 'plugins/translation/translation', 'searching', 'Đang tìm kiếm...', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2635, 0, 'vi', 'plugins/translation/translation', 'total', 'Tổng cộng', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2636, 0, 'vi', 'plugins/translation/translation', 'translation_manager', 'Quản lý dịch thuật', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2637, 0, 'vi', 'plugins/translation/translation', 'translations', 'Dịch thuật', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2638, 0, 'vi', 'plugins/translation/translation', 'translations_description', 'Dịch tất cả các từ trong hệ thống', '2020-03-29 20:07:01', '2020-04-24 09:38:58'),
(2639, 1, 'vi', 'auth', 'failed', 'Thông tin tài khoản không tìm thấy trong hệ thống.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2640, 1, 'vi', 'auth', 'throttle', 'Vượt quá số lần đăng nhập cho phép. Vui lòng thử lại sau :seconds giây.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2641, 1, 'vi', 'pagination', 'previous', '&laquo; Trước', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2642, 1, 'vi', 'pagination', 'next', 'Sau &raquo;', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2643, 1, 'vi', 'passwords', 'password', 'Mật khẩu phải ít nhất 6 kí tự và trùng khớp với xác nhận mật khẩu.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2644, 1, 'vi', 'passwords', 'reset', 'Mật khẩu của bạn đã được khôi phục', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2645, 1, 'vi', 'passwords', 'sent', 'Hệ thống đã gửi một email cho bạn chứa liên kết khôi phục mật khẩu!', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2646, 1, 'vi', 'passwords', 'token', 'Mã khôi phục mật khẩu không hợp lệ.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2647, 1, 'vi', 'passwords', 'user', 'Không thể tìm thấy người dùng với địa chỉ email này.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2648, 1, 'vi', 'validation', 'accepted', 'Trường :attribute phải được chấp nhận.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2649, 1, 'vi', 'validation', 'active_url', 'Trường :attribute không phải là một URL hợp lệ.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2650, 1, 'vi', 'validation', 'after', 'Trường :attribute phải là một ngày sau ngày :date.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2651, 1, 'vi', 'validation', 'alpha', 'Trường :attribute chỉ có thể chứa các chữ cái.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2652, 1, 'vi', 'validation', 'alpha_dash', 'Trường :attribute chỉ có thể chứa chữ cái, số và dấu gạch ngang.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2653, 1, 'vi', 'validation', 'alpha_num', 'Trường :attribute chỉ có thể chứa chữ cái và số.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2654, 1, 'vi', 'validation', 'array', 'Kiểu dữ liệu của trường :attribute phải là dạng mảng.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2655, 1, 'vi', 'validation', 'before', 'Trường :attribute phải là một ngày trước ngày :date.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2656, 1, 'vi', 'validation', 'between.numeric', 'Trường :attribute phải nằm trong khoảng :min - :max.', '2020-03-29 20:07:01', '2020-03-29 20:07:01'),
(2657, 1, 'vi', 'validation', 'between.file', 'Dung lượng tập tin trong trường :attribute phải từ :min - :max kB.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2658, 1, 'vi', 'validation', 'between.string', 'Trường :attribute phải từ :min - :max ký tự.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2659, 1, 'vi', 'validation', 'between.array', 'Trường :attribute phải có từ :min - :max phần tử.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2660, 1, 'vi', 'validation', 'boolean', 'Trường :attribute phải là true hoặc false.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2661, 1, 'vi', 'validation', 'confirmed', 'Giá trị xác nhận trong trường :attribute không khớp.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2662, 1, 'vi', 'validation', 'date', 'Trường :attribute không phải là định dạng của ngày-tháng.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2663, 1, 'vi', 'validation', 'date_format', 'Trường :attribute không giống với định dạng :format.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2664, 1, 'vi', 'validation', 'different', 'Trường :attribute và :other phải khác nhau.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2665, 1, 'vi', 'validation', 'digits', 'Độ dài của trường :attribute phải gồm :digits chữ số.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2666, 1, 'vi', 'validation', 'digits_between', 'Độ dài của trường :attribute phải nằm trong khoảng :min and :max chữ số.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2667, 1, 'vi', 'validation', 'email', 'Trường :attribute phải là một địa chỉ email hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2668, 1, 'vi', 'validation', 'exists', 'Giá trị đã chọn trong trường :attribute không hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2669, 1, 'vi', 'validation', 'file', 'Trường :attribute phải là một tập tin.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2670, 1, 'vi', 'validation', 'image', 'Các tập tin trong trường :attribute phải là định dạng hình ảnh.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2671, 1, 'vi', 'validation', 'in', 'Giá trị đã chọn trong trường :attribute không hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2672, 1, 'vi', 'validation', 'integer', 'Trường :attribute phải là một số nguyên.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2673, 1, 'vi', 'validation', 'ip', 'Trường :attribute phải là một địa chỉa IP.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2674, 1, 'vi', 'validation', 'max.numeric', 'Trường :attribute không được lớn hơn :max.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2675, 1, 'vi', 'validation', 'max.file', 'Dung lượng tập tin trong trường :attribute không được lớn hơn :max kB.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2676, 1, 'vi', 'validation', 'max.string', 'Trường :attribute không được lớn hơn :max ký tự.', '2020-03-29 20:07:02', '2020-03-29 20:07:02');
INSERT INTO `translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(2677, 1, 'vi', 'validation', 'max.array', 'Trường :attribute không được lớn hơn :max phần tử.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2678, 1, 'vi', 'validation', 'mimes', 'Trường :attribute phải là một tập tin có định dạng: :values.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2679, 1, 'vi', 'validation', 'min.numeric', 'Trường :attribute phải tối thiểu là :min.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2680, 1, 'vi', 'validation', 'min.file', 'Dung lượng tập tin trong trường :attribute phải tối thiểu :min kB.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2681, 1, 'vi', 'validation', 'min.string', 'Trường :attribute phải có tối thiểu :min ký tự.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2682, 1, 'vi', 'validation', 'min.array', 'Trường :attribute phải có tối thiểu :min phần tử.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2683, 1, 'vi', 'validation', 'not_in', 'Giá trị đã chọn trong trường :attribute không hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2684, 1, 'vi', 'validation', 'numeric', 'Trường :attribute phải là một số.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2685, 1, 'vi', 'validation', 'regex', 'Định dạng trường :attribute không hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2686, 1, 'vi', 'validation', 'required', 'Trường :attribute không được bỏ trống.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2687, 1, 'vi', 'validation', 'required_if', 'Trường :attribute không được bỏ trống khi trường :other là :value.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2688, 1, 'vi', 'validation', 'required_with', 'Trường :attribute không được bỏ trống khi trường :values có giá trị.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2689, 1, 'vi', 'validation', 'required_with_all', 'The :attribute field is required when :values is present.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2690, 1, 'vi', 'validation', 'required_without', 'Trường :attribute không được bỏ trống khi trường :values không có giá trị.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2691, 1, 'vi', 'validation', 'required_without_all', 'Trường :attribute không được bỏ trống khi tất cả :values không có giá trị.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2692, 1, 'vi', 'validation', 'same', 'Trường :attribute và :other phải giống nhau.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2693, 1, 'vi', 'validation', 'size.numeric', 'Trường :attribute phải bằng :size.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2694, 1, 'vi', 'validation', 'size.file', 'Dung lượng tập tin trong trường :attribute phải bằng :size kB.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2695, 1, 'vi', 'validation', 'size.string', 'Trường :attribute phải chứa :size ký tự.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2696, 1, 'vi', 'validation', 'size.array', 'Trường :attribute phải chứa :size phần tử.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2697, 1, 'vi', 'validation', 'timezone', 'Trường :attribute phải là một múi giờ hợp lệ.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2698, 1, 'vi', 'validation', 'unique', 'Trường :attribute đã có trong CSDL.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2699, 1, 'vi', 'validation', 'url', 'Trường :attribute không giống với định dạng một URL.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2700, 1, 'vi', 'validation', 'uploaded', 'Không thể tải lên, hãy thử lại.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2701, 1, 'vi', 'validation', 'custom.email.email', 'Địa chỉ email không hợp lệ', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2702, 1, 'vi', 'validation', 'custom.email.required', 'Email không được để trống!', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2703, 1, 'vi', 'validation', 'custom.email.unique', 'Email đã được sử dụng, vui lòng chọn email khác!', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2704, 1, 'vi', 'validation', 'custom.password.min', 'Mật khẩu phải ít nhất :min kí tự.', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2705, 1, 'vi', 'validation', 'custom.password.required', 'Mật khẩu không được để trống!', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2706, 1, 'vi', 'validation', 'custom.repassword.same', 'Mật khẩu và xác nhận mật khẩu không trùng khớp', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2707, 1, 'vi', 'validation', 'custom.username.min', 'Tên phải ít nhất 6 kí tự', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2708, 1, 'vi', 'validation', 'custom.username.required', 'Tên đăng nhập không được để trống!', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2709, 1, 'vi', 'validation', 'custom.username.unique', 'Tên này đã được sử dụng, vui lòng chọn tên khác!', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2710, 1, 'vi', 'validation', 'attributes', 'Thuộc tính', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2711, 1, 'vi', 'validation', 'after_or_equal', 'Thuộc tính :attribute phải là ngày lớn hơn hoặc bằng :date', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2712, 1, 'vi', 'validation', 'before_or_equal', 'Trường :attribute phải là ngày trước hoặc bằng ngày :date', '2020-03-29 20:07:02', '2020-03-29 20:07:02'),
(2713, 0, 'en', 'plugins/impersonate/impersonate', 'leave_impersonation', 'Leave impersonation', '2020-04-24 09:38:53', '2020-04-26 08:02:16'),
(2714, 0, 'en', 'plugins/impersonate/impersonate', 'login_as_this_user', 'Login as this user', '2020-04-24 09:38:53', '2020-04-26 08:02:16'),
(2715, 0, 'en', 'plugins/language/language', 'theme-translations', 'Theme translations', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2716, 0, 'en', 'plugins/language/language', 'translate_from', 'Translate from', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2717, 0, 'en', 'plugins/language/language', 'to', 'to', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2718, 0, 'en', 'plugins/language/language', 'no_other_languages', 'No other language to translate!', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2719, 0, 'vi', 'plugins/language/language', 'translate_from', 'Dịch từ', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2720, 0, 'vi', 'plugins/language/language', 'to', 'sang', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2721, 0, 'vi', 'plugins/language/language', 'no_other_languages', 'Không có ngôn ngữ khác để thực hiện dịch!', '2020-04-24 09:38:54', '2020-04-26 08:02:16'),
(2722, 0, 'en', 'plugins/member/dashboard', 'joined_on', 'Joined :date', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2723, 0, 'en', 'plugins/member/dashboard', 'dob', 'Born :date', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2724, 0, 'en', 'plugins/member/dashboard', 'email', 'Email', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2725, 0, 'en', 'plugins/member/dashboard', 'password', 'Password', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2726, 0, 'en', 'plugins/member/dashboard', 'password-confirmation', 'Confirm Password', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2727, 0, 'en', 'plugins/member/dashboard', 'remember-me', 'Remember Me', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2728, 0, 'en', 'plugins/member/dashboard', 'login-title', 'Login', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2729, 0, 'en', 'plugins/member/dashboard', 'login-cta', 'Login', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2730, 0, 'en', 'plugins/member/dashboard', 'register-title', 'Register', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2731, 0, 'en', 'plugins/member/dashboard', 'register-cta', 'Register', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2732, 0, 'en', 'plugins/member/dashboard', 'forgot-password-cta', 'Forgot Your Password?', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2733, 0, 'en', 'plugins/member/dashboard', 'name', 'Name', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2734, 0, 'en', 'plugins/member/dashboard', 'reset-password-title', 'Reset Password', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2735, 0, 'en', 'plugins/member/dashboard', 'reset-password-cta', 'Reset Password', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2736, 0, 'en', 'plugins/member/dashboard', 'cancel-link', 'Cancel', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2737, 0, 'en', 'plugins/member/dashboard', 'logout-cta', 'Logout', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2738, 0, 'en', 'plugins/member/dashboard', 'header_profile_link', 'Profile', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2739, 0, 'en', 'plugins/member/dashboard', 'header_settings_link', 'Settings', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2740, 0, 'en', 'plugins/member/dashboard', 'header_logout_link', 'Logout', '2020-04-24 09:38:55', '2020-04-26 08:02:16'),
(2741, 0, 'en', 'plugins/member/dashboard', 'unknown_state', 'Unknown', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2742, 0, 'en', 'plugins/member/dashboard', 'close', 'Close', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2743, 0, 'en', 'plugins/member/dashboard', 'save', 'Save', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2744, 0, 'en', 'plugins/member/dashboard', 'loading', 'Loading...', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2745, 0, 'en', 'plugins/member/dashboard', 'new_image', 'New image', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2746, 0, 'en', 'plugins/member/dashboard', 'change_profile_image', 'Change avatar', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2747, 0, 'en', 'plugins/member/dashboard', 'save_cropped_image_failed', 'Save cropped image failed!', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2748, 0, 'en', 'plugins/member/dashboard', 'failed_to_crop_image', 'Failed to crop image', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2749, 0, 'en', 'plugins/member/dashboard', 'failed_to_load_data', 'Failed to load data', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2750, 0, 'en', 'plugins/member/dashboard', 'read_image_failed', 'Read image failed', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2751, 0, 'en', 'plugins/member/dashboard', 'update_avatar_success', 'Update avatar successfully!', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2752, 0, 'en', 'plugins/member/dashboard', 'change_avatar_description', 'Click on image to change avatar', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2753, 0, 'en', 'plugins/member/dashboard', 'notices.error', 'Error!', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2754, 0, 'en', 'plugins/member/dashboard', 'notices.success', 'Success!', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2755, 0, 'en', 'plugins/member/dashboard', 'sidebar_title', 'Personal settings', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2756, 0, 'en', 'plugins/member/dashboard', 'sidebar_information', 'Account Information', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2757, 0, 'en', 'plugins/member/dashboard', 'sidebar_security', 'Security', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2758, 0, 'en', 'plugins/member/dashboard', 'account_field_title', 'Account Information', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2759, 0, 'en', 'plugins/member/dashboard', 'profile-picture', 'Profile picture', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2760, 0, 'en', 'plugins/member/dashboard', 'uploading', 'Uploading...', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2761, 0, 'en', 'plugins/member/dashboard', 'phone', 'Phone', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2762, 0, 'en', 'plugins/member/dashboard', 'first_name', 'First name', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2763, 0, 'en', 'plugins/member/dashboard', 'last_name', 'Last name', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2764, 0, 'en', 'plugins/member/dashboard', 'description', 'Short description', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2765, 0, 'en', 'plugins/member/dashboard', 'description_placeholder', 'Tell something about yourself...', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2766, 0, 'en', 'plugins/member/dashboard', 'verified', 'Verified', '2020-04-24 09:38:55', '2020-04-26 08:02:17'),
(2767, 0, 'en', 'plugins/member/dashboard', 'verify_require_desc', 'Please verify email by link we sent to you.', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2768, 0, 'en', 'plugins/member/dashboard', 'birthday', 'Birthday', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2769, 0, 'en', 'plugins/member/dashboard', 'year_lc', 'year', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2770, 0, 'en', 'plugins/member/dashboard', 'month_lc', 'month', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2771, 0, 'en', 'plugins/member/dashboard', 'day_lc', 'day', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2772, 0, 'en', 'plugins/member/dashboard', 'gender', 'Gender', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2773, 0, 'en', 'plugins/member/dashboard', 'gender_male', 'Male', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2774, 0, 'en', 'plugins/member/dashboard', 'gender_female', 'Female', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2775, 0, 'en', 'plugins/member/dashboard', 'gender_other', 'Other', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2776, 0, 'en', 'plugins/member/dashboard', 'security_title', 'Security', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2777, 0, 'en', 'plugins/member/dashboard', 'current_password', 'Current password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2778, 0, 'en', 'plugins/member/dashboard', 'password_new', 'New password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2779, 0, 'en', 'plugins/member/dashboard', 'password_new_confirmation', 'Confirmation password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2780, 0, 'en', 'plugins/member/dashboard', 'password_update_btn', 'Update password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2781, 0, 'en', 'plugins/member/dashboard', 'activity_logs', 'Activity Logs', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2782, 0, 'en', 'plugins/member/dashboard', 'oops', 'Oops!', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2783, 0, 'en', 'plugins/member/dashboard', 'no_activity_logs', 'You have no activity logs yet', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2784, 0, 'en', 'plugins/member/dashboard', 'actions.create_post', 'You have created post \":name\"', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2785, 0, 'en', 'plugins/member/dashboard', 'actions.update_post', 'You have updated post \":name\"', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2786, 0, 'en', 'plugins/member/dashboard', 'actions.delete_post', 'You have deleted post \":name\"', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2787, 0, 'en', 'plugins/member/dashboard', 'actions.update_setting', 'You have updated your settings', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2788, 0, 'en', 'plugins/member/dashboard', 'actions.update_security', 'You have updated your security settings', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2789, 0, 'en', 'plugins/member/dashboard', 'actions.your_post_updated_by_admin', 'Your post \":name\" is updated by administrator', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2790, 0, 'en', 'plugins/member/dashboard', 'actions.changed_avatar', 'You have changed your avatar', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2791, 0, 'en', 'plugins/member/dashboard', 'load_more', 'Load more', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2792, 0, 'en', 'plugins/member/dashboard', 'loading_more', 'Loading...', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2793, 0, 'en', 'plugins/member/dashboard', 'back-to-login', 'Back to login page', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2794, 0, 'en', 'plugins/member/member', 'create', 'New member', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2795, 0, 'en', 'plugins/member/member', 'edit', 'Edit member', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2796, 0, 'en', 'plugins/member/member', 'menu_name', 'Members', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2797, 0, 'en', 'plugins/member/member', 'confirmation_subject', 'Email verification', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2798, 0, 'en', 'plugins/member/member', 'confirmation_subject_title', 'Verify your email', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2799, 0, 'en', 'plugins/member/member', 'not_confirmed', 'The given email address has not been confirmed. <a href=\":resend_link\">Resend confirmation link.</a>', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2800, 0, 'en', 'plugins/member/member', 'confirmation_successful', 'You successfully confirmed your email address.', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2801, 0, 'en', 'plugins/member/member', 'confirmation_info', 'Please confirm your email address.', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2802, 0, 'en', 'plugins/member/member', 'confirmation_resent', 'We sent you another confirmation email. You should receive it shortly.', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2803, 0, 'en', 'plugins/member/member', 'form.email', 'Email', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2804, 0, 'en', 'plugins/member/member', 'form.password', 'Password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2805, 0, 'en', 'plugins/member/member', 'form.password_confirmation', 'Password confirmation', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2806, 0, 'en', 'plugins/member/member', 'form.change_password', 'Change password?', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2807, 0, 'en', 'plugins/member/member', 'forgot_password', 'Forgot password', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2808, 0, 'en', 'plugins/member/member', 'login', 'Login', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2809, 0, 'en', 'plugins/post-scheduler/post-scheduler', 'name', 'Post Scheduler', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2810, 0, 'en', 'plugins/product/base', 'menu_name', 'Product', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2811, 0, 'en', 'plugins/product/base', 'product_page', 'Product Page', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2812, 0, 'en', 'plugins/product/cart', 'name', 'Carts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2813, 0, 'en', 'plugins/product/cart', 'namelist', 'List Carts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2814, 0, 'en', 'plugins/product/cart', 'create', 'New cart', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2815, 0, 'en', 'plugins/product/cart', 'edit', 'Edit cart', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2816, 0, 'en', 'plugins/product/cart', 'form.pricecost', 'Price Cost', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2817, 0, 'en', 'plugins/product/color', 'name', 'Colors', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2818, 0, 'en', 'plugins/product/color', 'create', 'New color', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2819, 0, 'en', 'plugins/product/color', 'edit', 'Edit color', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2820, 0, 'en', 'plugins/product/currency', 'currencies', 'Currencies', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2821, 0, 'en', 'plugins/product/currency', 'setting_description', 'List of currencies using on website', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2822, 0, 'en', 'plugins/product/currency', 'name', 'Name', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2823, 0, 'en', 'plugins/product/currency', 'symbol', 'Symbol', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2824, 0, 'en', 'plugins/product/currency', 'number_of_decimals', 'Number of decimals', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2825, 0, 'en', 'plugins/product/currency', 'exchange_rate', 'Exchange rate', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2826, 0, 'en', 'plugins/product/currency', 'is_prefix_symbol', 'Position of symbol', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2827, 0, 'en', 'plugins/product/currency', 'is_default', 'Is default?', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2828, 0, 'en', 'plugins/product/currency', 'remove', 'Remove', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2829, 0, 'en', 'plugins/product/currency', 'new_currency', 'Add a new currency', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2830, 0, 'en', 'plugins/product/currency', 'save_settings', 'Save settings', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2831, 0, 'en', 'plugins/product/currency', 'before_number', 'Before number', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2832, 0, 'en', 'plugins/product/currency', 'after_number', 'After number', '2020-04-24 09:38:56', '2020-04-26 08:03:14'),
(2833, 0, 'en', 'plugins/product/features', 'name', 'Features', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2834, 0, 'en', 'plugins/product/features', 'create', 'New features', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2835, 0, 'en', 'plugins/product/features', 'edit', 'Edit features', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2836, 0, 'en', 'plugins/product/features', 'form.features', 'Tiện ích', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2837, 0, 'en', 'plugins/product/features', 'form.button_add_image', 'Add image', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2838, 0, 'en', 'plugins/product/features', 'form.button_add_color', 'Add color', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2839, 0, 'en', 'plugins/product/features', 'form.button_add_size', 'Add size', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2840, 0, 'en', 'plugins/product/features', 'form.images', 'Images', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2841, 0, 'en', 'plugins/product/features', 'form.colors', 'Colors', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2842, 0, 'en', 'plugins/product/features', 'form.sizes', 'Sizes', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2843, 0, 'en', 'plugins/product/features', 'form.option1', 'Option', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2844, 0, 'en', 'plugins/product/member', 'dob', 'Born :date', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2845, 0, 'en', 'plugins/product/member', 'draft_posts', 'Draft Posts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2846, 0, 'en', 'plugins/product/member', 'pending_posts', 'Pending Posts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2847, 0, 'en', 'plugins/product/member', 'published_posts', 'Published Posts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2848, 0, 'en', 'plugins/product/member', 'posts', 'Posts', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2849, 0, 'en', 'plugins/product/member', 'write_post', 'Write a post', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2850, 0, 'en', 'plugins/product/orderstatus', 'name', 'Order Status', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2851, 0, 'en', 'plugins/product/orderstatus', 'namelist', 'List Orders', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2852, 0, 'en', 'plugins/product/orderstatus', 'create', 'New order', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2853, 0, 'en', 'plugins/product/orderstatus', 'edit', 'Edit order', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2854, 0, 'en', 'plugins/product/orderstatus', 'form.orderstatus', 'Orders Status...!', '2020-04-24 09:38:56', '2020-04-26 08:02:17'),
(2855, 0, 'en', 'plugins/product/orderstatus', 'form.status', 'All Status Order', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2856, 0, 'en', 'plugins/product/payment', 'name', 'Payments', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2857, 0, 'en', 'plugins/product/payment', 'create', 'New payment', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2858, 0, 'en', 'plugins/product/payment', 'edit', 'Edit payment', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2859, 0, 'en', 'plugins/product/payment', 'form.paymentstatus', 'Payment Status', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2860, 0, 'en', 'plugins/product/procategories', 'create', 'Create new Pro category', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2861, 0, 'en', 'plugins/product/procategories', 'edit', 'Edit Product category', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2862, 0, 'en', 'plugins/product/procategories', 'menu', 'Product Categories', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2863, 0, 'en', 'plugins/product/procategories', 'edit_this_procategory', 'Edit this Product category', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2864, 0, 'en', 'plugins/product/procategories', 'menu_name', 'Product Categories', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2865, 0, 'en', 'plugins/product/procategories', 'none', 'None', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2866, 0, 'en', 'plugins/product/procategories', 'menu_type', 'Dạng Menu', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2867, 0, 'en', 'plugins/product/procategories', 'is_hot', 'Show icon Hot', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2868, 0, 'en', 'plugins/product/procategories', 'is_new', 'Show icon New', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2869, 0, 'en', 'plugins/product/products', 'model', 'Product', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2870, 0, 'en', 'plugins/product/products', 'models', 'Products', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2871, 0, 'en', 'plugins/product/products', 'create', 'Create new product', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2872, 0, 'en', 'plugins/product/products', 'edit', 'Edit product', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2873, 0, 'en', 'plugins/product/products', 'form.name', 'Name', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2874, 0, 'en', 'plugins/product/products', 'form.name_placeholder', 'Product\'s name (Maximum :c characters)', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2875, 0, 'en', 'plugins/product/products', 'form.description', 'Description', '2020-04-24 09:38:56', '2020-04-26 08:02:18'),
(2876, 0, 'en', 'plugins/product/products', 'form.description_placeholder', 'Short description for product (Maximum :c characters)', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2877, 0, 'en', 'plugins/product/products', 'form.categories', 'Categories', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2878, 0, 'en', 'plugins/product/products', 'form.tags', 'Tags', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2879, 0, 'en', 'plugins/product/products', 'form.tags_placeholder', 'Tags', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2880, 0, 'en', 'plugins/product/products', 'form.content', 'Content', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2881, 0, 'en', 'plugins/product/products', 'form.is_featured', 'Is featured?', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2882, 0, 'en', 'plugins/product/products', 'form.note', 'Note content', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2883, 0, 'en', 'plugins/product/products', 'form.format_type', 'Format', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2884, 0, 'en', 'plugins/product/products', 'form.button_add_product', 'Thêm sản phẩm', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2885, 0, 'en', 'plugins/product/products', 'form.currency', 'Currency', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2886, 0, 'en', 'plugins/product/products', 'form.video', 'video', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2887, 0, 'en', 'plugins/product/products', 'cannot_delete', 'Product could not be deleted', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2888, 0, 'en', 'plugins/product/products', 'product_deleted', 'Product deleted', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2889, 0, 'en', 'plugins/product/products', 'products', 'Products', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2890, 0, 'en', 'plugins/product/products', 'edit_this_product', 'Edit this product', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2891, 0, 'en', 'plugins/product/products', 'no_new_product_now', 'There is no new product now!', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2892, 0, 'en', 'plugins/product/products', 'menu_name', 'Products', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2893, 0, 'en', 'plugins/product/products', 'widget_products_recent', 'Recent Products', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2894, 0, 'en', 'plugins/product/products', 'categories', 'Categories', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2895, 0, 'en', 'plugins/product/products', 'author', 'Author', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2896, 0, 'en', 'plugins/product/protags', 'model', 'ProTag', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2897, 0, 'en', 'plugins/product/protags', 'models', 'Tags Product', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2898, 0, 'en', 'plugins/product/protags', 'form.name', 'Name', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2899, 0, 'en', 'plugins/product/protags', 'form.name_placeholder', 'ProTag\'s name (Maximum 120 characters)', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2900, 0, 'en', 'plugins/product/protags', 'form.description', 'Description', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2901, 0, 'en', 'plugins/product/protags', 'form.description_placeholder', 'Short description for ProTag (Maximum 400 characters)', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2902, 0, 'en', 'plugins/product/protags', 'form.procategories', 'Product Categories', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2903, 0, 'en', 'plugins/product/protags', 'notices.no_select', 'Please select at least one ProTag to take this action!', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2904, 0, 'en', 'plugins/product/protags', 'create', 'Create new ProTag', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2905, 0, 'en', 'plugins/product/protags', 'edit', 'Edit ProTag', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2906, 0, 'en', 'plugins/product/protags', 'cannot_delete', 'ProTag could not be deleted', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2907, 0, 'en', 'plugins/product/protags', 'deleted', 'ProTag deleted', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2908, 0, 'en', 'plugins/product/protags', 'menu', 'ProTags', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2909, 0, 'en', 'plugins/product/protags', 'edit_this_protag', 'Edit this ProTag', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2910, 0, 'en', 'plugins/product/protags', 'menu_name', 'ProTags', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2911, 0, 'en', 'plugins/product/real-estate', 'name', 'Real Estate', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2912, 0, 'en', 'plugins/product/real-estate', 'settings', 'Settings', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2913, 0, 'en', 'plugins/product/real-estate', 'google_map', 'Google Map', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2914, 0, 'en', 'plugins/product/real-estate', 'google_map_description', 'Settings for Google Map to search location', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2915, 0, 'en', 'plugins/product/real-estate', 'api_key', 'API key', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2916, 0, 'en', 'plugins/product/sell', 'form.pricecost', 'Price Cost', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2917, 0, 'en', 'plugins/product/sell', 'form.pricesell', 'Price Sell', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2918, 0, 'en', 'plugins/product/sell', 'form.pricetime', 'Price Time', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2919, 0, 'en', 'plugins/product/sell', 'form.change', 'Click Time', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2920, 0, 'en', 'plugins/product/sell', 'form.timesale', 'Price SaleOff', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2921, 0, 'en', 'plugins/product/sell', 'form.amoundprice', 'Price Amound', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2922, 0, 'en', 'plugins/product/sell', 'form.priceSaleOff', ' SaleOff', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2923, 0, 'en', 'plugins/product/sell', 'form.price_promotion_end', 'Time SaleOff End', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2924, 0, 'en', 'plugins/product/sell', 'form.price_promotion_start', 'Time SaleOff Start', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2925, 0, 'en', 'plugins/product/sell', 'form.button_add_timesaleoff', 'Add Time SaleOff', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2926, 0, 'en', 'plugins/product/sell', 'form.currency', 'Currency', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2927, 0, 'en', 'plugins/product/settings', 'title', 'Product', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2928, 0, 'en', 'plugins/product/settings', 'description', 'Settings for Product plugin', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2929, 0, 'en', 'plugins/product/settings', 'select', '-- Select --', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2930, 0, 'en', 'plugins/product/settings', 'product_page_id', 'Product page', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2931, 0, 'en', 'plugins/product/settings', 'currency', 'Currency', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2932, 0, 'en', 'plugins/product/shipping', 'name', 'Shippings', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2933, 0, 'en', 'plugins/product/shipping', 'create', 'New shipping', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2934, 0, 'en', 'plugins/product/shipping', 'edit', 'Edit shipping', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2935, 0, 'en', 'plugins/product/shipping', 'form.shippingstatus', 'Shipping Status', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2936, 0, 'en', 'plugins/product/size', 'name', 'Sizes', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2937, 0, 'en', 'plugins/product/size', 'create', 'New size', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2938, 0, 'en', 'plugins/product/size', 'edit', 'Edit size', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2939, 0, 'en', 'plugins/product/store', 'name', 'Stores', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2940, 0, 'en', 'plugins/product/store', 'create', 'New store', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2941, 0, 'en', 'plugins/product/store', 'edit', 'Edit store', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2942, 0, 'en', 'plugins/simple-slider/simple-slider', 'create', 'New slider', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2943, 0, 'en', 'plugins/simple-slider/simple-slider', 'edit', 'Edit slider', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2944, 0, 'en', 'plugins/simple-slider/simple-slider', 'menu', 'Simple sliders', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2945, 0, 'en', 'plugins/simple-slider/simple-slider', 'settings.title', 'Simple sliders', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2946, 0, 'en', 'plugins/simple-slider/simple-slider', 'settings.description', 'Settings for Simple sliders', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2947, 0, 'en', 'plugins/simple-slider/simple-slider', 'settings.using_assets', 'Using default assets?', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2948, 0, 'en', 'plugins/simple-slider/simple-slider', 'settings.using_assets_description', 'If using assets option is enabled then below scripts will be auto added to front site.', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2949, 0, 'en', 'plugins/simple-slider/simple-slider', 'forms.type_slider', 'Kiểu hiển thị Item slider', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2950, 0, 'en', 'plugins/simple-slider/simple-slider', 'forms.description1', 'Mô tả ngắn style', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2951, 0, 'en', 'plugins/simple-slider/simple-slider', 'forms.description2', 'Mô tả ngắn style', '2020-04-24 09:38:57', '2020-04-26 08:02:18'),
(2952, 0, 'en', 'plugins/simple-slider/simple-slider', 'forms.description3', 'Mô tả ngắn style', '2020-04-24 09:38:57', '2020-04-26 08:02:19'),
(2953, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'settings.title', 'Cookie Consent', '2020-04-26 08:02:15', '2020-04-26 08:02:47'),
(2954, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'settings.description', 'Settings for cookie consent', '2020-04-26 08:02:15', '2020-04-26 08:02:47'),
(2955, 0, 'en', 'plugins/cookie-consent/cookie-consent', 'settings.enable', 'Enable cookie consent?', '2020-04-26 08:02:15', '2020-04-26 08:02:47'),
(2956, 0, 'en', 'plugins/product/currency', 'million', 'Million', '2020-04-26 08:02:17', '2020-04-26 08:03:14'),
(2957, 0, 'en', 'plugins/product/currency', 'billion', 'Billion', '2020-04-26 08:02:17', '2020-04-26 08:03:14'),
(2958, 0, 'en', 'plugins/product/store-product', 'create', 'New Product', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2959, 0, 'en', 'plugins/product/store-product', 'edit', 'Edit product', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2960, 0, 'en', 'plugins/product/store-product', 'menu', 'Store products', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2961, 0, 'en', 'plugins/product/store-product', 'settings.title', 'Store products', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2962, 0, 'en', 'plugins/product/store-product', 'settings.description', 'Settings for Store products', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2963, 0, 'en', 'plugins/product/store-product', 'settings.using_assets', 'Using default assets?', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2964, 0, 'en', 'plugins/product/store-product', 'settings.using_assets_description', 'If using assets option is enabled then below scripts will be auto added to front site.', '2020-04-26 08:02:18', '2020-04-26 08:02:49'),
(2965, 0, 'vi', 'plugins/product/currency', 'billion', 'Tỷ', '2020-04-26 08:03:06', '2020-04-26 08:03:14'),
(2966, 0, 'vi', 'plugins/product/currency', 'million', 'Triệu', '2020-04-26 08:03:10', '2020-04-26 08:03:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `super_user` tinyint(1) NOT NULL DEFAULT 0,
  `manage_supers` tinyint(1) NOT NULL DEFAULT 0,
  `avatar_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `permissions`, `last_login`, `first_name`, `last_name`, `username`, `super_user`, `manage_supers`, `avatar_id`) VALUES
(1, 'admin@botble.com', '$2y$10$A.uOgqPJthpIKaaUQ3QaueO19/dtXbntkRuNCRM4DA1D0wN33Jsd6', 'bbKJjpoDILVd5d8AFhbAkC6Gdi3C18DMA4aeI3uxZqBcVA8GCSiRiNivREUa', '2017-11-15 06:57:09', '2020-04-24 09:21:44', '{\"analytics.general\":true,\"analytics.page\":true,\"analytics.browser\":true,\"analytics.referrer\":true,\"backups.list\":true,\"backups.create\":true,\"backups.restore\":true,\"backups.delete\":true,\"block.list\":true,\"block.create\":true,\"block.edit\":true,\"block.delete\":true,\"categories.list\":true,\"categories.create\":true,\"categories.edit\":true,\"categories.delete\":true,\"contacts.list\":true,\"contacts.create\":true,\"contacts.edit\":true,\"contacts.delete\":true,\"custom-fields.list\":true,\"custom-fields.create\":true,\"custom-fields.edit\":true,\"custom-fields.delete\":true,\"dashboard.index\":true,\"galleries.list\":true,\"galleries.create\":true,\"galleries.edit\":true,\"galleries.delete\":true,\"languages.list\":true,\"languages.create\":true,\"languages.edit\":true,\"languages.delete\":true,\"logs.list\":true,\"logs.delete\":true,\"media.index\":true,\"files.list\":true,\"files.create\":true,\"files.edit\":true,\"files.trash\":true,\"files.delete\":true,\"folders.list\":true,\"folders.create\":true,\"folders.edit\":true,\"folders.trash\":true,\"folders.delete\":true,\"member.list\":true,\"member.create\":true,\"member.edit\":true,\"member.delete\":true,\"menus.list\":true,\"menus.create\":true,\"menus.edit\":true,\"menus.delete\":true,\"pages.list\":true,\"pages.create\":true,\"pages.edit\":true,\"pages.delete\":true,\"plugins.list\":true,\"posts.list\":true,\"posts.create\":true,\"posts.edit\":true,\"posts.delete\":true,\"roles.list\":true,\"roles.create\":true,\"roles.edit\":true,\"roles.delete\":true,\"settings.options\":true,\"tags.list\":true,\"tags.create\":true,\"tags.edit\":true,\"tags.delete\":true,\"translations.list\":true,\"translations.create\":true,\"translations.edit\":true,\"translations.delete\":true,\"users.list\":true,\"users.create\":true,\"users.edit\":true,\"users.delete\":true,\"widgets.list\":true,\"superuser\":true,\"manage_supers\":true}', '2020-04-24 09:21:44', 'System', 'Admin', 'botble', 1, 1, 117);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_meta`
--

CREATE TABLE `user_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user_meta`
--

INSERT INTO `user_meta` (`id`, `key`, `value`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'languages_current_data_language', 'en_US', 1, '2017-11-30 18:27:51', '2018-04-13 10:00:39'),
(2, 'admin-theme', 'default', 1, '2018-03-07 03:42:13', '2020-04-24 07:38:02'),
(3, 'admin-locale', 'en', 1, '2018-03-07 03:42:14', '2018-07-04 03:37:40'),
(4, 'site-locale', 'vi', 1, '2020-04-13 05:22:12', '2020-04-26 08:01:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `widgets`
--

CREATE TABLE `widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `widget_id` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sidebar_id` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `widgets`
--

INSERT INTO `widgets` (`id`, `widget_id`, `sidebar_id`, `theme`, `position`, `data`, `created_at`, `updated_at`) VALUES
(2, 'Botble\\Widget\\Widgets\\Text', 'second_sidebar', 'demo', 0, '{\"id\":\"Botble\\\\Widget\\\\Widgets\\\\Text\",\"name\":\"Text\",\"content\":\"\"}', '2016-12-18 04:47:20', '2016-12-18 04:47:20'),
(7, 'RecentPostsWidget', 'top_sidebar', 'ripple', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"Recent Posts\",\"number_display\":\"5\"}', '2016-12-18 04:48:00', '2016-12-18 04:48:00'),
(9, 'Botble\\Widget\\Widgets\\Text', 'primary_sidebar', 'demo', 0, '{\"id\":\"Botble\\\\Widget\\\\Widgets\\\\Text\",\"name\":\"Text\",\"content\":\"\"}', '2016-12-18 04:50:57', '2016-12-18 04:50:57'),
(30, 'RecentPostsWidget', 'footer_sidebar', 'ripple', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"Recent Posts\",\"number_display\":\"5\"}', '2016-12-24 07:42:58', '2016-12-24 07:42:58'),
(31, 'CustomMenuWidget', 'footer_sidebar', 'ripple', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Favorite website\",\"menu_id\":\"favorite-website\"}', '2016-12-24 07:42:58', '2016-12-24 07:42:58'),
(32, 'CustomMenuWidget', 'footer_sidebar', 'ripple', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"My links\",\"menu_id\":\"my-links\"}', '2016-12-24 07:42:58', '2016-12-24 07:42:58'),
(61, 'RecentPostsWidget', 'footer_sidebar', 'newstv', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"Recent posts\",\"number_display\":\"6\"}', '2017-04-30 19:56:39', '2017-04-30 19:56:39'),
(62, 'CustomMenuWidget', 'footer_sidebar', 'newstv', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Favorite websites\",\"menu_id\":\"favorite-website\"}', '2017-04-30 19:56:39', '2017-04-30 19:56:39'),
(63, 'CustomMenuWidget', 'footer_sidebar', 'newstv', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"My links\",\"menu_id\":\"my-links\"}', '2017-04-30 19:56:39', '2017-04-30 19:56:39'),
(64, 'CustomMenuWidget', 'footer_sidebar', 'newstv', 3, '{\"id\":\"CustomMenuWidget\",\"name\":\"Categories\",\"menu_id\":\"featured-categories\"}', '2017-04-30 19:56:39', '2017-04-30 19:56:39'),
(74, 'TagsWidget', 'primary_sidebar', 'ripple-vi', 0, '{\"id\":\"TagsWidget\",\"name\":\"Th\\u1ebb\",\"number_display\":\"5\"}', '2018-04-13 08:52:05', '2018-04-13 08:52:05'),
(75, 'CustomMenuWidget', 'primary_sidebar', 'ripple-vi', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Chuy\\u00ean m\\u1ee5c n\\u1ed5i b\\u1eadt\",\"menu_id\":\"featured-categories\"}', '2018-04-13 08:52:05', '2018-04-13 08:52:05'),
(76, 'CustomMenuWidget', 'primary_sidebar', 'ripple-vi', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"M\\u1ea1ng x\\u00e3 h\\u1ed9i \",\"menu_id\":\"social\"}', '2018-04-13 08:52:05', '2018-04-13 08:52:05'),
(78, 'RecentPostsWidget', 'top_sidebar', 'ripple-vi', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"B\\u00e0i vi\\u1ebft n\\u1ed5i b\\u1eadt\",\"number_display\":\"5\"}', '2018-04-13 08:52:59', '2018-04-13 08:52:59'),
(89, 'RecentPostsWidget', 'footer_sidebar', 'ripple-vi', 0, '{\"id\":\"RecentPostsWidget\",\"name\":\"B\\u00e0i vi\\u1ebft n\\u1ed5i b\\u1eadt\",\"number_display\":\"5\"}', '2018-04-13 08:54:28', '2018-04-13 08:54:28'),
(90, 'CustomMenuWidget', 'footer_sidebar', 'ripple-vi', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Website \\u01b0a th\\u00edch\",\"menu_id\":\"favorite-website\"}', '2018-04-13 08:54:28', '2018-04-13 08:54:28'),
(91, 'CustomMenuWidget', 'footer_sidebar', 'ripple-vi', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"Li\\u00ean k\\u1ebft \",\"menu_id\":\"main-menu\"}', '2018-04-13 08:54:28', '2018-04-13 08:54:28'),
(103, 'PopularPostsWidget', 'primary_sidebar', 'newstv-vi', 0, '{\"id\":\"PopularPostsWidget\",\"name\":\"B\\u00e0i vi\\u1ebft n\\u1ed5i b\\u1eadt\",\"number_display\":\"5\"}', '2018-04-13 10:10:24', '2018-04-13 10:10:24'),
(104, 'VideoPostsWidget', 'primary_sidebar', 'newstv-vi', 1, '{\"id\":\"VideoPostsWidget\",\"name\":\"Video \",\"number_display\":\"4\"}', '2018-04-13 10:10:24', '2018-04-13 10:10:24'),
(105, 'FacebookWidget', 'primary_sidebar', 'newstv-vi', 2, '{\"id\":\"FacebookWidget\",\"name\":\"Facebook\",\"facebook_name\":\"Botble Technologies \",\"facebook_id\":\"https:\\/\\/www.facebook.com\\/botble.technologies\\/\"}', '2018-04-13 10:10:24', '2018-04-13 10:10:24'),
(131, 'CustomMenuWidget', 'footer_sidebar', 'newstv-vi', 0, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u001fWebsite \\u01b0a th\\u00edch \",\"menu_id\":\"favorite-website\"}', '2018-04-13 10:12:50', '2018-04-13 10:12:50'),
(132, 'CustomMenuWidget', 'footer_sidebar', 'newstv-vi', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Li\\u00ean k\\u1ebft \",\"menu_id\":\"my-links\"}', '2018-04-13 10:12:50', '2018-04-13 10:12:50'),
(133, 'CustomMenuWidget', 'footer_sidebar', 'newstv-vi', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"\\u001fChuy\\u00ean m\\u1ee5c n\\u1ed5i b\\u1eadt \",\"menu_id\":\"featured-categories\"}', '2018-04-13 10:12:50', '2018-04-13 10:12:50'),
(134, 'CustomMenuWidget', 'footer_sidebar', 'newstv-vi', 3, '{\"id\":\"CustomMenuWidget\",\"name\":\"M\\u1ea1ng x\\u00e3 h\\u1ed9i \",\"menu_id\":\"social\"}', '2018-04-13 10:12:50', '2018-04-13 10:12:50'),
(143, 'PopularPostsWidget', 'primary_sidebar', 'newstv', 0, '{\"id\":\"PopularPostsWidget\",\"name\":\"Top Views\",\"number_display\":\"5\"}', '2019-11-03 17:32:32', '2019-11-03 17:32:32'),
(144, 'VideoPostsWidget', 'primary_sidebar', 'newstv', 1, '{\"id\":\"VideoPostsWidget\",\"name\":\"Videos\",\"number_display\":\"1\"}', '2019-11-03 17:32:32', '2019-11-03 17:32:32'),
(145, 'FacebookWidget', 'primary_sidebar', 'newstv', 2, '{\"id\":\"FacebookWidget\",\"name\":\"Facebook\",\"facebook_name\":\"Botble Technologies\",\"facebook_id\":\"https:\\/\\/www.facebook.com\\/botble.technologies\"}', '2019-11-03 17:32:32', '2019-11-03 17:32:32'),
(146, 'AdsWidget', 'primary_sidebar', 'newstv', 3, '{\"id\":\"AdsWidget\",\"image_link\":\"#\",\"image_new_tab\":\"0\",\"image_url\":\"\\/storage\\/ads\\/300x250.jpg\"}', '2019-11-03 17:32:32', '2019-11-03 17:32:32'),
(147, 'TagsWidget', 'primary_sidebar', 'ripple', 0, '{\"id\":\"TagsWidget\",\"name\":\"Tags\",\"number_display\":\"50\"}', '2020-04-26 04:24:22', '2020-04-26 04:24:22'),
(148, 'CustomMenuWidget', 'primary_sidebar', 'ripple', 1, '{\"id\":\"CustomMenuWidget\",\"name\":\"Categories\",\"menu_id\":\"menu-chinh\"}', '2020-04-26 04:24:22', '2020-04-26 04:24:22'),
(149, 'CustomMenuWidget', 'primary_sidebar', 'ripple', 2, '{\"id\":\"CustomMenuWidget\",\"name\":\"Social\",\"menu_id\":\"menu-chinh\"}', '2020-04-26 04:24:22', '2020-04-26 04:24:22');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activations_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `audit_histories`
--
ALTER TABLE `audit_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audit_history_user_id_index` (`user_id`),
  ADD KEY `audit_history_module_index` (`module`);

--
-- Chỉ mục cho bảng `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_index` (`parent_id`),
  ADD KEY `categories_user_id_index` (`author_id`);

--
-- Chỉ mục cho bảng `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contact_replies`
--
ALTER TABLE `contact_replies`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_fields_field_item_id_foreign` (`field_item_id`);

--
-- Chỉ mục cho bảng `dashboard_widgets`
--
ALTER TABLE `dashboard_widgets`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `dashboard_widget_settings`
--
ALTER TABLE `dashboard_widget_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dashboard_widget_settings_user_id_index` (`user_id`),
  ADD KEY `dashboard_widget_settings_widget_id_index` (`widget_id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `field_groups`
--
ALTER TABLE `field_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_groups_created_by_foreign` (`created_by`),
  ADD KEY `field_groups_updated_by_foreign` (`updated_by`);

--
-- Chỉ mục cho bảng `field_items`
--
ALTER TABLE `field_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_items_field_group_id_foreign` (`field_group_id`),
  ADD KEY `field_items_parent_id_foreign` (`parent_id`);

--
-- Chỉ mục cho bảng `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `gallery_meta`
--
ALTER TABLE `gallery_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_meta_content_id_index` (`reference_id`);

--
-- Chỉ mục cho bảng `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Chỉ mục cho bảng `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`lang_id`);

--
-- Chỉ mục cho bảng `language_meta`
--
ALTER TABLE `language_meta`
  ADD PRIMARY KEY (`lang_meta_id`),
  ADD KEY `language_meta_lang_meta_content_id_index` (`reference_id`);

--
-- Chỉ mục cho bảng `media_files`
--
ALTER TABLE `media_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_files_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `media_folders`
--
ALTER TABLE `media_folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_folders_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `media_settings`
--
ALTER TABLE `media_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_email_unique` (`email`);

--
-- Chỉ mục cho bảng `member_activity_logs`
--
ALTER TABLE `member_activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_activity_logs_member_id_index` (`member_id`);

--
-- Chỉ mục cho bảng `member_password_resets`
--
ALTER TABLE `member_password_resets`
  ADD KEY `member_password_resets_email_index` (`email`),
  ADD KEY `member_password_resets_token_index` (`token`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `menu_locations`
--
ALTER TABLE `menu_locations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menu_nodes`
--
ALTER TABLE `menu_nodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_nodes_menu_id_index` (`menu_id`),
  ADD KEY `menu_nodes_parent_id_index` (`parent_id`),
  ADD KEY `menu_nodes_related_id_index` (`reference_id`);

--
-- Chỉ mục cho bảng `meta_boxes`
--
ALTER TABLE `meta_boxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_boxes_content_id_index` (`reference_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Chỉ mục cho bảng `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Chỉ mục cho bảng `orderstatuses`
--
ALTER TABLE `orderstatuses`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_category_category_id_index` (`category_id`),
  ADD KEY `post_category_post_id_index` (`post_id`);

--
-- Chỉ mục cho bảng `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_tag_id_index` (`tag_id`),
  ADD KEY `post_tag_post_id_index` (`post_id`);

--
-- Chỉ mục cho bảng `procategories`
--
ALTER TABLE `procategories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_carts`
--
ALTER TABLE `product_carts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_colors`
--
ALTER TABLE `product_colors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_features`
--
ALTER TABLE `product_features`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_orderstatuses`
--
ALTER TABLE `product_orderstatuses`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_payments`
--
ALTER TABLE `product_payments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_shippings`
--
ALTER TABLE `product_shippings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_stores`
--
ALTER TABLE `product_stores`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_tags`
--
ALTER TABLE `product_tags`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `protags`
--
ALTER TABLE `protags`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `request_logs`
--
ALTER TABLE `request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revisions_revisionable_type_index` (`revisionable_type`),
  ADD KEY `revisions_revisionable_id_index` (`revisionable_id`),
  ADD KEY `revisions_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`),
  ADD KEY `roles_created_by_index` (`created_by`),
  ADD KEY `roles_updated_by_index` (`updated_by`);

--
-- Chỉ mục cho bảng `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_users_user_id_index` (`user_id`),
  ADD KEY `role_users_role_id_index` (`role_id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `settings_key_index` (`key`);

--
-- Chỉ mục cho bảng `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `simple_sliders`
--
ALTER TABLE `simple_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `simple_slider_items`
--
ALTER TABLE `simple_slider_items`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slugs`
--
ALTER TABLE `slugs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `store_products`
--
ALTER TABLE `store_products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `store_product_items`
--
ALTER TABLE `store_product_items`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_user_id_index` (`author_id`);

--
-- Chỉ mục cho bảng `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Chỉ mục cho bảng `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_meta_user_id_index` (`user_id`);

--
-- Chỉ mục cho bảng `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `audit_histories`
--
ALTER TABLE `audit_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT cho bảng `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `contact_replies`
--
ALTER TABLE `contact_replies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `dashboard_widgets`
--
ALTER TABLE `dashboard_widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `dashboard_widget_settings`
--
ALTER TABLE `dashboard_widget_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `field_groups`
--
ALTER TABLE `field_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `field_items`
--
ALTER TABLE `field_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `gallery_meta`
--
ALTER TABLE `gallery_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT cho bảng `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `languages`
--
ALTER TABLE `languages`
  MODIFY `lang_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT cho bảng `language_meta`
--
ALTER TABLE `language_meta`
  MODIFY `lang_meta_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT cho bảng `media_files`
--
ALTER TABLE `media_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT cho bảng `media_folders`
--
ALTER TABLE `media_folders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `media_settings`
--
ALTER TABLE `media_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `member_activity_logs`
--
ALTER TABLE `member_activity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `menu_locations`
--
ALTER TABLE `menu_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `menu_nodes`
--
ALTER TABLE `menu_nodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=610;

--
-- AUTO_INCREMENT cho bảng `meta_boxes`
--
ALTER TABLE `meta_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT cho bảng `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `orderstatuses`
--
ALTER TABLE `orderstatuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT cho bảng `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=357;

--
-- AUTO_INCREMENT cho bảng `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT cho bảng `procategories`
--
ALTER TABLE `procategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `product_carts`
--
ALTER TABLE `product_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `product_colors`
--
ALTER TABLE `product_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `product_features`
--
ALTER TABLE `product_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `product_orderstatuses`
--
ALTER TABLE `product_orderstatuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product_payments`
--
ALTER TABLE `product_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product_shippings`
--
ALTER TABLE `product_shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `product_stores`
--
ALTER TABLE `product_stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product_tags`
--
ALTER TABLE `product_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `protags`
--
ALTER TABLE `protags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `request_logs`
--
ALTER TABLE `request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT cho bảng `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `simple_sliders`
--
ALTER TABLE `simple_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `simple_slider_items`
--
ALTER TABLE `simple_slider_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `slugs`
--
ALTER TABLE `slugs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT cho bảng `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `store_products`
--
ALTER TABLE `store_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `store_product_items`
--
ALTER TABLE `store_product_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `translations`
--
ALTER TABLE `translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2967;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD CONSTRAINT `custom_fields_field_item_id_foreign` FOREIGN KEY (`field_item_id`) REFERENCES `field_items` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `field_groups`
--
ALTER TABLE `field_groups`
  ADD CONSTRAINT `field_groups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `field_groups_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `field_items`
--
ALTER TABLE `field_items`
  ADD CONSTRAINT `field_items_field_group_id_foreign` FOREIGN KEY (`field_group_id`) REFERENCES `field_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `field_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `field_items` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
