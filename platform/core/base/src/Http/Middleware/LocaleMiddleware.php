<?php

namespace Botble\Base\Http\Middleware;

use Assets;
use Closure;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

class LocaleMiddleware
{

    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    protected $app;

    /**
     * LocaleMiddleware constructor.
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->app = $application;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        $this->app->setLocale(env('APP_LOCALE', config('app.locale')));

        if ($request->session()->has('site-locale') &&
            array_key_exists($request->session()->get('site-locale'), Assets::getAdminLocales())
        ) {
            $this->app->setLocale($request->session()->get('site-locale'));
            $request->setLocale($request->session()->get('site-locale'));
        }

        return $next($request);
    }
}
