<?php

use Botble\Product\Repositories\Interfaces\StoreProductInterface;

if (!function_exists('get_all_store_products')) {
    /**
     * @param array $condition
     * @return mixed
     *
     */
    function get_all_store_products(array $condition = [])
    {
        return app(StoreProductInterface::class)->allBy($condition);
    }
}