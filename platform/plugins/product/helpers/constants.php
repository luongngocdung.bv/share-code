<?php

if (!defined('PRODUCT_MODULE_SCREEN_NAME')) {
    define('PRODUCT_MODULE_SCREEN_NAME', 'product');
}

if (!defined('PROCATEGORY_MODULE_SCREEN_NAME')) {
    define('PROCATEGORY_MODULE_SCREEN_NAME', 'procategory');
}

if (!defined('PROTAG_MODULE_SCREEN_NAME')) {
    define('PROTAG_MODULE_SCREEN_NAME', 'protag');
}

if (!defined('FEATURES_MODULE_SCREEN_NAME')) {
    define('FEATURES_MODULE_SCREEN_NAME', 'features');
}

if (!defined('STORE_MODULE_SCREEN_NAME')) {
    define('STORE_MODULE_SCREEN_NAME', 'store');
}

if (!defined('CART_MODULE_SCREEN_NAME')) {
    define('CART_MODULE_SCREEN_NAME', 'cart');
}

if (!defined('ORDERSTATUS_MODULE_SCREEN_NAME')) {
    define('ORDERSTATUS_MODULE_SCREEN_NAME', 'orderstatus');
}

if (!defined('PAYMENT_MODULE_SCREEN_NAME')) {
    define('PAYMENT_MODULE_SCREEN_NAME', 'payment');
}

if (!defined('SHIPPING_MODULE_SCREEN_NAME')) {
    define('SHIPPING_MODULE_SCREEN_NAME', 'shipping');
}

if (!defined('COLOR_MODULE_SCREEN_NAME')) {
    define('COLOR_MODULE_SCREEN_NAME', 'color');
}

if (!defined('SIZE_MODULE_SCREEN_NAME')) {
    define('SIZE_MODULE_SCREEN_NAME', 'size');
}

if (!defined('STORE_PRODUCT_MODULE_SCREEN_NAME')) {
    define('STORE_PRODUCT_MODULE_SCREEN_NAME', 'store-product');
}

if (!defined('STORE_PRODUCT_ITEM_MODULE_SCREEN_NAME')) {
    define('STORE_PRODUCT_ITEM_MODULE_SCREEN_NAME', 'store-product-item');
}