class StoreProductManagement {
    init() {
        let product = $('.product');
        product.each((index, el) => {
            let single = $(el).data('single');
            $(el).find('.post').hover(() => {
                let parent = $(el).parent().find('.product-control');
                if (!parent.hasClass('active')) {
                    parent.addClass('active');
                }

            }, () => {
                let parent = $(el).parent().find('.product-control');
                if (parent.hasClass('active')) {
                    parent.removeClass('active');
                }
            });
            $(el).owlCarousel({
                autoPlay: $(el).data('autoplay'),
                slideSpeed: 3000,
                paginationSpeed: 400,
                singleItem: single
            });

            $(el).siblings('.next').click(() => {
                $(el).trigger('owl.next');
            });
            $(el).siblings('.prev').click(() => {
                $(el).trigger('owl.prev');
            });
        });

        $('.product-wrap').fadeIn();
    }
}

$(document).ready(() => {
    new StoreProductManagement().init();
});