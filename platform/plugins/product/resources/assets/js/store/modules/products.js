import axios from 'axios';
const query = '/api/v1/'
const state = {
    products: [],
};
const getters = {
    allProducts: state => state.products,
};
const actions = {
    async fetchProducts({ commit }) {
        await axios.get(`${query}productcategories`) // do api đang sai đợi xíu 
        .then(response => {
            //console.log(111111111111111111, response);
            let dataApi = response.data;
            commit('setProducts', dataApi);

        })
        .catch(error => {
            console.log(error);
        });
    },
};
const mutations = {
    setProducts: (state, dataApi) => (state.products = dataApi),
};
export default {
    state,
    getters,
    actions,
    mutations
}
