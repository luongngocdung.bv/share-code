import Vue from 'vue';
// import InfiniteLoading from 'vue-infinite-loading';
// import vClickOutside from "v-click-outside";
// Vue.use(vClickOutside);
// Vue.use(InfiniteLoading, {});
import Vuex from 'vuex';
import App from './App.vue';
import store from './store';
Vue.use(Vuex)

Vue.config.devtools = true;

new Vue({
    store,
    render: h => h(App)
}).$mount('#app');
