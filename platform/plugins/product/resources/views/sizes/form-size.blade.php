<div class="form-group col-md-6">
	@foreach ($sizes as $size)
	    <label class="checkbox-inline">
	        <input name="sizes[]" type="checkbox" value="{{ $size->id }}" @if (in_array($size->id, $selectedSizes)) checked @endif>{{ $size->name }}
	    </label>&nbsp;
	@endforeach
</div>
