{{-- <script src="/platform/plugins/product/public/jscolor/colorpicker.js"></script> --}}
@php $object_option1 = old('option1', !empty($object) ? $object->option1 : null); @endphp
@if (is_array($object_option1) && !empty($object_option1))
    @foreach($object_option1 as $option1)
        @include('plugins/product::colors.components.option1', [
                'name' => 'option1[]',
                'value' => $option1
            ])
    @endforeach
@endif

<button type="button" id="add_sub_color" class="btn btn-flat btn-success">
        <i class="fa fa-plus" aria-hidden="true"></i>
        {{ trans('plugins/product::features.form.button_add_color') }}
</button>



<script>
var id_sub_color = {{ old('$object->option1')?count(old('$object->option1')):0 }};
$('#add_sub_color').click(function(event) {
    
    $(this).before('<div class="group-image option1-input"><div class="input-group"><input id="cp9" type="text" name="option1[]" class="form-control input-sm sub_color" value="pink" /><span class="input-group-btn"><span title="Remove" class="btn btn-flat btn-sm btn-danger removeColor"><i class="fa fa-times"></i></span></span></div></div>');

    $('.sub_color').colorpicker({
            //option1: old('$object->option1'),
            horizontal: true
        });


    $('.removeColor').click(function(event) {
        $(this).closest('.option1-input').remove();
    });
   
});
</script>


