<div class="form-group col-md-6">
	@foreach ($colors as $color)
	    <label class="checkbox-inline">
	        <input name="colors[]" type="checkbox" value="{{ $color->id }}" @if (in_array($color->id, $selectedColors)) checked @endif>
	        {{ $color->name }}
	    </label>&nbsp;
	    
	@endforeach
</div>
