

<div id="cp3" class="input-group colorpicker-component option1-old">
    <input type="text" name="{{$name}}" value="{{$value}}"  class="form-control sub_color" />
    <span class="input-group-addon"><i style="background-color: {{$value}}; width: 0;height: 0;padding: 2px 21px 17px 15px;position: static;"></i></span>

    <span class="input-group-btn">
		<span title="Remove" class="btn btn-flat btn-sm btn-danger removeColorOld"><i class="fa fa-times"></i></span>
	</span>

	{{-- <div class="input-group">
		  <div class="input-group-prepend">
		    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
		  </div>
		  <div class="custom-file">
		    <input type="file" class="custom-file-input" id="inputGroupFile01"
		      aria-describedby="inputGroupFileAddon01">
		    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
		  </div>
	</div> --}}
</div>




<script>

	$('.removeColorOld').click(function(event) {
        $(this).closest('.option1-old').remove();
    });
    
</script>
