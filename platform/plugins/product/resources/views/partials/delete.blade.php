<div class="modal-box-container">
    <form action="{{ route('store-product-item.delete.post', $product->id) }}" method="post" class="form-xs">
        {!! csrf_field() !!}
        {!! method_field('DELETE') !!}
        <div class="modal-title">
            <i class="til_img"></i> <strong>{{ __('Confirm delete product #:id', ['id' => $product->id]) }}</strong>
        </div>
        <div class="modal-body">
            <div class="form-body">
                <p>
                    {{ __('Do you really want to delete this slide ":title"?', ['title' => $product->title]) }}
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="javascript:;" class="btn btn-primary" data-fancybox-close>{{ __('No')  }}</a>
            <button type="submit" class="btn btn-info btn-delete-product">{{ __('Yes, let\'s delete it!') }}</button>
        </div>
    </form>
</div>