<div class="object-images-wrapper">
    <a href="#" class="add-new-object-image js-btn-trigger-add-image"
       data-name="products[]">{{ trans('plugins/product::images.form.button_add_image') }}
    </a>
    @php $object_products = old('products', !empty($object) ? $object->products : null); @endphp
    <div class="images-wrapper">
        <div data-name="products[]" class="text-center cursor-pointer js-btn-trigger-add-image default-placeholder-object-image @if (is_array($object_products) && !empty($object_products)) hidden @endif">
            <img src="{{ url('vendor/core/images/placeholder.png') }}" alt="{{ __('Image') }}" width="120">
            <br>
            <p style="color:#c3cfd8">{{ __('Sử dụng nút') }} <strong>{{ __('Chọn hình') }}</strong> {{ __('để thêm hình') }}.</p>
        </div>
        <ul class="list-unstyled list-gallery-media-images clearfix @if (!is_array($object_products) || empty($object_products)) hidden @endif" style="padding-top: 20px;">
            @if (is_array($object_products) && !empty($object_products))
                @foreach($object_products as $product)
                    <li class="object-image-item-handler">
                        @include('plugins/product::images.components.product', [
                            'name' => 'products[]',
                            'value' => $product
                        ])
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>

<script id="object_select_image_template" type="text/x-custom-template">
    @include('plugins/product::images.components.product', [
        'name' => '__name__',
        'value' => null
    ])
</script>
