<?php

return [
    'model' => 'ProTag',
    'models' => 'Tags Product',
    'form' => [
        'name' => 'Name',
        'name_placeholder' => 'ProTag\'s name (Maximum 120 characters)',
        'description' => 'Description',
        'description_placeholder' => 'Short description for ProTag (Maximum 400 characters)',
        'procategories' => 'Product Categories',
    ],
    'notices' => [
        'no_select' => 'Please select at least one ProTag to take this action!',
    ],
    'create' => 'Create new ProTag',
    'edit' => 'Edit ProTag',
    'cannot_delete' => 'ProTag could not be deleted',
    'deleted' => 'ProTag deleted',
    'menu' => 'ProTags',
    'edit_this_protag' => 'Edit this ProTag',
    'menu_name' => 'ProTags',
];
