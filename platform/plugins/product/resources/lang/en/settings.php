<?php

return [
    'title'        => 'Product',
    'description'  => 'Settings for Product plugin',
    'select'       => '-- Select --',
    'product_page_id' => 'Product page',
    'currency' => 'Currency',
];