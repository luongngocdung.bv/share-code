<?php

return [
    'name'   => 'Shippings',
    'create' => 'New shipping',
    'edit'   => 'Edit shipping',
    'form' 	=> [
    	'shippingstatus' => 'Shipping Status'
    ]
];
