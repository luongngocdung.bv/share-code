<?php

return [
	'name'   => 'Order Status',
	'namelist'   => 'List Orders',
    'create' => 'New order',
    'edit'   => 'Edit order',
    'form' => [
    	'orderstatus' => 'Orders Status...!',
    	'status' => 'All Status Order',
    ],
];
