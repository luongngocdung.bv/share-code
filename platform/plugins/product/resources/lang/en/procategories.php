<?php

return [
    'create' => 'Create new Pro category',
    'edit' => 'Edit Product category',
    'menu' => 'Product Categories',
    'edit_this_procategory' => 'Edit this Product category',
    'menu_name' => 'Product Categories',
    'none' => 'None',
    'menu_type' => 'Dạng Menu',
    'is_hot' => 'Show icon Hot',
    'is_new' => 'Show icon New',

];
