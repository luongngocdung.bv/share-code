<?php

namespace Botble\Product\Repositories\Interfaces;

use Botble\Support\Repositories\Interfaces\RepositoryInterface;

interface ColorInterface extends RepositoryInterface
{
}
