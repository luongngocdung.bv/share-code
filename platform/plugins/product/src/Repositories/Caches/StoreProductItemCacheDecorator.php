<?php

namespace Botble\Product\Repositories\Caches;

use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Botble\Support\Repositories\Caches\CacheAbstractDecorator;

class StoreProductItemCacheDecorator extends CacheAbstractDecorator implements StoreProductItemInterface
{

}
