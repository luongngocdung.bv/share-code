<?php

namespace Botble\Product\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Product\Repositories\Interfaces\ShippingInterface;

class ShippingCacheDecorator extends CacheAbstractDecorator implements ShippingInterface
{

}
