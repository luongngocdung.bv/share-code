<?php

namespace Botble\Product\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Product\Repositories\Interfaces\ColorInterface;

class ColorCacheDecorator extends CacheAbstractDecorator implements ColorInterface
{

}
