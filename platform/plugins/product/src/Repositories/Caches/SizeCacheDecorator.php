<?php

namespace Botble\Product\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Product\Repositories\Interfaces\SizeInterface;

class SizeCacheDecorator extends CacheAbstractDecorator implements SizeInterface
{

}
