<?php

namespace Botble\Product\Repositories\Caches;

use Botble\Support\Repositories\Caches\CacheAbstractDecorator;
use Botble\Product\Repositories\Interfaces\FeaturesInterface;

class FeaturesCacheDecorator extends CacheAbstractDecorator implements FeaturesInterface
{

}
