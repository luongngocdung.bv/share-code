<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Base\Enums\BaseStatusEnum;
use Botble\Product\Repositories\Interfaces\CartInterface;
use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Eloquent;

class CartRepository extends RepositoriesAbstract implements CartInterface
{
}
