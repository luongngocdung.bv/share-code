<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\SizeInterface;

class SizeRepository extends RepositoriesAbstract implements SizeInterface
{
}
