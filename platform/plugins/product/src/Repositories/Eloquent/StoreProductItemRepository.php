<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;

class StoreProductItemRepository extends RepositoriesAbstract implements StoreProductItemInterface
{

}
