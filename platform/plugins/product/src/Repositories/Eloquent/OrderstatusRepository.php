<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\OrderstatusInterface;

class OrderstatusRepository extends RepositoriesAbstract implements OrderstatusInterface
{
}
