<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\ShippingInterface;

class ShippingRepository extends RepositoriesAbstract implements ShippingInterface
{
}
