<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\StoreProductInterface;

class StoreProductRepository extends RepositoriesAbstract implements StoreProductInterface
{
}
