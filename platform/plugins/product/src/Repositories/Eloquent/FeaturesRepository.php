<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Product\Repositories\Interfaces\FeaturesInterface;
use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;

class FeaturesRepository extends RepositoriesAbstract implements FeaturesInterface
{
}
