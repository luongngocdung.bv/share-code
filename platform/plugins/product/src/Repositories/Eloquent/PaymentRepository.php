<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\PaymentInterface;

class PaymentRepository extends RepositoriesAbstract implements PaymentInterface
{
}
