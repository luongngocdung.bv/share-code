<?php

namespace Botble\Product\Repositories\Eloquent;

use Botble\Support\Repositories\Eloquent\RepositoriesAbstract;
use Botble\Product\Repositories\Interfaces\StoreInterface;

class StoreRepository extends RepositoriesAbstract implements StoreInterface
{
}
