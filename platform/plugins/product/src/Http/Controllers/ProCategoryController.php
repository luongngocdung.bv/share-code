<?php

namespace Botble\Product\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Base\Forms\FormBuilder;
use Botble\Base\Http\Controllers\BaseController;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Product\Forms\ProCategoryForm;
use Botble\Product\Tables\ProCategoryTable;
use Botble\Product\Http\Requests\ProCategoryRequest;
use Botble\Product\Repositories\Interfaces\ProCategoryInterface;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Illuminate\View\View;
use Throwable;

class ProCategoryController extends BaseController
{

    /**
     * @var CategoryInterface
     */
    protected $procategoryRepository;

    /**
     * @param CategoryInterface $categoryRepository
     */
    public function __construct(ProCategoryInterface $procategoryRepository)
    {
        $this->procategoryRepository = $procategoryRepository;
    }

    /**
     * Display all categories
     * @param CategoryTable $dataTable
     * @return Factory|View
     *
     * @throws Throwable
     */
    public function index(ProCategoryTable $dataTable)
    {
        page_title()->setTitle(trans('plugins/product::procategories.menu'));

        return $dataTable->renderTable();
    }

    /**
     * Show create form
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/product::procategories.create'));

        return $formBuilder->create(ProCategoryForm::class)->renderForm();
    }

    /**
     * Insert new Category into database
     *
     * @param CategoryRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function store(ProCategoryRequest $request, BaseHttpResponse $response)
    {
        if ($request->input('is_default')) {
            $this->procategoryRepository->getModel()->where('id', '>', 0)->update(['is_default' => 0]);
        }

        $procategory = $this->procategoryRepository->createOrUpdate(array_merge($request->input(), [
            'author_id' => Auth::user()->getKey(),
        ]));

        event(new CreatedContentEvent(PROCATEGORY_MODULE_SCREEN_NAME, $request, $procategory));

        return $response
            ->setPreviousUrl(route('procategories.index'))
            ->setNextUrl(route('procategories.edit', $procategory->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param Request $request
     * @param int $id
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit(Request $request, $id, FormBuilder $formBuilder)
    {
        $procategory = $this->procategoryRepository->findOrFail($id);

        event(new BeforeEditContentEvent($request, $procategory));

        page_title()->setTitle(trans('plugins/product::procategories.edit') . ' "' . $procategory->name . '"');

        return $formBuilder->create(ProCategoryForm::class, ['model' => $procategory])->renderForm();
    }

    /**
     * @param int $id
     * @param CategoryRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function update($id, ProCategoryRequest $request, BaseHttpResponse $response)
    {
        $procategory = $this->procategoryRepository->findOrFail($id);

        if ($request->input('is_default')) {
            $this->procategoryRepository->getModel()->where('id', '!=', $id)->update(['is_default' => 0]);
        }

        $procategory->fill($request->input());

        $this->procategoryRepository->createOrUpdate($procategory);

        event(new UpdatedContentEvent(PROCATEGORY_MODULE_SCREEN_NAME, $request, $procategory));

        return $response
            ->setPreviousUrl(route('procategories.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $procategory = $this->procategoryRepository->findOrFail($id);

            if (!$procategory->is_default) {
                $this->procategoryRepository->delete($procategory);
                event(new DeletedContentEvent(PROCATEGORY_MODULE_SCREEN_NAME, $request, $procategory));
            }

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $ex) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $procategory = $this->procategoryRepository->findOrFail($id);
            if (!$procategory->is_default) {
                $this->procategoryRepository->delete($procategory);

                event(new DeletedContentEvent(PROCATEGORY_MODULE_SCREEN_NAME, $request, $procategory));
            }
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }

    public function getDuplicate($id, BaseHttpResponse $response)
    {
        $baseProcategory = $this->procategoryRepository->findOrFail($id);

        $procategory = $this->procategoryRepository->createOrUpdate([
            'name'          => $baseProcategory->name . __('(Duplicate)'),
            'description'   => $baseProcategory->description,
            'parent_id'     => $baseProcategory->parent_id,
            'is_hot'        => $baseProcategory->is_hot,
            'is_new'        => $baseProcategory->is_new,
            'icon'          => $baseProcategory->icon,
            'type_menu'     => $baseProcategory->type_menu,
            'is_featured'   => $baseProcategory->is_featured,
            'order'         => $baseProcategory->order,
            'is_default'    => $baseProcategory->is_default,
            'status'        => $baseProcategory->status,
            'author_id'     => $baseProcategory->author_id,
            'created_at'     => $baseProcategory->author_id,
            'updated_at'     => $baseProcategory->author_id,
        ]);

        return $response
            ->setPreviousUrl(route('procategories.edit', $baseProcategory->id))
            ->setNextUrl(route('procategories.edit', $procategory->id))
            ->setMessage(trans('core/acl::permissions.duplicated_success'));
    }
}
