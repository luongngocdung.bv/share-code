<?php

namespace Botble\Product\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Product\Http\Resources\ProCategoryResource;
use Botble\Product\Http\Resources\ListProCategoryResource;
use Botble\Product\Repositories\Interfaces\ProCategoryInterface;
use Botble\Product\Supports\FilterProCategory;
use Botble\Slug\Repositories\Interfaces\SlugInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Botble\Product\Models\ProCategory;
use Botble\Base\Supports\SortItemsWithChildrenHelper;
use Illuminate\Support\Arr;
use Botble\Menu\Models\Menu;
use Botble\Menu\Repositories\Interfaces\MenuInterface;
use Botble\Menu\Models\MenuLocation;
use Botble\Menu\Repositories\Interfaces\MenuLocationInterface;
use Botble\Menu\Models\MenuNode;
use Botble\Menu\Repositories\Interfaces\MenuNodeInterface;
use DB;
class MenuController extends Controller
{
    /**
     * @var CategoryInterface
     */
    protected $procategoryRepository;

    /**
     * @var SlugInterface
     */
    protected $slugRepository;

    /**
     * AuthenticationController constructor.
     *
     * @param CategoryInterface $procategoryRepository
     */
    public function __construct(
        ProCategoryInterface $procategoryRepository, 
        MenuInterface  $menuRepository, 
        MenuLocationInterface $menuLocationRepository, 
        MenuNodeInterface $menuNodeRepository, 
        SlugInterface $slugRepository
    ){
        $this->procategoryRepository = $procategoryRepository;
        $this->menuRepository = $menuRepository;
        $this->menuLocationRepository = $menuLocationRepository;
        $this->menuNodeRepository = $menuNodeRepository;
        $this->slugRepository = $slugRepository;
    }

    /**
     * List categories
     *
     * @group Product
     *
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function getMenuAllProduct(Request $request, BaseHttpResponse $response)
    {
        $data = $this->procategoryRepository
            ->getModel()
            ->where(['status' => BaseStatusEnum::PUBLISHED,])
            ->select(['id', 'name', 'description','parent_id'])
            ->paginate($request->input('per_page', 20));

        return $response
            ->setData(ListProCategoryResource::collection($data))
            ->toApiResponse();

    }

    public function menuOfmenu(BaseHttpResponse $response){
        /*$data = $this->menuRepository
            ->join('menu_locations', 'menu_locations.id', '=', 'menus.id')
            ->getModel()
            
            ->select(['id', 'name', 'slug'])->get();*/

        /*$data = DB::table('menus')
            ->join('menu_locations', 'menus.id', '=', 'menu_locations.menu_id')
            ->join('menu_nodes', 'menus.id', '=', 'menu_nodes.menu_id')
            ->join('slugs', 'menu_nodes.position', '=', 'slugs.reference_id')
            ->join('language_meta', 'slugs.reference_id', '=', 'language_meta.reference_id')
            ->select('menus.id', 'menus.slug', 'menu_locations.location','menu_nodes.title','menu_nodes.url','menu_nodes.position','slugs.key','language_meta.lang_meta_code')

            ->where(['menu_locations.location' => 'menu-vue','language_meta.lang_meta_code'=>'vi'])
            ->get();

         
        return $data;*/
    }

    public function getproductcatevue(Request $request, BaseHttpResponse $response){
        //$data = collect(get_menu_index([]));
        $data = get_menu_index();

        //dd($data);
        return $data;  
    }


    public function getProductCategories(array $select, array $orderBy)
    {
        $data = $this->model->with('slugable')->select($select);
        foreach ($orderBy as $by => $direction) {
            $data = $data->orderBy($by, $direction);
        }

        return $this->applyBeforeExecuteQuery($data)->get();


    }


    //de em debug cho
     public function getAllProCategoriesVue(Request $request, BaseHttpResponse $response)
    {
        
        /*$data = $this->procategoryRepository
            ->getModel()
            ->where(['procategories.parent_id' => 0])  // mình đang lấy thằng cha ra trước à Tẹo nữa foreach tại đây lấy các thằng con thì ra ngoài nó sẽ nhận như ý tôi chứ. dc
            ->select(['id', 'name', 'description'])
            ->paginate($request->input('per_page', 10));
        nsoi đi 

        return $response
            ->setData(ListProCategoryResource::collection($data))
            ->toApiResponse();*/

        $data = [
            'data' => [],
            'status' => 400
        ];

        $datas = $this->procategoryRepository->select(['id', 'name', 'description'])->get();
        $datatest = [];
        foreach($datas as $key => $value){
            $datatest[] = $value->name;
        }

        $data  = [
            'data' => $datatest,
            'status' => 200
        ]; 

        // alo còn đó ko . 
        // lấy full rồi. thằng vue nó hiểu có thằng con rồi. thì foreach tại vue phải được chứ nhỉ . 
        // lúc này ông lấy full thằng cha rồi đúng ko,  đây vẫn lấy các thằng con . 
        // thì vue nó chỉ hiểu là ông đang lấy ra cùng 

        return $data;
    }

    public function getProCateVue(array $select, array $orderBy)
    {
        $data = $this->model->with('slugable')->select($select);
        foreach ($orderBy as $by => $direction) {
            $data = $data->orderBy($by, $direction);
        }

        return $this->applyBeforeExecuteQuery($data)->get();
    }

    

    public function get_procatevue(array $args = [])
    {
        $indent = Arr::get($args, 'indent', '——');

        $repo = app(ProCategoryInterface::class);

        $procategories = $repo->getProCateVue(Arr::get($args, 'select', ['*']), [
            'procategories.is_default' => 'DESC',
            'procategories.order'      => 'ASC',
        ]);

        $procategories = sort_item_with_children($procategories);

        foreach ($procategories as $procategory) {
            $indentText = '';
            $depth = (int)$procategory->depth;
            for ($i = 0; $i < $depth; $i++) {
                $indentText .= $indent;
            }
            $procategory->indent_text = $indentText;
        }

        return $procategories;
    }

    /**
     * Filters categories
     *
     * @group Product
     *
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function getFilters(Request $request, BaseHttpResponse $response)
    {
        $filters = FilterProCategory::setFilters($request->input());
        $data = $this->procategoryRepository->getFilters($filters);
        return $response
            ->setData(ProCategoryResource::collection($data))
            ->toApiResponse();
    }

    /**
     * Get category by slug
     *
     * @group Product
     * @queryParam slug Find by slug of category.
     * @param string $slug
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|JsonResponse
     */
    public function findBySlug(string $slug, BaseHttpResponse $response)
    {
        $slug = $this->slugRepository->getFirstBy(['key' => $slug, 'reference_type' => ProCategory::class]);
        if (!$slug) {
            return $response->setError()->setCode(404)->setMessage('Not found');
        }

        $procategory = $this->procategoryRepository->getProCategoryById($slug->reference_id);

        if (!$procategory) {
            return $response->setError()->setCode(404)->setMessage('Not found');
        }

        return $response
            ->setData(new ListProCategoryResource($procategory))
            ->toApiResponse();
    }
}
