<?php

namespace Botble\Product\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Base\Forms\FormBuilder;
use Botble\Base\Http\Controllers\BaseController;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Base\Traits\HasDeleteManyItemsTrait;
use Botble\Product\Forms\ProductForm;
use Botble\Product\Http\Requests\ProductRequest;
use Botble\Product\Models\Product;
use Botble\Product\Repositories\Interfaces\ProCategoryInterface;
use Botble\Product\Repositories\Interfaces\FeaturesInterface;
use Botble\Product\Repositories\Interfaces\ColorInterface;
use Botble\Product\Repositories\Interfaces\SizeInterface;
use Botble\Product\Repositories\Interfaces\ProductInterface;
use Botble\Product\Tables\ProductTable;
use Botble\Product\Repositories\Interfaces\ProTagInterface;
use Botble\Product\Services\StoreProCategoryService;
use Botble\Product\Services\StoreProTagService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Illuminate\View\View;
use Throwable;

class ProductController extends BaseController
{

    use HasDeleteManyItemsTrait;

    /**
     * @var ProductInterface
     */
    protected $productRepository;


    protected $featuresRepository;
    /**
     * @var TagInterface
     */
    protected $tagRepository;

    /**
     * @var CategoryInterface
     */
    protected $procategoryRepository;

    /**
     * @param ProductInterface $productRepository
     * @param TagInterface $tagRepository
     * @param CategoryInterface $categoryRepository
     */
    public function __construct(
        ProductInterface $productRepository,
        ProTagInterface $protagRepository,
        ProCategoryInterface $procategoryRepository,
        FeaturesInterface $featuresRepository
    ) {
        $this->productRepository = $productRepository;
        $this->protagRepository = $protagRepository;
        $this->procategoryRepository = $procategoryRepository;
        $this->featuresRepository = $featuresRepository;
    }

    /**
     * @param ProductTable $dataTable
     * @return Factory|View
     * @throws Throwable
     */
    public function index(ProductTable $dataTable)
    {
        page_title()->setTitle(trans('plugins/product::products.menu_name'));

        return $dataTable->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/product::products.create'));

        return $formBuilder->create(ProductForm::class)->renderForm();
    }

    /**
     * @param ProductRequest $request
     * @param StoreTagService $tagService
     * @param StoreCategoryService $categoryService
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function store(
        ProductRequest $request,
        StoreProTagService $protagService,
        StoreProCategoryService $procategoryService,
        BaseHttpResponse $response
    ) {


        /**
         * @var Product $product
         */
        $request->merge(['images' => json_encode($request->input('images', []))]);
        
        $request->merge(['option1' => json_encode($request->input('option1', []))]);

        //$request->merge(['sizes' => json_encode($request->input('sizes', []))]);

        $product = $this->productRepository->createOrUpdate(array_merge($request->input(), [
            'author_id' => Auth::user()->getKey(),
        ]));

        if ($product) {
            $product->features()->sync($request->input('features', []));
            $product->color()->sync($request->input('colors', []));
            $product->size()->sync($request->input('sizes', []));
        }

        event(new CreatedContentEvent(PRODUCT_MODULE_SCREEN_NAME, $request, $product));



        $protagService->execute($request, $product);

        $procategoryService->execute($request, $product);

        return $response
            ->setPreviousUrl(route('products.index'))
            ->setNextUrl(route('products.edit', $product->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * @param int $id
     * @param FormBuilder $formBuilder
     * @param Request $request
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $product = $this->productRepository->findOrFail($id);

        event(new BeforeEditContentEvent($request, $product));

        page_title()->setTitle(trans('plugins/product::products.edit') . ' "' . $product->name . '"');

        return $formBuilder->create(ProductForm::class, ['model' => $product])->renderForm();
    }

    /**
     * @param int $id
     * @param ProductRequest $request
     * @param StoreTagService $tagService
     * @param StoreCategoryService $categoryService
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function update(
        $id,
        ProductRequest $request,
        StoreProTagService $protagService,
        StoreProCategoryService $procategoryService,
        BaseHttpResponse $response
    ) {

        $product = $this->productRepository->findOrFail($id);

        $request->merge(['images' => json_encode($request->input('images', []))]);

        $request->merge(['option1' => json_encode($request->input('option1', []))]);

        //$request->merge(['sizes' => json_encode($request->input('sizes', []))]);

        $product->fill($request->input());

        $this->productRepository->createOrUpdate($product);

        $product->features()->sync($request->input('features', []));
        $product->color()->sync($request->input('colors', []));
        $product->size()->sync($request->input('sizes', []));

        event(new UpdatedContentEvent(PRODUCT_MODULE_SCREEN_NAME, $request, $product));

        $protagService->execute($request, $product);

        $procategoryService->execute($request, $product);

        return $response
            ->setPreviousUrl(route('products.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));

    }

    /**
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        try {
            $product = $this->productRepository->findOrFail($id);
            $this->productRepository->delete($product);

            event(new DeletedContentEvent(PRODUCT_MODULE_SCREEN_NAME, $request, $product));

            return $response
                ->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        return $this->executeDeleteItems($request, $response, $this->productRepository, PRODUCT_MODULE_SCREEN_NAME);
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Throwable
     */
    public function getWidgetRecentProducts(Request $request, BaseHttpResponse $response)
    {
        $limit = $request->input('paginate', 10);
        $products = $this->productRepository->getModel()
            ->orderBy('products.created_at', 'desc')
            ->paginate($limit);

        return $response
            ->setData(view('plugins/product::products.widgets.products', compact('products', 'limit'))->render());
    }

    public function getDuplicate($id, BaseHttpResponse $response)
    {
        $baseProduct = $this->productRepository->findOrFail($id);

        $product = $this->productRepository->createOrUpdate([
            
            //'slug'        => $this->productRepository->createSlug($baseProduct->slug, 0),


            'name'          => $baseProduct->name . __('(Duplicate)'),
            'description'   => $baseProduct->description,
            'content'       => $baseProduct->content,
            'imagedl'       => $baseProduct->imagedl,
            'video'         => $baseProduct->video,
            'pricecost'     => $baseProduct->pricecost,
            'pricesell'     => $baseProduct->pricesell,
            'pricesale'     => $baseProduct->pricesale,
            'pricetime'     => $baseProduct->pricetime,
            'amound'        => $baseProduct->amound,
            'currency_id'   => $baseProduct->currency_id,
            'price_sale_start'  => $baseProduct->price_sale_start,
            'price_sale_end'    => $baseProduct->price_sale_end,
            'is_featured'   => $baseProduct->is_featured,
            'format_type'   => $baseProduct->format_type,
            'status'        => $baseProduct->status,
            'author_id'     => $baseProduct->author_id,
            'author_type'   => $baseProduct->author_type,
        ]);

        return $response
            ->setPreviousUrl(route('products.edit', $baseProduct->id))
            ->setNextUrl(route('products.edit', $product->id))
            ->setMessage(trans('core/acl::permissions.duplicated_success'));
    }

}
