<?php

namespace Botble\Product\Http\Controllers;

use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Forms\FormBuilder;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Product\Forms\StoreProductItemForm;
use Botble\Product\Http\Requests\StoreProductItemRequest;
use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Product\Tables\StoreProductItemTable;

class StoreProductItemController extends BaseController
{
    /**
     * @var storeProductItemInterface
     */
    protected $storeProductItemRepository;

    /**
     * storeProductItemController constructor.
     * @param storeProductItemInterface $storeProductItemRepository
     */
    public function __construct(StoreProductItemInterface $storeProductItemRepository)
    {
        $this->storeProductItemRepository = $storeProductItemRepository;
    }

    /**
     * Display all store-product
     * @param storeProductItemTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @throws \Throwable
     */
    public function index(StoreProductItemTable $dataTable)
    {
        return $dataTable->renderTable();
    }

    /**
     * Show create form
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        return $formBuilder->create(StoreProductItemForm::class)
            ->setTitle(__('Create new store product'))
            ->setUseInlineJs(true)
            ->renderForm();
    }

    /**
     * Insert new storeProductItem into database
     *
     * @param storeProductItemRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function store(StoreProductItemRequest $request, BaseHttpResponse $response)
    {
        $storeProduct = $this->storeProductItemRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(STORE_PRODUCT_ITEM_MODULE_SCREEN_NAME, $request, $storeProduct));

        return $response->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder)
    {
        $storeProduct = $this->storeProductItemRepository->findOrFail($id);

        return $formBuilder->create(StoreProductItemForm::class, ['model' => $storeProduct])
            ->setTitle(__('Edit product #:id', ['id' => $storeProduct->id]))
            ->setUseInlineJs(true)
            ->renderForm();
    }

    /**
     * @param $id
     * @param storeProductItemRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function update($id, StoreProductItemRequest $request, BaseHttpResponse $response)
    {
        $storeProduct = $this->storeProductItemRepository->findOrFail($id);
        $storeProduct->fill($request->input());

        $this->storeProductItemRepository->createOrUpdate($storeProduct);

        event(new UpdatedContentEvent(STORE_PRODUCT_ITEM_MODULE_SCREEN_NAME, $request, $storeProduct));

        return $response->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function destroy($id)
    {
        $product = $this->storeProductItemRepository->findOrFail($id);

        return view('plugins/product::partials.delete', compact('product'))->render();
    }

    /**
     * @param Request $request
     * @param $id
     * @param BaseHttpResponse $response
     * @return array|BaseHttpResponse
     */
    public function postDelete(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $storeProduct = $this->storeProductItemRepository->findOrFail($id);
            $this->storeProductItemRepository->delete($storeProduct);

            event(new DeletedContentEvent(STORE_PRODUCT_ITEM_MODULE_SCREEN_NAME, $request, $storeProduct));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }
}
