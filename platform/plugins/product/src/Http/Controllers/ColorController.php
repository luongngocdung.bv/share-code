<?php

namespace Botble\Product\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Product\Http\Requests\ColorRequest;
use Botble\Product\Repositories\Interfaces\ColorInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Product\Tables\ColorTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Product\Forms\ColorForm;
use Botble\Base\Forms\FormBuilder;

class ColorController extends BaseController
{
    /**
     * @var ColorInterface
     */
    protected $colorRepository;

    /**
     * @param ColorInterface $colorRepository
     */
    public function __construct(ColorInterface $colorRepository)
    {
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param ColorTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(ColorTable $table)
    {
        page_title()->setTitle(trans('plugins/color::color.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/color::color.create'));

        return $formBuilder->create(ColorForm::class)->renderForm();
    }

    /**
     * @param ColorRequest $request
     * @return BaseHttpResponse
     */
    public function store(ColorRequest $request, BaseHttpResponse $response)
    {
        $color = $this->colorRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(COLOR_MODULE_SCREEN_NAME, $request, $color));

        return $response
            ->setPreviousUrl(route('color.index'))
            ->setNextUrl(route('color.edit', $color->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $color = $this->colorRepository->findOrFail($id);

        event(new BeforeEditContentEvent($request, $color));

        page_title()->setTitle(trans('plugins/color::color.edit') . ' "' . $color->name . '"');

        return $formBuilder->create(ColorForm::class, ['model' => $color])->renderForm();
    }

    /**
     * @param $id
     * @param ColorRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, ColorRequest $request, BaseHttpResponse $response)
    {
        $color = $this->colorRepository->findOrFail($id);

        $color->fill($request->input());

        $this->colorRepository->createOrUpdate($color);

        event(new UpdatedContentEvent(COLOR_MODULE_SCREEN_NAME, $request, $color));

        return $response
            ->setPreviousUrl(route('color.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $color = $this->colorRepository->findOrFail($id);

            $this->colorRepository->delete($color);

            event(new DeletedContentEvent(COLOR_MODULE_SCREEN_NAME, $request, $color));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $color = $this->colorRepository->findOrFail($id);
            $this->colorRepository->delete($color);
            event(new DeletedContentEvent(COLOR_MODULE_SCREEN_NAME, $request, $color));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
