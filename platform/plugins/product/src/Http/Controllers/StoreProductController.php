<?php

namespace Botble\Product\Http\Controllers;

use Assets;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Forms\FormBuilder;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Base\Traits\HasDeleteManyItemsTrait;
use Botble\Product\Forms\StoreProductForm;
use Botble\Product\Http\Requests\StoreProductRequest;
use Botble\Product\Repositories\Interfaces\StoreProductInterface;
use Botble\Base\Http\Controllers\BaseController;
use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Illuminate\Http\Request;
use Exception;
use Botble\Product\Tables\StoreProductTable;

class StoreProductController extends BaseController
{
    use HasDeleteManyItemsTrait;

    /**
     * @var storeProductInterface
     */
    protected $storeProductRepository;

    /**
     * @var storeProductItemInterface
     */
    protected $storeProductItemRepository;

    /**
     * storeProductController constructor.
     * @param storeProductInterface $storeProductRepository
     * @param storeProductItemInterface $storeProductItemRepository
     */
    public function __construct(
        StoreProductInterface $storeProductRepository,
        StoreProductItemInterface $storeProductItemRepository
    )
    {
        $this->storeProductRepository = $storeProductRepository;
        $this->storeProductItemRepository = $storeProductItemRepository;
    }

    /**
     * Display all store-product
     * @param storeProductTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @throws \Throwable
     */
    public function index(StoreProductTable $dataTable)
    {
        page_title()->setTitle(trans('plugins/product::store-product.menu'));

        return $dataTable->renderTable();
    }

    /**
     * Show create form
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/product::store-product.create'));

        return $formBuilder
            ->create(StoreProductForm::class)
            ->removeMetaBox('product-items')
            ->renderForm();
    }

    /**
     * Insert new storeProduct into database
     *
     * @param storeProductRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function store(StoreProductRequest $request, BaseHttpResponse $response)
    {
        $store_product = $this->storeProductRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(STORE_PRODUCT_MODULE_SCREEN_NAME, $request, $store_product));

        return $response
            ->setPreviousUrl(route('store-product.index'))
            ->setNextUrl(route('store-product.edit', $store_product->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * Show edit form
     *
     * @param $id
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder)
    {
        Assets::addStylesDirectly(['vendor/core/plugins/product/css/store-product-admin.css'])
            ->addScripts(['blockui', 'sortable'])
            ->addScriptsDirectly(['vendor/core/plugins/product/js/store-product-admin.js']);

        $storeProduct = $this->storeProductRepository->findOrFail($id);
        page_title()->setTitle(trans('plugins/product::store-product.edit') . ' "' . $storeProduct->name . '"');

        return $formBuilder
            ->create(StoreProductForm::class, ['model' => $storeProduct])
            ->renderForm();
    }

    /**
     * @param $id
     * @param storeProductRequest $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function update($id, StoreProductRequest $request, BaseHttpResponse $response)
    {
        $store_product = $this->storeProductRepository->findOrFail($id);
        $store_product->fill($request->input());

        $this->storeProductRepository->createOrUpdate($store_product);

        event(new UpdatedContentEvent(STORE_PRODUCT_MODULE_SCREEN_NAME, $request, $store_product));

        return $response
            ->setPreviousUrl(route('store-product.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param Request $request
     * @param $id
     * @param BaseHttpResponse $response
     * @return array|BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $storeProduct = $this->storeProductRepository->findOrFail($id);
            $this->storeProductRepository->delete($storeProduct);

            event(new DeletedContentEvent(STORE_PRODUCT_MODULE_SCREEN_NAME, $request, $storeProduct));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return array|BaseHttpResponse|\Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        return $this->executeDeleteItems($request, $response, $this->storeProductRepository, STORE_PRODUCT_MODULE_SCREEN_NAME);
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function postSorting(Request $request, BaseHttpResponse $response)
    {
        foreach ($request->input('items', []) as $key => $id) {
            $this->storeProductItemRepository->createOrUpdate(['order' => ($key + 1)], ['id' => $id]);
        }

        return $response->setMessage(__('Updated product position successfully!'));
    }
}
