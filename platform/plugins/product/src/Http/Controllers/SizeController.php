<?php

namespace Botble\Product\Http\Controllers;

use Botble\Base\Events\BeforeEditContentEvent;
use Botble\Product\Http\Requests\SizeRequest;
use Botble\Product\Repositories\Interfaces\SizeInterface;
use Botble\Base\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Exception;
use Botble\Product\Tables\SizeTable;
use Botble\Base\Events\CreatedContentEvent;
use Botble\Base\Events\DeletedContentEvent;
use Botble\Base\Events\UpdatedContentEvent;
use Botble\Base\Http\Responses\BaseHttpResponse;
use Botble\Product\Forms\SizeForm;
use Botble\Base\Forms\FormBuilder;

class SizeController extends BaseController
{
    /**
     * @var SizeInterface
     */
    protected $sizeRepository;

    /**
     * @param SizeInterface $sizeRepository
     */
    public function __construct(SizeInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }

    /**
     * @param SizeTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index(SizeTable $table)
    {
        page_title()->setTitle(trans('plugins/size::size.name'));

        return $table->renderTable();
    }

    /**
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function create(FormBuilder $formBuilder)
    {
        page_title()->setTitle(trans('plugins/size::size.create'));

        return $formBuilder->create(SizeForm::class)->renderForm();
    }

    /**
     * @param SizeRequest $request
     * @return BaseHttpResponse
     */
    public function store(SizeRequest $request, BaseHttpResponse $response)
    {
        $size = $this->sizeRepository->createOrUpdate($request->input());

        event(new CreatedContentEvent(SIZE_MODULE_SCREEN_NAME, $request, $size));

        return $response
            ->setPreviousUrl(route('size.index'))
            ->setNextUrl(route('size.edit', $size->id))
            ->setMessage(trans('core/base::notices.create_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @param FormBuilder $formBuilder
     * @return string
     */
    public function edit($id, FormBuilder $formBuilder, Request $request)
    {
        $size = $this->sizeRepository->findOrFail($id);

        event(new BeforeEditContentEvent($request, $size));

        page_title()->setTitle(trans('plugins/size::size.edit') . ' "' . $size->name . '"');

        return $formBuilder->create(SizeForm::class, ['model' => $size])->renderForm();
    }

    /**
     * @param $id
     * @param SizeRequest $request
     * @return BaseHttpResponse
     */
    public function update($id, SizeRequest $request, BaseHttpResponse $response)
    {
        $size = $this->sizeRepository->findOrFail($id);

        $size->fill($request->input());

        $this->sizeRepository->createOrUpdate($size);

        event(new UpdatedContentEvent(SIZE_MODULE_SCREEN_NAME, $request, $size));

        return $response
            ->setPreviousUrl(route('size.index'))
            ->setMessage(trans('core/base::notices.update_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy(Request $request, $id, BaseHttpResponse $response)
    {
        try {
            $size = $this->sizeRepository->findOrFail($id);

            $this->sizeRepository->delete($size);

            event(new DeletedContentEvent(SIZE_MODULE_SCREEN_NAME, $request, $size));

            return $response->setMessage(trans('core/base::notices.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.cannot_delete'));
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('core/base::notices.no_select'));
        }

        foreach ($ids as $id) {
            $size = $this->sizeRepository->findOrFail($id);
            $this->sizeRepository->delete($size);
            event(new DeletedContentEvent(SIZE_MODULE_SCREEN_NAME, $request, $size));
        }

        return $response->setMessage(trans('core/base::notices.delete_success_message'));
    }
}
