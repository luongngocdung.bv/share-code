<?php

namespace Botble\Product\Http\Requests;

use Botble\Support\Http\Requests\Request;

class StoreProductItemRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_product_id' => 'required',
            'title'            => 'required',
            //'image'            => 'required',
            'order'            => 'integer|min:0',
        ];
    }
}
