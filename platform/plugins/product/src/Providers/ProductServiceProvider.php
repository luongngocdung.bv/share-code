<?php

namespace Botble\Product\Providers;

use Botble\Shortcode\View\View;
use Illuminate\Routing\Events\RouteMatched;
use Botble\Base\Supports\Helper;
use Botble\Base\Traits\LoadAndPublishDataTrait;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Application;

use Botble\Product\Models\Product;
use Botble\Product\Repositories\Caches\ProductCacheDecorator;
use Botble\Product\Repositories\Eloquent\ProductRepository;
use Botble\Product\Repositories\Interfaces\ProductInterface;

use Botble\Product\Models\ProCategory;
use Botble\Product\Repositories\Caches\ProCategoryCacheDecorator;
use Botble\Product\Repositories\Eloquent\ProCategoryRepository;
use Botble\Product\Repositories\Interfaces\ProCategoryInterface;

use Botble\Product\Models\ProTag;
use Botble\Product\Repositories\Caches\ProTagCacheDecorator;
use Botble\Product\Repositories\Eloquent\ProTagRepository;
use Botble\Product\Repositories\Interfaces\ProTagInterface;

use Botble\Product\Models\Features;
use Botble\Product\Repositories\Caches\FeaturesCacheDecorator;
use Botble\Product\Repositories\Eloquent\FeaturesRepository;
use Botble\Product\Repositories\Interfaces\FeaturesInterface;

use Botble\Product\Models\Color;
use Botble\Product\Repositories\Caches\ColorCacheDecorator;
use Botble\Product\Repositories\Eloquent\ColorRepository;
use Botble\Product\Repositories\Interfaces\ColorInterface;

use Botble\Product\Models\Size;
use Botble\Product\Repositories\Caches\SizeCacheDecorator;
use Botble\Product\Repositories\Eloquent\SizeRepository;
use Botble\Product\Repositories\Interfaces\SizeInterface;

use Botble\Product\Models\Store;
use Botble\Product\Repositories\Caches\StoreCacheDecorator;
use Botble\Product\Repositories\Eloquent\StoreRepository;
use Botble\Product\Repositories\Interfaces\StoreInterface;

use Botble\Product\Models\StoreProduct;
use Botble\Product\Repositories\Caches\StoreProductCacheDecorator;
use Botble\Product\Repositories\Eloquent\StoreProductRepository;
use Botble\Product\Repositories\Interfaces\StoreProductInterface;

use Botble\Product\Models\StoreProductItem;
use Botble\Product\Repositories\Caches\StoreProductItemCacheDecorator;
use Botble\Product\Repositories\Eloquent\StoreProductItemRepository;
use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;

use Botble\Product\Models\Cart;
use Botble\Product\Repositories\Caches\CartCacheDecorator;
use Botble\Product\Repositories\Eloquent\CartRepository;
use Botble\Product\Repositories\Interfaces\CartInterface;

use Botble\Product\Models\Orderstatus;
use Botble\Product\Repositories\Caches\OrderstatusCacheDecorator;
use Botble\Product\Repositories\Eloquent\OrderstatusRepository;
use Botble\Product\Repositories\Interfaces\OrderstatusInterface;

use Botble\Product\Models\Payment;
use Botble\Product\Repositories\Caches\PaymentCacheDecorator;
use Botble\Product\Repositories\Eloquent\PaymentRepository;
use Botble\Product\Repositories\Interfaces\PaymentInterface;

use Botble\Product\Models\Shipping;
use Botble\Product\Repositories\Caches\ShippingCacheDecorator;
use Botble\Product\Repositories\Eloquent\ShippingRepository;
use Botble\Product\Repositories\Interfaces\ShippingInterface;

use Botble\Product\Models\Currency;
use Botble\Product\Repositories\Interfaces\CurrencyInterface;
use Botble\Product\Repositories\Caches\CurrencyCacheDecorator;
use Botble\Product\Repositories\Eloquent\CurrencyRepository;

use MailVariable;
use Event;
use Language;
use SeoHelper;
use SlugHelper;




class ProductServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function register()
    {
        $this->app->bind(ProductInterface::class, function () {
            return new ProductCacheDecorator(new ProductRepository(new Product));
        });

        $this->app->bind(ProCategoryInterface::class, function () {
            return new ProCategoryCacheDecorator(new ProCategoryRepository(new ProCategory));
        });

        $this->app->bind(ProTagInterface::class, function () {
            return new ProTagCacheDecorator(new ProTagRepository(new ProTag));
        });

        $this->app->bind(FeaturesInterface::class, function () {
            return new FeaturesCacheDecorator(new FeaturesRepository(new Features));
        });

        $this->app->bind(ColorInterface::class, function () {
            return new ColorCacheDecorator(new ColorRepository(new Color));
        });

        $this->app->bind(SizeInterface::class, function () {
            return new SizeCacheDecorator(new SizeRepository(new Size));
        });

        $this->app->bind(StoreInterface::class, function () {
            return new StoreCacheDecorator(new StoreRepository(new Store));
        });

        $this->app->bind(StoreProductInterface::class, function () {
            return new StoreProductCacheDecorator(new StoreProductRepository(new StoreProduct));
        });

        $this->app->bind(StoreProductItemInterface::class, function () {
            return new StoreProductItemCacheDecorator(new StoreProductItemRepository(new StoreProductItem));
        });

        $this->app->bind(CartInterface::class, function () {
            return new CartCacheDecorator(new CartRepository(new Cart));
        });

        $this->app->bind(OrderstatusInterface::class, function () {
            return new OrderstatusCacheDecorator(new OrderstatusRepository(new Orderstatus));
        });

        $this->app->bind(PaymentInterface::class, function () {
            return new PaymentCacheDecorator(new PaymentRepository(new Payment));
        });

        $this->app->bind(ShippingInterface::class, function () {
            return new ShippingCacheDecorator(new ShippingRepository(new Shipping));
        });

        $this->app->bind(CurrencyInterface::class, function () {
            return new CurrencyCacheDecorator(new CurrencyRepository(new Currency));
        });





        Helper::autoload(__DIR__ . '/../../helpers');
    }

    public function boot()
    {
        $this->setNamespace('plugins/product')
            ->loadAndPublishConfigurations(['permissions'])
            ->loadAndPublishViews()
            ->loadAndPublishTranslations()
            ->loadRoutes(['web', 'api'])
            ->loadMigrations()
            ->publishAssets();

        $this->app->register(HookServiceProvider::class);
        $this->app->register(EventServiceProvider::class);

        Event::listen(RouteMatched::class, function () {
            dashboard_menu()
                ->registerItem([
                    'id'          => 'cms-plugins-product',
                    'priority'    => 3,
                    'parent_id'   => null,
                    'name'        => 'plugins/product::base.menu_name',
                    'icon'        => 'fas fa-store',
                    'url'         => route('products.index'),
                    'permissions' => ['products.index'],
                ])
                // Product
                ->registerItem([
                    'id'          => 'cms-plugins-product-product',
                    'priority'    => 2,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::products.menu_name',
                    'icon'        => null,
                    'url'         => route('products.index'),
                    'permissions' => ['products.index'],
                ])
                
                // Category
                ->registerItem([
                    'id'          => 'cms-plugins-product-procategories',
                    'priority'    => 1,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::procategories.menu_name',
                    'icon'        => null,
                    'url'         => route('procategories.index'),
                    'permissions' => ['procategories.index'],
                ])
                // Store Product
                ->registerItem([
                    'id'          => 'cms-plugins-store-product',
                    'priority'    => 2,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::store-product.menu',
                    'icon'        => null,
                    'url'         => route('store-product.index'),
                    'permissions' => ['store-product.index'],
                ])
                // Features
                ->registerItem([
                    'id'          => 'cms-plugins-product-features',
                    'priority'    => 5,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::features.name',
                    'icon'        => null,
                    'url'         => route('features.index'),
                    'permissions' => ['features.index'],
                ])
                // Color
                ->registerItem([
                    'id'          => 'cms-plugins-product-color',
                    'priority'    => 6,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::color.name',
                    'icon'        => null,
                    'url'         => route('color.index'),
                    'permissions' => ['color.index'],
                ])
                // Size
                ->registerItem([
                    'id'          => 'cms-plugins-product-size',
                    'priority'    => 7,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::size.name',
                    'icon'        => null,
                    'url'         => route('size.index'),
                    'permissions' => ['size.index'],
                ])
                // Store
                /*->registerItem([
                    'id'          => 'cms-plugins-product-store',
                    'priority'    => 8,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::store.name',
                    'icon'        => null,
                    'url'         => route('store.index'),
                    'permissions' => ['store.index'],
                ])*/
                // Tags Product
                ->registerItem([
                    'id'          => 'cms-plugins-product-tag',
                    'priority'    => 9,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::protags.models',
                    'icon'        => null,
                    'url'         => route('protags.index'),
                    'permissions' => ['protags.index'],
                ])
                
                // Setting $$
                ->registerItem([
                    'id'          => 'cms-plugins-real-estate-settings',
                    'priority'    => 999,
                    'parent_id'   => 'cms-plugins-product',
                    'name'        => 'plugins/product::real-estate.settings',
                    'icon'        => null,
                    'url'         => route('real-estate.settings'),
                    'permissions' => ['real-estate.settings'],
                ])
                //CART
                ->registerItem([
                    'id'          => 'cms-plugins-cart',
                    'priority'    => 4,
                    'parent_id'   => null,
                    'name'        => 'plugins/product::cart.name',
                    'icon'        => 'fas fa-cart-arrow-down',
                    'url'         => route('cart.index'),
                    'permissions' => ['cart.index'],
                ])
                // Product CART
                ->registerItem([
                    'id'          => 'cms-plugins-product-cart',
                    'priority'    => 1,
                    'parent_id'   => 'cms-plugins-cart',
                    'name'        => 'plugins/product::cart.namelist',
                    'icon'        => null,
                    'url'         => route('cart.index'),
                    'permissions' => ['cart.index'],
                ])
                // orderstatus CART
                ->registerItem([
                    'id'          => 'cms-plugins-orderstatus-cart',
                    'priority'    => 2,
                    'parent_id'   => 'cms-plugins-cart',
                    'name'        => 'plugins/product::orderstatus.name',
                    'icon'        => null,
                    'url'         => route('orderstatus.index'),
                    'permissions' => ['orderstatus.index'],
                ])
                // payment CART
                ->registerItem([
                    'id'          => 'cms-plugins-payment-cart',
                    'priority'    => 3,
                    'parent_id'   => 'cms-plugins-cart',
                    'name'        => 'plugins/product::payment.name',
                    'icon'        => null,
                    'url'         => route('payment.index'),
                    'permissions' => ['payment.index'],
                ])
                // shipping CART
                ->registerItem([
                    'id'          => 'cms-plugins-shipping-cart',
                    'priority'    => 4,
                    'parent_id'   => 'cms-plugins-cart',
                    'name'        => 'plugins/product::shipping.name',
                    'icon'        => null,
                    'url'         => route('shipping.index'),
                    'permissions' => ['shipping.index'],
                ]);
        });

        $this->app->booted(function () {
            $models = [Product::class, ProCategory::class,ProTag::class,Features::class, Color::class,Size::class];

            if (defined('LANGUAGE_MODULE_SCREEN_NAME')) {
                Language::registerModule($models);
                SlugHelper::registerModule($models);
            }
            SlugHelper::registerModule($models);
            SlugHelper::setPrefix(ProTag::class, 'protag');
            SeoHelper::registerModule($models);
            
        });


        if (function_exists('shortcode')) {
            view()->composer([
                'plugins/product::themes.product',
                'plugins/product::themes.procategory',
        
                'plugins/product::themes.protag',
            ], function (View $view) {
                $view->withShortcodes();
            });
        }
    }
}
