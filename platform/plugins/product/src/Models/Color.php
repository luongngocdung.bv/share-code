<?php

namespace Botble\Product\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;

class Color extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'colors';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'status',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_colors', 'colors_id', 'product_id');
    }

    
    /*public function parent()
    {
        return $this->belongsTo(Color::class, 'parent_id')->withDefault();
    }

   
    public function children()
    {
        return $this->hasMany(Color::class, 'parent_id');
    }

    protected static function boot()
    {
        parent::boot();

        self::deleting(function (Color $colors) {
            $colors->products()->detach();
        });
    }*/
}
