<?php

namespace Botble\Product\Models;

use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Traits\EnumCastable;
use Botble\Base\Models\BaseModel;

class StoreProduct extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_products';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'key',
        'description',
        'status',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productItems()
    {
        return $this->hasMany(StoreProductItem::class)->orderBy('store_product_items.order', 'asc');
    }

    protected static function boot()
    {
        parent::boot();

        self::deleting(function (StoreProduct $product) {
            StoreProductItem::where('store_product_id', $product->id)->delete();
        });
    }
}
