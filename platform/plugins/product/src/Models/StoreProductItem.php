<?php

namespace Botble\Product\Models;

use Botble\Base\Models\BaseModel;

class StoreProductItem extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_product_items';

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'link',
        'image',
        'order',
        'store_product_id',
    ];
}
