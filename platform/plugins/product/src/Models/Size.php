<?php

namespace Botble\Product\Models;

use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Models\BaseModel;

class Size extends BaseModel
{
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sizes';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'status',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];


    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_sizes', 'sizes_id', 'product_id');
    }

}
