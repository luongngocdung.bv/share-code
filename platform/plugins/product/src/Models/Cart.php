<?php

namespace Botble\Product\Models;

use Botble\ACL\Models\User;
use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Revision\RevisionableTrait;
use Botble\Slug\Traits\SlugTrait;
use Botble\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use DB;

class Cart extends BaseModel
{
    use RevisionableTrait;
    use EnumCastable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'carts';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'author_id',
        'author_type',
        'status',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    /*Order status*/
    public function orderstatus(): BelongsToMany
    {
        return $this->belongsToMany(Orderstatus::class, 'product_orderstatuses', 'cart_id', 'orderstatuses_id');
    }

    /*Payment status*/
    
    public function payment(): BelongsToMany
    {
        return $this->belongsToMany(Payment::class, 'product_payments', 'cart_id', 'payments_id');
    }

    /*shippings status*/
    
    public function shipping(): BelongsToMany
    {
        return $this->belongsToMany(Shipping::class, 'product_shippings', 'cart_id', 'shippings_id');
    }

    /**
     * @return MorphTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Cart $cart) {
           
          /*  $cart->orderstatus()->detach();
            $cart->payment()->detach();
            $cart->shipping()->detach();*/
           
        });
    }

     public static function insertData($data){

        //$value=DB::table('product_carts')->where('username', $data['username'])->get();
            //if($value->count() == 0){
                //$insertid = DB::table('product_carts')->insertGetId($data);
                return $data;
            //}else{
               //return 0;
            //}
    }
}


class Product_Carts extends BaseModel
{
    use RevisionableTrait;
    use EnumCastable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_carts';

    /**
     * @var array
     */
    protected $fillable = [
        'carts_id',
        'carts_size',
        'carts_amound',
        'product_id',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

}

