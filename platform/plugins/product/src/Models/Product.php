<?php

namespace Botble\Product\Models;

use Botble\ACL\Models\User;
use Botble\Base\Traits\EnumCastable;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Revision\RevisionableTrait;
use Botble\Slug\Traits\SlugTrait;
use Botble\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Botble\Product\Models\Orderstatus;
use Illuminate\Support\Arr;


class Product extends BaseModel
{
    use RevisionableTrait;
    use SlugTrait;
    use EnumCastable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * @var mixed
     */
    protected $revisionEnabled = true;

    /**
     * @var mixed
     */
    protected $revisionCleanup = true;

    /**
     * @var int
     */
    protected $historyLimit = 20;

    /**
     * @var array
     */
    protected $dontKeepRevisionOf = [
        'content',
        'views',
    ];

    /**
     * The date fields for the model.clear
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'content',
        'image',
        'images',
        'imagedl',
        'video',
        //'color',
        'option1',
        //'sizes',
        'pricecost',
        'pricesell',
        'pricesale',
        'pricetime',
        'amound',
        'currency_id',
        'price_sale_start',
        'price_sale_end',
        'is_featured',
        'format_type',
        'status',
        'author_id',
        'author_type',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'status' => BaseStatusEnum::class,
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
/*image*/

     public function getImagesAttribute($value)
    {
        try {
            if ($value === '[null]') {
                return [];
            }

            return json_decode($value) ?: [];
        } catch (Exception $exception) {
            return [];
        }
    }

    /**
     * @return string|null
     */
    public function getImageAttribute(): ?string
    {
        return Arr::first($this->images) ?? null;
    }
/*endimage*/
/*color*/
     public function getOption1Attribute($value)
    {
        try {
            if ($value === '[null]') {
                return [];
            }

            return json_decode($value) ?: [];
        } catch (Exception $exception) {
            return [];
        }
    }
     public function getOptionAttribute(): ?string
    {
        return Arr::first($this->options) ?? null;
    }
/*endcolor*/
/*Size*/
   /*  public function getSizesAttribute($value)
    {
        try {
            if ($value === '[null]') {
                return [];
            }

            return json_decode($value) ?: [];
        } catch (Exception $exception) {
            return [];
        }
    }
     public function getSizeAttribute(): ?string
    {
        return Arr::first($this->sizes) ?? null;
    }*/
/*endSize*/

    /**
     * @return BelongsToMany
     */
    public function protags()
    {
        return $this->belongsToMany(ProTag::class, 'product_tags');
    }

    /**
     * @return BelongsToMany
     */
    public function procategories()
    {
        return $this->belongsToMany(ProCategory::class, 'product_categories');
    }

    public function features(): BelongsToMany
    {
        return $this->belongsToMany(Features::class, 'product_features', 'product_id', 'features_id');
    }

    public function color(): BelongsToMany
    {
        return $this->belongsToMany(Color::class, 'product_colors', 'product_id', 'colors_id');
    }

     public function size(): BelongsToMany
    {
        return $this->belongsToMany(Size::class, 'product_sizes', 'product_id', 'sizes_id');
    }


    /**
     * @return MorphTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Product $product) {
            $product->procategories()->detach();
            //$product->features()->detach();
            $product->protags()->detach();
        });
    }
}
