<?php

namespace Botble\Product\Forms;

use Assets;
use Botble\Base\Enums\BaseStatusEnum;
use Botble\Base\Forms\FormAbstract;
use Botble\Product\Enums\PropertyStatusEnum;
use Botble\Product\Forms\Fields\ProCategoryMultiField;
use Botble\Product\Http\Requests\ProductRequest;
use Botble\Product\Models\Product;
use Botble\Product\Repositories\Interfaces\ProCategoryInterface;
use Botble\Product\Repositories\Interfaces\FeaturesInterface;
use Botble\Product\Repositories\Interfaces\ColorInterface;
use Botble\Product\Repositories\Interfaces\SizeInterface;
use Botble\Product\Repositories\Interfaces\ProductInterface;
use Botble\Product\Repositories\Interfaces\CurrencyInterface;
use Botble\Product\Repositories\Interfaces\StoreProductInterface;
use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Throwable;
use Illuminate\Support\Arr;


class ProductForm extends FormAbstract
{


    /**
     * @var string
     */
    protected $template = 'core/base::forms.form-tabs';

    protected $featuresRepository;

    /**
     * @return mixed|void
     * @throws Throwable
     */
    public function __construct(
        
        CurrencyInterface $currencyRepository,
        StoreProductInterface $storeProductRepository,
        StoreProductItemInterface $storeProductItemRepository
        
    ) {
        parent::__construct();
        $this->currencyRepository = $currencyRepository;
        $this->storeProductRepository = $storeProductRepository;
        $this->storeProductItemRepository = $storeProductItemRepository;
    }

    public function buildForm()
    {
        Assets::addScripts(['bootstrap-tagsinput', 'typeahead', 'datetimepicker','colorpicker','input-mask'])
            ->addStyles(['bootstrap-tagsinput','colorpicker'])
            ->addScriptsDirectly('vendor/core/js/tags.js')
            ->addScriptsDirectly('vendor/core/plugins/product/js/features.js')
            ->addScriptsDirectly('vendor/core/libraries/bootstrap-datepicker/js/bootstrap-datepicker.min.js')
            ->addScriptsDirectly('vendor/core/plugins/product/js/colors.js')
            ->addStylesDirectly('vendor/core/plugins/product/css/features.css')
            ->addStylesDirectly('vendor/core/plugins/product/css/colors.css');

          

        $selectedProCategories = [];
        if ($this->getModel()) {
            $selectedProCategories = $this->getModel()->procategories()->pluck('pro_category_id')->all();
        }

        if (empty($selectedProCategories)) {
            $selectedProCategories = app(ProCategoryInterface::class)
                ->getModel()
                ->where('is_default', 1)
                ->pluck('id')
                ->all();
        }


        $protags = null;

        if ($this->getModel()) {
            $protags = $this->getModel()->protags()->pluck('name')->all();
            $protags = implode(',', $protags);
        }

        if (!$this->formHelper->hasCustomField('procategoryMulti')) {
            $this->formHelper->addCustomField('procategoryMulti', ProCategoryMultiField::class);
        }


        $selectedFeatures = [];
        if ($this->getModel()) {
            $selectedFeatures = $this->getModel()->features()->pluck('features_id')->all();

        }

        $features = app(FeaturesInterface::class)->allBy([], [], ['features.id', 'features.name']);

        $selectedColors = [];
        if ($this->getModel()) {
            $selectedColors = $this->getModel()->color()->pluck('colors_id')->all();

        }
        $colors = app(ColorInterface::class)->allBy([], [], ['colors.id', 'colors.name']);


        $selectedSizes = [];
        if ($this->getModel()) {
            $selectedSizes = $this->getModel()->size()->pluck('sizes_id')->all();

        }
        $sizes = app(SizeInterface::class)->allBy([], [], ['sizes.id', 'sizes.name']);

        /*$selectedStoreProduct = [];
        if ($this->getModel()) {
            $selectedStoreProduct = $this->getModel()->storeproductitem()->pluck('store_product_id')->all();

        }
        $storeproducts = app(StoreProductInterface::class)->allBy([], [], ['store_product_items.id', 'store_product_items.title']);*/



        $storeProducts = $this->storeProductItemRepository->pluck('store_product_items.title', 'store_product_items.id');

        

        $currencies = ($this->currencyRepository->allBy([], [], ['currencies.title', 'currencies.id']) != '') ? $this->currencyRepository->pluck('currencies.title', 'currencies.id') : 'None';


        /*if(!isset($currencies)){
            $currencies = 'none';
        }*/



        $this
            ->setupModel(new Product)
            ->setValidatorClass(ProductRequest::class)
            ->withCustomFields()
            ->add('name', 'text', [
                'label'      => trans('core/base::forms.name'),
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'placeholder'  => trans('core/base::forms.name_placeholder'),
                    'data-counter' => 120,
                ],
            ])

            /*->add('name', 'customSelect', [
                'label'      => trans('plugins/product::sell.form.currency'),
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class' => 'form-control select-search-full',
                ],
                'choices'    => $storeProducts,

            ])*/

            ->add('description', 'textarea', [
                'label'      => trans('core/base::forms.description'),
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'rows'         => 4,
                    'placeholder'  => trans('core/base::forms.description_placeholder'),
                    'data-counter' => 400,
                ],
            ])

            ->add('is_featured', 'onOff', [
                'label'         => trans('core/base::forms.is_featured'),
                'label_attr'    => ['class' => 'control-label'],
                'default_value' => false,
            ])
            ->add('content', 'editor', [
                'label'      => trans('core/base::forms.content'),
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'rows'            => 4,
                    'placeholder'     => trans('core/base::forms.description_placeholder'),
                    'with-short-code' => true,
                ],
            ])
            ->add('video', 'text', [
                'label'      => trans('plugins/product::products.form.video'),
                'label_attr' => ['class' => 'control-label required'],
                'helper' => 'Copyright on footer of site',
                'attr'       => [
                    'placeholder'  => trans(' Example : https://www.youtube.com/watch?v=C0DPdy98e4c '),
                    'data-counter' => 120,
                ],
                
            ])
             ->add('status', 'customSelect', [
                'label'      => trans('core/base::tables.status'),
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'class' => 'form-control select-search-full',
                ],
                'choices'    => BaseStatusEnum::labels(),
            ])
            ->add('format_type', 'customRadio', [
                'label'      => trans('plugins/product::products.form.format_type'),
                'label_attr' => ['class' => 'control-label required'],
                'choices'    => get_product_formats(true),
            ])
            
            ->add('procategories[]', 'procategoryMulti', [
                'label'      => trans('plugins/product::procategories.menu_name'),
                'label_attr' => ['class' => 'control-label required'],
                'choices'    => get_procategories_with_children(),
                'value'      => old('procategories', $selectedProCategories),
            ])
            
            


            ->add('imagedl', 'mediaImage', [
                'label'      => trans('core/base::forms.image'),
                'label_attr' => ['class' => 'control-label'],
            ])
            
            ->add('pricesell', 'text', [
                'label'      => trans('plugins/product::sell.form.pricesell'),
                'label_attr' => ['class' => 'control-label'],
                'wrapper'    => [
                    'class' => ' col-md-6',
                ],
                'attr'       => [
                    'id'    => 'pricesell',
                    'class' => 'form-control input-mask-number',
                ],
            ])
               
            ->add('pricecost', 'text', [
                'label'      => trans('plugins/product::sell.form.pricecost'),
                'label_attr' => ['class' => 'control-label'],
                'wrapper'    => [
                    'class' => 'col-md-6',
                ],
                'attr'       => [
                    'id'    => 'pricecost',
                    'class' => 'form-control input-mask-number',
                ],
            ])
            

            ->add('currency_id', 'customSelect', [
                'label'      => trans('plugins/product::sell.form.currency'),
                'label_attr' => ['class' => 'control-label'],
                'wrapper'    => [
                    'class' => 'form-group col-md-6',
                ],
                'attr'       => [
                    'class' => 'form-control select-full',
                ],
                'choices'    => $currencies,

            ])

            

            ->add('pricetime', 'text', [
                'label'      => trans('plugins/product::sell.form.pricetime'),
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'placeholder'      => trans('plugins/product::sell.form.change'),
                    'class'            => 'form-control datepicker',
                    'data-date-format' => config('core.base.general.date_format.js.date'),
                ],
            ])
            
            ->add('protag', 'text', [
                'label'      => trans('plugins/product::products.form.tags'),
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'id'          => 'protags',
                    'data-role'   => 'tagsinput',
                    'placeholder' => trans('plugins/product::products.form.tags_placeholder'),
                ],
                'value'      => $protags,
                'help_block' => [
                    'text' => 'protag route',
                    'protag'  => 'div',
                    'attr' => [
                        'data-protag-route' => route('protags.all'),
                        'class'          => 'hidden',
                    ],
                ],
            ])
           
            ->addMetaBoxes([
                
               'image'    => [
                    'title'    => trans('plugins/product::features.form.images'),
                    'content'  => view('plugins/product::features.form-images',
                        ['object' => $this->getModel()])->render(),
                    'priority' => 0,
                ],
                'features' => [
                    'title'    => trans('plugins/product::features.form.features'),
                    'content'  => view('plugins/product::features.form-features',
                        compact('selectedFeatures', 'features'))->render(),
                    'priority' => 1,
                ],

                'colors' => [
                    'title'    => trans('plugins/product::features.form.colors'),
                    'content'  => view('plugins/product::colors.form-color',
                        compact('selectedColors', 'colors'))->render(),
                    'priority' => 2,
                ],

                'sizes' => [
                    'title'    => trans('plugins/product::features.form.sizes'),
                    'content'  => view('plugins/product::sizes.form-size',
                        compact('selectedSizes', 'sizes'))->render(),
                    'priority' => 2,
                ],

                'option1'            => [
                    'title'   => trans('plugins/product::features.form.option1'),
                    'content' => view('plugins/product::colors.form-option1', 
                        ['object'           => $this->getModel()])->render(),
                    'priority' => 2,
                    
                ],

                /*'size'            => [
                    'title'   => trans('plugins/product::features.form.sizes'),
                    'content' => view('plugins/product::sizes.form-sizes', 
                        ['object'           => $this->getModel()])->render(),
                    'priority' => 3,
                    'src'      => [
                        'local' => '/vendor/core/libraries/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css',
                    ],
                ],*/
                'timesale'            => [
                    'title'   => trans('plugins/product::sell.form.timesale'),
                    'content' => view('plugins/product::timesale.form-timesales', 
                        ['object'           => $this->getModel()])->render(),
                    'priority' => 4,
                    
                ],
            ])
            
            ->setActionButtons(view('plugins/product::products.actions', ['product' => $this->getModel()])->render())
            ->setBreakFieldPoint('status');

    }
}
