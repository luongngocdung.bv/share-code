<?php

namespace Botble\Product\Forms\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class ProCategoryMultiField extends FormField
{

    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'plugins/product::procategories.procategories-multi';
    }
}
