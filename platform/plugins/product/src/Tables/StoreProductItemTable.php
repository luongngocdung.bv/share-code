<?php

namespace Botble\Product\Tables;

use Illuminate\Support\Facades\Auth;
use Botble\Product\Repositories\Interfaces\StoreProductItemInterface;
use Botble\Table\Abstracts\TableAbstract;
use Illuminate\Contracts\Routing\UrlGenerator;
use Yajra\DataTables\DataTables;

class StoreProductItemTable extends TableAbstract
{
    /**
     * @var string
     */
    protected $type = self::TABLE_TYPE_SIMPLE;

    /**
     * @var string
     */
    protected $view = 'plugins/product::items';

    /**
     * @var StoreProductItemInterface
     */
    protected $repository;

    /**
     * StoreProductItemTable constructor.
     * @param DataTables $table
     * @param UrlGenerator $urlGenerator
     * @param StoreProductItemInterface $StoreProductItemRepository
     */
    public function __construct(
        DataTables $table,
        UrlGenerator $urlGenerator,
        StoreProductItemInterface $storeProductItemRepository
    ){
        $this->repository = $storeProductItemRepository;
        $this->setOption('id', 'store-product-items-table');
        parent::__construct($table, $urlGenerator);

        if (!Auth::user()->hasAnyPermission(['store-product-item.edit', 'store-product-item.destroy'])) {
            $this->hasOperations = false;
            $this->hasActions = false;
        }
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @since 2.1
     */
    public function ajax()
    {
        $data = $this->table
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                return view('plugins/product::partials.thumbnail', compact('item'))->render();
            })
            ->editColumn('title', function ($item) {
                if (!Auth::user()->hasPermission('store-product-item.edit')) {
                    return $item->title;
                }

                return anchor_link('#', $item->title, [
                    'data-fancybox' => true,
                    'data-type'     => 'ajax',
                    'data-src'      => route('store-product-item.edit', $item->id),
                ]);
            })
            ->editColumn('checkbox', function ($item) {
                return table_checkbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at, config('core.base.general.date_format.date'));
            });

        return apply_filters(BASE_FILTER_GET_LIST_DATA, $data, $this->repository->getModel())
            ->addColumn('operations', function ($item) {
                return view('plugins/product::partials.actions', compact('item'))->render();
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Get the query object to be processed by table.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     *
     * @since 2.1
     */
    public function query()
    {
        $model = $this->repository->getModel();
        $query = $model
            ->select([
                'store_product_items.id',
                'store_product_items.title',
                'store_product_items.image',
                'store_product_items.order',
                'store_product_items.created_at',
            ])
            ->where('store_product_id', request()->route()->parameter('id'));

        return $this->applyScopes(apply_filters(BASE_FILTER_TABLE_QUERY, $query, $model));
    }

    /**
     * @return array
     *
     * @since 2.1
     */
    public function columns()
    {
        $operation = $this->getOperationsHeading();
        $operation['operations']['width'] = '170px;';

        return [
                'id'         => [
                    'title' => trans('core/base::tables.id'),
                    'width' => '20px',
                ],
                'image'      => [
                    'title' => trans('core/base::tables.image'),
                    'class' => 'text-center',
                ],
                'title'      => [
                    'title' => trans('core/base::tables.title'),
                    'class' => 'text-left',
                ],
                'order'      => [
                    'title' => trans('core/base::tables.order'),
                    'class' => 'text-left order-column',
                ],
                'created_at' => [
                    'title' => trans('core/base::tables.created_at'),
                    'width' => '100px',
                ],
            ] + $operation;
    }

    /**
     * @return array
     *
     * @since 2.1
     */
    public function buttons()
    {
        return [];
    }

    /**
     * @return array
     *
     * @since 2.1
     */
    public function actions()
    {
        return [];
    }
}
