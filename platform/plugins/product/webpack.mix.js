const path = require('path');
let mix = require('laravel-mix');

const publicPath = 'public/vendor/core/plugins/product';
const resourcePath = './platform/plugins/product';
const publicPathVue = './../../../public/vendor/core/plugins/product';

//----------------------khi nào chạy fore cho đống js này thì mở ra và đóng toàn bộ phần dưới lại------------------------//

/* mix
     .js(resourcePath + '/resources/assets/js/product.js', publicPath + '/js')
     .copy(publicPath + '/js/product.js', resourcePath + '/public/js')

     .js(resourcePath + '/resources/assets/js/features.js', publicPath + '/js')
     .copy(publicPath + '/js/features.js', resourcePath + '/public/js')

     .sass(resourcePath + '/resources/assets/sass/features.scss', publicPath + '/css')
     .copy(publicPath + '/css/features.css', resourcePath + '/public/css')

 	  .js(resourcePath + '/resources/assets/js/featurescurrencies.js', publicPath + '/js')
     .copy(publicPath + '/js/featurescurrencies.js', resourcePath + '/public/js')

 	  .js(resourcePath + '/resources/assets/vue/app.js', publicPathVue + '/vue')
     .copy(publicPathVue + '/vue/app.js', resourcePath + '/public/vue')

     .sass(resourcePath + '/resources/assets/sass/featurescurrencies.scss', publicPath + '/css')
     .copy(publicPath + '/css/featurescurrencies.css', resourcePath + '/public/css');*/

//----------------------khi nào chạy fore cho đống js này thì mở ra------------------------//




//------------------- khi nào xài vue thì mở đống này ra ------------------------------//
mix.autoload({})


mix.js('resources/assets/js/app.js', '')

mix.sourceMaps()
mix.disableNotifications()
mix.setPublicPath(publicPathVue)

if (mix.inProduction()) {
    mix.version()
  }
  
  mix.webpackConfig({
    externals: {
      'pusher-js': 'Pusher'
    },
  
    resolve: {
      alias: {
        '@': path.join(__dirname, './resources/assets/js')
      }
    },
  
    output: {
      library: 'Product',
      libraryTarget: 'umd'
    }
  })


  //------------------- khi nào xài vue thì mở đống này ra ------------------------------//