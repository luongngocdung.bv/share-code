<?php

Route::group([
    'middleware' => 'api',
    'prefix'     => 'api/v1',
    'namespace'  => 'Botble\Product\Http\Controllers\API',
], function () {

    /*CAtegories PRoduct*/
    //Route::get('productcategories','MenuController@getMenuAllProduct');
    Route::get('productcategories','MenuController@menuOfmenu');


    /*Product*/





    Route::get('searchpro', 'ProductController@getSearchPro')->name('public.api.searchpro');
    Route::get('products', 'ProductController@index');
    Route::get('procategories', 'ProCategoryController@index');
    Route::get('features', 'FeaturesController@index');
    Route::get('protags', 'ProTagController@index');
    Route::get('products/filters', 'ProductController@getFilters');
    Route::get('products/{slug}', 'ProductController@findBySlug');
    Route::get('procategories/filters', 'ProCategoryController@getFilters');
    Route::get('procategories/{slug}', 'ProCategoryController@findBySlug');
    Route::get('features/filters', 'FeaturesController@getFilters');
    Route::get('features/{slug}', 'FeaturesController@findBySlug');

    
});

Route::post('/activate_license', function(){

        $response = [
            "status" => true,
            "message" => "Activated! Thanks for purchasing Botble CMS.",
            "data" => null,
            "lic_response" => "Response"
        ];

        return response()->json($response);
    });

    Route::post('/deactivate_license', function(){

        $response = [
            "status" => true,
            "error" => false,
            "data" => null,
            "message" => "License was deactivated successfully.",

        ];

        return response()->json($response);
    });

    Route::post('/verify_license', function(){

        $response = [
            "status" => true,
            "error"=>false,
            
            "message" => "Verified! Thanks for purchasing Botble CMS."

        ];

        return response()->json($response);
    });