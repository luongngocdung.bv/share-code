<?php






Route::group(['namespace' => 'Botble\Product\Http\Controllers', 'middleware' => 'web'], function () {

    Route::group(['prefix' => config('core.base.general.admin_dir'), 'middleware' => 'auth'], function () {


        Route::get('settings', [
            'as'   => 'real-estate.settings',
            'uses' => 'SettingController@getSettings',
        ]);

        Route::post('settings', [
            'as'   => 'real-estate.settings',
            'uses' => 'SettingController@postSettings',
        ]);

        Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
            Route::resource('', 'ProductController')->parameters(['' => 'product']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'ProductController@deletes',
                'permission' => 'products.destroy',
            ]);
            Route::get('widgets/recent-products', [
                'as'         => 'widget.recent-products',
                'uses'       => 'ProductController@getWidgetRecentProducts',
                'permission' => 'products.index',
            ]);

            Route::get('duplicate/{id}', [
                    'as'         => 'duplicate',
                    'uses'       => 'ProductController@getDuplicate',
                    'permission' => 'products.create',
            ]);
        });




        Route::group(['prefix' => 'procategories', 'as' => 'procategories.'], function () {
            Route::resource('', 'ProCategoryController')->parameters(['' => 'procategory']);

            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'ProCategoryController@deletes',
                'permission' => 'procategories.destroy',
            ]);
            Route::get('produplicate/{id}', [
                'as'         => 'produplicate',
                'uses'       => 'ProCategoryController@getDuplicate',
                'permission' => 'procategories.create',
            ]);
        });




        Route::group(['prefix' => 'protags', 'as' => 'protags.'], function () {
            Route::resource('', 'ProTagController')->parameters(['' => 'protag']);

            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'ProTagController@deletes',
                'permission' => 'protags.destroy',
            ]);

            Route::get('all', [
                'as'         => 'all',
                'uses'       => 'ProTagController@getAllProTags',
                'permission' => 'protags.index',
            ]);
        });


         Route::group(['prefix' => 'features', 'as' => 'features.'], function () {
            Route::resource('', 'FeaturesController')->parameters(['' => 'features']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'FeaturesController@deletes',
                'permission' => 'features.destroy',
            ]);
        });
         
        Route::group(['prefix' => 'stores', 'as' => 'store.'], function () {
            Route::resource('', 'StoreController')->parameters(['' => 'store']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'StoreController@deletes',
                'permission' => 'store.destroy',
            ]);
        });

        Route::group(['prefix' => 'carts', 'as' => 'cart.'], function () {
            Route::resource('', 'CartController')->parameters(['' => 'cart']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'CartController@deletes',
                'permission' => 'cart.destroy',
            ]);
        });

        Route::group(['prefix' => 'orderstatuses', 'as' => 'orderstatus.'], function () {
            Route::resource('', 'OrderstatusController')->parameters(['' => 'orderstatus']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'OrderstatusController@deletes',
                'permission' => 'orderstatus.destroy',
            ]);
        });

        Route::group(['prefix' => 'payments', 'as' => 'payment.'], function () {
            Route::resource('', 'PaymentController')->parameters(['' => 'payment']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'PaymentController@deletes',
                'permission' => 'payment.destroy',
            ]);
        });

        Route::group(['prefix' => 'shippings', 'as' => 'shipping.'], function () {
            Route::resource('', 'ShippingController')->parameters(['' => 'shipping']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'ShippingController@deletes',
                'permission' => 'shipping.destroy',
            ]);
        });

        Route::group(['prefix' => 'colors', 'as' => 'color.'], function () {
            Route::resource('', 'ColorController')->parameters(['' => 'color']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'ColorController@deletes',
                'permission' => 'color.destroy',
            ]);
        });

        Route::group(['prefix' => 'sizes', 'as' => 'size.'], function () {
            Route::resource('', 'SizeController')->parameters(['' => 'size']);
            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'SizeController@deletes',
                'permission' => 'size.destroy',
            ]);
        });


        //Store product
        Route::group(['prefix' => 'store-products', 'as' => 'store-product.'], function () {

            Route::resource('', 'StoreProductController')->parameters(['' => 'store-product']);

            Route::delete('items/destroy', [
                'as'         => 'deletes',
                'uses'       => 'StoreProductController@deletes',
                'permission' => 'store-product.destroy',
            ]);

            Route::post('sorting', [
                'as'         => 'sorting',
                'uses'       => 'StoreProductController@postSorting',
                'permission' => 'store-product.edit',
            ]);
        });

        Route::group(['prefix' => 'store-product-items', 'as' => 'store-product-item.'], function () {

            Route::resource('', 'StoreProductItemController')->except(['index', 'destroy'])->parameters(['' => 'store-product-item']);

            Route::get('list/{id}', [
                'as'   => 'index',
                'uses' => 'StoreProductItemController@index',
            ]);

            Route::get('delete/{id}', [
                'as'   => 'destroy',
                'uses' => 'StoreProductItemController@destroy',
            ]);

            Route::delete('delete/{id}', [
                'as'         => 'delete.post',
                'uses'       => 'StoreProductItemController@postDelete',
                'permission' => 'store-product-item.destroy',
            ]);
        });
        //End Store product
        
    });

    if (defined('THEME_MODULE_SCREEN_NAME')) {
        Route::group(apply_filters(BASE_FILTER_GROUP_PUBLIC_ROUTE, []), function () {
            Route::get('searchproduct', [
                'as'   => 'public.search',
                'uses' => 'PublicController@getSearchPro',
            ]);

            Route::get('protag/{slug}', [
                'as'   => 'public.protag',
                'uses' => 'PublicController@getProTag',
            ]);

            Route::get('/product_info', [
                'as'   => 'public.product_info',
                'uses' => 'CartController@getAllProductAjax',
            ]);

            /*Route::get('/tags', function() {
                return view('tags');
            });*/
           
            Route::get('/searchac', [
                'as'   => 'products.search',
                'uses' => 'CartController@search',
            ]);

            Route::post('/add_item', [
                'as'   => 'products.add_item',
                'uses' => 'CartController@postAddItem',
            ]);
            
           
        });
    }
});
