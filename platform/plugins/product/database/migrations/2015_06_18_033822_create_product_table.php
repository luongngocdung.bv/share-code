<?php

use Botble\ACL\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->integer('parent_id')->unsigned()->default(0);
            $table->string('description', 400)->nullable();
            $table->string('status', 60)->default('published');
            $table->integer('author_id');
            $table->string('author_type', 255)->default(addslashes(User::class));
            $table->string('icon', 60)->nullable();
            $table->tinyInteger('order')->default(0);
            $table->string('type_menu',60)->default(0);
            $table->tinyInteger('is_hot')->default(0);
            $table->tinyInteger('is_new')->default(0);
            $table->tinyInteger('is_featured')->default(0);
            $table->tinyInteger('is_default')->unsigned()->default(0);
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('description', 400)->nullable();
            $table->text('content')->nullable();
            $table->string('status', 60)->default('published');
            $table->integer('author_id');
            $table->string('author_type', 255)->default(addslashes(User::class));
            $table->tinyInteger('is_featured')->unsigned()->default(0);
            //$table->text('image')->nullable();
            $table->text('imagedl')->nullable();
            $table->text('images')->nullable();
            //$table->string('pricecost', 255)->nullable();
            $table->decimal('pricecost', 20, 0)->nullable();
            $table->decimal('pricesell', 20, 0)->nullable();
           // $table->string('pricesell', 255);
            $table->string('pricetime', 255)->nullable();
            $table->string('amound', 40)->nullable();
           // $table->string('color', 400)->nullable(); 
            $table->text('option1', 400)->nullable();
            //$table->string('sizes', 400)->nullable();
            $table->string('video', 400)->nullable();
           // $table->string('pricesale', 255)->nullable();
            $table->decimal('pricesale', 20, 0)->nullable();
            $table->string('price_sale_start', 400)->nullable();
            $table->string('price_sale_end', 400)->nullable();
            $table->integer('views')->unsigned()->default(0);
            $table->integer('currency_id')->unsigned()->nullable();
            $table->string('format_type', 30)->nullable();
            $table->timestamps();
        });

         Schema::create('store_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('key', 120);
            $table->string('description', 255)->nullable();
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('store_product_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_product_id', false, true);
            $table->string('title', 255);
            $table->string('image', 255)->nullable();
            $table->string('link', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();
        });


        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('colors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('code_color', 120)->nullable();
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

         Schema::create('sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('code_sizes', 120)->nullable();
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->text('products')->nullable();
            $table->integer('author_id');
            $table->string('author_type', 255)->default(addslashes(User::class));
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('orderstatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('protags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->integer('author_id');
            $table->string('author_type', 255)->default(addslashes(User::class));
            $table->string('description', 400)->nullable()->default('');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->string('status', 60)->default('published');
            $table->timestamps();
        });

        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 60);
            $table->string('symbol', 10);
            $table->tinyInteger('is_prefix_symbol')->unsigned()->default(0);
            $table->tinyInteger('decimals')->unsigned()->default(0);
            $table->integer('order')->default(0)->unsigned();
            $table->tinyInteger('is_default')->default(0);
            $table->double('exchange_rate')->default(1);
            $table->timestamps();
        });

        

        Schema::create('product_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('protag_id')->unsigned()->references('id')->on('protags')->onDelete('cascade')->nullable();;
            $table->integer('pro_tag_id')->unsigned()->references('id')->on('protags')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procategory_id')->unsigned()->references('id')->on('procategories')->onDelete('cascade')->nullable();
            $table->integer('pro_category_id')->unsigned()->references('id')->on('procategories')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carts_id')->unsigned()->references('id')->on('carts')->onDelete('cascade')->nullable();
            $table->string('carts_size', 255)->nullable();
            $table->string('carts_amound', 255)->nullable();
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });


        Schema::create('product_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('features_id')->unsigned()->references('id')->on('features')->onDelete('cascade')->nullable();
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('colors_id')->unsigned()->references('id')->on('colors')->onDelete('cascade')->nullable();
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });
        Schema::create('product_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sizes_id')->unsigned()->references('id')->on('sizes')->onDelete('cascade')->nullable();
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stores_id')->unsigned()->references('id')->on('stores')->onDelete('cascade')->nullable();
            $table->integer('product_id')->unsigned()->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('product_orderstatuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderstatuses_id')->unsigned()->references('id')->on('orderstatuses')->onDelete('cascade')->nullable();
            $table->integer('cart_id')->unsigned()->references('id')->on('carts')->onDelete('cascade');
        });
        Schema::create('product_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payments_id')->unsigned()->references('id')->on('payments')->onDelete('cascade')->nullable();
            $table->integer('cart_id')->unsigned()->references('id')->on('carts')->onDelete('cascade');
        });

        Schema::create('product_shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shippings_id')->unsigned()->references('id')->on('shippings')->onDelete('cascade')->nullable();
            $table->integer('cart_id')->unsigned()->references('id')->on('carts')->onDelete('cascade');
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('product_tags');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('product_features');
        Schema::dropIfExists('product_colors');
        Schema::dropIfExists('product_sizes');
        Schema::dropIfExists('product_stores');
        Schema::dropIfExists('product_orderstatuses');
        Schema::dropIfExists('product_payments');
        Schema::dropIfExists('product_shippings');
        Schema::dropIfExists('product_carts');
        Schema::dropIfExists('products');
        Schema::dropIfExists('features');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('sizes');
        Schema::dropIfExists('stores');
        Schema::dropIfExists('carts');
        Schema::dropIfExists('shippings');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('orderstatuses');
        Schema::dropIfExists('procategories');
        Schema::dropIfExists('protags');
        Schema::dropIfExists('store_products');
        Schema::dropIfExists('store_product_items');
    }
}
