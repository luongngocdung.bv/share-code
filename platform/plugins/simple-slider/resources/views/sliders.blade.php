
@if (count($sliders) > 0)
<div class="tp-banner-container hidden-xs">
    <div class="tp-banner">
        <ul>
            @php
                $i = 1;
            @endphp
            <!-- SLIDE  -->
            @foreach($sliders as $slider)
            <li id="slide{{$i++}}" data-transition="zoomout" data-slotamount="7" data-masterspeed="500" data-title="{{ $slider->title }}" data-link="{{ $slider->link }}">
                <!-- MAIN IMAGE -->
                <img src="/themes/coolbaby/images/dummy.png" alt="slide{{$i++}}-{{ $slider->title }}" data-lazyload="/themes/coolbaby/images/sliders/slide1.png">

                <!-- LAYERS img 1 -->
                <div class="tp-caption fadein fadeout rs-parallaxlevel-1" data-x="500" data-y="0" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 4;">
                    <img src="{{ get_object_image($slider->image) }}" alt="{{ $slider->title }}">
                </div>
               
                <!-- LAYER NR. 0  -->
                <div class="tp-caption text0 fadeout" data-x="1050" data-y="140" data-speed="800" data-start="2500" data-easing="Power3.easeInOut" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                     &#8220;
                </div>
                <!-- LAYER NR. 1 -->
                <div class="tp-caption text1 fadeout" data-x="1080" data-y="150" data-speed="800" data-start="3000" data-easing="Power3.easeInOut" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                    {!! $slider->content_des1 !!}
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption text2 fadeout" data-x="1080" data-y="255" data-speed="500" data-start="3500" data-easing="Power3.easeInOut" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                    {!! $slider->content_des2 !!}
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption text3 fadeout" data-x="1080" data-y="305" data-speed="1000" data-start="4000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    {!! $slider->content_des3 !!}
                </div>
            @endforeach
        </ul>
    </div>
</div>
@endif