@php Theme::set('section-name', $protag->name) @endphp

@if ($products->count() > 0)
    @foreach ($products as $product)
       
        <article class="post post__horizontal mb-40 clearfix">
            <div class="post__thumbnail">
                <img src="{{ get_object_image($product->image, 'medium') }}" alt="{{ $product->name }}"><a href="{{ $product->url }}" class="post__overlay"></a>
            </div>
            <div class="post__content-wrap">
                <header class="post__header">
                    <h3 class="post__title"><a href="{{ $product->url }}">{{ $product->name }}</a></h3>
                    <div class="post__meta"><span class="post__created-at"><i class="ion-clock"></i><a href="#">{{ date_from_database($product->created_at, 'M d, Y') }}</a></span>
                        @if ($product->user->username)
                            <span class="post__author"><i class="ion-android-person"></i><span>{{ $product->user->getFullName() }}</span></span>
                        @endif
                        <span class="post-category"><i class="ion-cube"></i>
                            @if ($product->procategories->first())
                                <a href="{{ $product->procategories->first()->url }}">{{ $product->procategories->first()->name }}</a>
                            @endif
                        </span>
                    </div>
                </header>
                <div class="post__content">
                    <p data-number-line="4">{{ $product->description }}</p>
                </div>
            </div>
        </article>
    @endforeach
    <div class="page-pagination text-right">
        {!! $products->links() !!}
    </div>
@else
    <div class="alert alert-warning">
        <p>{{ __('There is no data to display!') }}</p>
    </div>
@endif
