<section class="bread-crumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <li class="home">
                        <a itemprop="url" href="/" ><span itemprop="title">Trang chủ</span></a>
                    </li> 
                    <li>
                        <strong itemprop="title">{{ __('Từ khóa tìm kiếm: ') }} "{{ Request::input('q') }}"</strong>

                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</section>
<section class="blog-articles blog">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 col-md-push-4 col-lg-push-3">
                <div class="article-lists">
                    
                    <h1 class="title-head blog-title">
                        <span>{{ __('Từ khóa tìm kiếm: ') }} "{{ Request::input('q') }}"</span>
                    </h1>
                   
                    @if ($products->count() > 0)
                    <div class="fw">
                        @foreach ($products as $post)
                        <article class="article-item blog-article-item">
                            <div class="article-thumbnail image-hover-1">
                                <a href="{{ route('public.single', $post->slug) }}" title="{{ $post->name }}">
                                    
                                    <img src="{{ get_object_image($post->image, 'medium') }}" data-lazyload="{{ get_object_image($post->image, 'medium') }}" class="img-responsive" alt="{{ $post->name }}"/>
                                    
                                </a>
                            </div>
                            <div class="article-info">
                                <h3 class="fw article-title">
                                    <a href="{{ route('public.single', $post->slug) }}" title="{{ $post->name }}">
                                        <span>{{ $post->name }}</span>
                                    </a>
                                </h3>
                                <div class="article-summary">
                                    
                                    <i class="fa fa-edit" aria-hidden="true"></i> 
                                    
                                </div>
                                <div class="article-summary-bottom" style="martin">
                                    
                                </div>
                                <div class="libra-health___viewmore">
                                    <a href="{{ route('public.single', $post->slug) }}" title="Xem thêm">
                                        <span>Xem thêm</span>
                                    </a>
                                </div>
                                
                            </div>
                        </article>
                        @endforeach
                    </div>      
                    <nav class="fw pagination-parent">
                       
                    </nav>                                                        
                    @else
                        <div class="fw">
                            <p>{{ __('Với từ khóa tìm kiếm: ') }} "{{ Request::input('q') }}" {{ __('không tìm thấy bài viết nào!') }}</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 col-md-pull-8 col-lg-pull-9">
                <!-- DANH MỤC BLOG -->
               
                <!-- FEEDBACK -->
            </div>
        </div>
    </div>
</section>
