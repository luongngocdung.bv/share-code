<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="{{ get_image_url(theme_option('favicon')) }}">

        <link rel="canonical" href="{{ url('/') }}">
        <meta http-equiv="content-language" content="{{ app()->getLocale() }}">

        {!! Theme::header() !!}
    </head>
    <body class="responsive">
        {!! Theme::partial('top-header') !!}
        <div id="outer">
            <div id="outer-canvas">
                {!! Theme::partial('header') !!}
                {!! Theme::content() !!}
                {!! Theme::partial('footer') !!}
            </div>
        </div>
        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        {!! Theme::footer() !!}
    </body>
</html>
