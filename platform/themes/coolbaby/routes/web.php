<?php

Theme::routes();

Route::group(['namespace' => 'Theme\Coolbaby\Http\Controllers', 'middleware' => 'web'], function () {
    Route::group(apply_filters(BASE_FILTER_GROUP_PUBLIC_ROUTE, []), function () {

        Route::get('/', 'CoolbabyController@getIndex')->name('public.index');

        Route::get('sitemap.xml', [
            'as'   => 'public.sitemap',
            'uses' => 'CoolbabyController@getSiteMap',
        ]);

        Route::get('{slug?}' . config('core.base.general.public_single_ending_url'), [
            'as'   => 'public.single',
            'uses' => 'CoolbabyController@getView',
        ]);

    });

});

Route::group([
    'middleware' => 'api',
    'prefix'     => 'api/v1',
    'namespace'  => 'Theme\Coolbaby\Http\Controllers',
], function () {
    Route::get('search', 'RippleController@getSearch')->name('public.api.search');
});



Route::group([
    'middleware' => 'api',
    'prefix'     => 'api/v1',
    'namespace'  => 'Theme\Coolbaby\Http\Controllers',
], function () {
    Route::get('searchpro', 'RippleController@getSearchPro')->name('public.api.searchpro');
});


Route::get('language', [
            'as'   => 'public.language',
            'uses' => 'CoolbabyController@languageSwitcher',
        ]);