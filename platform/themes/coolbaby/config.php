<?php

use Botble\Theme\Theme;

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials" and "views"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function(Theme $theme)
        {
            // You may use this event to set up your assets.
            // $theme->asset()->usePath()->add('core', 'core.js');
            // $theme->asset()->usePath()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->usePath()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', ['jquery']);

            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', \Auth::user());
            // });


             $theme
                ->asset()
                ->container('footer')
                ->usePath()->add('jquery-1.11.3.min.js', 'js/jquery-1.11.3.min.js')
                ->usePath()->add('modernizr.custom.02163.js', 'js/modernizr.custom.02163.js')
                ->usePath()->add('jquery.finger.min.js', 'js/jquery.finger.min.js')
                ->usePath()->add('doubletaptogo.js', 'js/doubletaptogo.js')
                ->usePath()->add('bootstrap.min.js', 'js/bootstrap.min.js')
                ->usePath()->add('jquery.easing.1.3.min.js', 'js/jquery.easing.1.3.min.js')
                ->usePath()->add('slick.min.js', 'js/slick.min.js')
                ->usePath()->add('jquery.parallax.min.js', 'js/jquery.parallax.min.js')
                ->usePath()->add('jquery.inview.min.js', 'js/jquery.inview.min.js')
                ->usePath()->add('jquery.liMarquee.min.js', 'js/jquery.liMarquee.min.js')
                ->usePath()->add('jquery.colorbox-min.js', 'js/jquery.colorbox-min.js')
                ->usePath()->add('jquery.plugin.min.js', 'js/jquery.plugin.min.js')
                ->usePath()->add('jquery.countdown.min.js', 'js/jquery.countdown.min.js')
                ->usePath()->add('coolbaby.js', 'js/coolbaby.js')
                ->usePath()->add('jquery.themepunch.plugins', 'rs-plugin/js/jquery.themepunch.plugins.min.js', ['jquery'])
                ->usePath()->add('jquery.themepunch.revolution', 'rs-plugin/js/jquery.themepunch.revolution.min.js', ['jquery'])
                ->usePath()->add('jquery.themepunch', 'rs-plugin/js/jquery.themepunch.ini.js', ['jquery'])
                /*->add('app.js', 'vendor/product/app.js')*/;
               

            $theme
                ->asset()
                ->usePath()->add('bootstrap-css', 'css/bootstrap.css')
                ->usePath()->add('flaticon-css', 'fonts/flaticon/flaticon.css')
                ->usePath()->add('slick-css', 'css/slick.css')
                ->usePath()->add('liMarquee-css', 'css/liMarquee.css')
                ->usePath()->add('colorbox-css', 'css/colorbox.css')
                ->usePath()->add('jquery-nouislider-css', 'css/jquery.nouislider.css')
                ->usePath()->add('coolbaby-css', 'css/coolbaby.css')
                //->usePath()->add('coolbaby-art-css', 'css/coolbaby-art.css')
                ->usePath()->add('settings-css', 'rs-plugin/css/settings.css')
                ->usePath()->add('extralayers-css', 'rs-plugin/css/extralayers.css');
           

            /*if (app()->environment() != 'production') {
                $theme->asset()->container('footer')->usePath()->add('demo-js', 'js/demo.js');
                $theme->asset()->usePath()->add('demo-css', 'css/demo.min.css');
            }*/



            if (function_exists('shortcode')) {
                $theme->composer(['index', 'page', 'post'], function(\Botble\Shortcode\View\View $view) {
                    $view->withShortcodes();
                });
            }
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }
        ]
    ]
];
