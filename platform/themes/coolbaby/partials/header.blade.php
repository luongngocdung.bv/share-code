<!--Navbar -->
<header>
		<!-- Search -->
		<div id="openSearch">
			<div class="container">
				<div class="inside">
					<form id="searchHeader" method="get" action="{{ route('public.search') }}">
						<div class="input-outer">
							<input type="text" class="search-input" name="q"  onblur="if (this.value == '') {this.value = 'SEARCH...';}" onfocus="if(this.value == 'SEARCH...') {this.value = '';}">
						</div>
						<div class="button-outer">
							<button type="button" class="pull-right search-close"><i class="icon">&#10005;</i></button>
							<button type="submit" class="pull-right"><i class="icon icon-xl flaticon-zoom45"></i></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- //end Search -->
		<div id="newsLine">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-md-2 hidden-xs">
						<div class="title upper">
							<i class="icon flaticon-news"></i>{{__('News')}}
						</div>
					</div>
					<div class="col-xs-5 col-sm-6 col-md-7 col-xs-push-1 col-sm-push-0">
						<div id="newsCarousel" class="slick-style1">
							@foreach(get_featured_posts(5) as $post_featured)
							<div class="item upper">
								<div class="marquee">
									<span class="date">{{ date_from_database($post_featured->created_at, 'd.m.Y') }}.</span> {{$post_featured->name}}
								</div>
							</div>
							@endforeach
							
						</div>
					</div>
					<div class="col-xs-5 col-sm-3 col-md-2 top-link pull-right">
						<div class="btn-outer btn-search">
							<a href="#" class="btn btn-xs btn-default" data-toggle="dropdown"><span class="icon icon-lg flaticon-zoom45"></span></a>
						</div>
						<div class="btn-outer btn-shopping-cart">
							<a href="#drop-shopcart" class="btn btn-xs btn-default open-cart" data-toggle="dropdown"><span class="icon icon-md flaticon-shopping66"></span><span class="badge">3</span></a>
							<div class="hidden">
								<div id="drop-shopcart" class="shoppingcart-box">
									<div class="title">
										{{__('Shopping cart')}}
									</div>
									<div class=" hidden" id="liTemplate">
										<div class="item animate-delay fadeInRight">
											<div class="image">
											</div>
											<div class="description">
												<span class="product-name"></span><strong class="price"></strong>
											</div>
											<div class="buttons">
												<a href="#" class="icon icon-sm flaticon-write13"></a><a href="#" class="icon icon-sm flaticon-recycle59 remove-from-cart"></a>
											</div>
										</div>
									</div>
									<div class="list animate-delay-outer">
										<div class="item animate-delay fadeInRight">
											<div class="image">
												<a href="product.html" class="preview-image"><img src="/themes/coolbaby/images/products/product-03.jpg" alt=""></a>
											</div>
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div>
											<div class="buttons">
												<a href="#" class="icon icon-sm flaticon-write13"></a><a href="#" class="icon icon-sm flaticon-recycle59 remove-from-cart"></a>
											</div>
										</div>
										<div class="item animate-delay fadeInRight">
											<div class="image">
												<a href="product.html" class="preview-image"><img src="/themes/coolbaby/images/products/product-05.jpg" alt=""></a>
											</div>
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div>
											<div class="buttons">
												<a href="#" class="icon icon-sm flaticon-write13"></a><a href="#" class="icon icon-sm flaticon-recycle59 remove-from-cart"></a>
											</div>
										</div>
									</div>
									<div class="total">
										Total: <strong>$44.95</strong>
									</div>
									<div class="empty">
										Shopping cart is empty
									</div>
									<a href="checkout.html" class="btn btn-cool">Proceed to Checkout</a>
									<div class="view-link">
										<a href="shopping-cart.html">View shopping cart </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Back to top -->
		<div class="back-to-top">
			<span class="arrow-up"><img src="/themes/coolbaby/images/icon-scroll-arrow.png" alt=""></span>
			<img src="/themes/coolbaby/images/icon-scroll-mouse.png" alt="">
		</div>
		<!-- //end Back to top -->
		<section class="navbar">
		<div class="background">
			<div class="container">
				<div class="row">
					<div class="header-left col-sm-5 col-md-8">
						<div class="row">
							<div class="navbar-welcome col-md-6 compact-hidden hidden-sm hidden-xs">
								{{__('Default welcome msg!')}}
							</div>
							<!-- Mobile menu Button-->
							<div class="col-xs-2 visible-xs">
								<div class="expand-nav compact-hidden">
									<a href="#off-canvas-menu" id="off-canvas-menu-toggle"><span class="icon icon-xl flaticon-menu29"></span></a>
								</div>
							</div>
							<!-- //end Mobile menu Button -->
							<!-- Logo -->
							<div class="navbar-logo col-xs-10 col-sm-10 col-md-6 text-center">
								<a href="{{ route('public.single') }}"><img src="{{ get_image_url(theme_option('logo')) }}" alt="{{theme_option('site_title')}}"></a>
							</div>
							<!-- //end Logo -->
							<div class="clearfix visible-xs">
							</div>
							<!-- Secondary menu -->
							<div class="top-link pull-right compact-visible">
								<div class="btn-outer btn-shopping-cart">
									<a href="#drop-shopcart" class="btn btn-xs btn-default open-cart" data-toggle="dropdown"><span class="icon icon-md flaticon-shopping66"></span><span class="badge">3</span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="navbar-secondary-menu col-sm-7 col-md-4 compact-hidden">
						<div class="btn-group">
							<a href="#" title="{{__('Account')}}" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><span class="icon icon-lg flaticon-business137"></span><span class="drop-title">{{__('Account')}}</span></a>
							@if (is_plugin_active('member'))
							    <ul class="dropdown-menu" role="menu">
							        @if (Auth::guard('member')->check())
							            <li><a href="{{ route('public.member.dashboard') }}" rel="nofollow"><i class="fa fa-user"></i> <span>{{ Auth::guard('member')->user()->getFullName() }}</span></a></li>
							            <li><a href="#">Wishlist</a></li>
							            <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" rel="nofollow"><i class="fa fa-sign-out"></i> {{ __('Logout') }}</a></li>
							        @else
							        	
							            <li><a href="{{ route('public.member.login') }}" rel="nofollow"><i class="fa fa-sign-in"></i> {{ __('Login') }}</a></li>
							            <li><a href="{{ route('public.member.register') }}" rel="nofollow"><i class="fa fa-sign-in"></i> {{ __('Sign Up') }}</a></li>
							        @endif
							    </ul>
							    @auth('member')
							        <form id="logout-form" action="{{ route('public.member.logout') }}" method="POST" style="display: none;">
							            @csrf
							        </form>
							    @endauth
							@endif
						</div>
						<div class="btn-group">
							{!! apply_filters('language_switcher') !!}

							
						</div>
						<!-- <div class="btn-group">
							<a href="#" title="Currency" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><span class="icon">$</span><span class="drop-title">Currency</span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">($) US Dollars</a></li>
								<li><a href="#">(€) Euro</a></li>
								<li><a href="#">(&pound;) British Pounds</a></li>
							</ul>
						</div>
						<div class="btn-group">
							<a href="#" title="Compare" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><span class="icon icon-lg flaticon-bars34"></span><span class="drop-title">Compare</span></a>
							<div class="dropdown-menu shoppingcart-box empty" role="menu">
								 No items to compare
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<!-- Main menu -->
			<div class="navbar-main-menu-outer hidden-xs">
				<div class="container">
					<dl class="navbar-main-menu">
						<dt class="item"><a href="{{ route('public.single') }}" class="btn-main"><span class="icon icon-xl flaticon-home113"></span></a></dt>
						<dd></dd>

						{!!
                            Menu::generateMenu([

                                'slug' => 'menu-chinh',

                                'options' => ['class' => 'item'],

                                'view' => 'menu.menu-top',
                            ])
                        !!}


					</dl>
				</div>
			</div>

			{{-- <div class="navbar-main-menu-outer hidden-xs">
				<div class="container">
					<dl class="navbar-main-menu">
						@if(is_plugin_active('product'))
						    @include('plugins/product::product-vue.product')
						@endif
					</dl>
				</div>
			</div> --}}
			
			<!-- //end Main menu -->
		</div>
		</section>
		<!-- Navbar height -->
		<div class="navbar-height">
		</div>
		<!-- //end Navbar height -->
		</header>
		<!-- //end Navbar -->
		<!-- Services -->
		<section class="services-block hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<a href="index.html" class="item anim-icon"><span class="icon"><img src="/themes/coolbaby/images/anim-icon-1.gif" data-hover="/themes/coolbaby/images/anim-icon-1-hover.gif" alt=""/></span><span class="title">Free shipping on orders over $200</span></a>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<a href="index.html" class="item anim-icon"><span class="icon"><img src="/themes/coolbaby/images/anim-icon-2.gif" data-hover="/themes/coolbaby/images/anim-icon-2-hover.gif" alt=""/></span><span class="title">30-day returns</span></a>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<a href="index.html" class="item anim-icon"><span class="icon"><img src="/themes/coolbaby/images/anim-icon-3.gif" data-hover="/themes/coolbaby/images/anim-icon-3-hover.gif" alt=""/></span><span class="title">24/7 Support </span></a>
				</div>
			</div>
		</div>
		</section>
		<!-- //end Services -->



		