
{{-- {{dd($menu_nodes)}} --}}
@foreach ($menu_nodes as $key => $row)
        <dt {!! $options !!}>
            @if($row->icon_font == 'hot')
                <div class="label label-hot">
                    HOT
                </div>
            @elseif($row->icon_font == 'new')
                <div class="label label-newl">
                    NEW
                </div>
            @endif
            <a href="{{ $row->url }}" target="{{ $row->target }}" class="btn-main">{{ $row->title }}</a>
        </dt>
        <dd class="item-content content-small">
            <div class="megamenuClose">
            </div>
            @if ($row->has_child)
                {!!
                    Menu::generateMenu([
                        'slug'      => $menu->slug,
                        'view'      => 'menu.row-list',
                        'options'   => ['class' => 'row-list'],
                        'parent_id' => $row->id,
                    ])
                !!}
            @endif
        </dd>
        
@endforeach