<ul {!! $options !!}>
	@foreach ($menu_nodes as $key => $row)
	<li><span class="name">@if ($row->has_child)<span class="expander">-</span>@endif<a href="{{ $row->url }}" target="{{ $row->target }}">{{ $row->title }}</a></span>
		@if ($row->has_child)
            {!!
                Menu::generateMenu([
                    'slug'      => $menu->slug,
                    'view'      => 'menu.menu-mobile-top',
                    'options'   => ['class' => ''],
                    'parent_id' => $row->id,
                ])
            !!}
        @endif
	</li>
	@endforeach
</ul>
