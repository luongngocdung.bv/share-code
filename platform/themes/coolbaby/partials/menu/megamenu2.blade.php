<div class="navbar-main-submenu">
	{!!
        Menu::generateMenu([
            'slug' => 'gift-up',
            'options' => ['class' => 'exclusive top'],
            'view' => 'menu.gift-up',
        ])
    !!}
	<div class="wrapper-border">
		<div class="row">
			<!-- caregories -->
			<div class="col-sm-12 col-md-9">
				<div class="row">
					@foreach ($menu_nodes as $key => $row)
					<div class="col-xs-6 col-md-4 col-lg-3">
						<div class="submenu-block">
							
							 <span class="icon"><img src="/themes/coolbaby/images/icon-category3.png" alt=""></span><a class="name" href="{{ $row->url }}"  target="{{ $row->target }}">{{ $row->title }}</a>
							@if ($row->has_child)
                                {!!
                                    Menu::generateMenu([
                                        'slug'      => $menu->slug,
                                        'view'      => 'menu.sub-megamenu1',
                                        'options'   => ['class' => ''],
                                        'parent_id' => $row->id,
                                    ])
                                !!}
                            @endif
						</div>
					</div>
					@endforeach
				
					<div class="col-xs-6 col-md-4 col-lg-3">
						<div class="submenu-block">
							<span class="icon"><img src="/themes/coolbaby/images/icon-category9.png" alt=""></span><a class="name" href="listing.html">Tops</a>
						</div>
					</div>
				</div>
			</div>
			<!-- //end caregories -->
			<!-- html block -->
			<div class="col-md-3 hidden-sm hidden-xs">
				<div class="img-fullheight">
					<img class="img-responsive" src="/themes/coolbaby/images/menu-img-right-1.png" alt="">
				</div>
			</div>
			<!-- //end html block -->
		</div>
	</div>
	
	{!!
        Menu::generateMenu([
            'slug' => 'gift-down',
            'options' => ['class' => 'exclusive bottom'],
            'view' => 'menu.gift-down',
        ])
    !!}
</div>