
<div class="navbar-main-submenu">
    <div>
       {{theme_option('seo_description')}}
    </div>
    <div class="wrapper">
        <div class="row">
            <!-- categories -->
            <div class="col-xs-6 col-sm-6 col-md-7 col-lg-7">
                <div class="row col-divider">
                    @foreach ($menu_nodes as $key => $row)
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hidden-sm clear-fix">
                        
                        <div class="submenu-block">
                            <span class="icon"><img src="/themes/coolbaby/images/icon-category3.png" alt=""></span><a class="name" href="{{ $row->url }}"  target="{{ $row->target }}">{{ $row->title }}</a>
                            @if ($row->has_child)
                                {!!
                                    Menu::generateMenu([
                                        'slug'      => $menu->slug,
                                        'view'      => 'menu.sub-megamenu1',
                                        'options'   => ['class' => ''],
                                        'parent_id' => $row->id,
                                    ])
                                !!}
                            @endif
                        </div>
                        
                    </div>

                    @endforeach
                </div>
            </div>
            <!-- categories -->
            <!-- html block -->
            <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="pull-left vertical_title_outer title-md">
                            <span>category bestseller</span>
                        </div>

                        <div class="pull-left carousel_outer">
                            <div class="header-product-carousel slick-style2">
                                <div class="carousel-item">
                                    <div class="product-preview">
                                        <div class="preview">
                                            <a href="product.html" class="preview-image"><img class="img-responsive" src="/themes/coolbaby/images/products/product-listing-08.jpg" alt=""></a>
                                        </div>
                                        <h3 class="title"><a href="product.html">Product Example</a></h3>
                                        <span class="price">$214.19</span>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="product-preview">
                                        <div class="preview">
                                            <a href="product.html" class="preview-image"><img class="img-responsive" src="/themes/coolbaby/images/products/product-listing-01.jpg" alt=""></a>
                                        </div>
                                        <h3 class="title"><a href="product.html">Product Example</a></h3>
                                        <span class="price">$214.19</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="visible-sm divider-lg">
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <a href="#"><img src="/themes/coolbaby/images/banner.jpg" class="img-responsive" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- //end html block -->
        </div>
    </div>
    <div class="row col-divider">
        <div class="col-xs-3">
            <img src="{{ get_image_url(theme_option('logo')) }}" class="img-responsive" alt="{{theme_option('site_title')}}">
        </div>
        <div class="col-xs-9">
            <h5>{{__('ABOUT US')}}</h5>
            
                {!! theme_option('about_description') !!}
            
        </div>
    </div>
</div>
