<div class="loader">
	<div class="fond">
		<div class="contener_general">
			<div class="contener_mixte">
				<div class="ballcolor ball_1">
					&nbsp;
				</div>
			</div>
			<div class="contener_mixte">
				<div class="ballcolor ball_2">
					&nbsp;
				</div>
			</div>
			<div class="contener_mixte">
				<div class="ballcolor ball_3">
					&nbsp;
				</div>
			</div>
			<div class="contener_mixte">
				<div class="ballcolor ball_4">
					&nbsp;
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Off Canvas Menu -->
<div class="hidden">
	<nav id="off-canvas-menu"><span class="icon icon-xl flaticon-delete30" id="off-canvas-menu-close"></span>
		{!!
            Menu::generateMenu([

                'slug' => 'menu-chinh',

                'options' => ['class' => 'expander-list'],

                'view' => 'menu.menu-mobile-top',
            ])
        !!}

	</nav>
</div>
<!-- //end Off Canvas Menu -->

