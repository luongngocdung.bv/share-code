@foreach ($product->images as $image)
    <div class="item"><img  src="{{ get_object_image($image) }}" class="showfullimg" rel="{{ $loop->index }}" alt="{{ $product->name }}"></div>
@endforeach




@foreach($product->color as $color)
        {{ $color->code_color }}
@endforeach

@foreach($product->size as $size)
        {{ $size->name }}
@endforeach

<br>
<br>
<br>
<br>
<br>
<br>
<select>

</select>

giá tiền {{ format_price($product->pricesell, $product->currency_id, false, true) }}

{{--  {{ format_price($product->pricesell, $product->currency, true)  }} --}}


<video src="{{$product->video}}" controls="true"></video>

<!-- <video controls="true">
    <source src="www.youtube.com/watch?v=3bGNuRtlqAQ" type="video/mp4" />
</video> -->







    <style>
        .product-img {
        width: 50%;
        display: block;
        position: relative;
        height:250px;
        }
        .product-img img {
        width: auto;
        height: 100%;
        position: absolute;
        opacity: 0;
        transition: all 0.3s ease;
        }
        .product-img img.active {
        opacity: 1;
        }
        /* Product Configuration */
        .product-color span {
        font-size: 14px;
        font-weight: 400;
        color: #86939E;
        }
        /* Product Color */
        .color-choose div {
        display: inline-block;
        }
        .color-choose input[type="radio"] {
        display: none;
        }
        .color-choose input[type="radio"] + label span {
        display: inline-block;
        width: 25px;
        height: 25px;
        margin: -1px 2px 0 0;
        vertical-align: middle;
        cursor: pointer;
        /* border-radius: 50%; */
        border: 2px solid #FFFFFF;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
        }
        .color-choose input[type="radio"]#red + label span {
        background-color: #C91524;
        }
        .color-choose input[type="radio"]#blue + label span {
        background-color: #314780;
        z-index: 10;
        }
        .color-choose input[type="radio"]#black + label span {
        background-color: #323232;
        }
        .color-choose input[type="radio"]:checked + label span {
        background-image: url(http://lisenme.com/demo/color_change/images/check-icn.svg);
        background-repeat: no-repeat;
        background-position: center;
        }
    </style>



<div style="display: inline-block">
    <div class="product-img">
        <img data-image="black" src="http://lisenme.com/demo/color_change/images/black.png" alt="">
        <img data-image="blue" src="http://lisenme.com/demo/color_change/images/blue.png" alt="">
        <img data-image="red" class="active" src="http://lisenme.com/demo/color_change/images/red.png" alt="">
    </div>
    <!-- Product Color -->
    <div class="product-color">
        <span>Select Color</span>
        <div class="color-choose">
            <div>
                <input data-image="red" type="radio" id="red" name="color" value="red" checked>
                <label for="red"><span></span></label>
            </div>
            <div>
                <input data-image="blue" type="radio" id="blue" name="color" value="blue">
                <label for="blue"><span></span></label>
            </div>
            <div>
                <input data-image="black" type="radio" id="black" name="color" value="black">
                <label for="black"><span></span></label>
            </div>
        </div>
    </div>
</div>




    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
        $('.color-choose input').on('click', function() {

          var fridge = $(this).attr('data-image');

          $('.active').removeClass('active');
          $('.product-img img[data-image = ' + fridge + ']').addClass('active');
          $(this).addClass('active');

          $checkbox.prop('checked', true);

        });

        });

    </script>
    

























<div>
    <h3>{{ $product->name }}</h3>
    {!! Theme::breadcrumb()->render() !!}
</div>
<header>
    <img src="{{ get_object_image($product->imagedl) }}" alt="{{ $product->name }}">
    <h3>{{ $product->name }}</h3>
    <div>
        @if (!$product->procategories->isEmpty())
            <span>
                <a href="{{ $product->procategories->first()->url }}">{{ $product->procategories->first()->name }}</a>
            </span>
        @endif
        <span>{{ date_from_database($product->created_at, 'M d, Y') }}</span>

        @if (!$product->protags->isEmpty())
            <span>
                @foreach ($product->protags as $protag)
                    <a href="{{ $protag->url }}">{{ $protag->name }}</a>
                @endforeach
            </span>
        @endif
    </div>
</header>
{!! clean($product->content) !!}
<br />
{!! apply_filters(BASE_FILTER_PUBLIC_COMMENT_AREA, null) !!}
<footer>
    @if(count(get_related_products($product->slug, 2)) >= 2)
        @foreach (get_related_products($product->slug, 2) as $pro_item)
            <div>
                <article>
                    <div><a href="{{ $pro_item->url }}"></a>
                        <img src="{{ get_object_image($pro_item->imagedl) }}" alt="{{ $pro_item->name }}">
                    </div>
                    <header><a href="{{ $pro_item->url }}"> {{ $pro_item->name }}</a></header>
                </article>
            </div>
        @endforeach
    @endif
</footer>

