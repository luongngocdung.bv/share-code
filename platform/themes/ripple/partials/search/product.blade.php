<div class="row">
    @if (count($products) > 0)
        <ul class="search-list">
            @foreach ($products as $product)
                <li class="col-md-4 col-sm-6 col-xs-12">
                    <a href="{{ $product->url }}" class="squared has-image">
                        <span class="img" style="background-image: url({{ get_object_image($product->image, 'thumb') }});"></span>
                        <span class="spoofer">{{ $product->name }}</span>
                        <span class="visible">{{ $product->name }}</span>
                    </a>
                </li>
            @endforeach
            <div class="clearfix"></div>
        </ul>
    @else
        <div class="col-md-12">
            <p>{{ __('No result available for :name', ['name' => 'products']) }}</p>
        </div>
    @endif
</div>
<div class="clearfix"></div>
